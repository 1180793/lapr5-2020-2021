using System.Threading.Tasks;

namespace MDVApi.Domain.Shared
{
    public interface IUnitOfWork
    {
        Task<int> CommitAsync();
    }
}