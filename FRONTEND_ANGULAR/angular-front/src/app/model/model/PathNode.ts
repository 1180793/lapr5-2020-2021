import { Nodes } from "./Nodes";
export class PathNode {

    key: String;
    node: Nodes;
    duration: Number;
    distance: Number;

    constructor(node: Nodes, duration: Number, distance: Number) {
        this.node = node;
        this.duration = duration;
        this.distance = distance;
    }


    public get PathNode() {
        return this.node;
    }
    public get PathNodeDuration() {
        return this.duration;
    }
    public get LineStartDistance() {
        return this.distance;
    }


}
