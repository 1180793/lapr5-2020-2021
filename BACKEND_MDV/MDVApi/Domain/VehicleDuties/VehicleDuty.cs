using System;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Trips;
using MDVApi.Domain.WorkBlocks;

namespace MDVApi.Domain.VehicleDuties
{
    public class VehicleDuty : Entity<VehicleDutyId>, IAggregateRoot
    {
        private static readonly string DEPORTS_BY_DEFAULT = "StartEnd";
        public string name{ get; set; }
        public string color { get; set; }
        public string depots { get; set; }
        public DateTime date { get; set; }
        public IList<Trip> Trips { get; set; }
        public IList<WorkBlock> WorkBlocks { get; set; }
        public VehicleDuty(string name, string color, DateTime date){
            
            this.Id = new VehicleDutyId(Guid.NewGuid());
            this.name = name;
            this.color = color;
            this.depots = DEPORTS_BY_DEFAULT;
            this.date = date;
        }
    }
}