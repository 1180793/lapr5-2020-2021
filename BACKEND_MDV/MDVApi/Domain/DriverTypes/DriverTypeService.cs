using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MDVApi.Domain.Shared;
using Newtonsoft.Json;

namespace MDVApi.Domain.DriverTypes
{
    public class DriverTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDriverTypeRepository _repo;

        public DriverTypeService(IUnitOfWork unitOfWork, IDriverTypeRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<DriverTypeDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<DriverTypeDto> listDto = list.ConvertAll<DriverTypeDto>(dType =>
                new DriverTypeDto { Id = dType.Id.AsGuid(), name = dType.name });

            return listDto;
        }


        public async Task<DriverTypeDto> GetByIdAsync(DriverTypeId id)
        {
            var driverType = await this._repo.GetByIdAsync(id);

            if (driverType == null)
                return null;

            return new DriverTypeDto { Id = driverType.Id.AsGuid(), name = driverType.name };
        }

        public async Task<DriverTypeDto> AddAsync(CreatingDriverTypeDto dto)
        {
            var driverType = new DriverType(dto.name);

            await this._repo.AddAsync(driverType);

            await this._unitOfWork.CommitAsync();

            return new DriverTypeDto { Id = driverType.Id.AsGuid(), name = driverType.name };
        }

        public async Task<DriverType> getDriverTypeByNameAsync(string driverTypeName){

            var driverType = await _repo.getDriverTypeByNameAsync(driverTypeName);

            if (driverType == null)
                throw new BusinessRuleValidationException("Driver Type doesn't exist.");
            else
                return driverType;

        }

    }
}