const DriverTypeRepository = require('../repository/DriverTypeRepository');
const DriverTypeDTO = require('../dto/DriverTypeDTO');
const validateDriverType = require('../schemas/DriverTypeSchema').validate;
const createDriverTypeFromJSON = require('../model/entities/DriverType').createDriverTypeFromJSON;

class DriverTypeService {

    constructor() { }

    async createDriverType(driverTypeJSON) {
        return new Promise(async (resolve, reject) => {
            const driverTypeRepo = new DriverTypeRepository();
            try {
                const { error } = validateDriverType(driverTypeJSON);
                if (error) {
                    throw new Error(error.message);
                } else {
                    var driverTypeValidationDTO = new DriverTypeDTO(driverTypeJSON);
                }
            } catch (err) {
                reject(`Invalid Driver Type. ${err.message}`);
            }
            if (driverTypeValidationDTO == null) {
                reject("Invalid Driver Type.");
            } else {
                // console.log(driverTypeValidationDTO);
                try {
                    const pExistingDriverType = new Promise((resolve, reject) => {
                        driverTypeRepo.exists(driverTypeJSON.name, function (err, existingDriverType) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(existingDriverType);
                            }
                        })
                    });

                    pExistingDriverType
                        .then(function (existingDriverType) {
                            if (existingDriverType == null) {
                                const driverTypeToSave = createDriverTypeFromJSON(driverTypeJSON);
                                driverTypeRepo.save(driverTypeToSave, error => {
                                    if (error) {
                                        reject(error)
                                    } else {
                                        resolve(driverTypeValidationDTO);
                                    }
                                });
                            } else {
                                reject('Repeated Driver Type!');
                            }
                        })
                        .catch((reason) => {
                            reject(reason)
                        });
                } catch (err) {
                    reject(err);
                }
            }
        });
    }

    async findAll() {
        return new Promise(async (resolve, reject) => {
            const driverTypeRepo = new DriverTypeRepository();
            driverTypeRepo.findAll().then(driverTypes => {
                var lstDriverTypesDTO = [];
                driverTypes.forEach((element) => {
                    let driverTypeDTO = new DriverTypeDTO(element);
                    lstDriverTypesDTO.push(driverTypeDTO);
                });
                resolve(lstDriverTypesDTO);
            });
        });
    }
    
    async findByName(name) {
        return new Promise(async (resolve, reject) => {
            const driverTypeRepo = new DriverTypeRepository();
            driverTypeRepo.findByName(name, (err, driverType) => {
                if (driverType != null) {
                    const driverTypeDTO = new DriverTypeDTO(driverType);
                    resolve(driverTypeDTO);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async validateVehicleType(driverTypeName) {
        return new Promise((resolve, reject) => {
            const driverTypeRepo = new DriverTypeRepository();
            driverTypeRepo.exists(driverTypeName, function (err, driverType) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(driverType);
                }
            });
        });
    }

}

module.exports = DriverTypeService;