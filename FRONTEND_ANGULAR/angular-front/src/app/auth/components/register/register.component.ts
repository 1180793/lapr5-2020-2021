import { Component, OnInit, AfterViewInit, Output } from "@angular/core";
import {
    FormGroup,
    Validators,
    FormControl,
    FormBuilder
} from "@angular/forms";
import { AuthService } from "../../../services/auth/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { AuthEvent } from "src/app/services/auth/AuthEvent";
import { MatDialog } from "@angular/material/dialog";
import Swal from 'sweetalert2'

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    //Variables
    registerForm: FormGroup;
    control: FormControl;
    hide = true;
    hideConfirm = true;
    return: string;
    errorMsg: string = '';
    customTermsErrors = { required: 'Please accept the terms and conditions' }

    private _unsubscribeAll: Subject<any>;

    constructor(
        public dialog: MatDialog,
        private builder: FormBuilder,
        private authEvent: AuthEvent,
        private jwtAuth: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        const user = this.jwtAuth.getUser();
        if (user) {
            this.router.navigateByUrl(this.return);
        }
        this.hide = true;
        this.hideConfirm = true;
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.control = this.builder.control('', Validators.required);

        this.registerForm = new FormGroup({
            name: new FormControl('', Validators.compose(
                [Validators.minLength(5), Validators.maxLength(50), Validators.required])),
            email: new FormControl('', Validators.compose(
                [Validators.minLength(5), Validators.maxLength(255), Validators.required])),
            password: new FormControl('', Validators.compose(
                [Validators.minLength(5), Validators.maxLength(1024), Validators.required])),
            confirmPassword: new FormControl('', Validators.compose(
                [Validators.minLength(5), Validators.maxLength(1024), Validators.required]))
        });

        this.route.queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => this.return = params['return'] || '/');
    }

    ngOnDestroy() {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    openTerms() {
        const dialogRef = this.dialog.open(TermsAndConditionsDialog);
    }


    onRegister() {
        if (this.registerForm.valid) {
            const registerData = this.registerForm.value;
            this.jwtAuth.register(registerData.name, registerData.email, registerData.password)
                .subscribe(response => {
                    this.router.navigateByUrl('/profile');
                    Swal.fire('Success!', 'User was registered successfully!', 'success')
                    this.authEvent.emitAuthStatus('Logout');
                }, err => {
                    this.errorMsg = err.error;
                    Swal.fire('Error!', this.errorMsg + '!', 'error')

                })
        }
        else {
            this.validatorMessage();
        }
    }

    validatorMessage() {
        if (this.registerForm.get('name').invalid) {
            if (this.registerForm.get('name').hasError('required')) {
                Swal.fire('Error!', 'Name is required!', 'error')

            } else if (this.registerForm.get('name').hasError('minlength')) {
                Swal.fire('Error!', 'Name must be at least 5 characters long!', 'error')

            } else if (this.registerForm.get('name').hasError('maxlength')) {

                Swal.fire('Error!', 'Name can only be 50 characters long!', 'error')

            }
        } else if (this.registerForm.get('email').invalid) {
            if (this.registerForm.get('email').hasError('required')) {
                Swal.fire('Error!', 'Email is required!', 'error')

            } else if (this.registerForm.get('email').hasError('minlength')) {
                Swal.fire('Error!', 'Email must be at least 5 characters long!', 'error')

            } else if (this.registerForm.get('email').hasError('maxlength')) {
                Swal.fire('Error!', 'Email can only be 255 characters long!', 'error')

            }
        } else if (this.registerForm.get('password').invalid) {
            if (this.registerForm.get('password').hasError('required')) {
                Swal.fire('Error!', 'Password is required!', 'error')

            } else if (this.registerForm.get('password').hasError('minlength')) {
                Swal.fire('Error!', 'Password must be at least 5 characters long!', 'error')

            } else if (this.registerForm.get('password').hasError('maxlength')) {
                Swal.fire('Error!', 'Password can only be 1024 characters long!', 'error')

            }
        } else if (this.registerForm.get('password').valid) {
            if (this.registerForm.get('password').value != this.registerForm.get('confirmPassword').value) {
                Swal.fire('Error!', 'Passwords do not match!', 'error')

            } else {
                if (this.registerForm.get('terms').invalid) {
                    Swal.fire('Error!', 'You must accept Terms and Conditions!', 'error')

                }
            }
        }
    }

}

@Component({
    selector: 'terms-and-conditions-dialog',
    templateUrl: 'terms-and-conditions-dialog.html',
})
export class TermsAndConditionsDialog { }