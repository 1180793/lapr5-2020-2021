
class LinePathDTO {

    constructor(linePath) {
        this._path = linePath.path;
        this._orientation = linePath.orientation;
    }

    get path() {
        return this._path;
    }
    get orientation() {
        return this._orientation;
    }

}

module.exports = LinePathDTO;