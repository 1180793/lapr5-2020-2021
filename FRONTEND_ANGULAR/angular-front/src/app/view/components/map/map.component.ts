import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as mapboxgl from 'mapbox-gl';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';

import { environment } from 'src/environments/environment';

import { MapService } from 'src/app/services/map/map.service';
import { NodeService } from 'src/app/services/node/node.service';
import { LineService } from 'src/app/services/line/line.service';
import { PathService } from 'src/app/services/path/path.service';
import { PathMap } from 'src/app/mappers/PathMap';
import { Nodes } from 'src/app/model/model/Nodes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { _getOptionScrollPosition } from '@angular/material/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.styl'],
})
export class MapComponent implements OnInit {
  //VARIÁVEIS GLOBAIS------------------------------------------------------------------------------------------------------------
  isFirstPerson:boolean;
  //MDR
  mapService: MapService;
  nodeService: NodeService;
  lineService: LineService;
  pathService: PathService;
  pathMap: PathMap;
  nodeList: any[] = new Array();
  pathList: any[] = new Array();
  lineList: any[] = new Array();

  //THREE
  // Scene Configurations
  canvas: HTMLCanvasElement;
  WIDTH: number;
  HEIGHT: number;
  VIEW_ANGLE: number;
  ASPECT: number;
  NEAR: number;
  FAR: number;

  // Scene, camera, canvas, renderer
  camera:THREE.Camera;
  renderer: THREE.WebGLRenderer;

  //3DMODEL
  loader: GLTFLoader;
  DEFAULT_DEPOT_PATH: string = 'assets/Models/Model1/scene.gltf';
  DEFAULT_NODE_PATH: string = 'assets/Models/BusStop2/scene.gltf';
  DEFAULT_RELIEF_PATH: string = 'assets/Models/LondonBusStop/scene.gltf';
  selectedNode: Nodes;
  selectedModel: any;
  currentFile: File;
  nodeModelForm: FormGroup;
  clock:THREE.Clock;


  bus = {
    velocity:{
      x:0,
      y:0,
    },
    position: 0,
    rotation : 0,
  };

  //MAPS
  map2D: any;
  map3D: any;

  options = {
    lat: 41.201478,
    lng: -8.38531,
    zoom: 11,
    pitch: 0,
  };



  models: any[] = [
    {
      name: 'Bus Stop City',
      path: '/assets/Models/Model1/scene.gltf',
    },
    {
      name: 'Bus Stop Gray Futuristic',
      path: '/assets/Models/BusStop2/scene.gltf',
    },
    {
      name: 'London Bus Stop',
      path: 'assets/Models/LondonBusStop/scene.gltf',
    },
    {
      name: 'Futuristic Bus Stop',
      path: 'assets/Models/WingedBusStop/scene.gltf',
    },
  ];



  //------------------------------------------------------------------------------------------------------------

  constructor(public http: HttpClient, public fb: FormBuilder) {
    this.mapService = new MapService(this.http);
    this.nodeService = new NodeService(this.http);
    this.lineService = new LineService(this.http);
    this.pathService = new PathService(this.http);
    this.pathMap = new PathMap();
  }

  //------------------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.reactiveForm();

    this.isFirstPerson=false;
    const div2D = document.getElementById('map2D');
    const div3D = document.getElementById('map3D');

    div3D.style.display = 'grid';
    div2D.style.display = 'none';

    this.populateNodes();
    this.populateLines();
    this.populatePaths();

    setTimeout(() => {
      this.populate3D_Map();
      this.populateMap();
    }, 1000);

    const orig = this.map3D.project([this.options.lat,this.options.lng]);
    this.bus.position=orig;


  }

  reactiveForm() {
    this.nodeModelForm = this.fb.group({
      selectedNode: [''],
      selectedModel: [''],
    });
  }

  upload(): void {
    console.log(this.selectedNode);
    console.log(this.selectedModel);

    if (this.selectedNode == null || this.selectedNode == undefined) {
      alert('You must choose a file to upload');
      return;
    }

    this.map3D.removeLayer('3d-model-NODE-' + this.selectedNode);

    for (let node of this.nodeList) {
      if (node._shortName == this.selectedNode) {
        this.loadModel(node, this.selectedModel.path);
        break;
      }
    }
  }

  selectFile(event: any): void {
    this.currentFile = event.target.files;
  }

  changeMapVisibility(): void {
    const div2D = document.getElementById('map2D');
    const div3D = document.getElementById('map3D');

    if (div2D.style.display === 'grid') {
      div3D.style.display = 'grid';

      div2D.style.display = 'none';

      this.map3D.resize();
      this.map2D.resize();
    } else {
      div3D.style.display = 'none';

      div2D.style.display = 'grid';

      this.map3D.resize();
      this.map2D.resize();
    }
  }

  //------------------------------------------------------------------------------------------------------------

  //Obter lista de nós
  async populateNodes() {
    (await this.nodeService.getNodes()).subscribe(
      (value) => {
        this.nodeList = value;
      },
      (error) => {
        alert(`Server error: ${error.status} - Details: ${error.error}`);
        console.log(error);
      }
    );
  }

  //Obter lista de linhas
  async populateLines() {
    (await this.lineService.getLines()).subscribe((returnedLineList) => {
      returnedLineList.forEach(async (returnedLine) => {
        this.lineList.push(returnedLine);
      });
    });
  }

  //Obter lista de paths
  async populatePaths() {
    (await this.pathService.getPaths()).subscribe(
      (returnedPathList) => {
        returnedPathList.forEach(async (returnedPath) => {
          var pathAux = this.pathMap.fromBackend(returnedPath);
          if (pathAux != null) {
            returnedPath.color = pathAux.color;
            this.pathList.push(returnedPath);
          }
        });
      },
      (error) => {
        alert(`Server error: ${error.status} - Details: ${error.error}`);
        console.log(error);
      }
    );
  }

  //Carregar o Mapa e os dados
  async populateMap() {
    //MAPBOX GL Token de acesso
    mapboxgl.accessToken = environment.mapboxgl.accessToken;

    //MAPA MAPBOX
    this.map2D = new mapboxgl.Map({
      container: 'map2D',
      style: 'mapbox://styles/1180838/ckiyh4vb74pqk19mq8f3cr09g', // stylesheet location
      center: [this.options.lng, this.options.lat], // starting position [lng, lat]
      zoom: this.options.zoom, // starting zoom
    });

    // disable map rotation using right click + drag
    this.map2D.dragRotate.disable();

    // disable map rotation using touch rotation gesture
    this.map2D.touchZoomRotate.disableRotation();

    var popup = new mapboxgl.Popup({ offset: 10 }).setHTML('center');
    new mapboxgl.Marker()
      .setLngLat([this.options.lng, this.options.lat])
      .setPopup(popup)
      .addTo(this.map2D);

    //CRIAR MARKERS PARA NÓS
    this.nodeList.forEach((node: any) => {
      var description =
        '<strong>' +
        node._name +
        '</strong><p><b>ID: </b>' +
        node._shortName +
        '</p><p><b>LAT: </b>' +
        node._coordinates.latitude +
        '</p><p><b>LON: </b>' +
        node._coordinates.longitude +
        '</p>';

      var popup = new mapboxgl.Popup({ offset: 10 }).setHTML(description);

      var long = node._coordinates.longitude;
      var lat = node._coordinates.latitude;

      new mapboxgl.Marker()
        .setLngLat([long, lat])
        .setPopup(popup)
        .addTo(this.map2D);
    });

    // Set an event listener that will fire when the map has finished loading
    this.map2D.on('load', () => {
      //Lista de segmentos de rede já desenhados
      var drawnSegmentos: any[] = new Array();
      var drawnSegmentosColor: any[] = new Array();

      //Para cada linha
      this.lineList.forEach((line: any) => {
        //Cor da linha
        var color: any = this.mapService.rgbToHex(line._color);

        //Para cada Path
        this.pathList.forEach((path: any) => {
          //Se pertencer à linha
          if (path._line.name == line._name) {
            var segmento1: any[] = new Array();
            var segmento2: any[] = new Array();

            var segsRedePath: any[] = new Array();

            var flagPos = 0;
            var tam = path._pathNodeList.length;
            var tamAtual = 0;

            //Para cada Nó do path
            path._pathNodeList.forEach((element: any) => {
              var node = this.mapService.getNodeByName(
                element.node.shortName,
                this.nodeList
              );

              //Se for o primeiro da linha cria um segmento
              if (flagPos == 0) {
                var coord = [
                  node._coordinates.longitude,
                  node._coordinates.latitude,
                ];
                segmento1.push(coord);
                flagPos = 1;
                tamAtual++;
                //Adiciona o nó ao segmento anterior e cria um novo, A-B <= B => B-?
              } else if (flagPos == 1) {
                var coord = [
                  node._coordinates.longitude,
                  node._coordinates.latitude,
                ];

                if (tam > tamAtual) {
                  segmento1.push(coord);
                  segmento2.push(coord);
                } else if (tam <= tamAtual) {
                  //Se for o ultimo nó só completa o segmento, A-B <= B
                  segmento1.push(coord);
                }

                segsRedePath.push(segmento1);
                segmento1 = [];
                flagPos = 2;
                tamAtual++;
                //Adiciona o nó ao segmento anterior e cria um novo, B-C <= C => C-?
              } else if (flagPos == 2) {
                var coord = [
                  node._coordinates.longitude,
                  node._coordinates.latitude,
                ];

                if (tam > tamAtual) {
                  segmento1.push(coord);
                  segmento2.push(coord);
                } else if (tam <= tamAtual) {
                  //Se for o ultimo nó só completa o segmento B-C <= C
                  segmento2.push(coord);
                }

                segsRedePath.push(segmento2);
                segmento2 = [];
                flagPos = 1;
                tamAtual++;
              }
            });

            var idHelper = 0; //var de suporte ao id de segmentos/source
            var layerIds = new Array();

            segsRedePath.forEach((seg: any) => {
              // PARA CADA SEGMENTO DE UM PATH

              var drawn = this.mapService.containsSeg(
                seg,
                color,
                drawnSegmentosColor
              ); //Verifica se o segmento jáfoi desenhado
              if (drawn != null) {
                if (drawn == true) {
                  // SE JA TIVER SIDO DESENHADO O MESMO SEGMENTO AUMENTA SE O OFFSET
                  var support = this.mapService.countDrawnSegment(
                    seg,
                    drawnSegmentosColor
                  ); //Conta o nr de vezes que o segmento foi desenhado
                  var id = path._key + idHelper;

                  //Source segmento de reta entre dois nós
                  this.map2D.addSource(id, {
                    type: 'geojson',
                    data: {
                      type: 'Feature',
                      properties: {
                        description: line._name,
                      },
                      geometry: {
                        type: 'LineString',
                        coordinates: seg,
                      },
                    },
                  });

                  //Adicionar layer com a source do segmento
                  this.map2D.addLayer({
                    id: id,
                    type: 'line',
                    source: id,
                    layout: {
                      'line-join': 'round',
                      'line-cap': 'round',
                    },
                    paint: {
                      'line-color': color,
                      'line-width': 5,
                      'line-offset': support * 6,
                    },
                  });

                  //this.createPopUp(this.map2D, id, seg);
                  layerIds.push({ id, seg });

                  drawnSegmentos.push([seg[0], seg[1]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentos.push([seg[1], seg[0]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentosColor.push([[seg[0], seg[1]], color]);
                  drawnSegmentosColor.push([[seg[1], seg[0]], color]);
                  idHelper++;
                } else {
                  //Se nunca foi desenhado o offset é zero

                  var id = path._key + idHelper;

                  //Source segmento de reta entre dois nós
                  this.map2D.addSource(id, {
                    type: 'geojson',
                    data: {
                      type: 'Feature',
                      properties: {
                        description: line._name,
                      },
                      geometry: {
                        type: 'LineString',
                        coordinates: seg,
                      },
                    },
                  });

                  //Adicionar layer com a source do segmento
                  this.map2D.addLayer({
                    id: id,
                    type: 'line',
                    source: id,
                    layout: {
                      'line-join': 'round',
                      'line-cap': 'round',
                    },
                    paint: {
                      'line-color': color,
                      'line-width': 5,
                      'line-offset': 0,
                    },
                  });

                  //this.createPopUp(this.map2D, id, seg);
                  layerIds.push({ id, seg });

                  drawnSegmentos.push([seg[0], seg[1]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentos.push([seg[1], seg[0]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentosColor.push([[seg[0], seg[1]], color]);
                  drawnSegmentosColor.push([[seg[1], seg[0]], color]);
                  idHelper++;
                }
              }
            });

            layerIds.forEach((layer: any) => {
              var lat = (layer.seg[0][0] + layer.seg[1][0]) / 2;
              var lng = (layer.seg[0][1] + layer.seg[1][1]) / 2;
              this.createPopUp(this.map2D, layer.id, { lng, lat });
            });
          }
        });
      });

      var idHelper = 0;

      //Para cada nó adicionar um circulo na posição
      this.nodeList.forEach((node: any) => {
        //Coordenadas do nó
        var long = node._coordinates.longitude;
        var lat = node._coordinates.latitude;

        //ID
        var id = 'Nid' + idHelper;

        //Source circulo do nó
        this.map2D.addSource(id, {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [long, lat],
                },
              },
            ],
          },
        });

        //Adicionar layer com a source do circulo
        this.map2D.addLayer({
          id: id,
          type: 'circle',
          source: id,
          paint: {
            'circle-radius': 8,
            'circle-color': '#808080',
          },
        });

        idHelper++;
      });
    });
  }

  async populate3D_Map() {
    mapboxgl.accessToken = environment.mapboxgl.accessToken;

    //MAPA MAPBOX
    this.map3D = new mapboxgl.Map({
      container: 'map3D',
      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
      center: [this.options.lng, this.options.lat], // starting position [lng, lat]
      zoom: this.options.zoom, // starting zoom
    });

    this.map3D.on('load', () => {
      //Lista de segmentos de rede já desenhados
      var drawnSegmentos: any[] = new Array();
      var drawnSegmentosColor: any[] = new Array();

      //Para cada linha
      this.lineList.forEach((line: any) => {
        //Cor da linha
        var color: any = this.mapService.rgbToHex(line._color);

        //Para cada Path
        this.pathList.forEach((path: any) => {
          //Se pertencer à linha
          if (path._line.name == line._name) {
            var segmento1: any[] = new Array();
            var segmento2: any[] = new Array();

            var segsRedePath: any[] = new Array();

            var flagPos = 0;
            var tam = path._pathNodeList.length;
            var tamAtual = 0;

            //Para cada Nó do path
            path._pathNodeList.forEach((element: any) => {
              var node = this.mapService.getNodeByName(
                element.node.shortName,
                this.nodeList
              );

              //Se for o primeiro da linha cria um segmento
              if (flagPos == 0) {
                var coord = [
                  node._coordinates.longitude,
                  node._coordinates.latitude,
                ];
                segmento1.push(coord);
                flagPos = 1;
                tamAtual++;
                //Adiciona o nó ao segmento anterior e cria um novo, A-B <= B => B-?
              } else if (flagPos == 1) {
                var coord = [
                  node._coordinates.longitude,
                  node._coordinates.latitude,
                ];

                if (tam > tamAtual) {
                  segmento1.push(coord);
                  segmento2.push(coord);
                } else if (tam <= tamAtual) {
                  //Se for o ultimo nó só completa o segmento, A-B <= B
                  segmento1.push(coord);
                }

                segsRedePath.push(segmento1);
                segmento1 = [];
                flagPos = 2;
                tamAtual++;
                //Adiciona o nó ao segmento anterior e cria um novo, B-C <= C => C-?
              } else if (flagPos == 2) {
                var coord = [
                  node._coordinates.longitude,
                  node._coordinates.latitude,
                ];

                if (tam > tamAtual) {
                  segmento1.push(coord);
                  segmento2.push(coord);
                } else if (tam <= tamAtual) {
                  //Se for o ultimo nó só completa o segmento B-C <= C
                  segmento2.push(coord);
                }

                segsRedePath.push(segmento2);
                segmento2 = [];
                flagPos = 1;
                tamAtual++;
              }
            });

            var idHelper = 0; //var de suporte ao id de segmentos/source
            var layerIds = new Array();

            segsRedePath.forEach((seg: any) => {
              // PARA CADA SEGMENTO DE UM PATH

              var drawn = this.mapService.containsSeg(
                seg,
                color,
                drawnSegmentosColor
              ); //Verifica se o segmento jáfoi desenhado
              if (drawn != null) {
                if (drawn == true) {
                  // SE JA TIVER SIDO DESENHADO O MESMO SEGMENTO AUMENTA SE O OFFSET
                  var support = this.mapService.countDrawnSegment(
                    seg,
                    drawnSegmentosColor
                  ); //Conta o nr de vezes que o segmento foi desenhado
                  var id = path._key + '3D' + idHelper;

                  //Source segmento de reta entre dois nós
                  this.map3D.addSource(id, {
                    type: 'geojson',
                    data: {
                      type: 'Feature',
                      properties: {
                        description: line._name,
                      },
                      geometry: {
                        type: 'LineString',
                        coordinates: seg,
                      },
                    },
                  });

                  //Adicionar layer com a source do segmento
                  this.map3D.addLayer({
                    id: id,
                    type: 'line',
                    source: id,
                    layout: {
                      'line-join': 'round',
                      'line-cap': 'round',
                    },
                    paint: {
                      'line-color': color,
                      'line-width': 5,
                      'line-offset': support * 6,
                    },
                  });

                  layerIds.push({ id, seg });
                  //this.createPopUp(this.map3D, id, seg);

                  drawnSegmentos.push([seg[0], seg[1]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentos.push([seg[1], seg[0]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentosColor.push([[seg[0], seg[1]], color]);
                  drawnSegmentosColor.push([[seg[1], seg[0]], color]);
                  idHelper++;
                } else {
                  //Se nunca foi desenhado o offset é zero

                  var id = path._key + '3D' + idHelper;

                  //Source segmento de reta entre dois nós
                  this.map3D.addSource(id, {
                    type: 'geojson',
                    data: {
                      type: 'Feature',
                      properties: {
                        description: line._name,
                      },
                      geometry: {
                        type: 'LineString',
                        coordinates: seg,
                      },
                    },
                  });

                  //Adicionar layer com a source do segmento
                  this.map3D.addLayer({
                    id: id,
                    type: 'line',
                    source: id,
                    layout: {
                      'line-join': 'round',
                      'line-cap': 'round',
                    },
                    paint: {
                      'line-color': color,
                      'line-width': 5,
                      'line-offset': 0,
                    },
                  });

                  layerIds.push({ id, seg });
                  //this.createPopUp(this.map3D, id, seg);

                  drawnSegmentos.push([seg[0], seg[1]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentos.push([seg[1], seg[0]]); // Adicionar o segmento aos já desenhados
                  drawnSegmentosColor.push([[seg[0], seg[1]], color]);
                  drawnSegmentosColor.push([[seg[1], seg[0]], color]);
                  idHelper++;
                }
              }
            });

            layerIds.forEach((layer: any) => {
              var lat = (layer.seg[0][0] + layer.seg[1][0]) / 2;
              var lng = (layer.seg[0][1] + layer.seg[1][1]) / 2;
              this.createPopUp(this.map3D, layer.id, { lng, lat });
            });
          }
        });
      });
    });
    var layerIds = new Array();

    this.nodeList.forEach((node: any) => {
      var id;
      var coord = [node._coordinates.longitude, node._coordinates.latitude];

      if (node._isDepot) {
        id = this.loadModel(node, this.DEFAULT_DEPOT_PATH);
      } else if (node._isReliefPoint) {
        id = this.loadModel(node, this.DEFAULT_RELIEF_PATH);
      } else {
        id = this.loadModel(node, this.DEFAULT_NODE_PATH);
      }

      layerIds.push({ id, coord });
    });

    layerIds.forEach((layer: any) => {
      this.createPopUp(this.map3D, layer.id, layer.coord);
    });
  } //Fim 3d map

  loadModel(node, modelURL) {
    //add modelUrl

    var nodeModelAltitude = 0;
    var nodeModelRotate = [Math.PI / 2, 0, 0];
    var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
      [node._coordinates.longitude, node._coordinates.latitude],
      nodeModelAltitude
    );

    // transformation parameters to position, rotate and scale the 3D model onto the map
    var modelTransform = {
      translateX: modelAsMercatorCoordinate.x,
      translateY: modelAsMercatorCoordinate.y,
      translateZ: modelAsMercatorCoordinate.z,
      rotateX: nodeModelRotate[0],
      rotateY: nodeModelRotate[1],
      rotateZ: nodeModelRotate[2],
      /* Since our 3D model is in real world meters, a scale transform needs to be
       * applied since the CustomLayerInterface expects units in MercatorCoordinates.
       */
      scale: 5.41843220338983e-7, //modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
    };

    var id = '3d-model-NODE-' + node._shortName;

    var customLayer = {
      id: id,
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map: any, gl: any): void {
        this.camera = new THREE.Camera();
        this.scene = new THREE.Scene();

        // create two three.js lights to illuminate the model

        // var directionalLight = new THREE.DirectionalLight(0xffffff);
        // directionalLight.position.set(0, -70, 100).normalize();
        // this.scene.add(directionalLight);

        //Directional Light
        var directionalLight = new THREE.DirectionalLight(0xffffff, 5);
        const d = 10;

        //Set up shadow properties for the light
        directionalLight.position.set(10, 50, 50);
        directionalLight.castShadow = true;
        directionalLight.shadow.radius = d;
        directionalLight.shadow.camera.left = -d;
        directionalLight.shadow.camera.right = d;
        directionalLight.shadow.camera.top = d;
        directionalLight.shadow.camera.bottom = -d;
        directionalLight.shadow.mapSize.width = 512; // default
        directionalLight.shadow.mapSize.height = 512; // default
        directionalLight.shadow.camera.near = 0.5; // default
        directionalLight.shadow.camera.far = 500; // default
        this.scene.add(directionalLight);
        //Camera Helper
        this.scene.add(new THREE.CameraHelper(directionalLight.shadow.camera));
        //Light Helper
        this.scene.add(new THREE.DirectionalLightHelper(directionalLight));

        // var directionalLight1 = new THREE.DirectionalLight(0xffffff, 20);
        // directionalLight1.position.set(50, 70, 50);
        // this.scene.add(directionalLight1);

        // const helper1 = new THREE.DirectionalLightHelper( directionalLight1 );
        // this.scene.add( helper1 );

        // var light = new THREE.PointLight(0xffffff, 50, 100);
        // light.position.set(0, -70, 100).normalize();
        // this.scene.add(light);

        //const light = new THREE.AmbientLight(0x404040); // soft white light
        //this.scene.add(light);

        //this.scene.receiveShadow = true;

        // use the three.js GLTF loader to add the 3D model to the three.js scene
        var loader = new GLTFLoader();
        loader.load(
          modelURL, //ModelURL
          function (gltf) {
            gltf.scene.traverse(function (model) {
              if (model.isMesh) {
                model.castShadow = true;
                model.receiveShadow = true;
              }
            });

            this.scene.add(gltf.scene);

            //Adding plane to recieve shadow
            const s = new THREE.Box3().setFromObject(gltf.scene).getSize(new THREE.Vector3(0, 0, 0));
            const sizes = [s.x, s.y, s.z];
            const planeSize = Math.max(...sizes) * 10;
            const planeMat = new THREE.ShadowMaterial();
            const planeGeo = new THREE.PlaneBufferGeometry(planeSize,planeSize);
            planeMat.opacity = 0.5;
            const plane = new THREE.Mesh(planeGeo, planeMat);
            plane.rotation.x = -Math.PI / 2;
            plane.receiveShadow = true;
            this.scene.add(plane);
          }.bind(this)
        );

        this.map = map;

        // use the Mapbox GL JS map canvas for three.js
        this.renderer = new THREE.WebGLRenderer({
          canvas: this.map.getCanvas(),
          context: gl,
          antialias: true,
        });
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMapSoft = true;
        this.renderer.autoClear = false;
      },

      render: function (gl, matrix) {
        var rotationX = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(1, 0, 0),
          modelTransform.rotateX
        );
        var rotationY = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(0, 1, 0),
          modelTransform.rotateY
        );
        var rotationZ = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(0, 0, 1),
          modelTransform.rotateZ
        );

        var m = new THREE.Matrix4().fromArray(matrix);
        var l = new THREE.Matrix4()
          .makeTranslation(
            modelTransform.translateX,
            modelTransform.translateY,
            modelTransform.translateZ
          )
          .scale(
            new THREE.Vector3(
              modelTransform.scale,
              -modelTransform.scale,
              modelTransform.scale
            )
          )
          .multiply(rotationX)
          .multiply(rotationY)
          .multiply(rotationZ);

        this.camera.projectionMatrix = m.multiply(l);
        this.renderer.state.reset();
        this.renderer.render(this.scene, this.camera);
        this.map.triggerRepaint();
      },
    };

    this.map3D.on('load', () => {
      this.map3D.addLayer(customLayer, 'waterway-label');
    });

    return id;
  }

  createPopUp(map: any, id: any, coords: any) {
    var popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });

    map.on('mouseenter', id, function (e) {
      // Change the cursor style as a UI indicator.
      map.getCanvas().style.cursor = 'pointer';

      var description = e.features[0].properties.description;
      var coordinates = e.features[0].geometry.coordinates.slice();

      // console.log(id)
      // console.log("popup coords", [coords.lat, coords.lng])
      // console.log("XY:", JSON.stringify(e.point))
      // console.log("CoordsRato", JSON.stringify(e.lngLat.wrap()));
      //console.log("Rato fdp", JSON.stringify(e.lngLat));

      // Ensure that if the map is zoomed out such that multiple
      // copies of the feature are visible, the popup appears
      // over the copy being pointed to. coordinates[0][0][0]
      // while (Math.abs(e.lngLat.lng - lng) > 180) {
      //     lng += e.lngLat.lng > lng ? 360 : -360;
      // }

      // Populate the popup and set its coordinates
      // based on the feature found.
      popup
        .setLngLat([coords.lat, coords.lng])
        .setHTML('<strong>' + description + '</strong>')
        .addTo(map);
    });

    map.on('mouseleave', id, function () {
      setTimeout(() => {
        map.getCanvas().style.cursor = '';
        popup.remove();
      }, 1000);
    });
  }

  keyboardInput(event: KeyboardEvent) {
        var delta = this.clock.getDelta(); // seconds
        var moveDistance = 500 * delta; // n pixels per second
        var dir = new THREE.Vector3(this.bus.position[0], this.bus.position[1], 0);
        dir.sub(this.camera.position).normalize(); // direction vector between the camera and the ball
        var sensitivity = 150;
        var rotateAngle = Math.PI / 2 * delta * sensitivity;

    // PRESS LEFT ARROW
    if (event.keyCode == 37||event.keyCode == 65) {
       this.bus.rotation -= rotateAngle;
    }
    // PRESS UP ARROW
    else if (event.keyCode == 38||event.keyCode == 87) {
       this.bus.velocity.x += moveDistance * dir.x;
    }
    // PRESS RIGHT ARROW
    else if (event.keyCode == 39||event.keyCode == 68) {

       this.bus.rotation += rotateAngle;
    }
    // PRESS DOWN ARROW
    else if (event.keyCode == 40||event.keyCode == 83) {
       this.bus.velocity.x -= moveDistance * dir.x;

    }
    // PRESS SPACE BAR
    else if (event.keyCode == 32) {
       window.alert("Space Key Pressed");
    }
 }

  addFirstPersonControlsListner(){
    this.map3D.getCanvas().addEventListner('keydown',this.keyboardInput);
    }

  switchToFirstPerson(){
    this.camera=new THREE.Camera();
    this.map3D.setBearing(-12);
    this.map3D.setZoom(17);
  }

}
