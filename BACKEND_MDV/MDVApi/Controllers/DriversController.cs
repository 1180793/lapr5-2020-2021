using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MDVApi.Domain.Drivers;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Authentication;
using Microsoft.Extensions.Configuration;
using MDVApi.Domain.DriverTypes;


namespace MDVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriversController : ControllerBase
    {   
         private static readonly string driverException = "IX_Drivers_driversLicenceNumber";
        private readonly DriverService _service;
        private readonly DriverTypeService _serviceDt;

        public DriversController(DriverService service, DriverTypeService serviceDt)
        {
            _service = service;
            _serviceDt = serviceDt;
        }

        // GET: api/Drivers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DriverDto>>> GetAll()
        {
             return await _service.GetAllAsync();
        }

        // GET: api/Drivers/5
        [HttpGet("{id}")]
         public async Task<ActionResult<DriverDto>> GetGetById(Guid id)
        {
            var driver = await _service.GetByIdAsync(new DriverId(id));

            if (driver == null)
            {
                return NotFound();
            }

            return driver;
        }
        
        // POST: api/Drivers
        [HttpPost]
        public async Task<ActionResult<DriverDto>> Create(CreatingDriverDto dto)
        {

            List<DriverType> driverTypeList = new List<DriverType>();

            foreach(CreatingDriverTypeDto dTypeDto in dto.driverTypeNames){
                try{
                    driverTypeList.Add(await _serviceDt.getDriverTypeByNameAsync(dTypeDto.name));
                }catch(Exception e){
                    Console.WriteLine("DriverAlready Exists" + e);
                    DriverTypeDto dType = await _serviceDt.AddAsync(dTypeDto);
                    driverTypeList.Add(await _serviceDt.getDriverTypeByNameAsync(dTypeDto.name));
                } 
            }

            try
            {
                var driver = await _service.AddAsync(dto, driverTypeList);

                return CreatedAtAction(nameof(GetGetById), new { id = driver.Id }, driver);
            }
            catch(Exception ex)
            {

                if( ex.InnerException.Message.Contains(driverException)){
                    return BadRequest("A Driver with the given drivers licence number is already registered.");
                }
                
                return BadRequest(new {Message = ex.InnerException.Message});
            }  
        }

    }
}
