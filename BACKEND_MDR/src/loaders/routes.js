const nodesRoute = require('../routes/nodes');
const vehicleTypesRoute = require('../routes/vehicleTypes');
const driverTypesRoute = require('../routes/driverTypes');
const linesRoute = require('../routes/lines');
const pathsRoute = require('../routes/paths');
const networkRoute = require('../routes/network');
const scheduleRoute = require('../routes/schedule');
const registerRoute = require('../routes/register');
const loginRoute = require('../routes/login');
const usersRoute = require('../routes/users');
// const error = require('../middleware/error');

module.exports = function (app) {
    app.use('/api/nodes', nodesRoute);
    app.use('/api/vehicleTypes', vehicleTypesRoute);
    app.use('/api/driverTypes', driverTypesRoute);
    app.use('/api/lines', linesRoute);
    app.use('/api/paths', pathsRoute);
    app.use('/api/network', networkRoute);
    app.use('/api/schedule', scheduleRoute);
    app.use('/api/registerClient', registerRoute);
    app.use('/api/auth', loginRoute);
    app.use('/api/users', usersRoute);
    //   app.use(error);
}