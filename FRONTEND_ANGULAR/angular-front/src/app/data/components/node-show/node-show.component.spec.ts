import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeShowComponent } from './node-show.component';

describe('NodeShowComponent', () => {
  let component: NodeShowComponent;
  let fixture: ComponentFixture<NodeShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
