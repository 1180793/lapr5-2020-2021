import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportNetworkComponent } from './import-network.component';

describe('ImportNetworkComponent', () => {
  let component: ImportNetworkComponent;
  let fixture: ComponentFixture<ImportNetworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportNetworkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
