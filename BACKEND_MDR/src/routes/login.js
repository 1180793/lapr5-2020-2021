const express = require('express');
const router = express.Router();

const LoginController = require('../controllers/LoginController');

var loginController = new LoginController();

router.post('/', loginController.loginUser);

module.exports = router;
