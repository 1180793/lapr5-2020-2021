const mongoose = require('mongoose');
const LineSchema = require('../../schemas/LineSchema').lineSchema;

const NodeService = require('../../services/NodeService');
const nodeService = new NodeService();

const VehicleTypeService = require('../../services/VehicleTypeService');
const vehicleTypeService = new VehicleTypeService();

const DriverTypeService = require('../../services/DriverTypeService');
const driverTypeService = new DriverTypeService();

/**
 * Creates Line instance from JSON Object
 * @param {JSON} lineJSON 
 */
async function createLineFromJSON(lineJSON) {
    return new Promise(async (resolve, reject) => {
        var line = null;
        var Line = mongoose.model('Line', LineSchema);
        try {
            var allowedVehicleTypesList = (lineJSON.allowedVehicleTypes != null) ? lineJSON.allowedVehicleTypes : [];
            var allowedDriverTypesList = (lineJSON.allowedDriverTypes != null) ? lineJSON.allowedDriverTypes : [];

            var startNodeID;
            var endNodeID;
            var finalAllowedVehicleTypesID = [];
            var finalAllowedDriverTypesID = [];

            // Query Start Node
            let foundStartNode = await nodeService.validateNode(lineJSON.startNode);
            if (foundStartNode == null) {
                reject('Invalid Start Node!');
                return null;
            }
            startNodeID = foundStartNode._id;

            // Query End Node
            let foundEndNode = await nodeService.validateNode(lineJSON.endNode);
            if (foundEndNode == null) {
                reject('Invalid End Node!');
                return null;
            }
            endNodeID = foundEndNode._id;

            // Query Allowed Vehicles
            var allowedVehicleTypesPromises = [];
            allowedVehicleTypesList.forEach(element => {
                allowedVehicleTypesPromises.push(vehicleTypeService.validateVehicleType(element));
            });

            Promise.all(allowedVehicleTypesPromises)
                .then(function (allowedVehicleTypes) {
                    var isValidAllowedVehicleTypesList = true;
                    allowedVehicleTypes.forEach(element => {
                        if (element == null) {
                            isValidAllowedVehicleTypesList = false;
                        } else {
                            finalAllowedVehicleTypesID.push(element._id);
                        }
                    })
                    if (isValidAllowedVehicleTypesList == false) {
                        reject('One or more invalid Vehicle Types');
                        return null;
                    } else {

                        // Query AllowedDriver Types
                        var allowedDriverTypesPromises = [];
                        allowedDriverTypesList.forEach(element => {
                            allowedDriverTypesPromises.push(driverTypeService.validateVehicleType(element));
                        });

                        Promise.all(allowedDriverTypesPromises)
                            .then(function (allowedDriverTypes) {
                                var isValidAllowedDriverTypesList = true;
                                allowedDriverTypes.forEach(element => {
                                    if (element == null) {
                                        isValidAllowedDriverTypesList = false;
                                    } else {
                                        finalAllowedDriverTypesID.push(element._id);
                                    }
                                })
                                if (isValidAllowedDriverTypesList == false) {
                                    reject('One or more invalid Driver Types');
                                    return null;
                                } else {
                                    // Build Line
                                    line = new Line({
                                            name: lineJSON.name,
                                        color: lineJSON.color,
                                        startNode: startNodeID,
                                        endNode: endNodeID,
                                        allowedVehicleTypes: finalAllowedVehicleTypesID,
                                        allowedDriverTypes: finalAllowedDriverTypesID
                                    });
                                    
                                    resolve(line);
                                }
                            });
                    }
                });
        } catch (err) {
            reject(err);
            return null;
        }
    });
}

exports.Line = mongoose.model('Line', LineSchema);
exports.createLineFromJSON = createLineFromJSON;