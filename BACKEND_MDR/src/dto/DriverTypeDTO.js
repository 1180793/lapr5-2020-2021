
class DriverTypeDTO {
    constructor(driverType) {
        this._name = driverType.name;
    }

    get name() {
        return this._name;
    }
}

module.exports = DriverTypeDTO;