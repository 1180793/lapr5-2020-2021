const generateAuthToken = require('../../../model/entities/User').generateAuthToken;
const Roles = require('../../../model/entities/Roles');
const auth = require('../../../middleware/auth');
const mongoose = require('mongoose');

describe('auth middleware', () => {
    it('deve dar populate ao req.user com o payload de um JWT valido', async () => {
        const user = {
            _id: new mongoose.Types.ObjectId().toHexString(),
            email: 'f_madureira@facebook.com',
            name: "Fernando Madureira",
            roles: [
                Roles.CLIENT.id,
                Roles.DATA_ADMINISTRATOR.id
            ]
        };
        const token = await generateAuthToken(user);
        const req = {
            header: jest.fn().mockReturnValue(token)
        };
        const res = {};
        const next = jest.fn();

        auth(req, res, next);

        expect(req.user).toMatchObject(user);
    });
});