
import { HttpClient } from '@angular/common/http';
import { Nodes } from './../../model/model/Nodes'
import { Coordinate } from 'src/app/model/model/Coordinate';
import { NodeMap } from './../../../app/mappers/NodeMap';
import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class NodeService {

    uri = environment.uri; //'http://localhost:3000/api';
    mapper = new NodeMap();

    constructor(private http: HttpClient) {
    }

    nodeList!: Nodes[];

    //(David) Assigned get to nodes array
    async getNodes() {
        return this.http.get<Array<Nodes>>(`${this.uri}/nodes`);
    }

    getNodesById(id: string) {
        return this.http.get(`${this.uri}/nodes/${id}`);
    }

    async addNode(name: string, shortName: string, latitude: number, longitude: number, busDepot: boolean, reliefPoint: boolean) {

        const node: Nodes = new Nodes(name, shortName, latitude, longitude, busDepot, reliefPoint);
        return this.addNodeDto(node);
    };

    //addNode using Mapper
    async addNodeDto(node: Nodes) {
        return this.http.post<Nodes>(`${this.uri}/nodes `, this.mapper.toDTO(node))
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Node was successfully created!', 'success')
                },
                error => {
                    Swal.fire('Error!', error.error + '!', 'error')
                    // alert("Couldn't add the node")
                    // alert(`Server error: ${error.status} - Details: ${error.error}`)
                    // console.log(error);
                }

            )
    }
    async deleteNode(id: string) {
        return this.http.delete(`${this.uri}/nodes/${id}`);
    }

}
