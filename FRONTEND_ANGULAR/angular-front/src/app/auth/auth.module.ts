import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SplashscreenComponent } from './components/splashscreen/splashscreen.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent, TermsAndConditionsDialog } from './components/register/register.component';
import { DeleteAccountDialog, ProfileComponent } from './components/profile/profile.component';
import { NoPermissionComponent } from './components/no-permission/no-permission.component';



@NgModule({
  entryComponents: [TermsAndConditionsDialog, DeleteAccountDialog],
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
  ]
})
export class AuthModule { }
export const authComponents = [SplashscreenComponent, 
  NoPermissionComponent,
  LoginComponent,
  RegisterComponent,
  TermsAndConditionsDialog,
  DeleteAccountDialog,
  ProfileComponent]
