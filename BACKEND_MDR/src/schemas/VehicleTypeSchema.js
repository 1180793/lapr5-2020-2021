const mongoose = require('mongoose');
const Joi = require('joi');
const round = require('../utils/Utils').round;
const validateEnergySourceByName = require('../utils/Utils').validateEnergySourceByName;

// Persistence Schema
const vehicleTypeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        maxlength: 20
    },
    autonomy: {
        type: Number,
        set: v => Math.round(v),
        min: 0
    },
    cost: {
        type: Number,
        set: v => round(v, 2),
        min: 0
    },
    averageSpeed: {
        type: Number,
        set: v => Math.round(v),
        min: 0
    },
    energySource: {
        type: String,
        validate: {
            validator: validateEnergySourceByName,
            message: props => `${props.value} is not a valid Energy Source!`
        },
    },
    consumption: {
        type: Number,
        set: v => round(v, 3),
        min: 0
    },
    emissions: {
        type: Number,
        min: 0
    }
});

const joiValidateEnergySource = (value, helpers) => {
    var isValid = validateEnergySourceByName(value);
    if (!isValid) {
        return helpers.error("any.invalid");
    } else {
        return value;
    }
};

// Joi Schema
const joiVehicleTypeSchema = Joi.object({
    name: Joi.string()
        .required()
        .max(20),
    autonomy: Joi.number()
        .required()
        .min(0),
    cost: Joi.number()
        .required()
        .min(0),
    averageSpeed: Joi.number()
        .required()
        .min(0),
    energySource: Joi.string()
        .required()
        .custom(joiValidateEnergySource, "Energy Source Validation"),
    consumption: Joi.number()
        .required()
        .min(0),
    emissions: Joi.number()
        .required()
        .min(0),
});

// Validate
function validateVehicleType(vehicleType) {
    return joiVehicleTypeSchema.validate(vehicleType);
}

exports.joiVehicleTypeSchema = joiVehicleTypeSchema;
exports.vehicleTypeSchema = vehicleTypeSchema;
exports.validate = validateVehicleType;