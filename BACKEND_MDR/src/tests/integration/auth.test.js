const generateAuthToken = require('../../model/entities/User').generateAuthToken;
const mongoose = require('mongoose');
const NodeSchema = require('../../schemas/NodeSchema').nodeSchema;
const Node = mongoose.model('Node', NodeSchema);
const Roles = require('../../model/entities/Roles');
const request = require('supertest');

describe('auth middleware', () => {
    beforeEach(() => { server = require('../../app'); })
    afterEach(async () => {
        await Node.deleteOne({ "shortName": "ISEP" });
        await server.close();
    });

    let token;

    let userWithoutPermissions = {
        _id: new mongoose.Types.ObjectId().toHexString(),
        email: 'f_madureira@facebook.com',
        name: "Fernando Madureira",
        roles: [
            Roles.CLIENT.id
        ]
    };

    let user = {
        _id: new mongoose.Types.ObjectId().toHexString(),
        email: 'f_madureira@facebook.com',
        name: "Fernando Madureira",
        roles: [
            Roles.CLIENT.id,
            Roles.DATA_ADMINISTRATOR.id
        ]
    };

    const exec = () => {
        return request(server)
            .post('/api/nodes')
            .set('x-auth-token', token)
            .send({
                "shortName": "ISEP",
                "name": "Instituto Superior de Engenharia do Porto",
                "coordinates": {
                    "latitude": 3,
                    "longitude": 54
                },
                "isReliefPoint": false,
                "isDepot": false
            });
    }

    it('should return 401 if no token is provided', async () => {
        token = '';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 400 if token is invalid', async () => {
        token = 'invalidToken';

        const res = await exec();

        expect(res.status).toBe(400);
    });

    it("should return 403 if token is valid but user doesn't have permission", async () => {
        token = await generateAuthToken(userWithoutPermissions);

        const res = await request(server)
            .post('/api/nodes')
            .set('x-auth-token', token)
            .send({
                "shortName": "ISEP",
                "name": "Instituto Superior de Engenharia do Porto",
                "coordinates": {
                    "latitude": 3,
                    "longitude": 54
                },
                "isReliefPoint": false,
                "isDepot": false
            });

        expect(res.status).toBe(403);
    });

    it('should return 201 if token is valid and user has permission', async () => {
        token = await generateAuthToken(user);

        const res = await request(server)
            .post('/api/nodes')
            .set('x-auth-token', token)
            .send({
                "shortName": "ISEP",
                "name": "Instituto Superior de Engenharia do Porto",
                "coordinates": {
                    "latitude": 3,
                    "longitude": 54
                },
                "isReliefPoint": false,
                "isDepot": false
            });

        expect(res.status).toBe(201);
    });
});