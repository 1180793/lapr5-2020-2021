%no(nome_paragem, abrev_paragem, flag_ponto_rendição, flag_estação_recolha, longitude, latitude).

no('Aguiar de Sousa', 'AGUIA', t, f, -8.4464785432391, 41.1293363229325).
no('Baltar', 'BALTR', t, f, -8.38716802227697, 41.1937898023744).
no('Besteiros', 'BESTR', t, f, -8.34043029659082, 41.217018845589).
no('Cete', 'CETE', t, f, -8.35164059584564, 41.183243425797).
%no('Cristelo', 'CRIST', t, f, -8.34639896125324, 41.2207801252676).
%no('Duas Igrejas', 'DIGRJ', t, f, -8.35481024956726, 41.2278665802794).
%no('Estação (Lordelo)', 'ESTLO', f, t, -8.4227924957086, 41.2521157104055).
%no('Estação (Paredes)', 'ESTPA', f, t, -8.33448520831829, 41.2082119860192).
no('Gandra', 'GAND', t, f, -8.43958765792976, 41.1956579348384).
no('Lordelo', 'LORDL', t, f, -8.42293614720057, 41.2452627470645).
no('Mouriz', 'MOURZ', t, f, -8.36577272258403, 41.1983610215263).
no('Parada de Todeia', 'PARAD', t, f, -8.37023578802149, 41.1765780321068).
no('Paredes', 'PARED', t, f, -8.33566951069481, 41.2062947118362).
no('Recarei', 'RECAR', f, f, -8.42215867462191, 41.1599363478137).
%no('Sobrosa', 'SOBRO', t, f, -8.38118071581788, 41.2557331783506).
no('Vandoma', 'VANDO', t, f, -8.34160692293342, 41.2328015719913).
%no('Vila Cova de Carros', 'VCCAR', t, f, -8.35109395257277, 41.2090666564063).

%linha(nome_linha, número_path, lista_abrev_paragens, tempo_minutos,distância_metros).

linha('Paredes_Aguiar', 1, ['AGUIA','RECAR', 'PARAD', 'CETE', 'PARED'], 31, 15700).
linha('Paredes_Aguiar', 3, ['PARED', 'CETE','PARAD', 'RECAR', 'AGUIA'], 31, 15700).
linha('Paredes_Gandra', 5 , ['GAND', 'VANDO', 'BALTR', 'MOURZ', 'PARED'], 26, 13000).
linha('Paredes_Gandra', 8, ['PARED', 'MOURZ', 'BALTR', 'VANDO', 'GAND'], 26, 13000).
linha('Paredes_Lordelo', 9, ['LORDL','VANDO', 'BALTR', 'MOURZ', 'PARED'], 29, 14300).
linha('Paredes_Lordelo', 11, ['PARED','MOURZ', 'BALTR', 'VANDO', 'LORDL'], 29, 14300).
%linha('Lordelo_Parada', 24, ['LORDL', 'DIGRJ', 'CRIST', 'VCCAR', 'BALTR', 'PARAD'], 22, 11000).
%linha('Lordelo_Parada', 26, ['PARAD', 'BALTR', 'VCCAR', 'CRIST', 'DIGRJ', 'LORDL'], 22, 11000).
%linha('Sobrosa_Cete', 22, ['SOBRO', 'CRIST', 'BESTR', 'VCCAR', 'MOURZ', 'CETE'], 23, 11500).
%linha('Sobrosa_Cete', 20, ['CETE', 'MOURZ', 'VCCAR', 'BESTR', 'CRIST', 'SOBRO'], 23, 11500).
%linha('Estação(Lordelo)_Lordelo',34,['ESTLO','LORDL'], 2,1500).
%linha('Lordelo_Estação(Lordelo)',35,['LORDL','ESTLO'], 2,1500).
%linha('Estação(Lordelo)_Sobrosa',36,['ESTLO','SOBRO'], 5,1500).
%linha('Sobrosa_Estação(Lordelo)',37,['SOBRO','ESTLO'], 5,1800).
%linha('Estação(Paredes)_Paredes',38,['ESTPA','PARED'], 2,1500).
%linha('Paredes_Estação(Paredes)',39,['PARED','ESTPA'], 2,1500).


%horario(PathID,[Hora,Hora,Hora, ...]).

horario(3,[73800,74280,74580,74880,75420]).
horario(3,[72900,73380,73680,73980,74520]).
horario(3,[72000,72380,72780,73080,73620]).
horario(3,[71100,71580,71880,72180,72720]).
horario(3,[70200,70680,70980,71280,71820]).
horario(3,[69300,69780,70080,70380,70920]).
horario(3,[68400,68880,69180,69480,70020]).
horario(3,[67500,67980,68280,68580,69120]).
horario(3,[66600,67080,67380,67680,68220]).
horario(3,[65700,66180,66480,66780,67320]).
horario(3,[63900,64380,64680,64980,65520]).
horario(3,[63000,63480,63780,64080,64620]).
horario(3,[61200,61680,61980,62280,62820]).
horario(3,[59400,59880,60180,60480,61020]).
horario(3,[57600,58080,58380,58680,59220]).
horario(3,[55800,56280,56580,56880,57420]).
horario(3,[54000,54480,54780,55080,55620]).
horario(3,[52200,52680,52980,53280,53820]).
horario(3,[50400,50880,51180,51480,52020]).
horario(3,[48600,49080,49380,49680,50220]).

horario(1,[36000,36540,36840,37140,37620]).
horario(1,[37800,38340,38640,38940,39420]).
horario(1,[39600,40140,40440,40740,41220]).

horario(5,[30360,30960,31200,31440,31920]).
horario(5,[33960,34560,34800,35040,35520]).
horario(5,[37560,38160,38400,38640,39120]).

horario(8,[28860,29340,29580,29820,30360]).
horario(8,[32400,32880,33120,33360,33960]).
horario(8,[36000,36480,36720,36960,37560]).

horario(9,[29160,29940,30180,30420,30900]).
horario(9,[30060,30840,31080,31320,31800]).
horario(9,[30960,31740,31980,32220,32700]).

horario(11,[27420,27900,28140,28380,29160]).
horario(11,[28320,28800,29040,29280,30060]).
horario(11,[29220,29700,29940,30180,30960]).

horario(20,[30180,30480,30720,30960,31320,31560]).
horario(20,[31380,31680,31920,32160,32520,32760]).
horario(20,[32580,32880,33120,33360,33720,33960]).

horario(22,[34800,35040,35400,35640,35880,36180]).
horario(22,[33600,33840,34200,34440,34680,34980]).
horario(22,[32400,32640,33000,33240,33480,33780]).

horario(24,[26700,27000,27240,27480,27720,28020]).
horario(24,[72900,73200,73440,73680,73920,74420]).
horario(24,[71580,71880,72120,72360,72600,72900]).

horario(26,[33600,33900,34140,34380,34620,34920]).
horario(26,[32280,32580,32820,33060,33300,33600]).
horario(26,[30960,31260,31500,31740,31980,32280]).



