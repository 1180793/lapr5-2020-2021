const sendJsonResponse = require('../middleware/sendJsonResponse');
const AuthService = require('../services/AuthService');

class LoginController {

    loginUser(req, res, next) {
        const authService = new AuthService();
        authService.loginUser(req.body).then((user) => {
            sendJsonResponse(res, 201, user);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                sendJsonResponse(res, 400, reason);
            }
        });
    }
    
}

module.exports = LoginController;