import { Driver } from "../model/model/Driver";


export class DriverMap {

    public toDTO(driver: Driver) {
        return {
            name: driver.name,
            birthDate: driver.birthDate,
            driversLicenceDueDate: driver.driversLicenseDueDate,
            driversLicenceNumber: driver.driversLicenseNumber,
            driverTypeNames: driver.driverTypeNames
        }
    }

    public static async toDomain(raw: any) {
        return new Driver(raw.name, raw.birthDate, raw.driversLicenseNumber, raw.driversLicenseDueDate, raw.driverTypes);
    }

}
