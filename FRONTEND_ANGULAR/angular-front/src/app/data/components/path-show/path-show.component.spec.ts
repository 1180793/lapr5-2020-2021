import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PathShowComponent } from './path-show.component';

describe('PathShowComponent', () => {
  let component: PathShowComponent;
  let fixture: ComponentFixture<PathShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PathShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PathShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
