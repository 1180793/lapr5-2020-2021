using System;
using Xunit;
using Moq;
using MDVApi.Domain.Vehicles;
using MDVApi.Domain.VehicleTypes;
using MDVApi.Domain.Shared;
using MDVApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Newtonsoft.Json;

namespace MDVTest
{
    public class VehicleControllerTests
    {
        private readonly ITestOutputHelper _output;

        public VehicleControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void PostVehicle()
        {
            string matricula = "XA-EA-12";
            string vin = "V1ND1S3L";
            string vehicleTypeStr = "MiniBus";
            string vehicleTypeStrNew = "BigBus";

            //VehicleTypes
            VehicleType vType = new VehicleType(vehicleTypeStr);
            VehicleType vTypeNew = new VehicleType(vehicleTypeStrNew);
            
            //Vehicles
            CreatingVehicleDto vDto = new CreatingVehicleDto(matricula, vin, vehicleTypeStr);
            Vehicle v = new Vehicle(matricula, vin);
            v.vehicleType = vType;
            VehicleDto vehicleDto = new VehicleDto(v.Id.AsGuid(), v.matricula, v.vin, v.vehicleType.name, v.startDate);

            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockRepositoryVehicle = new Mock<IVehicleRepository>();
            var mockRepositoryVehicleType = new Mock<IVehicleTypeRepository>();

            mockUnitOfWork.Setup(uW => uW.CommitAsync()).ReturnsAsync(1);
            mockRepositoryVehicle.Setup(x => x.AddAsync(It.IsAny<Vehicle>())).ReturnsAsync(v);
            mockRepositoryVehicle.Setup(x => x.GetByIdAsync(It.IsAny<VehicleId>())).ReturnsAsync(v);
            mockRepositoryVehicleType.Setup(vT => vT.getVehicleTypeByNameAsync(vehicleTypeStr)).ReturnsAsync(vType);
            mockRepositoryVehicleType.Setup(vT => vT.AddAsync(vTypeNew)).ReturnsAsync(vTypeNew);

            var VehicleService = new VehicleService(mockUnitOfWork.Object, mockRepositoryVehicle.Object, mockRepositoryVehicleType.Object);
            var VehicleTypeService = new VehicleTypeService(mockUnitOfWork.Object, mockRepositoryVehicleType.Object);

            var controller = new VehiclesController(VehicleService, VehicleTypeService);

            // Act
            ActionResult<VehicleDto> actionResult = await controller.Create(vDto);

            var okResult = actionResult.Result as ObjectResult;

            var actualVehicle = okResult.Value as VehicleDto;

            
            _output.WriteLine(JsonConvert.SerializeObject(actualVehicle));
            _output.WriteLine(JsonConvert.SerializeObject(vehicleDto));

            // Assert
            Assert.NotNull(actionResult);
            Assert.Equal(vehicleDto.matricula, actualVehicle.matricula);
        }
    }
}
