aStar(Orig,Dest,Cam,Custo):-
    aStar2(Dest,[(_,0,[Orig])],Cam,Custo).

aStar2(Dest,[(_,Custo,[Dest|T])|_],Cam,Custo):-
    reverse([Dest|T],Cam).

aStar2(Dest,[(_,Ca,LA)|Outros],Cam,Custo):-
    LA=[Act|_],
    findall((CEX,CaX,[X|LA]),
    (Dest\==Act,liga(Act,X,CustoX),\+ member(X,LA),
     CaX is CustoX + Ca, estimativa(X,Dest,EstX),
     CEX is CaX +EstX),Novos),
    append(Outros,Novos,Todos),
    write('Novos='),write(Novos),nl,
    sort(Todos,TodosOrd),
    write('TodosOrd='),write(TodosOrd),nl,
    aStar2(Dest,TodosOrd,Cam,Custo).

estimativa(No1,No2,Estimativa):-
    no(_,No1,_,_,X1,Y1),
    no(_,No2,_,_,X2,Y2),
    Estimativa is sqrt((X1-X2)^2+(Y1-Y2)^2).


:-dynamic liga/3.
gera_ligacoes:- retractall(liga(_,_,_)),
findall(_,
((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),
(no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
No1\==No2,
linha(_,N,LNos,_,_),
ordem_membros(No1,No2,LNos),
assertz(liga(No1,No2,N))
),_).
ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).
