export class LinePath {

  pathName: string;
  orientation: string;

  constructor(pathName: string, orientation: string) {

    this.pathName = pathName;
    this.orientation = orientation;
  }


  getPathName(): string {
    return this.pathName;
  }

  getOrientation(): string {
    return this.orientation;
  }
}
