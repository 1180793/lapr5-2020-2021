using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Infrastructure.DriverDuties
{
    internal class VehicleDutyEntityTypeConfiguration : IEntityTypeConfiguration<VehicleDuty>
    {
        public void Configure(EntityTypeBuilder<VehicleDuty> builder)
        {
            
            builder.HasKey(vD => vD.Id);
            builder.HasIndex(vD => vD.name)
                   .IsUnique();
        }
    }
}