import { DriverType } from "./DriverType";
import { LinePath } from './LinePath';
import { Nodes } from './Nodes';
import { VehicleType } from "./VehicleType";

export class Line {

    name: string;
    color: string;
    startNode: string;
    endNode: string;
    allowedVehicleTypes = new Array<string>();
    allowedDriverTypes = new Array<string>();


    constructor(name: string, color: string, startNode: string, endNode: string, allowedVehicleTypes: string[], allowedDriverTypes: string[]) {
        this.name = name;
        this.color = color;
        this.startNode = startNode;
        this.endNode = endNode;
        this.allowedVehicleTypes = allowedVehicleTypes;
        this.allowedDriverTypes = allowedDriverTypes;

    }

    public get LineColor() {
        return this.color;
    }
    public get LineName() {
        return this.name;
    }
    public get LineStartNode() {
        return this.startNode;
    }
    public get LineEndNode() {
        return this.endNode;
    }
    public get LineAllowedDriverTypes() {
        return this.allowedDriverTypes;
    }
    public get LineAllowedVehicleTypes() {
        return this.allowedVehicleTypes;
    }


}
