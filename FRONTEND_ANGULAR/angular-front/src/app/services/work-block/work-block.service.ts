import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { WorkBlockMap } from '../../mappers/WorkBlockMap';
import { WorkBlock } from '../../model/model/WorkBlock';
import Swal from 'sweetalert2'
@Injectable({
    providedIn: 'root'
})
export class WorkBlockService {

    uri = environment.uri_mdv; //'http://localhost:3000/api';
    mapper = new WorkBlockMap();

    constructor(private http: HttpClient) {
    }

    workBlocksList!: any[];

    async getWorkBlocks() {
        return this.http.get<Array<WorkBlock>>(`${this.uri}/workBlocks`);
    }

    async getWorkBlockByID(id: string) {
        return this.http.get(`${this.uri}/workBlocks/${id}`);
    }
    async getWorkBlockByVehicleDutyID(id: string) {
        return this.http.get(`${this.uri}/WorkBlocks/venicleDuty/${id}`);
    }

    async addWorkBlock(isCrewTravelTime: boolean, vehicleDutyID: string, duration: number, numberOfBlocks: number) {

        const workBlock: WorkBlock = new WorkBlock(isCrewTravelTime, vehicleDutyID, duration, numberOfBlocks);
        return this.addWorkBlockDTO(workBlock);
    };

    async addWorkBlockDTO(workBlock: WorkBlock) {
        return this.http.post<WorkBlock>(`${this.uri}/workBlocks `, this.mapper.toDTO(workBlock))
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Work Block was successfully created!', 'success')
                },
                error => {
                    var err = error.error.errors;
                    if (err != null) {
                        var errMsg = err[Object.keys(err)[0]];
                        Swal.fire('Error!', errMsg + '!', 'error')
                    } else {
                        Swal.fire('Error!', error.error + '!', 'error')
                    }
                }

            )
    }
}
