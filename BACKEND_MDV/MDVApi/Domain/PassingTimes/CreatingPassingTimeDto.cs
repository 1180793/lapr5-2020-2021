using System;

namespace MDVApi.Domain.PassingTimes
{
    public class CreatingPassingTimeDto
    {
        public long time { get; set; }
        public string nodeName { get; set; }
        public Boolean isUsed { get; set; }
        public Boolean isReliefPoint { get; set; }

        public CreatingPassingTimeDto(long time, string nodeName, Boolean isUsed, Boolean isReliefPoint){

                this.time = time;
                this.nodeName = nodeName;
                this.isUsed = isUsed;
                this.isReliefPoint = isReliefPoint;  
        } 
    }
}