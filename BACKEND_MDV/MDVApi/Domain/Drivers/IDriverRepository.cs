using MDVApi.Domain.Shared;

namespace MDVApi.Domain.Drivers
{
    public interface IDriverRepository : IRepository<Driver, DriverId>
    {
    }
}