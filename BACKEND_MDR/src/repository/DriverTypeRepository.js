const mongoose = require('mongoose');
const DriverTypeSchema = require('../schemas/DriverTypeSchema').driverTypeSchema;
const DriverType = mongoose.model('DriverType', DriverTypeSchema);

class DriverTypeRepository {

    constructor() { }

    /**
     * Retorna uma promessa com todas os Driver Types
     */
    findAll() {
        return DriverType
            .find()
            .exec();
    }

    /**
     * Encontra um Driver Type por Name
     * @param {String} driverTypeName 
     * @param {(err,item)} cb 
     */
    findByName(driverTypeName, cb) {
        DriverType
            .findOne({
                name: { $regex: new RegExp("^" + driverTypeName + "$", "i") }
            })
            .exec(cb);
    }

    /**
     * Checks if Driver Type exists
     * @param {String} driverTypeName
     * @param {(err,item)} cb 
     */
    exists(driverTypeName, cb) {
        DriverType
            .findOne({
                name: { $regex: new RegExp("^" + driverTypeName + "$", "i") }
            })
            .exec(cb);
    }


    /**
     * Guarda um DriverType
     * @param {DriverType} driverType 
     * @param {(err, savedDriverType)} cb 
     */
    save(driverType, cb) {
        driverType.save(function (err, oi) {
            if (err) {
                console.log(err)
                cb(new Error("Invalid Driver Type Save"));
            }
            else {
                cb(err, oi);
            }
        });
    }
}

module.exports = DriverTypeRepository;