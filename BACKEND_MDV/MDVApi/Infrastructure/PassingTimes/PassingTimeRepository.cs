using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDVApi.Domain.PassingTimes;
using MDVApi.Domain.Trips;
using MDVApi.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDVApi.Infrastructure.PassingTimes
{
    public class PassingTimeRepository : BaseRepository<PassingTime, PassingTimeId>,IPassingTimeRepository
    {

        private readonly DbSet<PassingTime> _objs;

        public PassingTimeRepository(MDVApiDbContext context):base(context.PassingTimes)
        {
                _objs = context.PassingTimes ?? throw new ArgumentNullException(nameof(context.PassingTimes));
        }

        public async Task<IList<PassingTimeDto>> getPassingTimeByTripIdAsync(TripId TripId)
        {
            IList<PassingTimeDto> passingTimesList = new List<PassingTimeDto>(); 

            var passingTimes = await _objs.Where(x => TripId.Equals(x.tripId)).ToListAsync();

            foreach(PassingTime passingTime in passingTimes){
                passingTimesList.Add(new PassingTimeDto(passingTime.Id.AsGuid(), passingTime.time, passingTime.nodeName, passingTime.isUsed, passingTime.isReliefPoint, passingTime.tripId));
            }

            return passingTimesList;
        }
    }
}