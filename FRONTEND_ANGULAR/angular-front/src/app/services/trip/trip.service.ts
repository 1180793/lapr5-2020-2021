import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LineTripMap } from 'src/app/mappers/LineTripMap';
import { TripMap } from 'src/app/mappers/TripMap';
import { LineTrip } from 'src/app/model/model/LineTrip';
import { Trip } from 'src/app/model/model/Trip';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'
@Injectable({
    providedIn: 'root'
})
export class TripService {
    uri = environment.uri_mdv;

    mapper = new TripMap();
    lineTripMapper = new LineTripMap();

    constructor(private http: HttpClient) { }
    headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
    }

    requestOptions = {
        headers: new HttpHeaders(this.headerDict),
    };

    async getTrips() {
        return this.http.get<Array<any>>(`${this.uri}/trips`);
    }

    async addTrip(isEmpty: boolean, line: string, path: string, startTime: number) {
        const trip: Trip = new Trip(isEmpty, path, line, startTime);
        return this.addTripDto(trip);
    }

    async addTripDto(trip: Trip) {
        return this.http.post<Trip>(`${this.uri}/trips`, this.mapper.toDTO(trip), this.requestOptions)
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Trip was successfully created!', 'success')
                },
                error => {
                    Swal.fire('Error!', 'One or more input fields are invalid!', 'error')

                    // var err = error.error.errors;

                    // if (err != null) {
                    //   var errMsg = err[Object.keys(err)[0]];
                    //   Swal.fire('Error!', errMsg + '!', 'error')
                    // } else {
                    //   Swal.fire('Error!', error.error + '!', 'error')
                    // }
                }

            )
    }

    async addLineTrip(isEmpty: boolean, lineName: string, pathNameGo: string, pathNameReturn: string, frequency: number, numberOfTrips: number, startTime: number) {
        var lineTrip = new LineTrip(isEmpty, lineName, pathNameGo, pathNameReturn, frequency, numberOfTrips, startTime)
        return this.addLineTripDto(lineTrip);
    }

    async addLineTripDto(lineTrip: LineTrip) {
        return this.http.post<LineTrip>(`${this.uri}/trips/line`, this.lineTripMapper.toDTO(lineTrip), this.requestOptions)
            .subscribe(
                value => {
                    Swal.fire('Sucess!', 'Trip was sucessfully created !', 'success')
                },
                error => {
                    Swal.fire('Error!', 'One or more input fields are invalid !', 'error')
                    // var err = error.error.errors;
                    // if (err != null) {
                    //   var errMsg = err[Object.keys(err)[0]];
                    //   Swal.fire('Error!', errMsg + '!', 'error')
                    // } else {
                    //   Swal.fire('Error!', error.error + '!', 'error')
                    // }
                }

            )
    }

}
