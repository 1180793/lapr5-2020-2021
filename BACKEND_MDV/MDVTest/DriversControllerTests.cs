using System;
using Xunit;
using Moq;
using MDVApi.Domain.Shared;
using MDVApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Newtonsoft.Json;
using MDVApi.Domain.DriverTypes;
using System.Collections.Generic;
using MDVApi.Domain.Drivers;

namespace MDVTest
{
    public class DriversControllerTests
    {
        private readonly ITestOutputHelper _output;

        public DriversControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void PostDriver()
        {
            string name = "Fernando Driver";
            DateTime birthDate = new DateTime(2019-07-24);
            string driversLicenceNumber = "";
            DateTime driversLicenceDueDate = new DateTime(2019-07-24);
            string driverTypeName = "BusDriver";
            IList<CreatingDriverTypeDto> driverTypeNames = new List<CreatingDriverTypeDto>();

            //DriverTypes
            CreatingDriverTypeDto cdriverTypeDto = new CreatingDriverTypeDto(driverTypeName);
            driverTypeNames.Add(cdriverTypeDto);
            DriverType dType = new DriverType(driverTypeName);

            //Drivers
            CreatingDriverDto cDriverDto = new CreatingDriverDto(name, birthDate, driversLicenceNumber, driversLicenceDueDate, driverTypeNames);
            Driver driver = new Driver(name, birthDate, driversLicenceNumber, driversLicenceDueDate);
            DriverDto driverDto = new DriverDto(driver.Id.AsGuid(), driver.name, driver.birthDate, driver.driversLicenceNumber, driver.driversLicenceDueDate);

            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockRepositoryDriver = new Mock<IDriverRepository>();
            var mockRepositoryDriverType = new Mock<IDriverTypeRepository>();

            mockUnitOfWork.Setup(uW => uW.CommitAsync()).ReturnsAsync(1);
            mockRepositoryDriver.Setup(d => d.AddAsync(It.IsAny<Driver>())).ReturnsAsync(driver);
            mockRepositoryDriver.Setup(d => d.GetByIdAsync(It.IsAny<DriverId>())).ReturnsAsync(driver);
            mockRepositoryDriverType.Setup(dT => dT.getDriverTypeByNameAsync(driverTypeName)).ReturnsAsync(dType);
            //mockRepositoryDriverType.Setup(dT => dT.AddAsync(dTypeNew)).ReturnsAsync(dTypeNew);

            var DriverService = new DriverService(mockUnitOfWork.Object, mockRepositoryDriver.Object, mockRepositoryDriverType.Object);
            var DriverTypeService = new DriverTypeService(mockUnitOfWork.Object, mockRepositoryDriverType.Object);

            var controller = new DriversController(DriverService, DriverTypeService);

            // Act
            ActionResult<DriverDto> actionResult = await controller.Create(cDriverDto);

            var okResult = actionResult.Result as ObjectResult;

            var actualDriver = okResult.Value as DriverDto;

            
            _output.WriteLine(JsonConvert.SerializeObject(actualDriver));
            _output.WriteLine(JsonConvert.SerializeObject(driverDto));

            // Assert
            Assert.NotNull(actionResult);
            Assert.Equal(driverDto.driversLicenceNumber, actualDriver.driversLicenceNumber);
        }
    }
}
