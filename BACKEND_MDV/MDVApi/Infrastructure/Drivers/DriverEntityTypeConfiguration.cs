using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Infrastructure.Shared;
using MDVApi.Domain.Drivers;
using MDVApi.Domain.DriverTypes;
using System.Collections.Generic;
using System.Linq;
using System;

namespace MDVApi.Infrastructure.Drivers
{
    internal class DriverEntityTypeConfiguration : IEntityTypeConfiguration<Driver>
    {
        public void Configure(EntityTypeBuilder<Driver> builder)
        {
            builder.ToTable("Drivers");
            builder.HasKey(d => d.Id);
            builder.HasIndex(d => d.driversLicenceNumber)
                   .IsUnique();

        }
    }
}