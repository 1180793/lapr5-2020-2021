const constants = require("../../utils/constants");

class Coordinates {

    constructor(latitude, longitude, elevation) {
        if (validateLatitude(latitude) && validateLongitude(longitude) && validateElevation(elevation)) {
            this._latitude = latitude;
            this._longitude = longitude;
            this._elevation = elevation;
        } else {
            throw new Error('Invalid values for Coordinate!');
        }
    }

    get latitude() {
        return this._latitude;
    }

    get longitude() {
        return this._longitude;
    }

    get elevation() {
        return this._elevation;
    }
};

/*
    Validations
*/
function validateLatitude(latitude) {
    return latitude <= constants.MAX_LATITUDE && latitude >= constants.MIN_LATITUDE;
}

function validateLongitude(longitude) {
    return longitude <= constants.MAX_LONGITUDE && longitude >= constants.MIN_LONGITUDE;
}

function validateElevation(elevation) {
    return elevation <= constants.MAX_ELEVATION && elevation >= constants.MIN_ELEVATION;
}

module.exports = Coordinates;