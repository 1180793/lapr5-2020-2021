import { TestBed } from '@angular/core/testing';

import { ImportScheduleService } from './import-schedule.service';

describe('ImportScheduleService', () => {
  let service: ImportScheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImportScheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
