using System;
using MDVApi.Domain.Shared;
using Newtonsoft.Json;

namespace MDVApi.Domain.VehicleTypes
{
    public class VehicleTypeId : EntityId
    {
        
        [JsonConstructor]
        public VehicleTypeId(Guid value) : base(value)
        {
        }

        public VehicleTypeId(String value) : base(value)
        {
        }

        override
        protected  Object createFromString(String text){
            return new Guid(text);
        }
        
        override
        public String AsString(){
            Guid obj = (Guid) base.ObjValue;
            return obj.ToString();
        }
        public Guid AsGuid(){
            return (Guid) base.ObjValue;
        }
    }
}