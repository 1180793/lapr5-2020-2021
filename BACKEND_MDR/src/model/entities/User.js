const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('config');
const jwt = require('jsonwebtoken');
const UserSchema = require('../../schemas/UserSchema').userSchema;

/**
 * Creates User instance from JSON Object
 * @param {JSON} userJson 
 */
async function createUserFromJSON(userJson) {
    var user = null;
    var User = mongoose.model('User', UserSchema);
    try {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(userJson.password, salt);

        user = new User({
            name: userJson.name,
            email: userJson.email,
            password: hashedPassword
        });
    } catch (e) {
        return null;
    }
    return user;
}

async function generateAuthToken(user) {
    const token = jwt.sign({ _id: user._id,  name: user.name, email: user.email, roles: user.roles }, config.get('jwtPrivateKey'));
    return token;
}

exports.User = mongoose.model('User', UserSchema);
exports.createUserFromJSON = createUserFromJSON;
exports.generateAuthToken = generateAuthToken;
