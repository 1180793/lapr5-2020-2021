import { Trip } from "./Trip";

export class VehicleDuty {
    name: string;
    color: string;
    date: string;
    trips: string[];

    constructor(name: string, color: string, date: string, trips: string[]) {
        this.name = name;
        this.color = color;
        this.date = date;
        this.trips = trips;
    }

    get Name() {
        return this.name;
    }

    get Color() {
        return this.color;
    }

    get Date() {
        return this.date;
    }

    get Trips() {
        return this.trips;
    }

}
