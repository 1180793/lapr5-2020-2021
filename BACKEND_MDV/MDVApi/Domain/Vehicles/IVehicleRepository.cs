using MDVApi.Domain.Shared;

namespace MDVApi.Domain.Vehicles
{
    public interface IVehicleRepository: IRepository<Vehicle,VehicleId>
    {
    }
}