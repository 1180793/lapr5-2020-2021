const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const UserController = require('../controllers/UserController');

var userController = new UserController();

router.get('/me', auth, userController.getUserProfile);
router.delete('/me', auth, userController.deleteUser);

module.exports = router;
