import { DriverDuty } from "../model/model/DriverDuty";


export class DriverDutyMap {
    public toDTO(driverDuty: DriverDuty) {
        return {
            name: driverDuty.name,
            color: driverDuty.color,
            workBlockIds: driverDuty.workBlocks,
        }
    }

    public async toDomain(raw: any) {
        return new DriverDuty(raw.name, raw.color, raw.workBlocks);
    }
}
