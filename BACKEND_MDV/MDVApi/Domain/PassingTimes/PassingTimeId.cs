using System;
using MDVApi.Domain.Shared;
using Newtonsoft.Json;

namespace MDVApi.Domain.PassingTimes
{
    public class PassingTimeId : EntityId
    {
        [JsonConstructor]
        public PassingTimeId(Guid value) : base(value)
        {
        }

        public PassingTimeId(String value) : base(value)
        {
        }

        override
        protected  Object createFromString(String text){
            return new Guid(text);
        }
        
        override
        public String AsString(){
            Guid obj = (Guid) base.ObjValue;
            return obj.ToString();
        }
        public Guid AsGuid(){
            return (Guid) base.ObjValue;
        }
    }
}