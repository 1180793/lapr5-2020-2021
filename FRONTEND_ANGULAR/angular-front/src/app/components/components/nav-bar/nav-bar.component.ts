import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { routes } from 'src/app/app-routing.module'
import { AuthService } from 'src/app/services/auth/auth.service';
import { AppSettings } from 'src/utils/AppSettings';
import { Router } from '@angular/router';
import { AuthEvent } from 'src/app/services/auth/AuthEvent';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

    boolProfile = false;
    profileText: String = '';
    signinStatus: String = '';
    routes: any;
    authSubscription: Subscription;
    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches),
            shareReplay()
        );

    constructor(private breakpointObserver: BreakpointObserver, private jwtAuth: AuthService, private router: Router, private authEvent: AuthEvent) {
        this.routes = this.getRoutes();
        if (jwtAuth.getUser()) {
            this.signinStatus = 'Logout';
            this.profileText = 'My Profile';
            this.boolProfile = true;
        } else {
            this.signinStatus = 'Login';
            this.profileText = '';
            this.boolProfile = false;
        }
    }

    ngOnInit() {
        this.authSubscription = this.authEvent.authListener().subscribe(state => {
            this.signinStatus = state;
            this.routes = this.getRoutes();
            this.profileText = this.fixProfile();
        });
    }

    ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }

    fixProfile() {
        const user = this.jwtAuth.getUser();
        if (!user) {
            this.boolProfile = true;
            return '';
        } else {
            this.boolProfile = false;
            return 'My Profile';
        }
    }

    getRoutes() {
        const user = this.jwtAuth.getUser();

        const routesReturn = [];
        if (!user) {
            routes.filter(it => it.requiresAuth === false).forEach((route) => {
                routesReturn.push(route);
            })
        } else {
            routes.filter(it => it.requiresAuth === true).forEach((route) => {
                if (this.jwtAuth.hasPermission(route.roles)) {
                    routesReturn.push(route);
                }
            })

        }

        return routesReturn;
    }

    signinHandler() {
        if (this.jwtAuth.getUser()) {
            Swal.fire('Error!', 'User Logged Out!', 'error')

            this.jwtAuth.signout();
            this.authEvent.emitAuthStatus('Login');
        } else {
            this.router.navigateByUrl("auth/login");
        }
    }

    myProfile() {
        this.router.navigateByUrl('/profile');
    }

}
