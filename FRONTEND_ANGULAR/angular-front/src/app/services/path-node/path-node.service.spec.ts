import { TestBed } from '@angular/core/testing';

import { PathNodeService } from './path-node.service';

describe('PathNodeService', () => {
  let service: PathNodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PathNodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
