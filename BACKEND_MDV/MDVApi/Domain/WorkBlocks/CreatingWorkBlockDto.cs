using System;
using System.ComponentModel.DataAnnotations;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Domain.WorkBlocks
{
    public class CreatingWorkBlockDto
    {
        public Boolean isCrewTravelTime { get; set; }

        [Required(ErrorMessage = "Work Block is required")]
        public Guid VehicleDutyId { get; set; }

        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid Duration")]
        public long duration { get; set; }

        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid Number of Work Blocks")]
        public long numberOfBlocks { get; set; }
        
    }
}