using System;
using System.Collections.Generic;
using MDVApi.Domain.ValueObjects.PathNodes;
using MDVApi.Domain.VehicleTypes;

namespace MDVApi.Domain.ValueObjects.Nodes
{
    public class CreatingNodeDto
    {
        public string name { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string shortName { get; set; }
        public Boolean isDepot { get; set; }
        public Boolean isReliefPoint { get; set; }
    
    }

}