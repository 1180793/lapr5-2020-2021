const mongoose = require('mongoose');
const Joi = require('joi');

const coordinatesSchema = require('./CoordinatesSchema').coordinatesSchema;
const joiCoordinateSchema = require('./CoordinatesSchema').joiCoordinateSchema;

// Persistence Schema
const nodeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 200
    },
    shortName: {
        type: String,
        maxlength: 20,
        unique: true,
        required: true
    },
    coordinates: {
        type: coordinatesSchema,
        required: true
    },
    isDepot: {
        type: Boolean,
        default: false
    },
    isReliefPoint: {
        type: Boolean,
        default: false
    },
}, { retainKeyOrder: true });

nodeSchema.virtual('id').get(function () {
    return this.shortName;
});

// Joi Schema
const joiNodeSchema = Joi.object({
    name: Joi.string()
        .max(200)
        .required(),
    shortName: Joi.string()
        .max(20)
        .required(),
    coordinates: joiCoordinateSchema
        .required(),
    isDepot: Joi.boolean()
        .default(false),
    isReliefPoint: Joi.boolean()
        .default(false)
});

// Validate
function validateNode(node) {
    return joiNodeSchema.validate(node);
}

exports.joiNodeSchema = joiNodeSchema;
exports.nodeSchema = nodeSchema;
exports.validate = validateNode;
