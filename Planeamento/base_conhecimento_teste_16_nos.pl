%no(nome_paragem, abrev_paragem, flag_ponto_rendição, flag_estação_recolha, longitude, latitude).


no('7', 'NO7', 't', 'f', 2, 2).
no('8', 'NO8', 't', 'f', 2, 1).
no('12', 'NO12', 't', 'f', 2, 3).
no('13', 'NO13', 't', 'f', 3, 3).
no('14', 'NO14', 't', 'f', 3, 2).
no('15', 'NO15', 't', 'f', 3, 1).

no('19', 'NO19', 't', 'f', 2, 4).
no('20', 'NO20', 't', 'f', 3, 4).
no('21', 'NO21', 't', 'f', 4, 4).
no('22', 'NO22', 't', 'f', 4, 3).
no('23', 'NO23', 't', 'f', 4, 2).
no('24', 'NO24', 't', 'f', 4, 1).

no('32', 'NO32', 't', 'f', 5, 4).
no('33', 'NO33', 't', 'f', 5, 3).
no('34', 'NO34', 't', 'f', 5, 2).
no('35', 'NO35', 't', 'f', 5, 1).



%linha(nome_linha, número_path, lista_abrev_paragens, tempo_minutos, distância_metros).

linha('8_19', 1, ['NO8', 'NO7', 'NO12', 'NO19'], 10, 20).
linha('8_19', 2, ['NO19','NO12','NO7','NO8'], 10, 20).

linha('21_24', 3, ['NO21', 'NO22', 'NO23', 'NO24'], 10, 20).
linha('21_24', 4, ['NO24','NO23','NO22','NO21'], 10, 20).

linha('12_33', 5, ['NO12', 'NO13', 'NO22', 'NO33'], 10, 20).
linha('12_33', 6, ['NO33','NO22','NO13','NO12'], 10, 20).

linha('7_34', 7, ['NO11', 'NO12', 'NO13', 'NO22'], 10, 20).
linha('7_34', 8, ['NO22','NO13','NO12','NO11'], 10, 20).

%linha('8_35', 9, ['NO8', 'NO15', 'NO24', 'NO35'], 10, 20).
%linha('8_35', 10, ['NO35','NO24','NO15','NO8'], 10, 20).

%linha('19_32', 11, ['NO19', 'NO20', 'NO21', 'NO32'], 10, 20).
%linha('19_32', 12, ['NO32','NO21','NO20','NO19'], 10, 20).

%linha('15_20', 13, ['NO11', 'NO12', 'NO13', 'NO22'], 10, 20).
%linha('15_20', 14, ['NO22','NO13','NO12','NO11'], 10, 20).

%linha('32_35', 7, ['NO32', 'NO33', 'NO34', 'NO35'], 10, 20).
%linha('32_35', 8, ['NO35','NO34','NO33','NO32'], 10, 20).
