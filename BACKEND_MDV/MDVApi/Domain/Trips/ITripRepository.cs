using System.Collections.Generic;
using System.Threading.Tasks;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Domain.Trips
{
    public interface ITripRepository: IRepository<Trip,TripId>
    {
        Task<List<Trip>> GetByLineAsync(string line);
        IList<Trip> getTripsByVehicleDuty(VehicleDutyId vehicleDutyId);
    }
}