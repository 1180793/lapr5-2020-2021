:-dynamic melhor_sol_ntrocas/2.
:-dynamic liga/3.
:-dynamic nSol/1.

plan_mud_mot1(Noi,Nof,LCaminho_menostrocas,H_inicial):-
    get_time(Ti),
    asserta(nSol(0)),
    (melhor_caminho(Noi,Nof,H_inicial);true),
    retract(melhor_sol_ntrocas(LCaminho_menostrocas,Hora_final)),
    retract(nSol(NS)),
    get_time(Tf),
    TSol is Tf-Ti,
    write('Numero de Solucoes:'),write(NS),nl,
    write('Tempo de geracao da solucao:'),write(TSol),nl,
    write('Hora final:'),write(Hora_final),nl.

melhor_caminho(Noi,Nof,H_inicial):-
    asserta(melhor_sol_ntrocas(_,100000)),
    caminho(Noi,Nof,LCaminho),
    atualiza_melhor(LCaminho,H_inicial),
    fail.

atualiza_melhor(LCaminho,H_inicial):-
    retract(nSol(NS)),
    NS1 is NS + 1,
    asserta(nSol(NS1)),
    melhor_sol_ntrocas(_,N), %Fetch do horario final da melhor solução atual
    tempoChegada(LCaminho,H_inicial,Tempo), %Calcula o tempo minimo para a viagem
    Tempo<N,retract(melhor_sol_ntrocas(_,_)), %caso a hora final seja menor troca a melhor solucao
    asserta(melhor_sol_ntrocas(LCaminho,Tempo)). %caso a hora final seja menor troca a melhor solucao


tempoChegada([],Hn1,Hn1):- !. %Criterio de paragem

tempoChegada([(N1,N2,Li)|Lc], Hn1, C):-  %N1- No1 segmento, N2- No2 Segmento, Li-Linha do segmento, Hn1- Horario Inicial, C- Hora do final da viagem(vazio)
	horarioSegmento(Li,N1,Hi), %Guardar em Hi a hora de passagem no 1o no
	horarioSegmento(Li,N2,Hf), %Guardar em Hi a hora de passagem no 2o no
	Hi >= Hn1,  %Se passa depois da hora inicial
	Hi =< Hf,   %Se passa no primeiro no antes da hora final
	tempoChegada(Lc,Hf,C). %Continua para os restantes elementos de Lc, Hf- Nova hora de passagem inicial, C-tempo final(vazio)

horarioSegmento(Li,No,Hor):-  %Li-Linha, No- No atual, Hor- Hora de passagem no no (vazio)
	linha(_,Li,Pe,_,_), % Pe- Percurso da linha
	nth1(Po,Pe,No), %Se No pertence a Pe, Po guarda o index
	horario(Li,Hors), % Para os horarios da linha
	nth1(Po,Hors,Hor),!. %Na posição Po do horário guarda-se em Hor e impede o backtracking se a condição for satisfeita



caminho(Noi,Nof,LCaminho):- caminho(Noi,Nof,[],LCaminho).
caminho(No,No,Lusadas,Lfinal):- reverse(Lusadas,Lfinal).
caminho(No1,Nof,Lusadas,Lfinal):-
    liga(No1,No2,N),
    \+member((_,_,N),Lusadas),
    \+member((No2,_,_),Lusadas),
    \+member((_,No2,_),Lusadas),
    caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).



gera_ligacoes:- retractall(liga(_,_,_)),
    findall(_,
    ((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),
    (no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
    No1\==No2,
    linha(_,N,LNos,_,_),
    ordem_membros(No1,No2,LNos),
    assertz(liga(No1,No2,N))
    ),_).

ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).
