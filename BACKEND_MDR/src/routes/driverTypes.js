const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

const DriverTypeController = require('../controllers/DriverTypeController');

var driverTypeController = new DriverTypeController();


router.get('/', [auth], driverTypeController.listAll);
router.get('/:id', [auth, dataAdmin], driverTypeController.details);
router.post('/', [auth, dataAdmin], driverTypeController.create);

module.exports = router;
