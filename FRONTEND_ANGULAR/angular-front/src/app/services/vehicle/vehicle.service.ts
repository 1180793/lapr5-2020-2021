import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VehicleMap } from 'src/app/mappers/VehicleMap';
import { Vehicle } from 'src/app/model/model/Vehicle';
import { environment } from 'src/environments/environment';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})
export class VehicleService {

    uri = environment.uri_mdv;

    mapper = new VehicleMap();

    constructor(private http: HttpClient) { }
    headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
    }

    requestOptions = {
        headers: new HttpHeaders(this.headerDict),
    };


    async getVehicles() {
        return this.http.get(`${this.uri}/vehicles`);
    }


    async addVehicle(licensePlate: string, vin: string, vehicleType: string) {

        let vehicle: Vehicle = new Vehicle(licensePlate, vin, vehicleType);
        return this.addVehicleTypeDTO(vehicle);

    }

    //add Vehicle Type using a Mapper
    async addVehicleTypeDTO(vehicle: Vehicle) {
        return this.http.post<Vehicle>(`${this.uri}/vehicles`, this.mapper.toDTO(vehicle), this.requestOptions)
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Vehicle was successfully created!', 'success')
                },
                error => {
                    var err = error.error.errors;
                    if (err != null) {
                        var errMsg = err[Object.keys(err)[0]];
                        Swal.fire('Error!', errMsg + '!', 'error')
                    } else {
                        Swal.fire('Error!', error.error + '!', 'error')
                    }

                }

            )
    }
}
