class LineDTO {

    constructor(line) {
        this._name = line.name;
        this._color = line.color;
        this._startNode = line.startNode;
        this._endNode = line.endNode;
        this._allowedVehicleTypes = [];
        if (line.allowedVehicleTypes != null) {
            line.allowedVehicleTypes.forEach(element => {
                this._allowedVehicleTypes.push(element);
            });
        }
        this._allowedDriverTypes = [];
        if (line.allowedDriverTypes != null) {
            line.allowedDriverTypes.forEach(element => {
                this._allowedDriverTypes.push(element);
            });
        }
        this._linePaths = [];
        if (line.linePaths != null) {
            line.linePaths.forEach(element => {
                this._linePaths.push(element);
            });
        }
    }

    get name() {
        return this._name;
    }

    get color() {
        return this._color;
    }

    get startNode() {
        return this._startNode;
    }

    get endNode() {
        return this._endNode;
    }

    get allowedVehicleTypes() {
        return this._allowedVehicleTypes;
    }
    
    get allowedDriverTypes() {
        return this._allowedDriverTypes;
    }

    get linePaths() {
        return this._linePaths;
    }

}
module.exports = LineDTO;