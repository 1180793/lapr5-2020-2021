export class Vehicle {


    licensePlate: string;
    vin: string;
    vehicleType: string;
    startDate: Date;


    constructor(licensePlate: string, vin: string, vehicleType: string) {
        this.licensePlate = licensePlate
        this.vin = vin;
        this.vehicleType = vehicleType;
        this.startDate = new Date(); //sets the current date

    }
    get LicensePlate() {
        return this.licensePlate;
    }

    get Vin() {
        return this.vin;
    }
    get VehicleType() {
        return this.vehicleType;
    }
    get StartDate() {
        return this.startDate;
    }






}