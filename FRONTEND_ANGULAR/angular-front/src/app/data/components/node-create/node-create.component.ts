import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NodeService } from 'src/app/services/node/node.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-data-node-create',
    templateUrl: './node-create.component.html',
    styleUrls: ['./node-create.component.css']
})

export class NodeCreateComponent implements OnInit {

    nodeService = new NodeService(this.http);

    //Variables
    createNodeForm!: FormGroup
    newNodeName = ''
    newNodeShortName = ''
    isReliefPoint: boolean = false;
    isDepot: boolean = false;
    latitude = ''
    longitude = ''


    constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }

        this.isReliefPoint = false;
        this.isDepot = false;
    }

    ngOnInit(): void {
        this.reactiveForm();
    }

    //Reactive Form
    reactiveForm() {
        this.createNodeForm = this.fb.group({
            name: ['', [Validators.maxLength(200), Validators.minLength(3)]],
            shortName: ['', [Validators.maxLength(20)]],
            latitude: [0, [, Validators.min(-90), Validators.max(90)]],
            longitude: [0, [Validators.min(-180), Validators.max(180)]],
            isReliefPoint: [false,],
            isDepot: [false,]
        })
    }

    getName(): string {
        return this.createNodeForm.get('name')?.value;

    }
    getShortName(): string {
        return this.createNodeForm.get('shortName')?.value;

    }
    getLatitude(): number {
        return this.createNodeForm.get('latitude')?.value;

    }
    getLongitude(): number {
        return this.createNodeForm.get('longitude')?.value;

    }

    getisReliefPoint(): boolean {
        if (!this.createNodeForm.get('isReliefPoint')?.value == undefined) {
            this.isReliefPoint = false;
            return this.isReliefPoint;
        }
        return this.createNodeForm.get('isReliefPoint')?.value;
    }

    getIsDepot(): boolean {
        if (!this.createNodeForm.get('isDepot')?.value == undefined) {
            this.isDepot = false;
            return this.isDepot;
        }
        return this.createNodeForm.get('isDepot')?.value;

    }

    getIsDepotChecked(checkBox: MatCheckbox) {
        if (this.isDepot == false && checkBox.checked) {
            this.isDepot = true;
        }
        this.isDepot = false;

    }

    getIsReliefChecked(checkBox: MatCheckbox) {
        if (this.isReliefPoint == false && checkBox.checked) {
            this.isReliefPoint = true;
        }
        this.isDepot = false;
    }
    public hasError = (controlName: string, errorName: string) => {
        return this.createNodeForm.controls[controlName].hasError(errorName);
    }

    //consegue ir buscar todos os valores do formulario

    onCreateNode() {
        if (this.createNodeForm.valid) {
            this.nodeService.addNode(this.getName(), this.getShortName(), this.getLatitude(), this.getLongitude(), this.getIsDepot(), this.getisReliefPoint())
            this.createNodeForm.clearValidators();
            this.createNodeForm.clearAsyncValidators();
            this.createNodeForm.reset();
        }
        else {
            if (this.createNodeForm.get('isReliefPoint')?.errors) {
                Swal.fire('Error!', 'Node must be a relief point!', 'error')
            }
            Swal.fire('Error!', 'Invalid input fields for node!', 'error')
        }


    }
}
