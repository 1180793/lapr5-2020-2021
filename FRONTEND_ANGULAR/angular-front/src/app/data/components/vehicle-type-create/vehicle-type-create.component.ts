import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { VehicleTypeService } from 'src/app/services/vehicle-type/vehicle-type.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'
@Component({
    selector: 'app-vehicle-type-create',
    templateUrl: './vehicle-type-create.component.html',
    styleUrls: ['./vehicle-type-create.component.css']
})
export class VehicleTypeCreateComponent implements OnInit {

    //Service
    vehicleTypeService = new VehicleTypeService(this.http);

    //Variables
    createVehicleTypeForm!: FormGroup
    name = ''
    autonomy = ''
    cost = ''
    averageSpeed = ''
    energySource = ''
    consumption = ''
    emissions = ''
    fuelList: any[] = [
        { name: 'Gasoline' },
        { name: 'LGP' },
        { name: 'Diesel' },
        { name: 'Hydrogen' },
    ];

    constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.form();
    }

    //reactive form
    // vechicleTypeform() {
    //     this.createVehicleTypeForm = this.fb.group({
    //         name: ['', [Validators.required, Validators.maxLength(20)]],
    //         autonomy: ['', [Validators.required, Validators.min(0)]],
    //         cost: ['', [Validators.required, Validators.min(0)]],
    //         avgSpeed: ['', [Validators.required, Validators.min(0)]],
    //         energySource: ['', [Validators.required]],
    //         consumption: ['', [Validators.required, Validators.min(0)]],
    //         emissions: ['', [Validators.required, Validators.min(0)]]
    //     })
    // }
    form() {
        this.createVehicleTypeForm = this.fb.group({
            name: ['',],
            autonomy: ['',],
            cost: ['',],
            avgSpeed: ['',],
            energySource: ['',],
            consumption: ['',],
            emissions: ['',]
        })
    }

    getName() {
        return this.createVehicleTypeForm.get('name')?.value;
    }

    getAutonomy() {
        return this.createVehicleTypeForm.get('autonomy')?.value;
    }

    getCost() {
        return this.createVehicleTypeForm.get('cost')?.value;
    }

    getAverageSpeed() {
        return this.createVehicleTypeForm.get('avgSpeed')?.value;
    }

    getEnergySource() {
        return this.createVehicleTypeForm.get('energySource')?.value;
    }

    getConsumption() {
        return this.createVehicleTypeForm.get('consumption')?.value;
    }

    getEmissions() {
        return this.createVehicleTypeForm.get('emissions')?.value;
    }
    public hasError = (controlName: string, errorName: string) => {
        return this.createVehicleTypeForm.controls[controlName].hasError(errorName);
    }

    onCreateVehicleType() {
        if (this.createVehicleTypeForm.valid) {
            this.vehicleTypeService.addVehicleType(this.getName(), this.getAutonomy(), this.getCost(), this.getAverageSpeed(),
                this.getEnergySource(), this.getConsumption(), this.getEmissions());

            this.createVehicleTypeForm.reset();
        }

    }

}


