namespace MDVApi.Domain.Authentication
{
    public class Authentication
    {
        public string token { get; set; }
        public User user { get; set; }
    }

}