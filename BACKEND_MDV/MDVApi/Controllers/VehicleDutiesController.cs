using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.Trips;

namespace MDVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleDutiesController : ControllerBase
    {
        private readonly VehicleDutyService _service;
        private readonly TripService _serviceT;

        public VehicleDutiesController(VehicleDutyService service, TripService serviceT)
        {
            _service = service;
            _serviceT = serviceT;
        }

        // GET: api/VehicleDuties
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleDutyDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }


        // GET: api/VehicleDuties/id
        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleDutyDto>> GetGetById(Guid id)
        {

            var vehicleDuty = await _service.GetByIdAsync(new VehicleDutyId(id));

            if (vehicleDuty == null)
            {
                return NotFound();
            }

            return vehicleDuty;
        }

        // GET: api/VehicleDuties
        [HttpGet("date/{date}")]
        public async Task<ActionResult<IEnumerable<VehicleDutyDto>>> GetAllByDate(DateTime date)
        {
            return await _service.GetByDateAsync(date);
        }

        // POST: api/VehicleDuties
        [HttpPost]
        public async Task<ActionResult<VehicleDutyDto>> Create(CreatingVehicleDutyDto dto)
        {

            try
            {

                IList<Trip> tripList = new List<Trip>();

                foreach (Guid id in dto.tripIds)
                {
                    var trip = await _serviceT.GetTripByIdAsync(new TripId(id));

                    if(trip == null)
                        return BadRequest("Trip doesn't exist.");
                        
                    tripList.Add(trip);
                }


                var vehicleDuty = await _service.AddAsync(dto, tripList);

                return CreatedAtAction(nameof(GetGetById), new { id = vehicleDuty.Id }, vehicleDuty);
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.InnerException.Message });
            }

        }

    }
}
