namespace MDVApi.Domain.ValueObjects.Lines
{
    public class LineSimplified {
        public string name { get; set; }
        public string color { get; set; }
        
    }
}