const PathRepository = require('../repository/PathRepository');
const PathDTO = require('../dto/PathDTO');
const validatePath = require('../schemas/PathSchema').validate;
const createPathFromJSON = require('../model/entities/Path').createPathFromJSON;

const LineService = require('../services/LineService');

class PathService {

    constructor() { }

    async createPath(pathJSON) {
        return new Promise(async (resolve, reject) => {
            const pathRepo = new PathRepository();
            try {
                const { error } = validatePath(pathJSON);
                if (error) {
                    throw new Error(error.message);
                } else {
                    var pathValidationDTO = new PathDTO(pathJSON);
                }
            } catch (err) {
                reject(`Invalid Path. ${err.message}`);
            }
            if (pathValidationDTO == null) {
                console.log('IS NULLLLLLLLLLLLLLLLLLLLL');
                reject("Invalid Path.");
            } else {
                // console.log(pathValidationDTO);
                try {
                    const pExistingPath = new Promise((resolve, reject) => {
                        pathRepo.findByKey(pathJSON.key, function (err, existingPath) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(existingPath);
                            }
                        })
                    });

                    pExistingPath
                        .then(async function (existingPath) {
                            if (existingPath == null) {
                                try {
                                    const pathToSave = await createPathFromJSON(pathJSON);
                                    pathRepo.save(pathToSave, async (error) => {
                                        if (error) {
                                            console.log("PathService -> error", error)
                                            reject(error);
                                        } else {
                                            let lineService = new LineService();
                                            try {
                                                await lineService.addPathToLine(pathValidationDTO, pathValidationDTO.line);
                                            } catch (err) {
                                                console.log("PathController -> err", err)
                                                reject(err);
                                            }
                                            resolve(pathValidationDTO);
                                        }
                                    });
                                } catch (err) {
                                    console.log("PathService -> err", err)
                                    reject(err);
                                }
                            } else {
                                reject('Repeated Path Key!');
                            }
                        })
                        .catch((reason) => {
                            reject(reason)
                        });
                } catch (err) {
                    reject(err);
                }
            }
        });
    }

    async findAllByLine(lineID) {
        return new Promise(async (resolve, reject) => {
            const pathRepo = new PathRepository();
            pathRepo.findAllByLine(lineID, (err, paths) => {
                var lstPathDTO = [];
                paths.forEach((element) => {
                    let pathDTO = new PathDTO(element);
                    lstPathDTO.push(pathDTO);
                });
                resolve(lstPathDTO);
            });
        });
    }

    async findByKey(key) {
        return new Promise(async (resolve, reject) => {
            const pathRepo = new PathRepository();
            pathRepo.findByKey(key, (err, path) => {
                if (path != null) {
                    const pathDTO = new PathDTO(path);
                    resolve(pathDTO);
                } else {
                    resolve(null);
                }
            });
        });
    }

}

module.exports = PathService;