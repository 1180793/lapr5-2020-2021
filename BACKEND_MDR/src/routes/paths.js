const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

const PathController = require('../controllers/PathController');

var pathController = new PathController();

router.get('/', [auth], pathController.listAll);
router.get('/:id', [auth, dataAdmin], pathController.details);
router.post('/', [auth, dataAdmin], pathController.create);

module.exports = router;
