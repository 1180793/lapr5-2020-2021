using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.WorkBlocks;
using MDVApi.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDVApi.Infrastructure.Vehicles
{
    public class WorkBlockRepository : BaseRepository<WorkBlock, WorkBlockId>, IWorkBlockRepository
    {

        private readonly DbSet<WorkBlock> _objs;

        public WorkBlockRepository(MDVApiDbContext context):base(context.WorkBlocks)
        {
           _objs = context.WorkBlocks ?? throw new ArgumentNullException(nameof(context.WorkBlocks));
        }

        public async Task<IList<WorkBlock>> getWorkBlocksByVehicleDutyAsync(VehicleDutyId vehicleDutyId)
        {       
            var workBlocks = await _objs.Where(x => vehicleDutyId.Equals(x.VehicleDutyId)).ToListAsync();
    
            return workBlocks;
        }

    }
}