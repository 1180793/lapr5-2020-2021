using System;
using System.Collections.Generic;
using MDVApi.Domain.WorkBlocks;

namespace MDVApi.Domain.DriverDuties
{
    public class DriverDutyDto
    {   
        public Guid Id { get; set; }
        public string name{ get; set; }
        public string color { get; set; }

        public DriverDutyDto(Guid Id, string name, string color){

            this.Id = Id;
            this.name = name;
            this.color = color;
        } 
    }

}