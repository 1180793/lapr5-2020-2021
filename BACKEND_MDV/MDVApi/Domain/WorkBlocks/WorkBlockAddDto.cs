using System;
using System.ComponentModel.DataAnnotations;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Domain.WorkBlocks
{
    public class WorkBlockAddDto
    {
        public CreatingWorkBlockDto workBlockDto { get; set; }
        public long startTime { get; set; }
        public long endTime { get; set; }
        public string startNode { get; set; }
        public string endNode { get; set; }

        public WorkBlockAddDto(CreatingWorkBlockDto workBlockDto, long startTime, long endTime, string startNode, string endNode){

            this.workBlockDto = workBlockDto;
            this.startTime = startTime;
            this.endTime = endTime;
            this.startNode = startNode;
            this.endNode = endNode;
        }
        
    }
}