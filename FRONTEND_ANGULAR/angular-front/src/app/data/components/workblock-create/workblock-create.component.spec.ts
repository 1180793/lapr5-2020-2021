import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkblockCreateComponent } from './workblock-create.component';

describe('WorkblockCreateComponent', () => {
  let component: WorkblockCreateComponent;
  let fixture: ComponentFixture<WorkblockCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkblockCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkblockCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
