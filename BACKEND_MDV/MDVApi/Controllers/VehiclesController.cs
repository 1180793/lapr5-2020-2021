using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MDVApi.Domain.Vehicles;
using MDVApi.Domain.VehicleTypes;
using MDVApi.Domain.Shared;

namespace MDVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private static readonly string vehicleException = "IX_Vehicles_matricula";
        private static readonly string vinException = "IX_Vehicles_vin";
        private readonly VehicleService _service;
        private readonly VehicleTypeService _serviceVt;

        public VehiclesController(VehicleService service, VehicleTypeService serviceVt)
        {
            _service = service;
            _serviceVt = serviceVt;
        }

        // GET: api/Vehicle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        
        // GET: api/Vehicle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleDto>> GetGetById(Guid id)
        {

            var vehicle = await _service.GetByIdAsync(new VehicleId(id));

            if (vehicle == null)
            {
                return NotFound();
            }

            return vehicle;
        } 
        
        // POST: api/Vehicle
        [HttpPost]
        public async Task<ActionResult<VehicleDto>> Create(CreatingVehicleDto dto)
        {   

            VehicleType vehicleType = null;

            try{
                vehicleType = await _serviceVt.getVehicleTypeByNameAsync(dto.vehicleTypeName);
            }catch(Exception e){
                Console.WriteLine("Vehicle Type doesn't exist" + e);
                VehicleTypeDto vehicleTypeDto = await _serviceVt.AddAsync(new CreatingVehicleTypeDto(dto.vehicleTypeName));
                vehicleType = await _serviceVt.getVehicleTypeByNameAsync(dto.vehicleTypeName);
            }

            try
            {
                var vehicle = await _service.AddAsync(dto, vehicleType);

                return CreatedAtAction(nameof(GetGetById), new { id = vehicle.Id }, vehicle);
            }
            catch(Exception ex)
            {
                if( ex.InnerException.Message.Contains(vehicleException)){
                    return BadRequest("A vehicle with the given license plate number is already registered.");
                }
                if( ex.InnerException.Message.Contains(vinException)){
                    return BadRequest("A vehicle with the given vin is already registered.");
                }
                
                return BadRequest(new {Message = ex.InnerException.Message});
            }

        }

    }
}
