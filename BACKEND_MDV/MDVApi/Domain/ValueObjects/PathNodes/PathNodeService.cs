
using MDVApi.Domain.ValueObjects.Nodes;

namespace MDVApi.Domain.ValueObjects.PathNodes
{
    public class PathNodeService
    {

        public PathNodeService()
        {
        }

        public PathNode convertPathNodeDto(CreatingPathNodeDto dto, Node node){

            PathNode pathNode = new PathNode( dto.duration, dto.distance);

            return pathNode;
        }

    }
}