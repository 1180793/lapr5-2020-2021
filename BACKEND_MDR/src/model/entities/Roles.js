module.exports = Object.freeze({
    "DATA_ADMINISTRATOR": {
        id: "DATA_ADMINISTRATOR",
        name: "Data Administrator"
    },
    "CLIENT": {
        id: "CLIENT",
        name: "Client"
    },
    "MANAGER": {
        id: "GESTOR",
        name: "Gestor"
    },
}
);