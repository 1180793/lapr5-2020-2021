
export class DriverDuty {
    name: string;
    color: string;
    workBlocks: any[] = [];


    constructor(name: string, color: string, workBlocks: any[]) {
        this.name = name;
        this.color = color;

        if (workBlocks != null) {
            workBlocks.forEach(element => {
                this.workBlocks.push(
                    element
                )
            })
        }
    }

    public getDriverDutyName() {
        return this.name;
    }
    public getColor() {
        return this.color;
    }

    public getDriverDutyWorkBlocks() {
        return this.workBlocks;
    }



}