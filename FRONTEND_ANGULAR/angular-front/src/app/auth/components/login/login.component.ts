import { Component, OnInit, AfterViewInit, Output } from "@angular/core";
import {
    FormGroup,
    Validators,
    FormControl
} from "@angular/forms";
import { AuthService } from "../../../services/auth/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { AuthEvent } from "src/app/services/auth/AuthEvent";
import Swal from 'sweetalert2'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    errorMsg = '';
    hide = true;
    return: string;
    loading: Boolean;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private authEvent: AuthEvent,
        private jwtAuth: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        const user = this.jwtAuth.getUser();
        if (user) {
            this.router.navigateByUrl(this.return);
        }
        this.hide = true;
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.loginForm = new FormGroup({
            email: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });

        this.route.queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => this.return = params['return'] || '/');
    }

    ngOnDestroy() {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onLogin() {
        const signinData = this.loginForm.value;
        this.loading = true;
        this.jwtAuth.signin(signinData.email, signinData.password)
            .subscribe(response => {
                this.loading = false;
                this.router.navigateByUrl('/profile');
                Swal.fire('Success!', 'User login successfull!', 'success')

                this.authEvent.emitAuthStatus('Logout');
            }, err => {
                this.loading = false;
                this.errorMsg = err.error;
                Swal.fire('Error!', this.errorMsg + '!', 'error')

            })
    }
}
