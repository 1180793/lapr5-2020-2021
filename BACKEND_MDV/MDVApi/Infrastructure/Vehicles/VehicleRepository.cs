using MDVApi.Domain.Vehicles;
using MDVApi.Infrastructure.Shared;

namespace MDVApi.Infrastructure.Vehicles
{
    public class VehicleRepository : BaseRepository<Vehicle, VehicleId>,IVehicleRepository
    {
        public VehicleRepository(MDVApiDbContext context):base(context.Vehicles)
        {
           
        }
    }
}