import { VehicleType } from "../model/model/VehicleType";

export class VehicleTypeMap {

    public toDTO(vehicleType: VehicleType) {
        return {
            name: vehicleType.vehicleTypeDescription,
            autonomy: vehicleType.vehicleTypeAutonomy,
            cost: vehicleType.vehicleTypeCost,
            averageSpeed: vehicleType.vehicleTypeAverageSpeed,
            energySource: vehicleType.vehicleTypeEnergySource,
            consumption: vehicleType.vehicleTypeConsumption,
            emissions: vehicleType.vehicleTypeEmissions
        }
    }

    public static async toDomain(raw: any) {
        return new VehicleType(raw.description, raw.autonomy, raw.cost, raw.averageSpeed, raw.energySource, raw.consumption, raw.emissions);
    }

}
