using System;

namespace MDVApi.Domain.VehicleDuties
{
    public class VehicleDutyDto
    {   
        public Guid Id { get; set; }
        public string name{ get; set; }
        public string color { get; set; }
        public string depots { get; set; }
        public string date { get; set; }
        
        public VehicleDutyDto(Guid Id, string name, string color, string depots, DateTime date){

            this.Id = Id;
            this.name = name;
            this.color = color;
            this.depots = depots; 
            this.date = date.ToString("yyyy/MM/dd").Replace('-','/');
        } 
    }

}