const mongoose = require('mongoose');
const UserSchema = require('../schemas/UserSchema').userSchema;
const User = mongoose.model('User', UserSchema);
var ObjectId = require('mongodb').ObjectId;

class UserRepository {

    constructor() { }

    /**
     * Retorna uma promessa com todos os Users
     */
    findAll() {
        return User
            .find({
                isActive: true
            })
            .exec();
    }

    /**
     * Encontra um User por email
     * @param {String} userEmail 
     * @param {(err,item)} cb 
     */
    findByEmail(userEmail, cb) {
        User
            .findOne({
                email: { $regex: new RegExp("^" + userEmail + "$", "i") },
                isActive: true
            })
            .select({ _id: 1, name: 1, email: 1, roles: 1 })
            .exec(cb);
    }

    /**
     * Checks if User exists
     * @param {String} userEmail 
     * @param {(err,item)} cb 
     */
    exists(userEmail, cb) {
        User
            .findOne({
                email: { $regex: new RegExp("^" + userEmail + "$", "i") },
                isActive: true
            })
            .exec(cb);
    }

    /**
     * Guarda um User
     * @param {User} user 
     * @param {(err,savedUser)} cb 
     */
    save(user, cb) {
        user.save(function (err, oi) {
            if (err) cb(new Error("Invalid User Save"));
            else
                cb(err, oi);
        });
    }

    /**
     * Deletes an User
     * @param {User} user 
     * @param {(err,deletedUser)} cb 
     */
    deleteUser(id, cb) {
        User
            .findOne({
                "_id": ObjectId(id)
            }, function (err, user) {
                user.name = 'DELETED USER';
                user.email = undefined;
                user.password = undefined;
                user.roles = undefined;
                user.isActive = false;
                user.save();
            })
            .exec(cb);
    }
}

module.exports = UserRepository;