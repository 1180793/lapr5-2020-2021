using System;
using System.ComponentModel.DataAnnotations;

namespace MDVApi.Domain.Trips
{
    public class CreatingLineTripDto
    {
        public Boolean isEmpty { get; set; }
        [Required(ErrorMessage = "Line is required")]
        public string lineName { get; set; }
        [Required(ErrorMessage = "Path Go is required")]
        public string pathNameGo { get; set; }
        [Required(ErrorMessage = "Path Return is required")]
        public string pathNameReturn { get; set; }
        [Required(ErrorMessage = "Frequency is required")]
        public long frequency { get; set; }
        [Required(ErrorMessage = "Number of trips is required")]
        public long numberOfTrips { get; set; }
        [Required(ErrorMessage = "Start Time is required")]
        public long startTime { get; set; }

    }
}