import { TestBed } from '@angular/core/testing';

import { LinePathService } from './line-path.service';

describe('LinePathService', () => {
  let service: LinePathService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LinePathService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
