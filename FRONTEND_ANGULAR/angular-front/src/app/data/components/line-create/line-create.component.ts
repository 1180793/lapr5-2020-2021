import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from "@angular/forms";

import { Observable, Subscription } from 'rxjs';
import { NodeMap } from 'src/app/mappers/NodeMap';


import { DriverTypeService } from 'src/app/services/driver-type/driver-type.service';
import { VehicleTypeService } from 'src/app/services/vehicle-type/vehicle-type.service';
import { LineService } from 'src/app/services/line/line.service';
import { NodeService } from 'src/app/services/node/node.service';
import { ColorPickerControl, ColorsTable } from '@iplab/ngx-color-picker';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-data-line-create',
    templateUrl: './line-create.component.html',
    styleUrls: ['./line-create.component.css']
})


export class LineCreateComponent implements OnInit {
    //Services

    nodeService = new NodeService(this.http);
    lineService = new LineService(this.http);
    driverTypeService = new DriverTypeService(this.http);
    vehicleTypeService = new VehicleTypeService(this.http);

    //Mappers
    nodeMap = new NodeMap();

    //Variables
    createLineForm!: FormGroup
    newLineName = ''
    newLineCode = ''
    startNode = ''
    endNode = ''
    allowedDriverTypes: any;
    allowedVehicleTypes: any;
    color = ''
    nodeList: any;

    //List for Population 
    driverTypeList: any;
    vehicleTypeList: any;




    constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {

        this.reactiveForm();
        this.populateNodes();
        this.populateDriverTypes();
        this.populateVehicleTypes();
    }

    async populateVehicleTypes() {
        (await this.vehicleTypeService.getVehicleTypes()).subscribe(
            value => {
                this.vehicleTypeList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );


    }

    async populateDriverTypes() {
        (await this.driverTypeService.getDriverTypes()).subscribe(
            value => {

                this.driverTypeList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }

    async populateNodes() {
        (await this.nodeService.getNodes()).subscribe(
            value => {

                this.nodeList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );

    }

    //Reactive Form
    reactiveForm() {
        this.createLineForm = this.fb.group({
            name: ['', [Validators.maxLength(200)]],
            startNode: ['',],
            endNode: ['',],
            allowedDriverTypes: [[]],
            allowedVehicleTypes: [[]],

        })
    }
    public chromeControl = new ColorPickerControl()
        .setValueFrom(ColorsTable.aquamarine)
        .hideAlphaChannel();

    getName(): string {
        return this.createLineForm.get('name')?.value;

    }
    getCode(): string {
        return this.createLineForm.get('code')?.value;

    }


    getColor(): string {
        var color = this.chromeControl.value.getRgba();
        var nC = 'RGB(' + color.getRed() + ',' + color.getGreen() + ',' + color.getBlue() + ')';

        return nC;

    }
    getStartNode(): string {
        return this.createLineForm.get('startNode')?.value;

    }

    getEndNode(): string {
        return this.createLineForm.get('endNode')?.value;

    }

    getAllowedVehicleTypes(): string[] {
        var vList = new Array<string>();
        var list = this.createLineForm.get('allowedVehicleTypes')?.value;

        if (list == undefined) {
            return [];
        }
        for (let i = 0; i < list['length']; i++) {
            vList.push(list[i]);
        }

        return vList;
    }


    getAllowedDriverTypes(): string[] {
        var vList = new Array<string>();
        var list = this.createLineForm.get('allowedDriverTypes')?.value;

        if (list == undefined) {
            return [];
        }

        for (let i = 0; i < list['length']; i++) {
            vList.push(list[i]);
        }

        return vList;
    }

    public hasError = (controlName: string, errorName: string) => {
        return this.createLineForm.controls[controlName].hasError(errorName);
    }




    onCreateLine() {
        if (this.createLineForm.valid) {

            this.lineService.addLine(this.getName(), this.getColor(), this.getStartNode(), this.getEndNode(), this.getAllowedVehicleTypes(), this.getAllowedDriverTypes());
            this.createLineForm.reset();
        }

        else {
            Swal.fire('Error!', 'Invalid Line!', 'error')
        }
    }
}
