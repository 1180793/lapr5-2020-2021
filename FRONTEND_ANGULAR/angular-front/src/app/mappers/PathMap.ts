import { Path } from "../model/model/Path";

export class PathMap {

    public toDTO(path: Path) {
        return {
            key: path.getKey(),
            line: path.getLine(),
            orientation: path.getOrientation(),
            isEmpty: path.getIsEmpty(),
            pathNodeList: path.getPathNodeList(),
        }
    }

    public fromBackend(path: any) {
        if (path.hasOwnProperty('_line')) {
            return {
                key: path._key,
                line: path._line.name,
                color: path._line.color
            }
        } else {
            return null;
        }
    }

    public static async toDomain(raw: any) {
        return new Path(raw.key, raw.line, raw.orientation, raw.isEmpty, raw.pathNodeList);
    }

}
