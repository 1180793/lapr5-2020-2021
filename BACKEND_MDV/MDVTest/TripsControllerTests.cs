using System;
using Xunit;
using Moq;
using MDVApi.Domain.Shared;
using MDVApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Newtonsoft.Json;
using MDVApi.Domain.WorkBlocks;
using System.Collections.Generic;
using MDVApi.Domain.Trips;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.PassingTimes;
using System.Net.Http;
using MDVApi.Domain.Authentication;
using Microsoft.Extensions.Configuration;
using MDVApi.Domain.ValueObjects.Lines;
using MDVApi.Domain.ValueObjects.Paths;
using MDVApi.Domain.ValueObjects.Nodes;
using MDVApi.Domain.ValueObjects.PathNodes;

namespace MDVTest
{
    public class TripsControllerTests
    {
        private readonly ITestOutputHelper _output;

        public TripsControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        //[Fact]
        public async void PostTrip()
        {
            
            //Line
            CreatingLineDto line = new CreatingLineDto();

            //Path
           
            NodeSimplified node = new NodeSimplified{name = "Baltar", shortName = "Balt", isDepot = true, isReliefPoint = true};
            CreatingPathNodeDto cPathNodeDto = new CreatingPathNodeDto{duration = 100, distance = 100, node = node};
            IList<CreatingPathNodeDto> creatingPathNodeDtos = new List<CreatingPathNodeDto>();
            creatingPathNodeDtos.Add(cPathNodeDto);
            
            CreatingPathDto path = new CreatingPathDto{_key = "Path: Test", _isEmpty = true, _pathNodeList = creatingPathNodeDtos};

            //Trips
            CreatingTripDto cTripDto = new CreatingTripDto(false, "Line", "Path", 17000);

            Trip trip = new Trip(false, "Return", "Lordelo_Parada", "Path:18");
            
            TripDto tripDto = new TripDto(trip.Id.AsGuid(), trip.isEmpty, trip.orientation, trip.lineName, trip.pathName, trip.isGenerated);

            //PassingTimes
            PassingTime passingTime = new PassingTime(40000, "Baltar", true);

            IList<PassingTimeDto> passingTimesListR = new List<PassingTimeDto>();
            PassingTimeDto passingTimeDto = new PassingTimeDto(Guid.NewGuid(), 40000, "Baltar", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto);
            PassingTimeDto passingTimeDto2 = new PassingTimeDto(Guid.NewGuid(), 40240, "Vila Cova de Carros", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto2);
            PassingTimeDto passingTimeDto3 = new PassingTimeDto(Guid.NewGuid(), 40480, "Cristelo", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto3);
            PassingTimeDto passingTimeDto4 = new PassingTimeDto(Guid.NewGuid(), 50000, "Cristelo", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto4);
            PassingTimeDto passingTimeDto5 = new PassingTimeDto(Guid.NewGuid(), 50240, "Vila Cova de Carros", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto5);
            PassingTimeDto passingTimeDto6 = new PassingTimeDto(Guid.NewGuid(), 50480, "Baltar", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto6);
            
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockRepositoryTrip = new Mock<ITripRepository>();
            var mockRepositoryPassingTimes = new Mock<IPassingTimeRepository>();
            var mockConfig = new Mock<IConfiguration>();
            var mockAuth = new Mock<AuthenticationService>();
            

            mockUnitOfWork.Setup(uW => uW.CommitAsync()).ReturnsAsync(1);
            mockRepositoryTrip.Setup(t => t.AddAsync(It.IsAny<Trip>())).ReturnsAsync(trip);
            mockRepositoryPassingTimes.Setup(pT => pT.AddAsync(It.IsAny<PassingTime>())).ReturnsAsync(passingTime);
            mockAuth.Setup(a => a.login(mockConfig.Object)).ReturnsAsync(new HttpClient());
            

            var TripService = new TripService(mockUnitOfWork.Object, mockRepositoryTrip.Object);
            var PassingTimeService = new PassingTimeService(mockUnitOfWork.Object, mockRepositoryPassingTimes.Object);
            var AuthenticationService = new AuthenticationService();

            var controller = new TripsController(TripService, PassingTimeService, mockConfig.Object, AuthenticationService);

            // Act
            ActionResult<TripDto> actionResult = await controller.Create(cTripDto);

            var okResult = actionResult.Result as ObjectResult;

            var actualTrip = okResult.Value as TripDto;

            _output.WriteLine("Trip");
            _output.WriteLine(JsonConvert.SerializeObject(actualTrip));
            _output.WriteLine(JsonConvert.SerializeObject(tripDto));

            // Assert
            Assert.NotNull(actionResult);
            Assert.Equal(tripDto.lineName, actualTrip.lineName);
            Assert.Equal(tripDto.pathName, actualTrip.pathName);
            Assert.Equal(tripDto.orientation, actualTrip.orientation);

        }
    }
}
