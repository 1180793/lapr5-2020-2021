const LineRepository = require('../repository/LineRepository');
const LineDTO = require('../dto/LineDTO');
const validateLine = require('../schemas/LineSchema').validate;
const createLineFromJSON = require('../model/entities/Line').createLineFromJSON;

class LineService {

    constructor() { }

    async createLine(lineJSON) {
        return new Promise(async (resolve, reject) => {
            const lineRepo = new LineRepository();
            try {
                const { error } = validateLine(lineJSON);
                if (error) {
                    throw new Error(error.message);
                } else {
                    var lineValidationDTO = new LineDTO(lineJSON);
                }
            } catch (err) {
                reject(`Invalid Line. ${err.message}`);
            }
            if (lineValidationDTO == null) {
                reject("Invalid Line.");
            } else {
                //console.log(lineValidationDTO);
                const pExistingLine = new Promise((resolve, reject) => {
                    lineRepo.exists(lineJSON.name, function (err, existingLine) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(existingLine);
                        }
                    })
                });

                pExistingLine
                    .then(async function (existingLine) {
                        if (existingLine == null) {
                            try {
                                var lineToSave = await createLineFromJSON(lineJSON);
                                lineRepo.save(lineToSave, error => {
                                    if (error) {
                                        reject(error)
                                    } else {
                                        resolve(lineValidationDTO);
                                    }
                                });
                            } catch (err) {
                                reject(err);
                            }
                        } else {
                            reject('Repeated Line Name!');
                        }
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    }

    async findAllSorted(customFilters) {
        return new Promise(async (resolve, reject) => {
            const lineRepo = new LineRepository();
            lineRepo.findAllSorted(customFilters.sortBy, customFilters.filterBy, (err, lines) => {
                var lstLineDTO = [];
                lines.forEach((element) => {
                    let lineDTO = new LineDTO(element);
                    lstLineDTO.push(lineDTO);
                });
                resolve(lstLineDTO);
            });
        });
    }

    async findByName(name) {
        return new Promise(async (resolve, reject) => {
            const lineRepo = new LineRepository();
            lineRepo.findByName(name, (err, line) => {
                if (line != null) {
                    const lineDTO = new LineDTO(line);
                    resolve(lineDTO);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async findID(name) {
        return new Promise(async (resolve, reject) => {
            const lineRepo = new LineRepository();
            lineRepo.findID(name, (err, line) => {
                if (line != null) {
                    resolve(line);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async validateLine(lineName) {
        return new Promise((resolve, reject) => {
            const lineRepo = new LineRepository();
            lineRepo.exists(lineName, function (err, line) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(line);
                }
            });
        });
    }

    async addPathToLine(path, line) {
        return new Promise((resolve, reject) => {
            const lineRepo = new LineRepository();
            lineRepo.addPathToLine(path, line, function (err, newPath) {
                if (err) {
                    throw err;
                } else {
                    resolve(newPath);
                }
            });
        })
    }

}

module.exports = LineService;