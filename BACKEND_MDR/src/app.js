const express = require('./loaders/express.js');
const cors = require('cors');
const config = require('config');
const logger = require('./loaders/logging');

// Express
const app = express();
app.use(cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", config.get('frontEndURL')); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

try {
    // Require JSON Web Token
    require("./loaders/config")();
} catch (err) {
    logger.error(err.message);
    process.exit(1);
}

// Validation
require('./loaders/validation');

// Set up mongoose connection
require('./loaders/db')(app.get('env'));

// Require Routes
require("./loaders/routes")(app);

const port = process.env.PORT || config.get("port");
const server = app.listen(port, () =>
    logger.info(`Listening on port ${port}...`)
);

module.exports = server;