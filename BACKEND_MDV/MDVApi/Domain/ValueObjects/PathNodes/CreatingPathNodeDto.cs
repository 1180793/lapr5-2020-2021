using MDVApi.Domain.ValueObjects.Nodes;

namespace MDVApi.Domain.ValueObjects.PathNodes
{
    public class CreatingPathNodeDto
    {
        public int duration { get; set; }
        public int distance { get; set; }
        public NodeSimplified node { get; set; }
    }

}