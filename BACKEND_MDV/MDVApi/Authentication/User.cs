using System.Collections.Generic;

namespace MDVApi.Domain.Authentication
{
    public class User
    {
        public string _id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public IList<string> roles { get; set; }
    }

}