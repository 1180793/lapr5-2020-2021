using System;

namespace MDVApi.Domain.DriverTypes
{
    public class DriverTypeDto
    {
        public Guid Id { get; set; }
        public string name { get; set; }

    }

}