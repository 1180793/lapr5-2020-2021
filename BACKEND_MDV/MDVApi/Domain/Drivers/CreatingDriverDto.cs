using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MDVApi.Domain.DriverTypes;

namespace MDVApi.Domain.Drivers
{
    public class CreatingDriverDto
    {
        [Required(ErrorMessage = "Name is required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Birth Date is required")]
        [DataType(DataType.Date)]
        public DateTime birthDate { get; set; }
        [RegularExpression(@"^P-[0-9]{7} [0-9]$", ErrorMessage = "Invalid Drivers licence")]
        [Required(ErrorMessage = "Drivers licence number is required")]
        public string driversLicenceNumber { get; set; }
       
        [Required(ErrorMessage = "Drivers licence due date is required")]
        public DateTime driversLicenceDueDate { get; set; }
        [Required(ErrorMessage = "Driver Types are required")]
        public IList<CreatingDriverTypeDto> driverTypeNames { get; set; }
        
        
        public CreatingDriverDto(string name, DateTime birthDate, string driversLicenceNumber, DateTime driversLicenceDueDate, IList<CreatingDriverTypeDto> driverTypeNames)
        {
            this.name = name;
            this.birthDate = birthDate;
            this.driversLicenceNumber = driversLicenceNumber;
            this.driversLicenceDueDate = driversLicenceDueDate;
            this.driverTypeNames = driverTypeNames;
        } 
    }
}