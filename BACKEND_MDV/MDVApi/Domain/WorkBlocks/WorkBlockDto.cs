using System;

namespace MDVApi.Domain.WorkBlocks
{
    public class WorkBlockDto
    {
        public Guid Id { get; set; }
        public long startTime { get; set; }
        public long endTime { get; set; }
        public string startNode { get; set; } 
        public string endNode { get; set; }
        public Boolean isCrewTravelTime { get; set; }
        public Boolean isActive { get; set; }
        public Guid vehicleDutyId { get; set; }

        public WorkBlockDto(Guid Id, long startTime, long endTime, string startNode, string endNode, Boolean isCrewTravelTime, Boolean isActive){

            this.Id = Id;
            this.startTime = startTime;
            this.endTime = endTime;
            this.startNode = startNode;
            this.endNode = endNode;
            this.isCrewTravelTime = isCrewTravelTime;
            this.isActive = isActive;
        } 

        public WorkBlockDto(Guid Id, long startTime, long endTime, string startNode, string endNode, Boolean isCrewTravelTime, Boolean isActive, Guid vehicleDutyId){

            this.Id = Id;
            this.startTime = startTime;
            this.endTime = endTime;
            this.startNode = startNode;
            this.endNode = endNode;
            this.isCrewTravelTime = isCrewTravelTime;
            this.isActive = isActive;
            this.vehicleDutyId = vehicleDutyId;
        } 
    }

}