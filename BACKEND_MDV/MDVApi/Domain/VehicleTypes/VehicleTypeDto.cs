using System;

namespace MDVApi.Domain.VehicleTypes
{
    public class VehicleTypeDto
    {
        public Guid Id { get; set; }
        public string name { get; set; }

    }

}