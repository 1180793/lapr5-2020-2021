import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDutyShowComponent } from './vehicle-duty-show.component';

describe('VehicleDutyShowComponent', () => {
  let component: VehicleDutyShowComponent;
  let fixture: ComponentFixture<VehicleDutyShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleDutyShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDutyShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
