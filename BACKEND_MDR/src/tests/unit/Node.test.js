/**
 * @jest-environment node
 */
//mudar smp o environment quando o teste usa algo com mongoose

const Node = require('../../../src/model/entities/Node');
const NodeDTO = require('../../../src/dto/NodeDTO');
const { expect } = require('@jest/globals');

test('Check if Node JSON create is working', () => {

    var json = {
        "name": "Node_TEST",
        "coordinates": {
            "latitude": 0,
            "longitude": 0,
            "elevation": 0
        },
        "shortName": "Node_SHORTNAME",
        "isDepot": true,
        "isReliefPoint": true
    };

    var node = Node.createNodeFromJSON(json);

    expect(node.name).toBe("Node_TEST");
    expect(node.coordinates.latitude).toBe(0);
    expect(node.coordinates.longitude).toBe(0);
    expect(node.coordinates.elevation).toBe(0);
    expect(node.shortName).toBe("Node_SHORTNAME");
    expect(node.isDepot).toBe(true);
    expect(node.isReliefPoint).toBe(true);

})

test('Check if vehicle type JSON create is working', () => {

    var dto = new NodeDTO({
        "name": "Node_TEST",
        "coordinates": {
            "latitude": 0,
            "longitude": 0,
            "elevation": 0
        },
        "shortName": "Node_SHORTNAME",
        "isDepot": true,
        "isReliefPoint": true
    });

    var node = Node.createNodeFromDTO(dto);

    expect(node.name).toBe("Node_TEST");
    expect(node.coordinates.latitude).toBe(0);
    expect(node.coordinates.longitude).toBe(0);
    expect(node.coordinates.elevation).toBe(0);
    expect(node.shortName).toBe("Node_SHORTNAME");
    expect(node.isDepot).toBe(true);
    expect(node.isReliefPoint).toBe(true);

});

