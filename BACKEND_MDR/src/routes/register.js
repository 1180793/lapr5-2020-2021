const express = require('express');
const router = express.Router();

const RegisterController = require('../controllers/RegisterController');

var registerController = new RegisterController();

router.post('/', registerController.registerUser);

module.exports = router;
