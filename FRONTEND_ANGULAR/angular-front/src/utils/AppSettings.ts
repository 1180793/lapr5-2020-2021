import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export class AppSettings {
   public static API_ENDPOINT = environment.uri;
   public static MDV_API_ENDPOINT = environment.uri_mdv;
   public static HTTP_HEADERS = {
      headers: new HttpHeaders({
         'Access-Control-Allow-Origin': '*',
         'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
         'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
      })
   };
   public static DATA_ADMINISTRATOR = 'DATA_ADMINISTRATOR';
   public static CLIENT = 'CLIENT';
   public static GESTOR = 'GESTOR';
   public static NO_PERMISSION_REDIRECT = 'access-denied';
}