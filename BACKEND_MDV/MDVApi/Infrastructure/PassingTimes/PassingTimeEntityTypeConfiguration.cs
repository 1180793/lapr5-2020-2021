using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Domain.PassingTimes;

namespace MDVApi.Infrastructure.PassingTimes
{
    internal class PassingTimeEntityTypeConfiguration : IEntityTypeConfiguration<PassingTime>
    {
        public void Configure(EntityTypeBuilder<PassingTime> builder)
        {
           
            builder.ToTable("PassingTimes");
            builder.HasKey(b => b.Id);
            
        }
    }
}