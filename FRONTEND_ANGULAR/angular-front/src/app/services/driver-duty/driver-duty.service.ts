import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DriverDutyMap } from 'src/app/mappers/DriverDutyMap';
import { DriverDuty } from 'src/app/model/model/DriverDuty';
import { environment } from 'src/environments/environment';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'
@Injectable({
    providedIn: 'root'
})
export class DriverDutyService {
    uri = AppSettings.MDV_API_ENDPOINT;

    mapper = new DriverDutyMap();


    constructor(private http: HttpClient) { }
    headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
    }
    requestOptions = {
        headers: new HttpHeaders(this.headerDict),
    };

    async getDriverDuties() {
        return this.http.get<Array<any>>(`${this.uri}/driverDuties`);
    }

    async addDriverDuties(name: string, color: string, workBlocks: any[]) {
        const driverDuty: DriverDuty = new DriverDuty(name, color, workBlocks);
        return this.addDriverDutyDto(driverDuty);

    }
    async addDriverDutyDto(driverDuty: DriverDuty) {
        return this.http.post<DriverDuty>(`${this.uri}/driverDuties`, this.mapper.toDTO(driverDuty), this.requestOptions)
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Driver Duty was successfully created!', 'success')
                },
                error => {
                    var err = error.error.errors;
                    if (err != null) {
                        var errMsg = err[Object.keys(err)[0]];
                        Swal.fire('Error!', errMsg + '!', 'error')
                    } else {
                        Swal.fire('Error!', error.error + '!', 'error')
                    }
                }

            )
    }
}
