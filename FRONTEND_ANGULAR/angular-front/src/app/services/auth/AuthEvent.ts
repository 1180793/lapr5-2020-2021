import { BehaviorSubject } from "rxjs";
import { AuthService } from "./auth.service";

export class AuthEvent {

    private authEvent = new BehaviorSubject<String>(this.getDefaultValue());
    constructor(private jwtAuth: AuthService) { }

    getDefaultValue() {
        if (this.jwtAuth.getUser()) {
            return 'Logout';
        } else {
            return 'Login';
        }
    }

    emitAuthStatus(state: string) {
        this.authEvent.next(state);
    }

    authListener() {
        return this.authEvent.asObservable();
    }

}