using System;
using Xunit;
using Moq;
using MDVApi.Domain.Shared;
using MDVApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Newtonsoft.Json;
using System.Collections.Generic;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.Trips;
using MDVApi.Domain.WorkBlocks;
using MDVApi.Domain.DriverDuties;
using Microsoft.Extensions.Configuration;

namespace MDVTest
{
    public class DriverDutiesControllerTests
    {
        private readonly ITestOutputHelper _output;

        public DriverDutiesControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void PostDriverDuty()
        {
            string name = "D 1";
            string color =  "RGB(96,02,41)";
            string MaxHours = "14400";
            IList<Guid> workBlockIds = new List<Guid>();

            //WorkBLocks
            IList<WorkBlock> workBlockList = new List<WorkBlock>();
            WorkBlock workBlock = new WorkBlock(40000, 40480, "Baltar", "Cristelo", true);
            WorkBlock workBlock1 = new WorkBlock(40480, 40800, "Baltar", "Cristelo", true);
            workBlockIds.Add(workBlock.Id.AsGuid());
            workBlockList.Add(workBlock);
            workBlockIds.Add(workBlock1.Id.AsGuid());
            workBlockList.Add(workBlock1);
            
            //DriverDuties
            CreatingDriverDutyDto cdriverDutyDto = new CreatingDriverDutyDto{name = name, color = color, workBlockIds = workBlockIds};
            DriverDuty driverDuty = new DriverDuty(cdriverDutyDto.name, cdriverDutyDto.color);
            driverDuty.WorkBlocks = workBlockList;

            DriverDutyDto driverDutyDto = new DriverDutyDto(driverDuty.Id.AsGuid(), driverDuty.name, driverDuty.color);

            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockRepositoryDriverDuty = new Mock<IDriverDutyRepository>();
            var mockRepositoryWorkBlock = new Mock<IWorkBlockRepository>();
            var mockConfiguration = new Mock<IConfiguration>();

            mockUnitOfWork.Setup(uW => uW.CommitAsync()).ReturnsAsync(1);
            mockRepositoryDriverDuty.Setup(d => d.AddAsync(It.IsAny<DriverDuty>())).ReturnsAsync(driverDuty);
            //mockRepositoryWorkBlock.Setup(wB => wB.GetByIdAsync(It.IsAny<WorkBlockId>())).ReturnsAsync(workBlock);
            mockRepositoryWorkBlock.Setup(wB => wB.GetByIdAsync(workBlock.Id)).ReturnsAsync(workBlock);
            mockRepositoryWorkBlock.Setup(wB => wB.GetByIdAsync(workBlock1.Id)).ReturnsAsync(workBlock1);
            mockConfiguration.Setup(c => c["DriverParameters:MaxHours"]).Returns(MaxHours);

            var DriverDutyService = new DriverDutyService(mockUnitOfWork.Object, mockRepositoryDriverDuty.Object);
            var WorkBlockService = new WorkBlockService(mockUnitOfWork.Object, mockRepositoryWorkBlock.Object, mockConfiguration.Object);
            
            var controller = new DriverDutiesController(DriverDutyService, WorkBlockService);

            // Act
            ActionResult<DriverDutyDto> actionResult = await controller.Create(cdriverDutyDto);

            var okResult = actionResult.Result as ObjectResult;

            var actualDriverDuty = okResult.Value as DriverDutyDto;

            _output.WriteLine(JsonConvert.SerializeObject(actionResult));

            _output.WriteLine(JsonConvert.SerializeObject(actualDriverDuty));
            _output.WriteLine(JsonConvert.SerializeObject(driverDutyDto));

            // Assert
            Assert.NotNull(actionResult);
            Assert.Equal(driverDutyDto.name, actualDriverDuty.name);
        }
    }
}
