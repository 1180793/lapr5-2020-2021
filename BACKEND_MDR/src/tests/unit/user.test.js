const generateAuthToken = require('../../model/entities/User').generateAuthToken;
const Roles = require('../../model/entities/Roles');
const jwt = require('jsonwebtoken');
const config = require('config');
const mongoose = require('mongoose');

describe('user.generateAuthToken', () => {
    it('should return a valid JWT', async () => {

        const user = {
            _id: new mongoose.Types.ObjectId().toHexString(),
            email: 'f_madureira@facebook.com',
            name: "Fernando Madureira",
            roles: [
                Roles.CLIENT,
                Roles.DATA_ADMINISTRATOR
            ]
        };
        const token = await generateAuthToken(user);
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));
        expect(decoded).toMatchObject(user);
    });
});