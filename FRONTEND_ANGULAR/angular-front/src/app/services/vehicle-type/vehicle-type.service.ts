import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { Observable } from 'rxjs';
import { AppSettings } from 'src/utils/AppSettings';
import { VehicleType } from 'src/app/model/model/VehicleType';
import { VehicleTypeMap } from 'src/app/mappers/VehicleTypeMap';

import Swal from 'sweetalert2'
@Injectable({
    providedIn: 'root'
})
export class VehicleTypeService {
    uri = AppSettings.API_ENDPOINT; // servers address

    mapper = new VehicleTypeMap();

    constructor(private http: HttpClient) { }


    async getVehicleTypes() {
        return this.http.get(`${this.uri}/vehicleTypes`);
    }


    async addVehicleType(name: string, autonomy: number, cost: number,
        averageSpeed: number, energySource: number, consumption: number,
        emissions: number) {

        let vehicleType: VehicleType = new VehicleType(name, autonomy, cost,
            averageSpeed, energySource, consumption, emissions);
        return this.addVehicleTypeDTO(vehicleType);

    }

    //add Vehicle Type using a Mapper
    async addVehicleTypeDTO(vehicleType: VehicleType) {
        return this.http.post<VehicleType>(`${this.uri}/vehicleTypes`, this.mapper.toDTO(vehicleType))
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Vehicle Type was successfully created!', 'success')
                },
                error => {
                    Swal.fire('Error!', error.error + '!', 'error')
                    // alert('Vehicle Type Invalid Input Fields!')
                    // alert(`Server error: ${error.status} - Details: ${error.error}`)
                }

            )
    }

}
