const mongoose = require('mongoose');
const PathSchema = require('../schemas/PathSchema').pathSchema;
const Path = mongoose.model('Path', PathSchema);

class PathRepository {

    constructor() { }

    /**
     * Retorna uma promessa com todos as Lines ordenados
     * @param {(err,item)} cb 
     */
    findAllByLine(lineID, cb) {
        if (lineID != null) {
            return Path
                .find({ line: lineID })
                .populate({
                    path: 'line',
                    select: 'name color -_id'
                })
                .populate({
                    path: 'pathNodeList',
                    populate: {
                        path: 'node',
                        select: 'isReliefPoint isDepot name shortName -_id'
                    }
                })
                .exec(cb);
        } else {
            return Path
                .find()
                .populate({
                    path: 'line',
                    select: 'name color -_id',
                })
                .populate({
                    path: 'pathNodeList',
                    populate: {
                        path: 'node',
                        select: 'isReliefPoint isDepot name shortName -_id'
                    }
                })
                .exec(cb);
        }
    }


    /**
     * Retorna uma promessa com todos os Paths
     */
    findAll() {
        return Path
                .find()
                .populate({
                    path: 'line',
                    select: 'name color -_id',
                })
                .populate({
                    path: 'pathNodeList',
                    populate: {
                        path: 'node',
                        select: 'isReliefPoint isDepot name shortName -_id'
                    }
                })
                .exec(cb);
    }

    /**
     * Encontra um Path por Key
     * @param {String} pathKey 
     * @param {(err, item)} cb 
     */
    findByKey(pathKey, cb) {
        mongoose.model('Path')
            .findOne({
                key: { $regex: new RegExp("^" + pathKey + "$", "i") }
            })
            .populate({
                path: 'line',
                select: 'name color -_id',
            })
            .populate({
                path: 'pathNodeList',
                populate: {
                    path: 'node',
                    select: 'isReliefPoint isDepot name shortName -_id'
                }
            })
            .exec(cb);
    }

    /**
     * Guarda um Path
     * @param {Path} path 
     * @param {(err, savedPath)} cb 
     */
    save(path, cb) {
        path.save(function (err, oi) {
            if (err) {
                cb(new Error(err));
            } else {
                cb(err, oi);
            }
        });
    }
}

module.exports = PathRepository;