import { LineTrip } from "../model/model/LineTrip";

export class LineTripMap {

    public toDTO(lineTrip: LineTrip) {
        return {
            isEmpty: lineTrip.IsEmpty,
            lineName: lineTrip.LineName,
            pathNameGo: lineTrip.PathNameGo,
            pathNameReturn: lineTrip.PathNameReturn,
            frequency: lineTrip.Frequency,
            numberOftrips: lineTrip.NumeberOfTrips,
            startTime: lineTrip.StartTime
        }
    }

    public static async toDomain(raw: any) {
        return new LineTrip(raw.isEmpty, raw.line, raw.pathNameGo, raw.pathNameReturn, raw.frequency, raw.numberOfTrips, raw.startTime);
    }

}
