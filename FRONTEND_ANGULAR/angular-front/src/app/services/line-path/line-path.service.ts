import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LinePathMap } from 'src/app/mappers/LinePathMap';
import { LinePath } from 'src/app/model/model/LinePath';
import { environment } from 'src/environments/environment';
import { AppSettings } from 'src/utils/AppSettings';


@Injectable({
    providedIn: 'root'
})
export class LinePathService {

    uri = environment;
    mapper = new LinePathMap();
    headers = AppSettings.HTTP_HEADERS;
    constructor(private http: HttpClient) {
    }

    getLinePaths() {
        return this.http.get(`${this.uri}/linePaths`, this.headers);
    }

    getLinePathsById(id: number) {
        return this.http.get(`${this.uri}/linePaths/${id}`, this.headers);
    }

    addLinePath(pathName: string, orientation: string) {
        const linePath = new LinePath(pathName, orientation);
        console.log(linePath);
        return this.addLinePathDto(linePath);
    }

    //addNode using Mapper
    addLinePathDto(linePath: LinePath) {
        return this.http.post<LinePath>(`${this.uri}linePath`, this.mapper.toDTO(linePath))
            .subscribe(
                value => {
                    alert('Line Path was sucessfully created ')
                },
                error => {
                    alert("Couldn't add Line Path")
                    alert(`Server error: ${error.status} - Details: ${error.error}`)
                    console.log(error);
                }

            )
    }



}
