
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { LineMap } from 'src/app/mappers/LineMap';
import { Line } from 'src/app/model/model/Line';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})
export class LineService {

    uri = AppSettings.API_ENDPOINT;
    mapper = new LineMap();


    constructor(private http: HttpClient) {
    }


    async getLines() {
        return this.http.get<Array<any>>(`${this.uri}/lines`);
    }

    async getLineByName(name: string) {
        return this.http.get<Line>(`${this.uri}/lines/${name}`);
    }

    async addLine(name: string, color: string, startNode: string, endNode: string, allowedVehicleTypes: string[], allowedDriverTypes: string[]) {


        const line: Line = new Line(name, color, startNode, endNode, allowedVehicleTypes, allowedDriverTypes)
        console.log(line);
        return this.addLineDto(line);
    }

    //addLine using Mapper
    addLineDto(line: Line) {
        return this.http.post<Line>(`${this.uri}/lines`, this.mapper.toDTO(line))
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Line was successfully created!', 'success')
                    // alert('Line was sucessfully created ')
                },
                error => {
                    Swal.fire('Error!', error.error + '!', 'error')
                    // alert("Couldn't add line")
                    // alert(`Server error: ${error.status} - Details: ${error.error}`)
                    // console.log(error);
                }

            )

    }
}
