using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.WorkBlocks;

namespace MDVApi.Domain.DriverDuties
{
    public class DriverDutyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDriverDutyRepository _repo;

        public DriverDutyService(IUnitOfWork unitOfWork, IDriverDutyRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<DriverDutyDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();
            
            List<DriverDutyDto> listDto = list.ConvertAll<DriverDutyDto>(driverDuty => 
                new DriverDutyDto(driverDuty.Id.AsGuid(), driverDuty.name, driverDuty.color));

            return listDto;
        }

        public async Task<DriverDutyDto> GetByIdAsync(DriverDutyId id)
        {
            var driverDuty = await this._repo.GetByIdAsync(id);
            
            if(driverDuty == null)
                return null;

            return new DriverDutyDto(driverDuty.Id.AsGuid(), driverDuty.name, driverDuty.color);
        } 

        public async Task<DriverDutyDto> AddAsync(CreatingDriverDutyDto dto, IList<WorkBlock> workBlocks)
        {

            var driverDuty = new DriverDuty(dto.name, dto.color);

            driverDuty.WorkBlocks = workBlocks;

            await this._repo.AddAsync(driverDuty);

            await this._unitOfWork.CommitAsync();

            return new DriverDutyDto(driverDuty.Id.AsGuid(), driverDuty.name, driverDuty.color);
        }

    }
}