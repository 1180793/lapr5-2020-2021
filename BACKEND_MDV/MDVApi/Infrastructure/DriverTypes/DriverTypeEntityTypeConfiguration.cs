using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Infrastructure.Shared;
using MDVApi.Domain.DriverTypes;

namespace MDVApi.Infrastructure.DriverTypes
{
    internal class DriverTypeEntityTypeConfiguration : IEntityTypeConfiguration<DriverType>
    {
        public void Configure(EntityTypeBuilder<DriverType> builder)
        {
            builder.ToTable("DriverTypes");
            builder.HasKey(b => b.Id);
        }
    }
}