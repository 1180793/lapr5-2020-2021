const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

const VehicleTypeController = require('../controllers/VehicleTypeController');

var vehicleTypeController = new VehicleTypeController();


router.get('/', [auth], vehicleTypeController.listAll);
router.get('/:id', [auth, dataAdmin], vehicleTypeController.details);
router.post('/', [auth, dataAdmin], vehicleTypeController.create);

module.exports = router;
