using Microsoft.EntityFrameworkCore;
using MDVApi.Domain.Vehicles;
using MDVApi.Domain.VehicleTypes;
using MDVApi.Domain.Drivers;
using MDVApi.Domain.DriverTypes;
using MDVApi.Domain.Trips;
using MDVApi.Domain.PassingTimes;

using MDVApi.Infrastructure.Vehicles;
using MDVApi.Infrastructure.VehicleTypes;
using MDVApi.Infrastructure.Drivers;
using MDVApi.Infrastructure.DriverTypes;
using MDVApi.Infrastructure.Trips;
using MDVApi.Infrastructure.PassingTimes;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.WorkBlocks;
using MDVApi.Infrastructure.VehicleDuties;
using MDVApi.Domain.DriverDuties;
using MDVApi.Infrastructure.DriverDuties;

namespace MDVApi.Infrastructure
{
    public class MDVApiDbContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<DriverType> DriverTypes { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<PassingTime> PassingTimes { get; set; }
        public DbSet<VehicleDuty> VehicleDuties { get; set; }
        public DbSet<WorkBlock> WorkBlocks { get; set; }
        public DbSet<DriverDuty> DriverDuties { get; set; }

        

        public MDVApiDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TripEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PassingTimeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleDutyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WorkBlockEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverDutyEntityTypeConfiguration());
        }

    }
}