//Angular Imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';

//Project Component Imports
import { AppComponent } from './app.component';
//import { MapTwoDComponent } from './view/components/map-two-d/map-two-d.component';

//Project Module Imports
import { ComponentsModule } from './components/components.module';
import { authComponents, AuthModule } from './auth/auth.module';
import { dataComponents, DataModule } from './data/data.module';
import { AppRoutingModule, routingComponents } from './app-routing.module';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';

import { viewComponents, ViewModule } from './view/view.module';

import { DriverTypeService } from './services/driver-type/driver-type.service';
import { VehicleTypeService } from './services/vehicle-type/vehicle-type.service';
import { NodeService } from './services/node/node.service';
import { LineService } from './services/line/line.service';
import { ImportNetworkService } from './services/import-network/import-network.service';
import { MapService } from './services/map/map.service';
import { PathService } from './services/path/path.service';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { HeaderInterceptor } from './auth/components/http-intercept/HeaderInterceptor';
import { httpInterceptorProviders } from './auth/components/http-intercept/HttpInterceptor';
import { AuthEvent } from './services/auth/AuthEvent';
import { VehicleService } from './services/vehicle/vehicle.service';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import Swal from 'sweetalert2';

import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { DriverService } from './services/driver/driver.service';
import { TripService } from './services/trip/trip.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ImportScheduleService } from './services/import-schedule/import-schedule.service';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    dataComponents,
    authComponents,
    viewComponents,


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    AuthModule,
    DataModule,
    ViewModule,
    ComponentsModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    ColorPickerModule,
    NoopAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    NgxMaterialTimepickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatInputModule,
    NgbModule,
    TimepickerModule,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [NodeService, VehicleTypeService, DriverTypeService, LineService, ImportNetworkService, MapService, PathService, httpInterceptorProviders,
    AuthEvent, VehicleService, DriverService, TripService, ImportScheduleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
