import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VehicleDutyMap } from 'src/app/mappers/VehicleDutyMap';
import { Trip } from 'src/app/model/model/Trip';
import { VehicleDuty } from 'src/app/model/model/VehicleDuty';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'
@Injectable({
    providedIn: 'root'
})
export class VehicleDutyService {
    uri = environment.uri_mdv;

    mapper = new VehicleDutyMap();

    constructor(private http: HttpClient) { }

    headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
    }

    requestOptions = {
        headers: new HttpHeaders(this.headerDict),
    };


    async getVehicleDuties() {
        return this.http.get<Array<any>>(`${this.uri}/vehicleDuties`);
    }

    async addVehicleDuty(name: string, color: string, date: string, trips: string[]) {
        const vehicleDuty: VehicleDuty = new VehicleDuty(name, color, date, trips);
        return this.addVehicleDutyDTO(vehicleDuty);
    }

    async addVehicleDutyDTO(vehicleDuty: VehicleDuty) {
        return this.http.post<VehicleDuty>(`${this.uri}/vehicleDuties`, this.mapper.toDTO(vehicleDuty), this.requestOptions)
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Vehicle Duty was successfully created!', 'success')
                },
                error => {
                    var err = error.error.errors;
                    if (err != null) {
                        var errMsg = err[Object.keys(err)[0]];
                        Swal.fire('Error!', errMsg + '!', 'error')
                    } else {
                        Swal.fire('Error!', error.error + '!', 'error')
                    }
                }
            )
    }
}
