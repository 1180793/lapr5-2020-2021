const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

const NodeController = require('../controllers/NodeController');

var nodeController = new NodeController();

router.get('/', [auth], nodeController.listAll);
router.get('/:id', [auth, dataAdmin], nodeController.details);
router.post('/', [auth, dataAdmin], nodeController.create);

module.exports = router;
