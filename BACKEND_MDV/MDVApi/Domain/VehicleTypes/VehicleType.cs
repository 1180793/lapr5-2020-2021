using System;
using System.Collections.Generic;
using MDVApi.Domain.Vehicles;
using MDVApi.Domain.Shared;

namespace MDVApi.Domain.VehicleTypes
{
    public class VehicleType : Entity<VehicleTypeId>
    {
        public string name { get; set; }
        public IList<Vehicle> Vehicles {get; set;}

        public VehicleType(string name){

            this.Id = new VehicleTypeId(Guid.NewGuid()); 
            this.name = name;
        } 
    }

}