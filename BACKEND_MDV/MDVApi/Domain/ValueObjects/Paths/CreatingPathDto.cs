using System.Collections.Generic;
using MDVApi.Domain.ValueObjects.Lines;
using MDVApi.Domain.ValueObjects.PathNodes;

namespace MDVApi.Domain.ValueObjects.Paths
{
    public class CreatingPathDto
    {
        public string _key { get; set; }
        public LineSimplified _line { get; set; }
        public bool _isEmpty { get; set; }
        public IList<CreatingPathNodeDto> _pathNodeList { get; set; }

    }

}