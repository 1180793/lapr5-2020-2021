import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Line } from 'src/app/model/model/Line';
import { Trip } from 'src/app/model/model/Trip';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LineService } from 'src/app/services/line/line.service';
import { PathService } from 'src/app/services/path/path.service';
import { TripService } from 'src/app/services/trip/trip.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'
@Component({
    selector: 'app-trip-show',
    templateUrl: './trip-show.component.html',
    styleUrls: ['./trip-show.component.css'],
})
export class TripShowComponent implements OnInit, AfterViewInit {
    //services
    tripService = new TripService(this.http);
    lineService = new LineService(this.http);
    pathService = new PathService(this.http);
    //tripList
    tripList = new Array<Trip>();
    tripListTable = new Array<Trip>();

    lineList = new Array<Line>();

    pathList = new Array<any>();

    //for the table
    displayedColumns: string[] = ['line', 'path', 'orientation', 'isEmpty'];

    dataSource: any;

    form: FormGroup;

    lineNamesList: any;

    reactiveForm() {
        this.form = this.fb.group({
            lineNames: [[]],
        });
    }

    constructor(public http: HttpClient, public fb: FormBuilder, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasPermission([AppSettings.DATA_ADMINISTRATOR, AppSettings.GESTOR, AppSettings.CLIENT]);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }


    async ngOnInit() {
        // this.ngAfterViewInit();
        try {
            this.populateTrips();
            this.populateLines();
            this.populatePaths();

            this.reactiveForm();

            setTimeout(() => {
                this.initializeDataSource();
            }, 1000);


        } catch (err) {
            console.log("🚀 ~ file: trip-show.component.ts ~ line 81 ~ TripShowComponent ~ ngOnInit ~ err", err)
        }
    }

    initializeDataSource() {
        this.dataSource = new MatTableDataSource(this.tripList);
        this.dataSource.filterPredicate = function (
            data: any,
            filter: string
        ): boolean {
            var ret = false;
            if (filter == '[]') {
                ret = true;
            } else {
                ret = filter.includes(data.lineName);
            }
            return ret;
        };
        this.dataSource.paginator = this.paginator;
    }

    async populateLines() {
        (await this.lineService.getLines()).subscribe(
            (value) => {
                this.lineList = value;
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')

            }
        );
    }

    async populateTrips() {
        (await this.tripService.getTrips()).subscribe(
            (value) => {
                this.tripList = value;
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')

            }
        );
    }

    async populatePaths() {
        (await this.pathService.getPaths()).subscribe(
            (value) => {
                this.pathList = value;
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')
            }
        );
    }

    getLinesToSearch() {
        return this.form.get('lineNames').value;
    }

    onSearchLines() {
        var linesToSearch = this.getLinesToSearch();
        this.dataSource.filter = JSON.stringify(linesToSearch);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    checkIfEmpty(isEmpty: boolean) {
        if (isEmpty) {
            return 'Empty'
        }
        else {
            return 'Not Empty'
        }
    }

    getPathNodeList(path: any) {
        var finalS = new String();
        var pathFound = null;
        this.pathList.forEach((element => {
            if (element._key == path) {
                pathFound = element;
            }
        }));
        pathFound._pathNodeList.forEach((element: any) => {
            finalS = finalS.concat(
                element.node.shortName.toString() +
                '(' +
                element.duration +
                ', ' +
                element.distance +
                ') ',
                '--> '
            );
        });

        return finalS.trim().slice(0, -3);
    }
}
