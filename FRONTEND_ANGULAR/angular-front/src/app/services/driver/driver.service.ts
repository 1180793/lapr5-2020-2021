import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DriverMap } from 'src/app/mappers/DriverMap';
import { Driver } from 'src/app/model/model/Driver';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})
export class DriverService {
    uri = environment.uri_mdv;
    mapper = new DriverMap();
    constructor(private http: HttpClient) { }

    headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
    }

    requestOptions = {
        headers: new HttpHeaders(this.headerDict),
    };


    async getDrivers() {
        return this.http.get<Array<any>>(`${this.uri}/drivers`);
    }

    async addDriver(driverName: string, birthDate: string, driversLicenseNumber: string, driversLicenseDueDate: string, driverTypes: string[]) {
        const driver: Driver = new Driver(driverName, birthDate, driversLicenseNumber, driversLicenseDueDate, driverTypes);
        return this.addDriverDto(driver);
    }

    async addDriverDto(driver: Driver) {


        console.log(this.mapper.toDTO(driver));

        return this.http.post<Driver>(`${this.uri}/drivers`, this.mapper.toDTO(driver), this.requestOptions)
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Vehicle was successfully created!', 'success')
                },
                error => {
                    var err = error.error.errors;
                    if (err != null) {
                        var errMsg = err[Object.keys(err)[0]];
                        Swal.fire('Error!', errMsg + '!', 'error')
                    } else {
                        Swal.fire('Error!', error.error + '!', 'error')
                    }
                }

            )
    }
}
