using System;
using MDVApi.Domain.Shared;
using System.Collections.Generic;
using MDVApi.Domain.Drivers;

namespace MDVApi.Domain.DriverTypes
{
    public class DriverType : Entity<DriverTypeId>
    {
        public string name { get; set; }
        public IList<Driver> Drivers { get; set; } = new List<Driver>();

        public DriverType(string name){

            this.Id = new DriverTypeId(Guid.NewGuid());
            this.name = name;   
        } 
    }

}