using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using MDVApi.Domain.Shared;
using MDVApi.Domain.Authentication;
using MDVApi.Domain.Vehicles;
using MDVApi.Domain.VehicleTypes;
using MDVApi.Domain.Drivers;
using MDVApi.Domain.DriverTypes;
using MDVApi.Domain.PassingTimes;
using MDVApi.Domain.Trips;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.DriverDuties;
using MDVApi.Domain.WorkBlocks;

using MDVApi.Infrastructure;
using MDVApi.Infrastructure.Shared;
using MDVApi.Infrastructure.Vehicles;
using MDVApi.Infrastructure.VehicleTypes;
using MDVApi.Infrastructure.Drivers;
using MDVApi.Infrastructure.DriverTypes;
using MDVApi.Infrastructure.Trips;
using MDVApi.Infrastructure.PassingTimes;
using MDVApi.Infrastructure.VehicleDuties;
using MDVApi.Infrastructure.DriverDuties;

using System.Web.Http;
using System.Web.Http.Cors;


namespace MDVApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConfigurationS = configuration;
        }

        public IConfiguration Configuration { get; }
        public static IConfiguration ConfigurationS { get; set;}

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<MDVApiDbContext>(opt =>
                //opt.UseInMemoryDatabase("MDVApiDB")
                opt.UseSqlServer(Configuration.GetConnectionString("MDVDatabase"))
                .ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());

            ConfigureMyServices(services);
            

            services.AddControllers().AddNewtonsoftJson();
        }

        public static void Register(HttpConfiguration config)
        {
            var corsAttr = new EnableCorsAttribute(ConfigurationS.GetConnectionString("ANGULAR"), "*", "*");
            config.EnableCors(corsAttr);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                );

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureMyServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork,UnitOfWork>();

            services.AddTransient<IVehicleRepository, VehicleRepository>();
            services.AddTransient<IVehicleTypeRepository, VehicleTypeRepository>();
            services.AddTransient<ITripRepository, TripRepository>();
            services.AddTransient<IPassingTimeRepository, PassingTimeRepository>();
            services.AddTransient<IDriverRepository, DriverRepository>();
            services.AddTransient<IDriverTypeRepository, DriverTypeRepository>();
            services.AddTransient<IVehicleDutyRepository, VehicleDutyRepository>();
            services.AddTransient<IWorkBlockRepository, WorkBlockRepository>();
            services.AddTransient<IDriverDutyRepository, DriverDutyRepository>();
            
            services.AddTransient<VehicleService>();
            services.AddTransient<VehicleTypeService>();
            services.AddTransient<TripService>();
            services.AddTransient<AuthenticationService>();
            services.AddTransient<PassingTimeService>();
            services.AddTransient<DriverService>();
            services.AddTransient<DriverTypeService>();
            services.AddTransient<VehicleDutyService>();
            services.AddTransient<DriverDutyService>();
            services.AddTransient<WorkBlockService>();

        }
    }
}
