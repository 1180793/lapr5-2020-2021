import { TestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { of } from 'rxjs/observable/of';

import { NodeService } from './node.service';
import { Nodes } from 'src/app/model/model/Nodes';

describe('NodeService', () => {
  let service: NodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});



describe('Node Service: getNodes', () => {
  let nodeService: NodeService;
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['get']);
  httpClientSpy.get.and.returnValue(of([new Nodes('NodeServiceTes', 'test', 12, 12, true, true)]));
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [nodeService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }]
    });
  });


  beforeEach(inject([nodeService], (service: NodeService) => {
    nodeService = service;
  }));

  it('should be defined', () => {
    expect(nodeService).toBeTruthy();
  });

  it('should get todoList using http request', () => {
    expect(httpClientSpy.get).toHaveBeenCalled();
    expect(nodeService.nodeList.length).toBe(1);
  });
});
