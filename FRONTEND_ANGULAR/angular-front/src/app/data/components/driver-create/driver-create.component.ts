import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DriverTypeService } from 'src/app/services/driver-type/driver-type.service';
import { DriverService } from 'src/app/services/driver/driver.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-driver-create',
    templateUrl: './driver-create.component.html',
    styleUrls: ['./driver-create.component.styl']
})
export class DriverCreateComponent implements OnInit {

    //services 
    driverTypeService = new DriverTypeService(this.http);
    driverService = new DriverService(this.http);


    //Forms
    createDriverForm!: FormGroup

    //Variables 
    name: string;
    birthDate: Date;
    driversLicenseNumber: string;
    driversLicenseDueDate: Date;
    driverTypes: string[];

    //Lists for Population
    driverTypeList: any;


    constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.populateDriverTypes();
        this.reactiveForm();
    }

    reactiveForm() {
        this.createDriverForm = this.fb.group({
            name: ['',],
            birthDate: ['',],
            driversLicenseNumber: ['',],
            driversLicenseDueDate: ['',],
            driverTypes: [[],],

        })
    }

    async populateDriverTypes() {
        (await this.driverTypeService.getDriverTypes()).subscribe(
            value => {

                this.driverTypeList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }

    getName() {
        if (this.createDriverForm.get('name')?.value == null || this.createDriverForm.get('name')?.value == undefined) {
            return '';
        } else {
            return this.createDriverForm.get('name')?.value;
        }

    }
    getBirthDate() {
        var temp = this.createDriverForm.get('birthDate')?.value;
        console.log('TEMP ' + temp)

        if (temp == undefined || temp == null || temp.length == 0) {

            return "";
        }

        return new DatePipe('en').transform(temp, 'yyyy-MM-dd');
    }
    getDriversLicenseNumber() {
        return this.createDriverForm.get('driversLicenseNumber')?.value;
    }

    getDriversLicenseDueDate() {
        var temp = this.createDriverForm.get('driversLicenseDueDate')?.value;

        if (temp === undefined || temp === null) {
            return "";
        }

        return new DatePipe('en').transform(temp, 'yyyy-MM-dd');

    }

    getDriverTypes(): string[] {
        var vList = new Array<string>();
        var list = this.createDriverForm.get('driverTypes')?.value;

        if (list == undefined) {
            return [];
        }

        for (let i = 0; i < list['length']; i++) {
            vList.push(list[i]);
        }

        return vList;
    }
    public hasError = (controlName: string, errorName: string) => {
        return this.createDriverForm.controls[controlName].hasError(errorName);
    }


    onCreateDriver() {
        console.log('name: ' + this.getName());
        console.log('birth date:' + this.getBirthDate());
        console.log('license number: ' + this.getDriversLicenseNumber());
        console.log('due date: ' + this.getDriversLicenseDueDate());
        console.log('driverTypes: ' + this.getDriverTypes());

        if (this.createDriverForm.valid && this.checkValues()) {
            //this.checkValues()
            this.driverService.addDriver(this.getName(), this.getBirthDate(), this.getDriversLicenseNumber(), this.getDriversLicenseDueDate(), this.getDriverTypes());
            this.createDriverForm.reset();

        }
    }

    checkValues() {
        if (this.getName() === undefined || this.getName() == '' || this.getName() === null) {
            Swal.fire('Error!', 'Name can\'t be empty!', 'error')
            return false;
        } if (this.getBirthDate() === undefined || this.getBirthDate() === null) {
            Swal.fire('Error!', 'Birth Date can\'t be empty!', 'error')
            return false
        }
        if (this.getDriversLicenseNumber() == undefined || this.getDriversLicenseNumber() == null) {
            Swal.fire('Error!', 'Drivers License Number can\'t be empty!', 'error')
            return false
        }
        if (this.getDriversLicenseDueDate() == undefined || this.getDriversLicenseDueDate() === null) {
            Swal.fire('Error!', 'Drivers License Due Date can\'t be empty!', 'error')
            return false
        }
        if (this.getDriverTypes().length === 0 || this.getDriverTypes() === null) {
            Swal.fire('Error!', 'You must choose at least one driver type!', 'error')
            return false
        } else {
            return true;
        }
    }

}
