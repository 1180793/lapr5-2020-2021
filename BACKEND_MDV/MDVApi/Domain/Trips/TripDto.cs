using System;

namespace MDVApi.Domain.Trips
{
    public class TripDto
    {
        public Guid Id { get; set; }
        public Boolean isEmpty { get; set; }
        public string orientation { get; set; } 
        public string lineName { get; set; } 
        public string pathName { get; set; } 
        public Boolean isGenerated { get; set; }

        public TripDto(Guid Id, Boolean isEmpty, string orientation, string lineName, string pathName, Boolean isGenerated){

                this.Id = Id;
                this.isEmpty = isEmpty;
                this.orientation = orientation;
                this.lineName = lineName;
                this.pathName = pathName;
                this.isGenerated = isGenerated;    
        }
    }

}