const mongoose = require('mongoose');
const Joi = require('joi');

// Persistence Schema
const pathNodeSchema = new mongoose.Schema({
    node: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Node'
    },
    duration: {
        type: Number,
        required: false,
        min: 0,
        default: 0
    },
    distance: {
        type: Number,
        required: false,
        min: 0,
        default: 0
    }
}, { _id: false });

// Joi Schema
const joiPathNodeSchema = Joi.object({
    node: Joi.string()
        .required(),
    duration: Joi.number()
        .default(0),
    distance: Joi.number()
        .default(0)
});

// Validate
function validatePathNode(pathNode) {
    return joiPathNodeSchema.validate(pathNode);
}

//PathNodeSchema.plugin(mongooseValidator);
exports.joiPathNodeSchema = joiPathNodeSchema;
exports.pathNodeSchema = pathNodeSchema;
exports.validate = validatePathNode;