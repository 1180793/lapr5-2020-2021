using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Threading.Tasks;
using MDVApi.Domain.Drivers;
using MDVApi.Domain.Trips;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDVApi.Infrastructure.Trips
{
    public class TripRepository : BaseRepository<Trip, TripId>, ITripRepository
    {

        private readonly DbSet<Trip> _objs;

        public TripRepository(MDVApiDbContext context) : base(context.Trips)
        {
            _objs = context.Trips ?? throw new ArgumentNullException(nameof(context.Trips));
        }

        public async Task<List<Trip>> GetByLineAsync(string line){
            
            return await _objs.Where(x => line.Equals(x.lineName)).ToListAsync();
        }

        public IList<Trip> getTripsByVehicleDuty(VehicleDutyId vehicleDutyId)
        {

            IList<Trip> trips = new List<Trip>();

            IQueryable<Trip> results = _objs.FromSqlRaw<Trip>
                    (@"SELECT *
                        FROM Trips
                        WHERE Id IN (Select TripsId 
                                                from TripVehicleDuty 
                                                where vehicleDutiesId = {0})", vehicleDutyId
                    );

            foreach (Trip trip in results)
            {
                trips.Add(trip);
            }

            return trips;
        }
    }
}