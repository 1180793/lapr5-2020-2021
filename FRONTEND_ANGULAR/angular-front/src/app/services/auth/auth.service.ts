import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { map, catchError, delay, tap } from "rxjs/operators";
import { of, BehaviorSubject, throwError } from "rxjs";
import { User } from "src/app/model/model/user.model";
import { environment } from 'src/environments/environment';
import { LocalStoreService } from "./local-store.service";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    uri = environment.uri; // servers address
    token: String;
    isAuthenticated: Boolean;
    user: User = null;
    signingIn: Boolean;
    JWT_TOKEN = "JWT_TOKEN";
    APP_USER = "USER";
    user$ = (new BehaviorSubject<User>(this.user));

    constructor(
        private ls: LocalStoreService,
        private http: HttpClient,
        private router: Router
    ) { }

    public signin(email: String, password: String) {
        // SENDS SIGNIN REQUEST TO SERVER
        this.signingIn = true;
        return this.http.post(`${this.uri}/auth`, { 'email': email, 'password': password })
            .pipe(
                map((res: any) => {
                    this.setUserAndToken(res.token, res.user, !!res);
                    this.signingIn = false;
                    return res;
                }),
                catchError((error) => {
                    return throwError(error);
                })
            );
    }

    public register(name: String, email: String, password: String) {
        // SENDS REGISTER REQUEST TO SERVER
        this.signingIn = true;
        return this.http.post(`${this.uri}/registerClient`, { 'name': name, 'email': email, 'password': password })
            .pipe(
                map((res: any) => {
                    this.setUserAndToken(res.token, res.user, !!res);
                    this.signingIn = false;
                    return res;
                }),
                catchError((error) => {
                    return throwError(error);
                })
            );
    }

    public deleteUser() {
        // SENDS DELETE REQUEST TO SERVER
        return this.http.delete(`${this.uri}/users/me`, {})
            .pipe(
                map((res: any) => {
                    this.signout();
                    return res;
                }),
                catchError((error) => {
                    return throwError(error);
                })
            );
    }

    /*
      checkTokenIsValid is called inside constructor of
      shared/components/layouts/admin-layout/admin-layout.component.ts
    */
    public checkTokenIsValid() {
        /*
          The following code get user data and jwt token is assigned to
          Request header using token.interceptor
          This checks if the existing token is valid when app is reloaded
        */
        return this.http.get(`${this.uri}/api/users/profile`)
            .pipe(
                map((profile: User) => {
                    this.setUserAndToken(this.getJwtToken(), profile, true);
                    return profile;
                }),
                catchError((error) => {
                    return of(error);
                })
            );
    }

    public signout() {
        this.setUserAndToken(null, null, false);
        this.router.navigateByUrl("auth/login");
    }

    isLoggedIn(): Boolean {
        return !!this.getJwtToken();
    }

    getJwtToken() {
        return this.ls.getItem(this.JWT_TOKEN);
    }
    getUser() {
        return this.ls.getItem(this.APP_USER);
    }

    hasRole(role: String) {
        try {
            return this.getUser().roles.includes(role);
        } catch (err) {
            return false;
        }
    }

    hasPermission(roleList: string[]) {
        try {
            var ret = false;
            roleList.forEach((role) => {
                if (this.hasRole(role)) {
                    ret = true;
                }
            })
            return ret;
        } catch (err) {
            return false;
        }
    }

    setUserAndToken(token: String, user: User, isAuthenticated: Boolean) {
        this.isAuthenticated = isAuthenticated;
        this.token = token;
        this.user = user;
        this.user$.next(user);
        this.ls.setItem(this.JWT_TOKEN, token);
        this.ls.setItem(this.APP_USER, user);
    }
}
