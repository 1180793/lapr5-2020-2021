using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using MDVApi.Domain.DriverDuties;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Trips;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Domain.WorkBlocks
{
    public class WorkBlock : Entity<WorkBlockId>, IAggregateRoot
    {
        private static readonly Boolean IS_ACTIVE_BY_DEFAULT = true;

        public long startTime { get; set; }
        public long endTime { get; set; }
        public string startNode { get; set; } 
        public string endNode { get; set; }
        public Boolean isCrewTravelTime { get; set; }
        public Boolean isActive { get; set; }
        public VehicleDutyId VehicleDutyId { get; set; }
        [ForeignKey("VehicleDutyId")]
        public VehicleDuty VehicleDuty { get; set; }
        public DriverDutyId DriverDutyId { get; set; }
        [ForeignKey("DriverDutyId")]
        public DriverDuty DriverDuty { get; set; }

        public WorkBlock(long startTime, long endTime, string startNode, string endNode, Boolean isCrewTravelTime){

            this.Id = new WorkBlockId(Guid.NewGuid());
            this.startTime = startTime;
            this.endTime = endTime;
            this.startNode = startNode;
            this.endNode = endNode;
            this.isCrewTravelTime = isCrewTravelTime;
            this.isActive = IS_ACTIVE_BY_DEFAULT;
        }
        
    }
}