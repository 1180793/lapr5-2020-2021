using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Domain.WorkBlocks;

namespace MDVApi.Infrastructure.Vehicles
{
    internal class WorkBlockEntityTypeConfiguration : IEntityTypeConfiguration<WorkBlock>
    {
        public void Configure(EntityTypeBuilder<WorkBlock> builder)
        {
            builder.HasKey(b => b.Id);
        }
    }
}