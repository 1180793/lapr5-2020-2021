using System;
using MDVApi.Domain.Trips;

namespace MDVApi.Domain.PassingTimes
{
    public class PassingTimeDto
    {
        public Guid Id { get; set; }
        public long time { get; set; }
        public string nodeName { get; set; }
        public Boolean isUsed { get; set; }
        public Boolean isReliefPoint { get; set; }
        public TripId TripId { get; set; }

        public PassingTimeDto(Guid Id, long time, string nodeName, Boolean isUsed, Boolean isReliefPoint, TripId TripId){

                this.Id = Id;
                this.time = time;
                this.nodeName = nodeName;
                this.isUsed = isUsed;
                this.isReliefPoint = isReliefPoint;
                this.TripId = TripId;
        } 
    }

}