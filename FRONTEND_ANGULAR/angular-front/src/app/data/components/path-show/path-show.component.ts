import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Line } from 'src/app/model/model/Line';

import { Path } from 'src/app/model/model/Path';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LineService } from 'src/app/services/line/line.service';
import { PathService } from 'src/app/services/path/path.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-path-show',
    templateUrl: './path-show.component.html',
    styleUrls: ['./path-show.component.css'],
})
export class PathShowComponent implements OnInit {
    //services
    pathService = new PathService(this.http);
    lineService = new LineService(this.http);
    //pathList
    pathList = new Array<Path>();
    pathListTable = new Array<Path>();

    lineList = new Array<Line>();

    //for the table
    displayedColumns: string[] = ['line', 'pathNodeList'];

    dataSource: any;

    form: FormGroup;

    lineNamesList: any;

    reactiveForm() {
        this.form = this.fb.group({
            lineNames: [[]],
        });
    }

    constructor(public http: HttpClient, public fb: FormBuilder, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.populateLines();
        this.populatePaths();

        this.reactiveForm();

        setTimeout(() => {
            this.initializeDataSource();
        }, 1000);
    }

    initializeDataSource() {
        this.dataSource = new MatTableDataSource(this.pathList);
        this.dataSource.filterPredicate = function (
            data: any,
            filter: string
        ): boolean {
            var ret = false;
            if (filter == '[]') {
                ret = true;
            } else {
                ret = filter.includes(data._line.name);
            }
            return ret;
        };
    }

    async populateLines() {
        (await this.lineService.getLines()).subscribe(
            (value) => {
                this.lineList = value;
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }

    async populatePaths() {
        (await this.pathService.getPaths()).subscribe(
            (value) => {
                this.pathList = value;
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }

    getLinesToSearch() {
        return this.form.get('lineNames').value;
    }

    onSearchLines() {
        var linesToSearch = this.getLinesToSearch();
        this.dataSource.filter = JSON.stringify(linesToSearch);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    getPathNodeList(path: any) {
        var finalS = new String();
        path._pathNodeList.forEach((element: any) => {
            finalS = finalS.concat(
                element.node.shortName.toString() +
                '(' +
                element.duration +
                ',' +
                element.distance +
                ') ',
                '--> '
            );
        });

        return finalS.trim().slice(0, -3);
    }
}
