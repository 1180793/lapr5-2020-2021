const mongoose = require('mongoose');
const Joi = require('joi');

// Persistence Schema
const driverTypeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        maxlength: 250
    },
});

// Joi Schema
const joiDriverTypeSchema = Joi.object({
    name: Joi.string()
        .required()
        .max(250),
});

// Validate
function validateDriverType(driverType) {
    return joiDriverTypeSchema.validate(driverType);
}

exports.joiDriverTypeSchema = joiDriverTypeSchema;
exports.driverTypeSchema = driverTypeSchema;
exports.validate = validateDriverType;