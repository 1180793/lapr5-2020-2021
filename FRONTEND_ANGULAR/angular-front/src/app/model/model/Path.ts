import { Line } from './Line';
import { PathNode } from './PathNode';

export class Path {

    key: string;
    line: any;
    orientation: string;
    isEmpty: Boolean;
    pathNodeList: PathNode[];
    lineColor: String;

    constructor(key: string, line: any, orientation: string, isEmpty: Boolean, list: PathNode[]) {
        this.key = key;
        this.line = line;
        this.orientation = orientation;
        this.isEmpty = isEmpty;
        this.pathNodeList = list;
    }

    public getKey() {
        return this.key;
    }

    public getLine() {
        return this.line;
    }

    public getIsEmpty() {
        return this.isEmpty;
    }

    public getPathNodeList() {
        return this.pathNodeList;
    }

    public getOrientation() {
        return this.orientation;
    }

    public setLineColor(color: String) {
        this.lineColor = color;
    }


}
