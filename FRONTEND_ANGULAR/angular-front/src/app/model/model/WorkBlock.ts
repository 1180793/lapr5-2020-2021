export class WorkBlock {
    wbIsCrewTravelTime: boolean;
    wbVehicleDutyID: string;
    wbDuration: number;
    wbNumberOfBlocks: number

    constructor(isCrewTravelTime: boolean, vehicleDutyID: string, duration: number, numberOfBlocks: number) {
        this.wbIsCrewTravelTime = isCrewTravelTime;
        this.wbVehicleDutyID = vehicleDutyID;
        this.wbDuration = duration;
        this.wbNumberOfBlocks = numberOfBlocks;
    }

    get isCrewTravelTime(): boolean {
        return this.wbIsCrewTravelTime;
    }


    get vehicleDutyID(): string {
        return this.wbVehicleDutyID;
    }

    get duration(): number {
        return this.wbDuration;
    }

    get numberOfBlocks(): number {
        return this.wbNumberOfBlocks;
    }
}
