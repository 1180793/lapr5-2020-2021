import { Component, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { ImportNetworkService } from 'src/app/services/import-network/import-network.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-import-network',
    templateUrl: './import-network.component.html',
    styleUrls: ['./import-network.component.css'],
})
export class ImportNetworkComponent implements OnInit {
    createImportNetworkForm!: FormGroup;
    selectedFiles: FileList;
    currentFile: File;
    message = '';

    constructor(
        public fb: FormBuilder,
        private importNetworkService: ImportNetworkService, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.reactiveForm();
    }

    //Reactive Form
    reactiveForm() {
        this.createImportNetworkForm = this.fb.group({
            glxFile: ['', [Validators.required]],
        });
    }

    selectFile(event: any): void {
        this.selectedFiles = event.target.files;
    }

    upload(): void {
        if (this.selectedFiles == null || this.selectedFiles == undefined) {
            Swal.fire('Error!', 'You must choose a file to upload!', 'error')
            return;
        }

        this.currentFile = this.selectedFiles[0];

        this.importNetworkService.upload(this.currentFile).subscribe(
            (event) => {
                if (event instanceof HttpResponse) {
                    this.message = event.body.message;
                    Swal.fire('Success!', this.message + '!', 'success')

                }
            },
            (err) => {

                this.message = 'Could not upload the file!';
                this.currentFile = undefined;
                Swal.fire('Error!', 'Invalid Network File! ' + err.error.message, 'error')

            }
        );
        this.selectedFiles = undefined;
    }
}
