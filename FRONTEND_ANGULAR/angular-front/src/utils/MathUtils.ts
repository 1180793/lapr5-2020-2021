export class MathUtils {


    public static calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
        let p = 0.017453292519943295;    // Math.PI / 180
        let c = Math.cos;
        let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
        let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
        return dis;
    }

    public static calculateTime(avgSpeed: number, distance: number) {
        return this.precisionRound(distance / avgSpeed, 2);
    }

    //Rounds the input number with the input precision
    //Ex:precisionRound(3.13,1)=3.1
    public static precisionRound(number: number, precision: number) {
        if (precision < 0) {
            let factor = Math.pow(10, precision);
            return Math.round(number * factor) / factor;
        }
        else
            return +(Math.round(Number(number + "e+" + precision)) +
                "e-" + precision);
    }
}