import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Coordinate } from './model/Coordinate';
import { DriverType } from './model/DriverType';
import { VehicleType } from './model/VehicleType';
import { Line } from './model/Line';
import { LinePath } from './model/LinePath';
import { Nodes } from './model/Nodes';
import { PathNode } from './model/PathNode';
import { User } from './model/User';
import { Path } from './model/Path';
import { Trip } from './model/Trip';
import { LineTrip } from './model/LineTrip';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ModelModule { }
export const modelComponents = [
  Path, Coordinate, DriverType, Line, LinePath, Nodes, PathNode, User, VehicleType, Trip, LineTrip
]
