using MDVApi.Domain.ValueObjects.Nodes;

namespace MDVApi.Domain.ValueObjects.PathNodes
{
    public class PathNode
    {
        public Node node { get; set; }
        public int duration { get; set; }
        public int distance { get; set; }
    
        public PathNode(/*Node node*/ int duration, int distance){

           //this.node = node;
            this.duration = duration;
            this.distance = distance;
            
        } 
    }
    
}