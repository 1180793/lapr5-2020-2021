:- dynamic melhor_sol_ntrocas/2.
:-dynamic nSol/1.

plan_mud_mot1(Noi,Nof,LCaminho_menostrocas):-
asserta(nSol(0)),
get_time(Ti),
(melhor_caminho(Noi,Nof);true),
retract(melhor_sol_ntrocas(LCaminho_menostrocas,_)),
retract(nSol(NS)),
get_time(Tf),
TSol is Tf-Ti,
write('Tempo de geracao da solucao:'),write(TSol),nl,
write('Numero de Solucoes:'),write(NS),nl.

melhor_caminho(Noi,Nof):-
asserta(melhor_sol_ntrocas(_,10000)),
caminho(Noi,Nof,LCaminho),
atualiza_melhor(LCaminho),
fail.

atualiza_melhor(LCaminho):-
nSol(NS),
retract(nSol(_)),
NS1 is NS + 1,
asserta(nSol(NS1)),
melhor_sol_ntrocas(_,N),
length(LCaminho,C),
C<N,retract(melhor_sol_ntrocas(_,_)),
asserta(melhor_sol_ntrocas(LCaminho,C)).


caminho(Noi,Nof,LCaminho):-caminho(Noi,Nof,[],LCaminho).
caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).
caminho(No1,Nof,Lusadas,Lfinal):-
liga(No1,No2,N),
\+member((_,_,N),Lusadas),
\+member((No2,_,_),Lusadas),
\+member((_,No2,_),Lusadas),
caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).


:-dynamic liga/3.
gera_ligacoes:- retractall(liga(_,_,_)),
findall(_,
((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),
(no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
No1\==No2,
linha(_,N,LNos,_,_),
ordem_membros(No1,No2,LNos),
assertz(liga(No1,No2,N))
),_).
ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).
