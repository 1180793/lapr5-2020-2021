import { VehicleDuty } from '../model/model/VehicleDuty';

export class VehicleDutyMap {
    public toDTO(vehicleDuty: VehicleDuty) {
        return {
            name: vehicleDuty.name,
            color: vehicleDuty.color,
            date: vehicleDuty.date,
            tripIds: vehicleDuty.trips,
        };
    }

    public static async toDomain(raw: any) {
        return new VehicleDuty(raw.name, raw.color, raw.date, raw.trips);
    }
}
