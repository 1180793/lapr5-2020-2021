import { Vehicle } from "../model/model/Vehicle";

export class VehicleMap {

    public toDTO(vehicle: Vehicle) {
        return {
            matricula: vehicle.LicensePlate,
            vin: vehicle.Vin,
            vehicleTypeName: vehicle.VehicleType,
            startDate: vehicle.StartDate

        }
    }

    public static async toDomain(raw: any) {
        return new Vehicle(raw.licensePlate, raw.vin, raw.vehicleType);
    }

}