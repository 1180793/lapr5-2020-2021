import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DriverTypeMap } from 'src/app/mappers/DriverTypeMap';
import { DriverType } from 'src/app/model/model/DriverType';
import { AppSettings } from 'src/utils/AppSettings';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class DriverTypeService {

  uri = environment.uri; // servers address
  mapper = new DriverTypeMap();
  headers = AppSettings.HTTP_HEADERS;

  constructor(private http: HttpClient) {
  }

  async getDriverTypes() {
    return this.http.get(`${this.uri}/driverTypes`, this.headers);
  }

  async getDriverTypesById(id: String) {
    return this.http.get(`${this.uri}/driverTypes/${id}`, this.headers);
  }

  async addDriverType(name: String) {
    let driverType: DriverType = new DriverType(name);

    return this.addDriverTypeDTO(driverType);
  }

  async addDriverTypeDTO(driverType: DriverType) {
    return this.http.post<DriverType>(`${this.uri}/driverTypes`, this.mapper.toDTO(driverType), this.headers)
      .subscribe(
        value => {
          Swal.fire('Success!', 'Driver Type was created!', 'success')
        },
        error => {
          Swal.fire('Error!', 'Could not add the Driver Type!', 'error')

        }

      )
  }

}
