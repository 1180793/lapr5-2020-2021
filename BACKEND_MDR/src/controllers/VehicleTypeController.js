const sendJsonResponse = require('../middleware/sendJsonResponse');
const VehicleTypeService = require('../services/VehicleTypeService');

class VehicleTypeController {

    listAll(req, res) {
        let vehicleTypeService = new VehicleTypeService();
        vehicleTypeService.findAll().then((vehicleTypes) => {
            if (Array.isArray(vehicleTypes) && vehicleTypes.length) {
                sendJsonResponse(res, 200, vehicleTypes);
            } else { 
                sendJsonResponse(res, 404, "Vehicle Types not found!");
            }
        });
    }

    details(req, res) {
        let vehicleTypeService = new VehicleTypeService();
        vehicleTypeService.findByName(req.params.id).then((vehicleType) => {
            if (vehicleType == null || Object.keys(vehicleType).length === 0) {
                sendJsonResponse(res, 404, "Vehicle Type not found!");
            }
            else {
                sendJsonResponse(res, 200, vehicleType);
            }
        });
    }

    create(req, res, next) {
        const vehicleTypeService = new VehicleTypeService();
        vehicleTypeService.createVehicleType(req.body).then((vehicleType) => {
            sendJsonResponse(res, 201, vehicleType);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                sendJsonResponse(res, 400, reason);
            }
        });
    }
}

module.exports = VehicleTypeController;