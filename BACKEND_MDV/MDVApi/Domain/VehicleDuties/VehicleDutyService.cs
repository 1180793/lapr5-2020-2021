using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Trips;
using System;

namespace MDVApi.Domain.VehicleDuties
{
    public class VehicleDutyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleDutyRepository _repo;

        public VehicleDutyService(IUnitOfWork unitOfWork, IVehicleDutyRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<VehicleDutyDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();
            
            List<VehicleDutyDto> listDto = list.ConvertAll<VehicleDutyDto>(vehicleDuty => 
                new VehicleDutyDto(vehicleDuty.Id.AsGuid(), vehicleDuty.name, vehicleDuty.color, vehicleDuty.depots, vehicleDuty.date));

            return listDto;
        }

        public async Task<VehicleDutyDto> GetByIdAsync(VehicleDutyId id)
        {
            var vehicleDuty = await this._repo.GetByIdAsync(id);
            
            if(vehicleDuty == null)
                return null;
           
            return new VehicleDutyDto(vehicleDuty.Id.AsGuid(), vehicleDuty.name, vehicleDuty.color, vehicleDuty.depots, vehicleDuty.date);
        }

        public async Task<List<VehicleDutyDto>> GetByDateAsync(DateTime date){
            
            var list = await this._repo.GetByDateAsync(date);
            
            List<VehicleDutyDto> listDto = list.ConvertAll<VehicleDutyDto>(vehicleDuty => 
                new VehicleDutyDto(vehicleDuty.Id.AsGuid(), vehicleDuty.name, vehicleDuty.color, vehicleDuty.depots, vehicleDuty.date));

            return listDto;
        } 

        public async Task<VehicleDuty> GetVehicleDutyByIdAsync(VehicleDutyId id)
        {
            var vehicleDuty = await this._repo.GetByIdAsync(id);
            
            if(vehicleDuty == null)
                return null;
           
            return vehicleDuty;
        }

        public async Task<VehicleDutyDto> AddAsync(CreatingVehicleDutyDto dto, IList<Trip> Trips)
        {
            var vehicleDuty = new VehicleDuty(dto.name, dto.color, dto.date);

            vehicleDuty.Trips = Trips;

            await this._repo.AddAsync(vehicleDuty);

            await this._unitOfWork.CommitAsync();

            return new VehicleDutyDto(vehicleDuty.Id.AsGuid(), vehicleDuty.name, vehicleDuty.color, vehicleDuty.depots, vehicleDuty.date);
        }

    }
}