const express = require('express');
const router = express.Router();
const multer = require('multer');
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

var storage = multer.diskStorage(
    {
        destination: './receivedScheduleFile/',
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    }
);

var upload = multer({ storage: storage });

const UploadScheduleController = require('../controllers/UploadScheduleController');
var scheduleController = new UploadScheduleController();

router.post('/', upload.single('scheduleFile'), scheduleController.uploadSchedule);

module.exports = router;