const Joi = require('joi');
const mongoose = require('mongoose');

const constants = require("../utils/constants");

const coordinatesSchema = new mongoose.Schema({
    latitude: {
        type: Number,
        required: true,
        min: constants.MIN_LATITUDE,
        max: constants.MAX_LATITUDE
    },
    longitude: {
        type: Number,
        required: true,
        min: constants.MIN_LONGITUDE,
        max: constants.MAX_LONGITUDE
    },
    elevation: {
        type: Number,
        min: constants.MIN_ELEVATION,
        max: constants.MAX_ELEVATION
    }
}, { _id: false });

// Joi Schema
const joiCoordinateSchema = Joi.object({
    latitude: Joi.number()
        .min(constants.MIN_LATITUDE)
        .max(constants.MAX_LATITUDE)
        .required(),
    longitude: Joi.number()
        .min(constants.MIN_LONGITUDE)
        .max(constants.MAX_LONGITUDE)
        .required(),
    elevation: Joi.number()
        .min(constants.MIN_ELEVATION)
        .max(constants.MAX_ELEVATION)
});

// Validate
function validateCoordinate(coordinate) {
    return joiCoordinateSchema.validate(coordinate);
}

exports.joiCoordinateSchema = joiCoordinateSchema;
exports.coordinatesSchema = coordinatesSchema;
exports.validate = validateCoordinate;
