import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/model/user.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AppSettings } from 'src/utils/AppSettings';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthEvent } from 'src/app/services/auth/AuthEvent';
import Swal from 'sweetalert2'

export interface ChipColor {
    name: string;
    color: ThemePalette;
}

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    profileForm: FormGroup;
    user: User;
    nameTitle: string = '';

    name: string = '';
    email: string = '';
    roles: string[] = [];

    availableColors: ChipColor[] = [];

    constructor(public dialog: MatDialog, public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        if (!this.jwtAuth.getUser()) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.user = this.jwtAuth.getUser();
        this.nameTitle = `${this.user.name}'s Profile`;
        this.name = this.user.name;
        this.email = this.user.email;
        this.roles = this.user.roles;
        for (var i = 0; i < this.roles.length; i++) {
            var color = '';
            if (this.roles[i] === AppSettings.DATA_ADMINISTRATOR) {
                color = 'admin';
            } else if (this.roles[i] === AppSettings.GESTOR) {
                color = 'gestor';
            } else if (this.roles[i] === AppSettings.CLIENT) {
                color = 'client';
            } else {
                color = 'undefined';
            }
            var colorPalette = color as ThemePalette;
            var newName = this.titleCase(this.roles[i]);
            if (this.roles[i] === AppSettings.DATA_ADMINISTRATOR) {
                this.availableColors.unshift({ name: newName, color: colorPalette });
            } else {
                this.availableColors.push({ name: newName, color: colorPalette });
            }
        }
        this.profileForm = new FormGroup({
            name: new FormControl(this.name),
            email: new FormControl(this.email),
            roles: new FormControl(this.roles)
        });
    }

    titleCase(str: string) {
        var newStr = str.toLowerCase();
        var splitStr = newStr.split('_');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }

    deleteAccountDialogue() {
        const dialogRef = this.dialog.open(DeleteAccountDialog);
    }

}

@Component({
    selector: 'delete-account-dialog',
    templateUrl: 'delete-account-dialog.html',
})
export class DeleteAccountDialog {

    constructor(public dialog: MatDialog, public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router, private authEvent: AuthEvent) {
        if (!this.jwtAuth.getUser()) {
            alert(`Access Denied`);
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    deleteAccount() {
        this.jwtAuth.deleteUser()
            .subscribe(response => {
                alert(`${response.message}`);
                this.authEvent.emitAuthStatus('Login');
            }, err => {
                console.log("🚀 ~ file: profile.component.ts ~ line 108 ~ DeleteAccountDialog ~ deleteAccount ~ err", err)
                alert(`Couldn't delete account.`);
            });
    }
}
