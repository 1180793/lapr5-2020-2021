const mongoose = require('mongoose');
const VehicleTypeSchema = require('../schemas/VehicleTypeSchema').vehicleTypeSchema;
const VehicleType = mongoose.model('VehicleType', VehicleTypeSchema);

class VehicleTypeRepository {

    constructor() {}

    /**
     * Retorna uma promessa com todas os VehicleTypes
     */
    findAll() {
        return VehicleType
            .find()
            .exec();
    }

    /**
     * Encontra um Vehicle Type por Name
     * @param {String} vehicleTypeName 
     * @param {(err,item)} cb 
     */
    findByName(vehicleTypeName, cb) {
        VehicleType
            .findOne({
                name : { $regex : new RegExp("^" + vehicleTypeName + "$", "i") }
            })
            .exec(cb);
    }

    /**
     * Checks if VehicleType exists
     * @param {String} vehicleTypeName
     * @param {(err,item)} cb 
     */
    exists(vehicleTypeName, cb) {
        VehicleType
            .findOne({
                name : { $regex : new RegExp("^" + vehicleTypeName + "$", "i") }
            })
            .exec(cb);
    }
    
    /**
     * Guarda um VehicleType
     * @param {VehicleType} vehicleType 
     * @param {(err, savedVehicleType)} cb 
     */
    save(vehicleType, cb) {
        vehicleType.save(function (err, oi) {
            if (err) cb(new Error("Invalid Save"));
            else
                cb(err, oi);
        });
    }
}

module.exports = VehicleTypeRepository;