import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as equal from 'fast-deep-equal';
import { LineService } from '../line/line.service';
import { NodeService } from '../node/node.service';
import { PathService } from '../path/path.service';



@Injectable({
    providedIn: 'root'
})
export class MapService {

    constructor(public http: HttpClient) {
    }

    //Retorna a cor da linha em hexadecimal
    getColorByLine(lineName: String, lineList: any): any {
        lineList.forEach((element: any) => {
            var test = JSON.stringify(lineName)
            if (test === lineName) {
                var res = this.rgbToHex(element._color);
                return res;
            } else {
                return null;
            }
        });
    }

    //Verifica se a lista contem o elemento recebido e retorna boolean
    containsSeg(element: any, color: String, drawnSegmentColor: any[]) {
        var sol = false;
        drawnSegmentColor.forEach((list_element: any) => {
            if (equal(element, list_element[0])) {
                sol = true;
                if (color == list_element[1]) {
                    sol = null;
                }
            }
        });
        return sol;
    }

    //Retorna o número de vezes que um metodo foi chamado
    countDrawnSegment(seg: any, drawnSegmentColor: any[]) {
        var cont = 0;
        drawnSegmentColor.forEach((list_element: any) => {
            if (equal(seg, list_element[0])) {
                cont++;
            }
        });
        
        return cont;
    }

    //Conversor de RGB to Hex
    rgbToHex(rgb: any) {
        var stringToSplit = rgb;
        var afterSubstring = stringToSplit.substring(4).slice(0, -1);
        var arrayOfStrings = afterSubstring.split(',');
        return "#" + this.componentToHex(parseInt(arrayOfStrings[0])) + this.componentToHex(parseInt(arrayOfStrings[1])) + this.componentToHex(parseInt(arrayOfStrings[2]));
    }

    //Conversor de RGB to Hex -- Utility
    componentToHex(c: any) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    //Retorna o nó pelo short name
    getNodeByName(name: String, nodeList: any[]): any {
        var nodeToReturn = null;
        nodeList.forEach((element: any) => {
            if (element._shortName == name) {
                nodeToReturn = element;
            }
        });
        return nodeToReturn;
    }

    //Retorna um path de uma lista pela key
    getPathFromList(pathKey: String, pathList: any[]): any {
        var pathToReturn = null;
        pathList.forEach((element: any) => {
            if (element.key == pathKey) {
                pathToReturn = element;
            }
        });
        return pathToReturn;
    }


}

