const express = require('express');

const app = express();

const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (e, req, res, next) {
    let status = e.status || 500;
    let error = { message: e.message };

    console.log(error);
    res.status(status).json(error);
});

module.exports = function () {
    return app;
};