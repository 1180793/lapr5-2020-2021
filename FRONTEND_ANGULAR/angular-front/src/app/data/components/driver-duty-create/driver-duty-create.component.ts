import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ColorPickerControl, ColorsTable } from '@iplab/ngx-color-picker';
import { VehicleDuty } from 'src/app/model/model/VehicleDuty';
import { WorkBlock } from 'src/app/model/model/WorkBlock';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DriverDutyService } from 'src/app/services/driver-duty/driver-duty.service';
import { VehicleDutyService } from 'src/app/services/vehicle-duty/vehicle-duty.service';
import { WorkBlockService } from 'src/app/services/work-block/work-block.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-driver-duty-create',
  templateUrl: './driver-duty-create.component.html',
  styleUrls: ['./driver-duty-create.component.css']
})
export class DriverDutyCreateComponent implements OnInit {

  //services
  workBlockService = new WorkBlockService(this.http);
  vehicleDutyService = new VehicleDutyService(this.http);
  driverDutyService = new DriverDutyService(this.http);

  //Variables
  name: string;
  color: string;
  workBlocks: any[];

  //lists
  workBlockList = new Array<WorkBlock>();
  workBlockTable = new Array<WorkBlock>();
  vehicleDutyList = new Array<VehicleDuty>();

  //for the table
  displayedColumns: string[] = ['select', 'startNode', 'endNode', 'duration'];
  dataSource: any;

  //Forms
  form: FormGroup;
  driverDutyForm: FormGroup;

  vehicleDutyNameList: any;
  selection = new SelectionModel<any>(true, []);
  dataSource1 = new MatTableDataSource<WorkBlock>(this.workBlockTable);

  reactiveForm() {
    this.form = this.fb.group({
      vDuty: [[]],
    });
  }
  driverDutyCreateForm() {
    this.driverDutyForm = this.fb.group({
      name: ['',],
      color: ['',],
    })
  }
  public chromeControl = new ColorPickerControl()
    .setValueFrom(ColorsTable.aquamarine)
    .hideAlphaChannel();

  constructor(public http: HttpClient, public fb: FormBuilder, private jwtAuth: AuthService, private router: Router) {
    const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
    if (!hasPermission) {
      Swal.fire('Error!', 'Access Denied!', 'error')
      this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
    }
  }

  ngOnInit(): void {
    //forms
    this.reactiveForm();
    this.driverDutyCreateForm();

    //population
    this.populateVehicleDuties();
    this.populateWorkBlocks();



    setTimeout(() => {
      this.initializeDataSource();
    }, 1500);
  }

  //returns the selected line name
  get selectedVehicleDuty() {
    return this.form.get('vDuty')?.value;
  }

  getName() {
    return this.driverDutyForm.get('name')?.value;
  }


  getColor(): string {
    var color = this.chromeControl.value.getRgba();
    var nC = 'RGB(' + color.getRed() + ',' + color.getGreen() + ',' + color.getBlue() + ')';
    return nC;
  }

  initializeDataSource() {

    this.dataSource = new MatTableDataSource(this.workBlockList);
    this.dataSource.filter = 'No Data Selected Yet';
    this.dataSource.filterPredicate = function (
      data: any,
      filter: string
    ): boolean {
      var ret = false;
      if (filter == '[]') {
        ret = true;
      } else {
        ret = filter.includes(data.vehicleDutyId);
      }
      return ret;
    };
  }

  async populateVehicleDuties() {
    (await this.vehicleDutyService.getVehicleDuties()).subscribe(
      (value) => {

        this.vehicleDutyList = value;
      },
      (error) => {

        Swal.fire('Error!', error.error + '!', 'error')
        console.log(error);
      }
    );
  }

  async populateWorkBlocks() {
    (await this.workBlockService.getWorkBlocks()).subscribe(
      (value) => {

        this.workBlockList = value;
      },
      (error) => {

        Swal.fire('Error!', error.error + '!', 'error')
        console.log(error);
      }
    );
  }


  getVehicleDutyToSearch() {
    return this.form.get('vDuty').value;
  }

  onSearchVehicleDuty() {
    var linesToSearch = this.selectedVehicleDuty;
    this.dataSource.filter = JSON.stringify(linesToSearch);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  getWorkBlocksFromTable() {

    var selectedWBlock = [];

    this.selection.selected.forEach(function (value) {
      selectedWBlock.push(value.id);
    });
    return selectedWBlock;
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.driverDutyForm.controls[controlName].hasError(errorName);
  }

  onCreateDriverDuty() {
    console.log(this.getColor());
    console.log(this.getName());
    console.log(this.getVehicleDutyToSearch())
    console.log(this.getWorkBlocksFromTable());

    if (this.driverDutyForm.valid) {

      console.log('VALID FORM');

      this.driverDutyService.addDriverDuties(this.getName(), this.getColor(), this.getWorkBlocksFromTable());
      this.driverDutyForm.reset();

    }
    else {
      this.driverDutyForm.reset();
    }

  }
  checkValues() {
    if (this.getName() === undefined || this.getName() == '' || this.getName() === null) {
      Swal.fire('Error!', 'Name can\'t be empty!', 'error')
      return false;
    }
    if (this.getVehicleDutyToSearch() == undefined || this.getVehicleDutyToSearch() == null) {
      Swal.fire('Error!', 'Please select one Vehicle Duty', 'error')
      return false
    }
    if (this.getWorkBlocksFromTable() == undefined || this.getWorkBlocksFromTable() === null) {
      Swal.fire('Error!', 'Plase select at least one work block !', 'error')
      return false
    }
    else {
      return true;
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource1.data.forEach(row => this.selection.select(row));
  }

}
