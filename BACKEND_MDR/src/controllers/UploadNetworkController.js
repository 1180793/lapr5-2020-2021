const xml2js = require('xml2js');
var fs = require('fs');
var DOMParser = require('xmldom').DOMParser;
var path = require('path');
const jwt = require("jsonwebtoken");
const config = require("config");
const Roles = require('../model/entities/Roles')
const sendJsonResponse = require('../middleware/sendJsonResponse');

const NodeService = require('../services/NodeService');
const nodeService = new NodeService();

const VehicleTypeService = require('../services/VehicleTypeService');
const vehicleTypeService = new VehicleTypeService();

const DriverTypeService = require('../services/DriverTypeService');
const driverTypeService = new DriverTypeService();

const LineRepository = require('../repository/LineRepository');
const lineRepo = new LineRepository();
const LineService = require('../services/LineService');
const lineService = new LineService();

const PathService = require('../services/PathService');
const pathService = new PathService();

const getEnergySourceByParameter = require('../utils/Utils').getEnergySourceByParameter;

class UploadNetworkController {

    async uploadNetwork(req, res, next) {
        if (config.get("requiresAuth")) {
            const token = req.header("x-auth-token");
            if (!token) {
                return res.status(401).send("Access denied. No token provided.");
            }

            try {
                const decoded = jwt.verify(token, config.get("jwtPrivateKey"));
                req.user = decoded;
            } catch (ex) {
                res.status(400).send("Invalid token.");
            }
            
            if (!req.user.roles.includes(Roles.DATA_ADMINISTRATOR.id)) {
                return res.status(403).send("Access denied.");
            }
        }
        try {
            const glxFile = './receivedFile/' + req.file.originalname;
            let fileExtension = path.extname(req.file.originalname);
            console.log("Received Network File", glxFile);
            if (fileExtension == '.xml' || fileExtension == '.glx') {
                fs.readFile(glxFile, 'utf8', async function (err, data) {
                    const entities = new DOMParser().parseFromString(data);
                    let parser = new xml2js.Parser(
                        {
                            attrNameProcessors: [xml2js.processors.firstCharLowerCase],
                            tagNameProcessors: [xml2js.processors.firstCharLowerCase],
                            attrValueProcessors: [xml2js.processors.parseBooleans],
                            explicitRoot: false,
                            mergeAttrs: true,
                            explicitArray: false,
                        });

                    // Import Nodes
                    let nodeList = entities.getElementsByTagName('Node');

                    for (let index = 0; index < nodeList.length; index++) {
                        parser.parseString(nodeList[index], async (err, result) => {
                            var nodeJSON = {
                                shortName: result.shortName,
                                name: result.name,
                                coordinates: {
                                    latitude: result.latitude,
                                    longitude: result.longitude,
                                },
                                isDepot: result.isDepot,
                                isReliefPoint: result.isReliefPoint
                            }
                            await nodeService.createNode(nodeJSON);
                        });
                    }

                    // Import Vehicle Types
                    let vehicleTypeList = entities.getElementsByTagName('VehicleType');

                    for (let index = 0; index < vehicleTypeList.length; index++) {
                        parser.parseString(vehicleTypeList[index], async (err, result) => {
                            var energy = getEnergySourceByParameter(result.energySource);

                            var vehicleTypeJSON = {
                                name: result.name,
                                autonomy: result.autonomy,
                                cost: result.cost,
                                averageSpeed: result.averageSpeed,
                                energySource: energy,
                                consumption: result.consumption,
                                emissions: result.emissions
                            }
                            await vehicleTypeService.createVehicleType(vehicleTypeJSON);
                        });
                    }

                    // Import Driver Types
                    let driverTypeList = entities.getElementsByTagName('DriverType');

                    for (let index = 0; index < driverTypeList.length; index++) {
                        parser.parseString(driverTypeList[index], async (err, result) => {
                            var driverTypeJSON = {
                                name: result.name,
                            }
                            await driverTypeService.createDriverType(driverTypeJSON);
                        });
                    }

                    // Import Lines and Paths
                    let lineList = entities.getElementsByTagName('Line');
                    let pathList = entities.getElementsByTagName('Path');

                    for (let index = 0; index < lineList.length; index++) {
                        parser.parseString(lineList[index], async (err, result) => {

                            var startNodeKey = await getStartNode(parser, pathList, result);
                            var endNodeKey = await getEndNode(parser, pathList, result);

                            var startNode = await fetchNode(parser, nodeList, startNodeKey);
                            var endNode = await fetchNode(parser, nodeList, endNodeKey);
                            var linePaths = await getLinePaths(result);

                            var lineJSON = {
                                name: result.name,
                                color: result.color,
                                startNode: startNode,
                                endNode: endNode
                            }

                            await lineService.createLine(lineJSON);

                            for (let x = 0; x < linePaths.length; x++) {
                                for (let y = 0; y < pathList.length; y++) {
                                    parser.parseString(pathList[y], async (err, pathResult) => {
                                        if (linePaths[x].key == pathResult.key) {
                                            var pathNodeKeyList = [];
                                            for (let k = 0; k < Object.keys(pathResult.pathNodes.pathNode).length; k++) {
                                                var pathNodeNode = await fetchNode(parser, nodeList, pathResult.pathNodes.pathNode[k].node);
                                                var pathNodeDuration = 0;
                                                if (pathResult.pathNodes.pathNode[k].hasOwnProperty('duration')) {
                                                    pathNodeDuration = parseInt(pathResult.pathNodes.pathNode[k].duration, 10);
                                                }
                                                var pathNodeDistance = 0;
                                                if (pathResult.pathNodes.pathNode[k].hasOwnProperty('distance')) {
                                                    pathNodeDistance = parseInt(pathResult.pathNodes.pathNode[k].distance, 10);
                                                }
                                                var pathNode = {
                                                    node: pathNodeNode,
                                                    distance: pathNodeDistance,
                                                    duration: pathNodeDuration,
                                                }

                                                pathNodeKeyList.push(pathNode);
                                            }

                                            var pathJSON = {
                                                key: pathResult.key,
                                                line: result.name,
                                                isEmpty: pathResult.isEmpty,
                                                orientation: linePaths[x].orientation,
                                                pathNodeList: pathNodeKeyList
                                            }
                                            await pathService.createPath(pathJSON);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                sendJsonResponse(res, 200, { message: 'Network File imported successfully.' });
            } else {
                sendJsonResponse(res, 400, { message: `Invalid File Type. '${fileExtension}'\nAllowed File Types: '.glx', '.xml'` });
            }
        } catch (err) {
            console.log("UploadNetworkController -> upload -> err", err)
            sendJsonResponse(res, 400, { message: 'Invalid File.' });
        }

    }

}

async function getStartNode(parser, pathList, result) {
    var startNode = null;
    for (let x = 0; x < Object.keys(result.linePaths.linePath).length; x++) {
        var pathKey = result.linePaths.linePath[x].path;
        var pathOrientation = result.linePaths.linePath[x].orientation;
        for (let y = 0; y < pathList.length; y++) {
            parser.parseString(pathList[y], async (err, pathResult) => {
                if (pathKey == pathResult.key) {
                    if (pathOrientation == 'Go') {
                        if (startNode == null) {
                            startNode = pathResult.pathNodes.pathNode[0].node;
                        }
                    }
                }
            });
        }
    }
    return startNode;
}

async function getEndNode(parser, pathList, result) {
    var endNode = null;
    for (let x = 0; x < Object.keys(result.linePaths.linePath).length; x++) {
        var pathKey = result.linePaths.linePath[x].path;
        var pathOrientation = result.linePaths.linePath[x].orientation;
        for (let y = 0; y < pathList.length; y++) {
            parser.parseString(pathList[y], async (err, pathResult) => {
                if (pathKey == pathResult.key) {
                    if (pathOrientation == 'Return') {
                        if (endNode == null) {
                            endNode = pathResult.pathNodes.pathNode[0].node;
                        }
                    }
                }
            });
        }
    }
    return endNode;
}

async function fetchNode(parser, nodeList, nodeKey) {
    var nodeToReturn = null;
    for (let index = 0; index < nodeList.length; index++) {
        parser.parseString(nodeList[index], async (err, result) => {
            if (nodeKey == result.key) {
                nodeToReturn = result.shortName;
            }
        });
    }
    return nodeToReturn;
}

async function getLinePaths(result) {
    var paths = [];
    for (let x = 0; x < Object.keys(result.linePaths.linePath).length; x++) {
        var pathKey = result.linePaths.linePath[x].path;
        var pathOrientation = result.linePaths.linePath[x].orientation;
        var path = {
            key: pathKey,
            orientation: pathOrientation
        }
        paths.push(path);
    }
    return paths;
}

module.exports = UploadNetworkController;