import {Coordinate} from '../model/model/Coordinate';

export class CoordinateMap{
  public static toDTO(coordinate : Coordinate) {
    return{
      latitude : coordinate.latitude,
      longitude : coordinate.longitude
    }
  }

  public async toDomain(raw : any){
    return new Coordinate(raw.latitude,raw.longitude);
}
}
