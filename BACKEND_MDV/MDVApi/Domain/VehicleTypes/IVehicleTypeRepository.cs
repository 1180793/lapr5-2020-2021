using System.Threading.Tasks;
using MDVApi.Domain.Shared;

namespace MDVApi.Domain.VehicleTypes
{
    public interface IVehicleTypeRepository: IRepository<VehicleType,VehicleTypeId>
    {
        Task<VehicleType> getVehicleTypeByNameAsync(string name);
    }
}