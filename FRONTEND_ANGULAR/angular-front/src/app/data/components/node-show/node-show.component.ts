import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { merge } from 'rxjs-compat/operator/merge';
import { Nodes } from 'src/app/model/model/Nodes';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NodeService } from 'src/app/services/node/node.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-node-show',
    templateUrl: './node-show.component.html',
    styleUrls: ['./node-show.component.css'],
})
export class NodeShowComponent implements OnInit {
    //services
    nodeService = new NodeService(this.http);

    //nodeList
    nodeList = new Array<Nodes>();

    //for the table
    displayedColumns: string[] = [
        '_name',
        '_shortName',
        '_coordinates.latitude',
        '_coordinates.longitude',
        '_isReliefPoint',
        '_isDepot',
    ];

    dataSource: any;

    constructor(public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    @ViewChild(MatSort) sort: MatSort;

    ngOnInit(): void {
        this.populateNodes();

        setTimeout(() => {
            this.initializaDataSource();
            // this.dataSource.filterPredicate = (data: Nodes, filter: string) => {
            //   return data.name == filter;
            // };
        }, 500);
    }

    initializaDataSource() {
        this.dataSource = new MatTableDataSource(this.nodeList);
        this.dataSource.sort = this.sort;
    }

    async populateNodes() {
        (await this.nodeService.getNodes()).subscribe(
            (value) => {
                this.nodeList = value;
                // console.log(value);
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }
    checkIfReliefPoint(isReliefPoint: boolean) {
        if (isReliefPoint) {
            return 'Yes'
        }
        else {
            return 'No'
        }
    }
    checkIfBusDepot(busDepot: boolean) {
        if (busDepot) {
            return 'Yes'
        }
        else {
            return 'No'
        }
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}
