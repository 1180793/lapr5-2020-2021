import { DriverType } from "../model/model/DriverType";

export class DriverTypeMap {

    public toDTO(driverType: DriverType) {
        return {
            name: driverType.driverTypeName
        }
    }

    public static async toDomain(raw: any) {
        return new DriverType(raw.name);
    }

}
