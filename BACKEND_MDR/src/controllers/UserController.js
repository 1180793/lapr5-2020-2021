const sendJsonResponse = require('../middleware/sendJsonResponse');
const UserService = require('../services/UserService');
const _ = require('lodash');

class UserController {

    async getUserProfile(req, res, next) {
        try {
            const userService = new UserService();
            const user = await userService.findUserByEmail(req.user.email);
            res.send(user);
        } catch (err) {
            sendJsonResponse(res, 400, { message: 'Invalid User.' });
        }
    }

    async deleteUser(req, res, next) {
        try {
            const userService = new UserService();
            const user = await userService.deleteUser(req.user._id);
            
            sendJsonResponse(res, 200, { message: 'User deleted successfully.' });
        } catch (err) {
            sendJsonResponse(res, 400, { message:  "Couldn't delete user." });
        }
    }
}

module.exports = UserController;