using MDVApi.Domain.Drivers;
using MDVApi.Infrastructure.Shared;

namespace MDVApi.Infrastructure.Drivers
{
    public class DriverRepository : BaseRepository<Driver, DriverId>,IDriverRepository
    {
        public DriverRepository(MDVApiDbContext context):base(context.Drivers)
        {
           
        }
    }
} 