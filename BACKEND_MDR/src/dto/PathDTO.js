class PathDTO {

    constructor(path) {
        this._key = path.key;
        this._line = path.line;
        this._orientation = path.orientation;
        this._isEmpty = path.isEmpty;
        this._pathNodeList = path.pathNodeList;
    }

    get key() {
        return this._key;
    }

    get line() {
        return this._line;
    }

    get orientation() {
        return this._orientation;
    }

    get isEmpty() {
        return this._isEmpty;
    }
    
    get pathNodeList() {
        return this._pathNodeList;
    }

}

module.exports = PathDTO;