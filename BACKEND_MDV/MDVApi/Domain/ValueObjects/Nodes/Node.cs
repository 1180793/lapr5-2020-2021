using System;

namespace MDVApi.Domain.ValueObjects.Nodes
{
    public class Node
    {
        public string name { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string shortName { get; set; }
        public Boolean isDepot { get; set; }
        public Boolean isReliefPoint { get; set; }
        public Node(string name, string latitude, string longitude, string shortName, Boolean isDepot, Boolean isReliefPoint){

            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.shortName = shortName;
            this.isDepot = isDepot;
            this.isReliefPoint = isReliefPoint;
            
        } 
    }

}