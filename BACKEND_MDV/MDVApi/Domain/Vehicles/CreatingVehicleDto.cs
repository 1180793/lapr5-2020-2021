using System.ComponentModel.DataAnnotations;

namespace MDVApi.Domain.Vehicles
{
    public class CreatingVehicleDto
    {
        [RegularExpression(@"^(([A-Z]{2}-\d{2}-(\d{2}|[A-Z]{2}))|(\d{2}-(\d{2}-[A-Z]{2}|[A-Z]{2}-\d{2})))$", ErrorMessage = "Invalid License Plate")]
        [Required(ErrorMessage = "License Plate is required")]
        public string matricula { get; set; }
        [Required(ErrorMessage = "Vin is required")]
        public string vin { get; set; }
        [Required(ErrorMessage = "Vehicle Type is required")]
        public string vehicleTypeName { get; set; } 

        public CreatingVehicleDto(string matricula, string vin, string vehicleTypeName)
        {
            this.matricula = matricula;
            this.vin = vin;
            this.vehicleTypeName = vehicleTypeName;
            
        }
    }
}