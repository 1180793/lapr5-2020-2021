
export class LineTrip {

//Variables---------------------------------------------------------------------------------------------------------------------------------------------------
  isEmpty: boolean;
  lineName: string;
  pathNameGo: string;
  pathNameReturn: string;
  frequency: number;
  numberOfTrips: number;
  startTime: number; //in seconds

  //-----------------------------------------------------------------------------------------------------------------------------------------------------------

  constructor(isEmpty: boolean, lineName: string, pathNameGo: string, pathNameReturn:string, frequency: number, numberOfTrips: number, startTime: number) {
      this.isEmpty = isEmpty;
      this.lineName=lineName;
      this.pathNameGo=pathNameGo;
      this.pathNameReturn = pathNameReturn;
      this.frequency=frequency;
      this.numberOfTrips=numberOfTrips;
      this.startTime = startTime;
  }

//Getters------------------------------------------------------------------------------------------------------------------------------------------------------

  get IsEmpty() {
      return this.isEmpty;
  }
  get LineName() {
    return this.lineName;
  }
  get PathNameGo() {
      return this.pathNameGo;
  }
  get PathNameReturn() {
    return this.pathNameReturn;
  }
  get Frequency() {
    return this.frequency;
  }
  get NumeberOfTrips() {
    return this.numberOfTrips;
  }
  get StartTime() {
      return this.startTime;
  }


}
