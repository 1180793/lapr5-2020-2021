const express = require('express');
const router = express.Router();
const multer = require('multer');
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

var storage = multer.diskStorage(
    {
        destination: './receivedFile/',
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    }
);

var upload = multer({ storage: storage });

const UploadNetworkController = require('../controllers/UploadNetworkController');
var networkController = new UploadNetworkController();

router.post('/', upload.single('networkFile'), networkController.uploadNetwork);

module.exports = router;