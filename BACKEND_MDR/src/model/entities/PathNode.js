const mongoose = require('mongoose');
const PathNodeSchema = require('../../schemas/PathNodeSchema').pathNodeSchema;

const NodeRepository = require('../../repository/NodeRepository');
const nodeRepo = new NodeRepository();

async function validateNode(nodeKey) {
    return new Promise((resolve, reject) => {
        nodeRepo.exists(nodeKey, function (err, node) {
            if (err) {
                reject(err);
            }
            else {
                resolve(node);
            }
        });
    });
}

/**
 * Creates Path Node instance from JSON Object
 * @param {JSON} pathNodeJSON 
 */
async function createPathNodeFromJSON(pathNodeJSON) {
    return new Promise(async (resolve, reject) => {
        var pathNode = null;
        var PathNode = mongoose.model('PathNode', PathNodeSchema);
        try {
            var nodeID;

            // Query Node
            let foundNode = await validateNode(pathNodeJSON.node);
            if (foundNode == null) {
                reject('Invalid Node!');
            }
            nodeID = foundNode._id;

            pathNode = new PathNode({
                key: pathNodeJSON.key,
                node: nodeID,
                duration: pathNodeJSON.duration,
                distance: pathNodeJSON.distance
            });

            resolve(pathNode);
        } catch (err) {
            reject(err);
        }
    });
}

exports.PathNode = mongoose.model('PathNode', PathNodeSchema);
exports.createPathNodeFromJSON = createPathNodeFromJSON;