import { Trip } from "../model/model/Trip";

export class TripMap {

    public toDTO(trip: Trip) {
        return {
            isEmpty: trip.IsEmpty,
            lineName: trip.Line,
            pathName: trip.Path,
            startTime: trip.StartTime
        }
    }



    public static async toDomain(raw: any) {
        return new Trip(raw.isEmpty, raw.path, raw.line, raw.startTime);
    }

}
