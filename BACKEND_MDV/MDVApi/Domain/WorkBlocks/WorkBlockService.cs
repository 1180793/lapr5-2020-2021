using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleDuties;
using System;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.Linq;
using MDVApi.Domain.PassingTimes;

namespace MDVApi.Domain.WorkBlocks
{
    public class WorkBlockService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkBlockRepository _repo;
        public IConfiguration Configuration { get; }

        public WorkBlockService(IUnitOfWork unitOfWork, IWorkBlockRepository repo, IConfiguration configuration)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            Configuration = configuration;
        }

        public async Task<List<WorkBlockDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<WorkBlockDto> listDto = list.ConvertAll<WorkBlockDto>(workBlock =>
                new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.startTime, workBlock.endTime, workBlock.startNode, workBlock.endNode, workBlock.isCrewTravelTime, workBlock.isActive, workBlock.VehicleDutyId.AsGuid()));

            return listDto;
        }

        public async Task<WorkBlockDto> GetByIdAsync(WorkBlockId id)
        {
            var workBlock = await this._repo.GetByIdAsync(id);

            if (workBlock == null)
                return null;

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.startTime, workBlock.endTime, workBlock.startNode, workBlock.endNode, workBlock.isCrewTravelTime, workBlock.isActive);
        }

        public async Task<WorkBlock> GetWorkBlockByIdAsync(WorkBlockId id)
        {
            var workBlock = await this._repo.GetByIdAsync(id);

            if (workBlock == null)
                return null;

            return workBlock;
        }

        public async Task<WorkBlockDto> AddAsync(WorkBlockAddDto dto, VehicleDuty vehicleDuty)
        {
            var workBlock = new WorkBlock(dto.startTime, dto.endTime, dto.startNode, dto.endNode, dto.workBlockDto.isCrewTravelTime);

            workBlock.VehicleDuty = vehicleDuty;

            await this._repo.AddAsync(workBlock);

            await this._unitOfWork.CommitAsync();

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.startTime, workBlock.endTime, workBlock.startNode, workBlock.endNode, workBlock.isCrewTravelTime, workBlock.isActive, vehicleDuty.Id.AsGuid());
        }

        public async Task<IList<WorkBlockDto>> getWorkBlocksByVehicleDuty(VehicleDutyId vehicleDutyId)
        {

            IList<WorkBlockDto> workBlockDtos = new List<WorkBlockDto>();

            var workBlocks = await _repo.getWorkBlocksByVehicleDutyAsync(vehicleDutyId);

            if (workBlocks.Count == 0)
            {
                return null;
            }

            foreach (WorkBlock workBlock in workBlocks)
            {
                WorkBlockDto workBlockDto = new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.startTime, workBlock.endTime, workBlock.startNode, workBlock.endNode, workBlock.isCrewTravelTime, workBlock.isActive);
                workBlockDtos.Add(workBlockDto);
            }

            return sortWorkBlockListByTime(workBlockDtos);
        }

        public IList<WorkBlockAddDto> generateWorkBlocks(CreatingWorkBlockDto dto, IList<PassingTimeDto> passingTimesList, VehicleDuty vehicleDuty)
        {
            var numberOfBlocks = dto.numberOfBlocks;
            IList<PassingTimeDto> usedPassingTimes = new List<PassingTimeDto>();
            IList<WorkBlockAddDto> createdWorkBlocks = new List<WorkBlockAddDto>();

            long startTime = passingTimesList.ElementAt(0).time;
            string startNode = passingTimesList.ElementAt(0).nodeName;

            while (numberOfBlocks > 0 && passingTimesList.Count > 0)
            {
                long maxEndTime = dto.duration + startTime;
                long endTime = 0;
                string endNode = "";
                PassingTimeDto last = passingTimesList.Last();

                foreach (PassingTimeDto pTDto in passingTimesList)
                {

                    if (maxEndTime < pTDto.time)
                    {
                        var workBlock = new WorkBlockAddDto(dto, startTime, endTime, startNode, endNode);
                        startNode = endNode;
                        startTime = endTime;
                        createdWorkBlocks.Add(workBlock);
                        break;
                    }
                    else if (pTDto == last)
                    {
                        endTime = pTDto.time;
                        endNode = pTDto.nodeName;
                        usedPassingTimes.Add(pTDto);
                        var workBlock = new WorkBlockAddDto(dto, startTime, endTime, startNode, endNode);
                        createdWorkBlocks.Add(workBlock);
                        break;
                    }
                    else
                    {
                        endTime = pTDto.time;
                        endNode = pTDto.nodeName;
                    }

                    usedPassingTimes.Add(pTDto);
                }

                foreach (PassingTimeDto pTDto in usedPassingTimes)
                {
                    passingTimesList.Remove(pTDto);
                }

                usedPassingTimes = new List<PassingTimeDto>();

                numberOfBlocks = numberOfBlocks - 1;
            }

            return createdWorkBlocks;
        }

        public IList<WorkBlockDto> sortWorkBlockListByTime(IList<WorkBlockDto> list)
        {

            int n = list.Count;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (list[j].endTime > list[j + 1].endTime)
                    {
                        WorkBlockDto temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                    }
                }
            }

            return list;
        }

        public async Task<IList<WorkBlock>> GetWorkBlocksForDriver(IList<Guid> list)
        {

            IList<WorkBlock> workBlocks = new List<WorkBlock>();

            foreach (Guid id in list)
            {
                var workBlock = await GetWorkBlockByIdAsync(new WorkBlockId(id));

                workBlocks.Add(workBlock);
            }

            workBlocks = sortWorkBlockListByTimeD(workBlocks);

            if (!isValid(workBlocks))
            {
                return null;
            }

            return workBlocks;
        }

        private IList<WorkBlock> sortWorkBlockListByTimeD(IList<WorkBlock> list)
        {

            int n = list.Count;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (list[j].endTime > list[j + 1].endTime)
                    {
                        WorkBlock temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                    }
                }
            }

            return list;
        }

        public Boolean isNull(IList<WorkBlockDto> list)
        {
            return list == null;
        }

        private Boolean isValid(IList<WorkBlock> list)
        {

            long MaxHours = (long)Convert.ToDouble(Configuration["DriverParameters:MaxHours"]);
            long timeEnlapsed = 0;
            long endTime = -1;

            foreach (WorkBlock wB in list)
            {

                if (endTime == -1)
                {
                    endTime = wB.startTime;
                }

                if (wB.startTime == endTime)
                {
                    timeEnlapsed = timeEnlapsed + (wB.endTime - wB.startTime);
                }
                else
                {
                    timeEnlapsed = wB.endTime - wB.startTime;
                    endTime = -1;
                }

                if (timeEnlapsed > MaxHours)
                {
                    return false;
                }

                endTime = wB.endTime;
            }

            return true;
        }

    }
}