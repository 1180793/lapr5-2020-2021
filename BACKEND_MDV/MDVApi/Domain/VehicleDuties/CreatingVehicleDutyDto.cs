using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDVApi.Domain.VehicleDuties
{
    public class CreatingVehicleDutyDto
    {
        [Required(ErrorMessage = "Name is required")]
        public string name{ get; set; }

        [Required(ErrorMessage = "Color is required")]
        public string color { get; set; }

        [Required(ErrorMessage = "Date is required")]
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

        [Required(ErrorMessage = "At least a Trip is required")]
        public IList<Guid> tripIds { get; set; }

        
    }
}