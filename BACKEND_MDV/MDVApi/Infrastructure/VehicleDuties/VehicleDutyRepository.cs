using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDVApi.Infrastructure.VehicleDuties
{
    public class VehicleDutyRepository : BaseRepository<VehicleDuty, VehicleDutyId>, IVehicleDutyRepository
    {
        private readonly DbSet<VehicleDuty> _objs;

        public VehicleDutyRepository(MDVApiDbContext context):base(context.VehicleDuties)
        {
           _objs = context.VehicleDuties ?? throw new ArgumentNullException(nameof(context.VehicleDuties));
        }

        public async Task<List<VehicleDuty>> GetByDateAsync(DateTime date){
            
            return await _objs.Where(x => date.Equals(x.date)).ToListAsync();
        }
    }
}