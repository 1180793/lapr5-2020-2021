
namespace MDVApi.Domain.ValueObjects.Nodes
{
    public class NodeService
    {

        public NodeService()
        {
        }

        public Node convertNodeDto(CreatingNodeDto dto){

            Node node = new Node(dto.name, dto.latitude, dto.longitude, dto.shortName, dto.isDepot, dto.isReliefPoint);

            return node;
        }
        
    }
}