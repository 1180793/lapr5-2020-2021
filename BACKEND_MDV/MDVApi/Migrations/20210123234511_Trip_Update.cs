﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDVApi.Migrations
{
    public partial class Trip_Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkBlocks_Trips_TripId",
                table: "WorkBlocks");

            migrationBuilder.DropIndex(
                name: "IX_WorkBlocks_TripId",
                table: "WorkBlocks");

            migrationBuilder.DropColumn(
                name: "TripId",
                table: "WorkBlocks");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TripId",
                table: "WorkBlocks",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_TripId",
                table: "WorkBlocks",
                column: "TripId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkBlocks_Trips_TripId",
                table: "WorkBlocks",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
