import { LinePath } from '../model/model/LinePath';


export class LinePathMap {
  public toDTO(linePath: LinePath) {
    return {
      lineName: linePath.pathName,
      distance: linePath.orientation
    }
  }

  public async toDomain(raw: any) {
    return new LinePath(raw.pathName, raw.orientation);
  }
}
