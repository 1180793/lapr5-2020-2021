using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Trips;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using MDVApi.Domain.ValueObjects.Lines;
using System.Text.Json;
using MDVApi.Domain.ValueObjects.Paths;
using MDVApi.Domain.Authentication;
using MDVApi.Domain.PassingTimes;
using Newtonsoft.Json;
using MDVApi.Domain.ValueObjects.PathNodes;

namespace MDVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripsController : ControllerBase
    {
        private readonly TripService _service;
        private readonly PassingTimeService _servicePt;
        private readonly IConfiguration _configuration;
        private readonly AuthenticationService _auth;

        


        public TripsController(TripService service, PassingTimeService servicePt, IConfiguration configuration, AuthenticationService auth)
        {
            _service = service;
            _servicePt = servicePt;
            _configuration = configuration;
            _auth = auth;
        }

        // GET: api/Trips
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TripDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/Trips/id
        [HttpGet("{id}")]
        public async Task<ActionResult<TripDto>> GetGetById(Guid id)
        {

            var trip = await _service.GetByIdAsync(new TripId(id));

            if (trip == null)
            {
                return NotFound();
            }

            return trip;
        }

        // GET: api/Trips/line/Line
        [HttpGet("line/{line}")]
        public async Task<ActionResult<IEnumerable<TripDto>>> GetAllByLine(string line)
        {
            return await _service.GetByLineAsync(line);
        }

        // POST: api/Trips
        [HttpPost]
        public async Task<ActionResult<TripDto>> Create(CreatingTripDto dto)
        {
            try
            {
                HttpClient client = await _auth.login(_configuration);

                var line = await _service.getLineForTrip(client, dto.lineName, _configuration);

                if (line == null)
                {
                    return BadRequest("Line doesn't exist.");
                }

                var path = await _service.getPathForTrip(client, dto.pathName, _configuration);
                
                if (path == null)
                {
                    return BadRequest("Path doesn't exist.");
                }

                IList<PassingTime> passingTimesList = _servicePt.generatePassingTimesAsync(path._pathNodeList, dto.startTime);

                var tripDto = await _service.AddAsync(dto, line, path, passingTimesList);

                return CreatedAtAction(nameof(GetGetById), new { id = tripDto.Id }, tripDto);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }

        }

        [HttpPost("line")]
        public async Task<ActionResult<TripDto>> CreateForLine(CreatingLineTripDto dto)
        {

            try
            {

                HttpClient client = await _auth.login(_configuration);

                var pathGo = await _service.getPathForTrip(client, dto.pathNameGo, _configuration);
                var pathReturn = await _service.getPathForTrip(client, dto.pathNameReturn, _configuration);

                if (pathGo == null)
                {
                    return BadRequest("Go Path doesn't exist.");
                }
                else if (pathReturn == null)
                {
                    return BadRequest("Return Path doesn't exist.");
                }

                var numberOfTrips = dto.numberOfTrips;
                var startTime = dto.startTime;
                string orientation = "";
                IList<TripDto> createdTrips = new List<TripDto>();

                while (numberOfTrips > 0)
                {

                    //Go Trip
                    orientation = Orientation.Go.ToString();

                    var passingTimesList = _servicePt.generatePassingTimesAsync(pathGo._pathNodeList, startTime);

                    var tripDto = await _service.AddLineAsync(dto, dto.pathNameGo, passingTimesList, orientation);
                    createdTrips.Add(tripDto);

                    //Return Trip
                    var endTime = passingTimesList.Last().time;

                    startTime = endTime;

                    orientation = Orientation.Return.ToString();

                    var passingTimesListReturn = _servicePt.generatePassingTimesAsync(pathReturn._pathNodeList, startTime);

                    var tripDtoReturn = await _service.AddLineAsync(dto, dto.pathNameReturn, passingTimesListReturn, orientation);
                    createdTrips.Add(tripDtoReturn);

                    startTime = startTime + dto.frequency;

                    numberOfTrips = numberOfTrips - 1;
                }


                return CreatedAtAction(nameof(GetGetById), new { id = createdTrips.Last().Id }, createdTrips);
            }
            catch (Exception ex)
            {

                return BadRequest(new { Message = ex.InnerException.Message });
            }

        }
    }
}
