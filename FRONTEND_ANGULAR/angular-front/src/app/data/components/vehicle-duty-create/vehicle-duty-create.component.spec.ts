import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDutyCreateComponent } from './vehicle-duty-create.component';

describe('VehicleDutyComponent', () => {
  let component: VehicleDutyCreateComponent;
  let fixture: ComponentFixture<VehicleDutyCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleDutyCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDutyCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
