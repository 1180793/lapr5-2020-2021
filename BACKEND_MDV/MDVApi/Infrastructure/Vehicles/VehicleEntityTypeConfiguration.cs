using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Domain.Vehicles;
using MDVApi.Domain.VehicleTypes;

namespace MDVApi.Infrastructure.Vehicles
{
    internal class VehicleEntityTypeConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.HasKey(v => v.Id);
            builder.HasOne<VehicleType>(b => b.vehicleType).WithMany(v => v.Vehicles).HasForeignKey(b => b.vehicleTypeId);
            builder.HasIndex(v => v.matricula)
                   .IsUnique();
            builder.HasIndex(v => v.vin)
                   .IsUnique();
        }
    }
}