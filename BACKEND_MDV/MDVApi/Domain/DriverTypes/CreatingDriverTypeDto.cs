
namespace MDVApi.Domain.DriverTypes
{
    public class CreatingDriverTypeDto
    {
        public string name { get; set; }

        public CreatingDriverTypeDto(string name)
        {
            this.name = name;
        }
    }
}