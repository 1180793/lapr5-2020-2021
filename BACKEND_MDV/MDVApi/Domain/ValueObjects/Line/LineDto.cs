using System;

namespace MDVApi.Domain.Lines
{
    public class LineDto
    {
        public Guid Id { get; set; }
        public string matricula { get; set; }
        public string vin { get; set; }
        public string vehicleTypeName { get; set; }
        public DateTime startDate { get; set; }
        public LineDto(Guid Id, string matricula, string vin, string vehicleTypeName, DateTime startDate){

            this.Id = Id;
            this.matricula = matricula;
            this.vin = vin;
            this.vehicleTypeName = vehicleTypeName;
            this.startDate = startDate;    
        } 
    }

}