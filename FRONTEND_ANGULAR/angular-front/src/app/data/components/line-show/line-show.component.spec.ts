import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LineShowComponent } from './line-show.component';

describe('LineShowComponent', () => {
  let component: LineShowComponent;
  let fixture: ComponentFixture<LineShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LineShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LineShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
