using System;
using Xunit;
using Moq;
using MDVApi.Domain.Shared;
using MDVApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Newtonsoft.Json;
using MDVApi.Domain.WorkBlocks;
using System.Collections.Generic;
using MDVApi.Domain.Trips;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.PassingTimes;
using Microsoft.Extensions.Configuration;

namespace MDVTest
{
    public class WorkBlocksControllerTests
    {
        private readonly ITestOutputHelper _output;

        public WorkBlocksControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void PostWorkBlocks()
        {
            string name = "V 1";
            string color =  "RGB(96,02,41)";
            DateTime date = DateTime.Today;
            long duration = 500;
            long numberOfBlocks = 5;
            
            //VehicleDuties
            CreatingVehicleDutyDto cvehicleDutyDto = new CreatingVehicleDutyDto{name = name, color = color, date = date};
            VehicleDuty vehicleDuty = new VehicleDuty(cvehicleDutyDto.name, cvehicleDutyDto.color, cvehicleDutyDto.date);

            //WorkBlocks
            CreatingWorkBlockDto cWorkBlockDto = new CreatingWorkBlockDto{isCrewTravelTime = false, VehicleDutyId = vehicleDuty.Id.AsGuid(), duration = duration, numberOfBlocks =  numberOfBlocks};
            
            WorkBlock workBlock = new WorkBlock(40000, 40480, "Baltar", "Cristelo", true);

            WorkBlockDto workBlockDto = new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.startTime, workBlock.endTime, workBlock.startNode, workBlock.endNode, false, true);
            
            //Trips
            IList<Trip> tripList = new List<Trip>();
            Trip trip = new Trip(false, "Return", "Lordelo_Parada", "Path:18");
            tripList.Add(trip);
            trip = new Trip(false, "Go", "Lordelo_Parada", "Path:13");
            

            //PassingTimes
            IList<PassingTimeDto> passingTimesListR = new List<PassingTimeDto>();
            PassingTimeDto passingTimeDto = new PassingTimeDto(Guid.NewGuid(), 40000, "Baltar", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto);
            PassingTimeDto passingTimeDto2 = new PassingTimeDto(Guid.NewGuid(), 40240, "Vila Cova de Carros", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto2);
            PassingTimeDto passingTimeDto3 = new PassingTimeDto(Guid.NewGuid(), 40480, "Cristelo", true, true, trip.Id);
            passingTimesListR.Add(passingTimeDto3);

            IList<WorkBlock> wBDtoList = new List<WorkBlock>();

            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockRepositoryVehicleDuty = new Mock<IVehicleDutyRepository>();
            var mockRepositoryTrip = new Mock<ITripRepository>();
            var mockRepositoryPassingTimes = new Mock<IPassingTimeRepository>();
            var mockRepositoryWorkBlocks = new Mock<IWorkBlockRepository>();
            var mockConfiguration = new Mock<IConfiguration>();

            mockUnitOfWork.Setup(uW => uW.CommitAsync()).ReturnsAsync(1);
            mockRepositoryVehicleDuty.Setup(vD => vD.GetByIdAsync(It.IsAny<VehicleDutyId>())).ReturnsAsync(vehicleDuty);
            mockRepositoryTrip.Setup(t => t.getTripsByVehicleDuty(It.IsAny<VehicleDutyId>())).Returns(tripList);
            mockRepositoryPassingTimes.Setup(pT => pT.getPassingTimeByTripIdAsync(It.IsAny<TripId>())).ReturnsAsync(passingTimesListR);
            mockRepositoryWorkBlocks.Setup(wB => wB.AddAsync(It.IsAny<WorkBlock>())).ReturnsAsync(workBlock);
            mockRepositoryWorkBlocks.Setup(wB => wB.getWorkBlocksByVehicleDutyAsync(It.IsAny<VehicleDutyId>())).ReturnsAsync(wBDtoList);

            var VehicleDutyService = new VehicleDutyService(mockUnitOfWork.Object, mockRepositoryVehicleDuty.Object);
            var TripService = new TripService(mockUnitOfWork.Object, mockRepositoryTrip.Object);
            var PassingTimeService = new PassingTimeService(mockUnitOfWork.Object, mockRepositoryPassingTimes.Object);
            var WorkBlockService = new WorkBlockService(mockUnitOfWork.Object, mockRepositoryWorkBlocks.Object, mockConfiguration.Object);

            var controller = new WorkBlocksController(WorkBlockService, TripService, PassingTimeService, VehicleDutyService);

            // Act
            ActionResult<WorkBlockDto> actionResult = await controller.Create(cWorkBlockDto);
            _output.WriteLine(JsonConvert.SerializeObject(actionResult));
            var okResult = actionResult.Result as ObjectResult;

            var actualWorkBlock = okResult.Value as WorkBlockDto;

            _output.WriteLine("WorkBlock");
            _output.WriteLine(JsonConvert.SerializeObject(actualWorkBlock));
            _output.WriteLine(JsonConvert.SerializeObject(workBlockDto));

            // Assert
            Assert.NotNull(actionResult);
            Assert.Equal(workBlockDto.startNode, actualWorkBlock.startNode);
            Assert.Equal(workBlockDto.endNode, actualWorkBlock.endNode);
            Assert.Equal(workBlockDto.startTime, actualWorkBlock.startTime);
            Assert.Equal(workBlockDto.endTime, actualWorkBlock.endTime);

        }
    }
}
