using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.ValueObjects.PathNodes;
using MDVApi.Domain.Trips;


namespace MDVApi.Domain.PassingTimes
{
    public class PassingTimeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPassingTimeRepository _repo;

        public PassingTimeService(IUnitOfWork unitOfWork, IPassingTimeRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<PassingTimeDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<PassingTimeDto> listDto = list.ConvertAll<PassingTimeDto>(passingTime =>
                new PassingTimeDto(passingTime.Id.AsGuid(), passingTime.time, passingTime.nodeName, passingTime.isUsed, passingTime.isReliefPoint, passingTime.tripId));

            return listDto;
        }

        public async Task<PassingTime> GetByIdAsync(PassingTimeId id)
        {
            var passingTime = await this._repo.GetByIdAsync(id);

            if (passingTime == null)
                return null;
            return passingTime;
            //return new PassingTimeDto(passingTime.Id.AsGuid(), passingTime.time,passingTime.nodeName, passingTime.isUsed, passingTime.isReliefPoint, passingTime.tripId);
        }

        public async Task<PassingTimeDto> AddAsync(CreatingPassingTimeDto dto)
        {
            var passingTime = new PassingTime(dto.time, dto.nodeName, dto.isReliefPoint);

            await this._repo.AddAsync(passingTime);

            await this._unitOfWork.CommitAsync();

            return new PassingTimeDto(passingTime.Id.AsGuid(), passingTime.time, passingTime.nodeName, passingTime.isUsed, passingTime.isReliefPoint, passingTime.tripId);
        }

        public IList<PassingTime> generatePassingTimesAsync(IList<CreatingPathNodeDto> pathNodeList, long startTime)
        {
            IList<PassingTime> passingTimeslist = new List<PassingTime>();
            int addTime = 0;

            foreach (CreatingPathNodeDto pNode in pathNodeList)
            {
                addTime = addTime + pNode.duration;

                PassingTime passingTime = new PassingTime(startTime + addTime, pNode.node.name, pNode.node.isReliefPoint);

                //var res = await AddAsync(passingTime.PassingTimeToDto());

                passingTimeslist.Add(passingTime);
            }

            return passingTimeslist;
        }

        public async Task<IList<PassingTimeDto>> getAllPassingTimesForTripAsync(Guid id)
        {

            IList<PassingTimeDto> passingTimesList = new List<PassingTimeDto>();

            TripId tId = new TripId(id);
            var passingTimes = await _repo.getPassingTimeByTripIdAsync(tId);

            if (passingTimes.ElementAt(0) == null)
                throw new BusinessRuleValidationException("PassingTimes do not exist.");
            else
            {
                foreach (PassingTimeDto pTimeDto in passingTimes)
                {
                    passingTimesList.Add(pTimeDto);
                }
            }

            return sortPassingTimeListByTime(passingTimesList);
        }

        public IList<PassingTimeDto> RemoveUsedPassingTimes(IList<PassingTimeDto> passingTimesList, long endTime)
        {

            var passingTimesToRemove = passingTimesList.Where(pT => pT.time <= endTime);

            foreach (PassingTimeDto pTDto in passingTimesToRemove.ToList())
            {
                passingTimesList.Remove(pTDto);
            }

            if (passingTimesList.Count == 0)
            {
                return null;
            }
            else
                return passingTimesList;
        }

        public IList<PassingTimeDto> sortPassingTimeListByTime(IList<PassingTimeDto> list)
        {

            int n = list.Count;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (list[j].time > list[j + 1].time)
                    {
                        PassingTimeDto temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                    }
                }
            }

            return list;
        }

    }
}