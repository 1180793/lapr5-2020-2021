
class NodeDTO {

    constructor(node) {
        this._name = node.name;
        this._coordinates = node.coordinates;
        this._shortName = node.shortName;
        this._isDepot = Boolean(node.isDepot);
        this._isReliefPoint = Boolean(node.isReliefPoint);
    }

    get name() {
        return this._name;
    }

    get coordinates() {
        return this._coordinates;
    }

    get shortName() {
        return this._shortName;
    }

    get isDepot() {
        return this._isDepot;
    }

    get isReliefPoint() {
        return this._isReliefPoint;
    }

}

module.exports = NodeDTO;