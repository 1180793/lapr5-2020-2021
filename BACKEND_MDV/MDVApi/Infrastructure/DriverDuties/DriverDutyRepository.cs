using MDVApi.Domain.DriverDuties;
using MDVApi.Infrastructure.Shared;

namespace MDVApi.Infrastructure.DriverDuties
{
    public class DriverDutyRepository : BaseRepository<DriverDuty, DriverDutyId>, IDriverDutyRepository
    {

        public DriverDutyRepository(MDVApiDbContext context):base(context.DriverDuties)
        {
          
        }

    }
}