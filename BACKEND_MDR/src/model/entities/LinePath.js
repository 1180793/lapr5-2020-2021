const mongoose = require('mongoose');
const LinePathSchema = require('../../schemas/LinePathSchema').linePathSchema;

const PathRepository = require('../../repository/PathRepository');
const pathRepo = new PathRepository();

async function validatePath(pathKey) {
    return new Promise((resolve, reject) => {
        pathRepo.findByKey(pathKey, function (err, path) {
            if (err) {
                reject(err);
            }
            else {
                resolve(path);
            }
        });
    });
}

/**
 * Creates LinePath instance from JSON Object
 * @param {JSON} pathJSON 
 */
async function createLinePathFromJSON(pathJSON) {
    return new Promise(async (resolve, reject) => {
        var linePath = null;
        var LinePath = mongoose.model('LinePath', LinePathSchema);
        try {
            var pathID;
            
            // Query Path
            let foundPath = await validatePath(pathJSON._key);
            if (foundPath == null) {
                reject('Invalid Path!');
            }
            pathID = foundPath._id;

            linePath = new LinePath({
                path: pathID,
                orientation: pathJSON._orientation
            });

            resolve(linePath);
        } catch (err) {
            reject(err)
        }
    });
}

exports.createLinePathFromJSON = createLinePathFromJSON;
