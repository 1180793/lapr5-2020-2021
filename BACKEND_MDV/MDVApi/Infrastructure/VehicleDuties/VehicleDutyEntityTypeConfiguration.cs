using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDVApi.Domain.DriverDuties;

namespace MDVApi.Infrastructure.DriverDuties
{
    internal class DriverDutyEntityTypeConfiguration : IEntityTypeConfiguration<DriverDuty>
    {
        public void Configure(EntityTypeBuilder<DriverDuty> builder)
        {
            
            builder.HasKey(vD => vD.Id);
            builder.HasIndex(vD => vD.name)
                   .IsUnique();
        }
    }
}