const _ = require('lodash');
const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const UserRepository = require('../repository/UserRepository');
const validateUser = require('../schemas/UserSchema').validate;
const validateLogin = require('../schemas/UserSchema').validateLogin;
const createUserFromJSON = require('../model/entities/User').createUserFromJSON;
const generateAuthToken = require('../model/entities/User').generateAuthToken;

class AuthService {

    constructor() { }

    async registerUser(userJSON) {
        return new Promise(async (resolve, reject) => {
            var joiValidation = true;
            const userRepo = new UserRepository();
            try {
                const { error } = validateUser(userJSON);
                if (error) {
                    throw new Error(error.message);
                }
            } catch (err) {
                joiValidation = false;
                reject(`Invalid User. ${err.message}`);
                return;
            }
            const pExistingUser = new Promise((resolve, reject) => {
                userRepo.exists(userJSON.email, function (err, existingUser) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(existingUser);
                    }
                })
            });

            pExistingUser
                .then(async function (existingUser) {
                    if (existingUser == null) {
                        try {
                            var user = await createUserFromJSON(userJSON);
                            userRepo.save(user, async (error, newUser) => {
                                if (error) {
                                    reject(error)
                                } else {
                                    const token = await generateAuthToken(user)
                                    const ret = {
                                        'token': token,
                                        'user': _.pick(newUser, '_id', 'email', 'name', 'roles')
                                    }
                                    
                                    resolve(ret);
                                }
                            });
                        } catch (err) {
                            reject(err);
                        }
                    } else {
                        reject('There is already a registered user with that email!');
                    }
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    async loginUser(loginJSON) {
        return new Promise(async (resolve, reject) => {
            const userRepo = new UserRepository();
            try {
                const { error } = validateLogin(loginJSON);
                if (error) {
                    throw new Error(error.message);
                }
            } catch (err) {
                reject(`Invalid Login. ${err.message}`);
            }
            const pExistingUser = new Promise((resolve, reject) => {
                userRepo.exists(loginJSON.email, function (err, existingUser) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(existingUser);
                    }
                })
            });

            pExistingUser
                .then(async function (existingUser) {
                    if (existingUser != null) {
                        try {
                            const validPassword = await bcrypt.compare(loginJSON.password, existingUser.password);
                            if (!validPassword) {
                                reject('Invalid email or password.');
                            }

                            const token = await generateAuthToken(existingUser);
                            const ret = {
                                'token': token,
                                'user': _.pick(existingUser, '_id', 'email', 'name', 'roles')
                            }

                            resolve(ret);
                        } catch (err) {
                            reject(err);
                        }
                    } else {
                        reject('Invalid email or password.');
                    }
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

}

module.exports = AuthService;