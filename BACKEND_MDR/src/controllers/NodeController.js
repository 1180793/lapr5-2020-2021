const sendJsonResponse = require('../middleware/sendJsonResponse');
const NodeService = require('../services/NodeService');

class NodeController {

    listAll(req, res) {
        let nodeService = new NodeService();
        nodeService.findAllSorted(req.query).then((nodes) => {
            if (Array.isArray(nodes) && nodes.length) {
                sendJsonResponse(res, 200, nodes);
            } else {
                sendJsonResponse(res, 404, "Nodes not found!");
            }
        });
    }

    details(req, res) {
        let nodeService = new NodeService();
        nodeService.findByName(req.params.id).then((node) => {
            if (node == null || Object.keys(node).length === 0) {
                sendJsonResponse(res, 404, "Node not found!");
            }
            else {
                sendJsonResponse(res, 200, node);
            }
        });
    }

    create(req, res, next) {
        const nodeService = new NodeService();
        nodeService.createNode(req.body).then((node) => {
            sendJsonResponse(res, 201, node);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                sendJsonResponse(res, 400, reason);
            }
        });
    }
}

module.exports = NodeController;