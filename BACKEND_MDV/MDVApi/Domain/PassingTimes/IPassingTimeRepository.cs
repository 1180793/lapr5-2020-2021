using System.Collections.Generic;
using System.Threading.Tasks;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Trips;

namespace MDVApi.Domain.PassingTimes
{
    public interface IPassingTimeRepository: IRepository<PassingTime,PassingTimeId>
    {
        Task<IList<PassingTimeDto>> getPassingTimeByTripIdAsync(TripId TripId);
    }
}