
export class Trip {

    isEmpty: boolean;
    path: string;
    line: string;
    startTime: number; //in seconds


    constructor(isEmpty: boolean, path: string, line: string, startTime: number) {
        this.line = line;
        this.isEmpty = isEmpty;
        this.path = path;
        this.startTime = startTime;
    }

    get Line() {
        return this.line;
    }

    get IsEmpty() {
        return this.isEmpty;
    }
    get Path() {
        return this.path;
    }
    get StartTime() {
        return this.startTime;
    }


}