import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VehicleTypeMap } from 'src/app/mappers/VehicleTypeMap';
import { AuthService } from 'src/app/services/auth/auth.service';
import { VehicleTypeService } from 'src/app/services/vehicle-type/vehicle-type.service';
import { VehicleService } from 'src/app/services/vehicle/vehicle.service';
import { AppSettings } from 'src/utils/AppSettings';

import { FormControl, AbstractControl } from "@angular/forms";
import Swal from 'sweetalert2'


@Component({
  selector: 'app-vehicle-create',
  templateUrl: './vehicle-create.component.html',
  styleUrls: ['./vehicle-create.component.css']
})
export class VehicleCreateComponent implements OnInit {

  //services
  vehicleTypeService = new VehicleTypeService(this.http);
  vehicleService = new VehicleService(this.http);


  //Mappers
  vehicleTypeMappper = new VehicleTypeMap();


  //Variables
  createVehicleForm!: FormGroup
  licensePlate: string
  vin: string;
  vehicleType: string;

  //List for population
  vehicleTypeList: any;

  constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {

    const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
    if (!hasPermission) {
      Swal.fire('Error!', 'Access Denied!', 'error')
      this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
    }
  }

  ngOnInit(): void {
    this.reactiveForm();
    this.populateVehicleTypes();
  }

  reactiveForm() {
    this.createVehicleForm = this.fb.group({
      licensePlate: ['', [Validators.maxLength(200)]],
      vin: ['', Validators.maxLength(200)],
      vehicleType: ['',],

    })
  }



  async populateVehicleTypes() {
    (await this.vehicleTypeService.getVehicleTypes()).subscribe(
      value => {
        this.vehicleTypeList = value;
      },
      error => {
        Swal.fire('Error!', error.error + '!', 'error')

      }
    );


  }

  get LicensePlate() {
    return this.createVehicleForm.get('licensePlate')?.value;
  }

  get Vin() {
    return this.createVehicleForm.get('vin')?.value;
  }

  get VehicleType() {
    return this.createVehicleForm.get('vehicleType')?.value;
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.createVehicleForm.controls[controlName].hasError(errorName);
  }


  onCreateVehicle() {
    if (this.createVehicleForm.valid) {


      this.vehicleService.addVehicle(this.LicensePlate, this.Vin, this.VehicleType);

      this.createVehicleForm.reset();
    }

  }
}
