const sendJsonResponse = require('../middleware/sendJsonResponse');
const PathService = require('../services/PathService');
const LineService = require('../services/LineService');

class PathController {

    async listAll(req, res) {
        let lineService = new LineService();
        let pathService = new PathService();
        if (req.query.line == null) {
            pathService.findAllByLine(null).then((paths) => {
                sendJsonResponse(res, 200, paths)
            });
        } else {
            var line = await lineService.findID(req.query.line);
            if (line == undefined || line == null || Object.keys(line).length === 0) {
                sendJsonResponse(res, 404, "Line not found!");
            } else {
                var finalLineID = line._id;
                // console.log("🚀 ~ file: PathController.js ~ line 21 ~ PathController ~ listAll ~ line", line)
                // console.log("🚀 ~ file: PathController.js ~ line 26 ~ PathController ~ lineService.findByName ~ finalLineID", finalLineID)
                pathService.findAllByLine(finalLineID).then((paths) => {
                    sendJsonResponse(res, 200, paths)
                });
            }
        } 
    }

    details(req, res) {
        let pathService = new PathService();
        pathService.findByKey(req.params.id).then((path) => {
            if (path == null || Object.keys(path).length === 0) {
                sendJsonResponse(res, 404, "Path not found!");
            }
            else {
                sendJsonResponse(res, 200, path);
            }
        });
    }

    create(req, res, next) {
        const pathService = new PathService();
        pathService.createPath(req.body).then((path) => {
            sendJsonResponse(res, 201, path);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                sendJsonResponse(res, 400, reason);
            }
        });
    }
}

module.exports = PathController;