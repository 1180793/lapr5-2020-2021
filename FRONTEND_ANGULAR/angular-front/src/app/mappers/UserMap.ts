import { User } from '../model/model/User';

export class UserMap{
  public toDTO( user : User ) {
    return{
      firstName : user.firstName,
      lastName : user.lastName,
      email : user.email,
      password : user.password,
      role : user.role
    }
  }

  public static async toDomain(raw : any){
    return new User(raw.firstName,raw.lastName,raw.email,raw.password,raw.role);
  }
}
