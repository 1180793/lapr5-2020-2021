import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatSortable, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Line } from 'src/app/model/model/Line';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LineService } from 'src/app/services/line/line.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-line-show',
    templateUrl: './line-show.component.html',
    styleUrls: ['./line-show.component.css'],
})
export class LineShowComponent implements OnInit {
    //services
    lineService = new LineService(this.http);

    //lineList
    lineList = new Array<Line>();

    //for the table
    displayedColumns: string[] = [
        '_name',
        'color',
        'startNode',
        'endNode',
        'allowedVehicles',
        'allowedDrivers',
    ];

    dataSource: any;

    @ViewChild(MatSort, { static: false }) sort: MatSort;

    constructor(public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.populateLines();

        setTimeout(() => {
            this.initializeDataSource();

            // this.dataSource.filterPredicate = (data: Line, filter: string) => {
            //   return data.startNode == filter;
            // };
        }, 500);
    }

    initializeDataSource() {
        this.dataSource = new MatTableDataSource(this.lineList);
        this.dataSource.sort = this.sort;
    }

    async populateLines() {
        (await this.lineService.getLines()).subscribe(
            (value) => {
                this.lineList = value;
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }

    getAllowedVehicleTypes(linha: any) {
        var finalS = new String();

        linha._allowedVehicleTypes.forEach((element: any) => {
            finalS = finalS.concat(element.name.toString(), '; ');
        });

        return finalS.trim().slice(0, -1);
    }

    getAllowedDriverTypes(linha: any) {
        var finalS = new String();

        linha._allowedDriverTypes.forEach((element: any) => {
            finalS = finalS.concat(element.name.toString(), '; ');
        });

        return finalS.trim().slice(0, -1);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    onMatSortChange(sort: Sort) {
        console.log(
            '🚀 ~ file: line-show.component.ts ~ line 89 ~ LineShowComponent ~ onMatSortChange ~ sort',
            sort
        );
        console.log(
            '🚀 ~ file: line-show.component.ts ~ line 89 ~ LineShowComponent ~ onMatSortChange ~ this.sort',
            sort
        );
        this.dataSource.sort.active = sort.active;
        this.dataSource.sort.direction = sort.direction;
    }
    compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
}
