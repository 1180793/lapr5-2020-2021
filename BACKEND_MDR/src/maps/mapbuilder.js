/* -----------
Three.js + Mappa
----------- */

// Load the CSV with d3-request
let dataLoaded = false;
let nodes;
const meshes = [];
// Repositories
const NodeRepository = new NodeRepository();
const LineRepository = new LineRepository();
const PathRepository = new PathRepository();

// Materials
const material_nodes = new THREE.MeshLambertMaterial({ color: 0xff0000, side: 2, shading: THREE.FlatShading });
const material_relief = new THREE.MeshLambertMaterial({ color: 0x93c47d, side: 2, shading: THREE.FlatShading });
const material_depot = new THREE.MeshLambertMaterial({ color: 0x9900ff, side: 2, shading: THREE.FlatShading });
const material_line = new THREE.LineBasicMaterial({color : 0xfc03e8});
// Visualization Constants
const radius = 50;
const lines_geometry;

// Convert range function
const convertRange = (value, r1, r2) => {
  return (value - r1[0]) * (r2[1] - r2[0]) / (r1[1] - r1[0]) + r2[0];
};

// // Load the data (NODES,LINES,ETC...)
// d3.csv("assets/data/Meteorite_Landings.csv", (d) => {
//   return {
//     lat: d.reclat,
//     lng: d.reclong,
//     size: d['mass (g)'],
//   }
// }, (data) => {
//   nodes = data;
//   for (let i = 0; i < 100; i++) {
//     const radius = convertRange(data[i].size, [558, 60000000], [2, 15]);
//     const tube = convertRange(data[i].size, [558, 60000000], [0.4, 4]);
//     const geometry = new THREE.TorusGeometry(radius, tube, 16, 100);
//     meshes.push(new THREE.Mesh(geometry, material));
//   }

//    dataLoaded = true;
// });

//Import Nodes and Lines
nodes = NodeRepository.findAll();
lines = LineRepository.findAll();
paths = PathRepository.findAll();
  for (let i = 0; i < nodes.size(); i++) {
    var geometry;
    var material;
    //gerar marcadores dos diferentes tipos de Nodes
    if(nodes[i].isDepot){
      geometry = new THREE.geometry.SphereGeometry(radius, 32, 32);
      material = material_depot;
     } else if (nodes[i].isRelief) {
        geometry = new THREE.geometry.SphereGeometry(radius, 32, 32);
        material = material_relief;
       } else if (!nodes[i].isRelief & !nodes[i].isDepot) {
        geometry = new THREE.geometry.TorusGeometry(radius, tube, 16, 100);
        material = material_nodes;
       }
        meshes.push(new THREE.Mesh(geometry, material));
  }

  for(let j=0; j<lines.size(); j++){
    const linepaths = lines[i].getLinePath();
    var pathNodes;
    paths.forEach(pathNodes => {
    
    });
       lines_geometry=new THREE.BufferGeometry().setFromPoints(nodes); 
       const line = new THREE.Line(geometry,material_line);
       scene.add(line);
  }


   dataLoaded = true;



// Scene Configurations
const WIDTH = 640;
const HEIGHT = 580;
const VIEW_ANGLE = 45;
const ASPECT = WIDTH / HEIGHT;
const NEAR = 0.1;
const FAR = 10000;

// Scene, camera, canvas, renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
const canvas = document.getElementById("canvas");
const renderer = new THREE.WebGLRenderer({ alpha: true, canvas: canvas });

camera.position.z = 300;
scene.add(camera);
renderer.setSize(WIDTH, HEIGHT);

// Light
const light = new THREE.PointLight(0xffffff, 1.2);
light.position.set(0, 0, 6);
scene.add(light);

// API Key for Mapboxgl. Get one here:
// https://www.mapbox.com/studio/account/tokens/
const key = 'pk.eyJ1IjoiZGF2aWRmbW9udGVpcm8iLCJhIjoiY2tob3Njd2NqMDN3YzJ1bWdybHN1dDUwYSJ9.hR5kaph_oj3Pz14yNTRmWQ'

const options = {
  lat: 0,
  lng: 0,
  zoom: 2,
  pitch: 50,
  style: 'mapbox://styles/davidfmonteiro/ckhot22l9257b19qtsazx5r2w',
}

const mappa = new Mappa('MapboxGL', key);
const myMap = mappa.tileMap(options);
myMap.overlay(canvas);
myMap.onChange(update);

function update() {
  if (dataLoaded) {
    meshes.forEach((mesh, item) => {
      //desenhar nodes
      const lat = nodes[i].getCoordinate().x;
      const lng = nodes[i].getCoordinate().y;
      const pos = myMap.latLngToPixel(lat, lng);
      const vector = new THREE.Vector3();
      vector.set((pos.x / WIDTH) * 2 - 1, -(pos.y / HEIGHT) * 2 + 1, 0.5);
      vector.unproject(camera);
      const dir = vector.sub(camera.position).normalize();
      const distance = -camera.position.z / dir.z;
      const newPos = camera.position.clone().add(dir.multiplyScalar(distance));
      mesh.position.set(newPos.x, newPos.y, newPos.z);
      scene.add(mesh);
      
      
    })
  }
}

// Animate loop
const animate = () => {
  requestAnimationFrame(animate);
  if (dataLoaded) {
    meshes.forEach((mesh) => {
      mesh.rotation.x += 0.01;
      mesh.rotation.y += 0.01;
    })
  }
  renderer.render(scene, camera);
};

animate();