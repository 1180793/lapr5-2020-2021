using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MDVApi.Domain.Shared;

namespace MDVApi.Domain.VehicleDuties
{
    public interface IVehicleDutyRepository: IRepository<VehicleDuty,VehicleDutyId>
    {
        Task<List<VehicleDuty>> GetByDateAsync(DateTime date);
    }
}