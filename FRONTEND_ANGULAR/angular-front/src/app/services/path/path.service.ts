import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PathMap } from 'src/app/mappers/PathMap';
import { Line } from 'src/app/model/model/Line';
import { Path } from 'src/app/model/model/Path';
import { PathNode } from 'src/app/model/model/PathNode';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'


@Injectable({
    providedIn: 'root'
})
export class PathService {

    uri = environment.uri; // servers address
    mapper = new PathMap();

    constructor(private http: HttpClient) {
    }

    async getPaths() {
        return this.http.get<Array<any>>(`${this.uri}/paths`);
    }

    getPathsById(key: any) {
        return this.http.get(`${this.uri}/paths/${key}`);
    }

    addPath(key: string, line: Line, orientation: string, isEmpty: boolean, pathNodeList: Array<PathNode>) {
        const path: Path = new Path(key, line, orientation, isEmpty, pathNodeList);
        return this.addPathDto(path);
    }

    //addPath using Mapper
    addPathDto(path: Path) {
        return this.http.post<Path>(`${this.uri}/paths `, this.mapper.toDTO(path))
            .subscribe(
                value => {
                    Swal.fire('Success!', 'Path was successfully created!', 'success')
                },
                error => {
                    Swal.fire('Error!', error.error + '!', 'error')
                    // alert("Couldn't add the path")
                    // alert(`Server error: ${error.status} - Details: ${error.error}`)
                    // console.log(error);
                }

            )
    }


    deletePath(id: number) {
        return this.http.get('${this.uri}/paths/delete/${id}');
    }

}
