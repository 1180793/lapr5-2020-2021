const mongoose = require('mongoose');
const LineSchema = require('../schemas/LineSchema').lineSchema;
const Line = mongoose.model('Line', LineSchema);

const LinePath = require('../model/entities/LinePath');

class LineRepository {

    constructor() { }

    /**
     * Retorna uma promessa com todas as Lines
     */
    findAll() {
        return Line
            .find()
            .populate('startNode', 'name, shortName -_id')
            .populate('endNode', 'name, shortName -_id')
            .populate('allowedVehicleTypes', 'name -_id')
            .populate('allowedDriverTypes', 'name -_id')
            .populate('allowedDriverTypes', 'name -_id')
            .populate('linePaths', 'path orientation')
            .populate('linePaths.path', 'key -_id')
            .exec();
    }

    /**
     * Retorna uma promessa com todos as Lines ordenados
     * @param {(err,item)} cb 
     */
    findAllSorted(sortParameter, filterName, cb) {
        if (sortParameter != null && ((sortParameter == 'name') || sortParameter == 'id')) {
            if (filterName != null) {
                return Line
                    .find({
                        name: {
                            '$regex': filterName,
                            '$options': 'i'
                        }
                    })
                    .populate('startNode', 'shortName -_id')
                    .populate('endNode', 'shortName -_id')
                    .populate('allowedVehicleTypes', 'name -_id')
                    .populate('allowedDriverTypes', 'name -_id')
                    .populate('linePaths', 'path orientation')
                    .populate('linePaths.path', 'key -_id')
                    .sort({ name: 1 })
                    .exec(cb);
            } else {
                return Line
                    .find()
                    .populate('startNode', 'shortName -_id')
                    .populate('endNode', 'shortName -_id')
                    .populate('allowedVehicleTypes', 'name -_id')
                    .populate('allowedDriverTypes', 'name -_id')
                    .populate('linePaths', 'path orientation')
                    .populate('linePaths.path', 'key -_id')
                    .sort({ name: 1 })
                    .exec(cb);
            }
        } else {
            if (filterName != null) {
                return Line
                    .find({
                        name: {
                            '$regex': filterName,
                            '$options': 'i'
                        }
                    })
                    .populate('startNode', 'shortName -_id')
                    .populate('endNode', 'shortName -_id')
                    .populate('allowedVehicleTypes', 'name -_id')
                    .populate('allowedDriverTypes', 'name -_id')
                    .populate('linePaths', 'path orientation')
                    .populate('linePaths.path', 'key -_id')
                    .exec(cb);
            } else {
                return Line
                    .find()
                    .populate('startNode', 'shortName -_id')
                    .populate('endNode', 'shortName -_id')
                    .populate('allowedVehicleTypes', 'name -_id')
                    .populate('allowedDriverTypes', 'name -_id')
                    .populate('linePaths', 'path orientation')
                    .populate('linePaths.path', 'key -_id')
                    .exec(cb);
            }
        }
    }

    /**
     * Encontra uma Line por Name
     * @param {String} lineName 
     * @param {(err,item)} cb 
     */
    findByName(lineName, cb) {
        Line
            .findOne({
                name: { $regex: new RegExp("^" + lineName + "$", "i") }
            })
            .populate('startNode', 'shortName -_id')
            .populate('endNode', 'shortName -_id')
            .populate('allowedVehicleTypes', 'name -_id')
            .populate('allowedDriverTypes', 'name -_id')
            .populate('linePaths', 'path orientation')
            .populate('linePaths.path', 'key -_id')
            .exec(cb);
    }

    /**
     * Encontra o ID de uma Line
     * @param {String} lineName 
     * @param {(err,item)} cb 
     */
    findID(lineName, cb) {
        Line
            .findOne({
                name: { $regex: new RegExp("^" + lineName + "$", "i") }
            })
            .populate('startNode', 'shortName')
            .populate('endNode', 'shortName')
            .populate('allowedVehicleTypes', 'name')
            .populate('allowedDriverTypes', 'name')
            .populate('linePaths', 'path orientation')
            .populate('linePaths.path', 'key -_id')
            .exec(cb);
    }

    /**
     * Checks if Line exists
     * @param {String} lineName 
     * @param {(err,item)} cb 
     */
    exists(lineName, cb) {
        Line
            .findOne({
                name: { $regex: new RegExp("^" + lineName + "$", "i") }
            })
            .populate('startNode', 'shortName -_id')
            .populate('endNode', 'shortName -_id')
            .populate('allowedVehicleTypes', 'name -_id')
            .populate('allowedDriverTypes', 'name -_id')
            .populate('linePaths', 'path orientation')
            .populate('linePaths.path', 'key -_id')
            .exec(cb);
    }

    /**
     * Guarda uma Line
     * @param {Line} line 
     * @param {(err, savedLine)} cb 
     */
    save(line, cb) {
        // console.log('Saving line: ' + line);
        line.save(function (err, oi) {
            if (err) cb(new Error(err.message));
            else
                cb(err, oi);
        });
    }

    async addPathToLine(path, lineName, cb) {
        this.findByName(lineName, async function (err, line) {
            if (err) {
                cb(err);
            } else {
                try {
                    var linePath = await LinePath.createLinePathFromJSON(path);
                    line.linePaths.push(linePath);
                    line.save(cb);
                } catch (err) {
                    cb(err);
                }
            }
        });
    }
}

module.exports = LineRepository;