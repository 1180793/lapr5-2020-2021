const mongoose = require('mongoose');
const DriverTypeSchema = require('../../schemas/DriverTypeSchema').driverTypeSchema;

/**
 * Creates DriverType instance from JSON Object
 * @param {JSON} driverTypeJSON 
 */
function createDriverTypeFromJSON(driverTypeJSON) {
    var driverType = null;
    var DriverType = mongoose.model('DriverType', DriverTypeSchema);
    try {
        driverType = new DriverType({
            name: driverTypeJSON.name
        });
    } catch (e) {
        throw new Error(e.message);
    }
    return driverType;
}

/**
 * Creates DriverType instance from DTO
 * @param {DriverTypeDTO} driverTypeDTO 
 */
function createDriverTypeFromDTO(driverTypeDTO) {
    var driverType = null;
    var DriverType = mongoose.model('DriverType', DriverTypeSchema);
    try {
        driverType = new DriverType({
            name: driverTypeDTO.name,
        });

    } catch (e) {
        throw new Error(e.message);
    }
    return driverType;
}

exports.DriverType = mongoose.model('DriverType', DriverTypeSchema);
exports.createDriverTypeFromJSON = createDriverTypeFromJSON;
exports.createDriverTypeFromDTO = createDriverTypeFromDTO;