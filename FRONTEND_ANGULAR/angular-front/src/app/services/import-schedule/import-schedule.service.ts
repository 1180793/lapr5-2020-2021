import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})
export class ImportScheduleService {

    baseUrl = AppSettings.API_ENDPOINT;

    constructor(private http: HttpClient) {
    }

    upload(file: File): Observable<HttpEvent<any>> {
        const formData: FormData = new FormData();

        formData.append('scheduleFile', file);

        const req = new HttpRequest('POST', `${this.baseUrl}/schedule`, formData, {
            reportProgress: true,
            responseType: 'json'
        });

        Swal.fire('Importing!', 'Schedule File is being imported!', 'info')
        return this.http.request(req);
    }
}
