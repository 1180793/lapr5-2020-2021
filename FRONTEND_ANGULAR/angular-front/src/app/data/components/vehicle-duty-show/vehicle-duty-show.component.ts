import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { VehicleDuty } from 'src/app/model/model/VehicleDuty';
import { AuthService } from 'src/app/services/auth/auth.service';
import { VehicleDutyService } from 'src/app/services/vehicle-duty/vehicle-duty.service';
import * as _moment from 'moment';
import Swal from 'sweetalert2'

const moment = _moment;

export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-vehicle-duty-show',
    templateUrl: './vehicle-duty-show.component.html',
    styleUrls: ['./vehicle-duty-show.component.css'],
    providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },

        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    ]
})
export class VehicleDutyShowComponent implements OnInit {

    //services
    vehicleDutyService = new VehicleDutyService(this.http);

    vehicleDutiesList = new Array<any>();

    //for the table
    displayedColumns: string[] = [
        'name',
        'color',
        'depots',
        'date'
    ];

    dataSource: any;

    showVehicleDutyForm: FormGroup;

    constructor(public http: HttpClient, public fb: FormBuilder, private router: Router) {
    }

    ngOnInit(): void {
        this.showVehicleDutyForm = new FormGroup({
            datePicker: new FormControl(moment())
        });
        this.populateVehicleDuties();

        setTimeout(() => {
            this.initializaDataSource();
        }, 1000);

    }

    initializaDataSource() {
        this.dataSource = new MatTableDataSource(this.vehicleDutiesList);
        this.dataSource.filter = 'No Data Selected Yet';
        this.dataSource.filterPredicate = function (
            data: any,
            filter: string
        ): boolean {
            var ret = false;
            if (filter == '[]') {
                ret = true;
            } else {
                console.log('filter - ', filter)
                console.log('data - ', data)
                ret = filter.includes(data.date);
            }
            return ret;
        };
    }

    async populateVehicleDuties() {
        (await this.vehicleDutyService.getVehicleDuties()).subscribe(
            (value) => {
                this.vehicleDutiesList = value;
                // console.log(value);
            },
            (error) => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );
    }

    getVehicleDutyDateToSearch() {
        return new DatePipe('en').transform(this.showVehicleDutyForm.get('datePicker')?.value, 'yyyy/MM/dd');
    }

    onSearchVehicleDuties() {
        var datesToSearch = this.getVehicleDutyDateToSearch();
        console.log("🚀 ~ file: vehicle-duty-show.component.ts ~ line 88 ~ VehicleDutyShowComponent ~ onSearchVehicleDuties ~ datesToSearch", datesToSearch)
        this.dataSource.filter = JSON.stringify(datesToSearch);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}
