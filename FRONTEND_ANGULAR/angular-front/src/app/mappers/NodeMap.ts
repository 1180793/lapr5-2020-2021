import { Coordinate } from './../model/model/Coordinate';
import { Nodes } from './../model/model/Nodes';

export class NodeMap {

  public toDTO(node: Nodes) {
    var nodeDTO = {
      name: node.nodeName,
      shortName: node.nodeShortName,
      coordinates: {
        latitude: node.coordinates.latitude,
        longitude: node.coordinates.longitude
      },
      isDepot: node.isDepot,
      isReliefPoint: node.reliefPoint
    }
    console.log(nodeDTO);

    return nodeDTO;
  }

  public async toDomain(raw: any) {
    return new Nodes(raw.nodeName, raw.nodeShortName, raw.latitude, raw.longitude, raw.isBusDepot, raw.isReliefPoint);
  }
}
