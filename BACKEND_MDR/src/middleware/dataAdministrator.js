const config = require("config");
const Roles = require('../model/entities/Roles');

module.exports = function (req, res, next) {
    // 401 Unauthorized
    // 403 Forbidden
    if (!config.get("requiresAuth")) {
        return next();
    }
    
    if (!req.user.roles.includes(Roles.DATA_ADMINISTRATOR.id)) {
        return res.status(403).send("Access denied.");
    }

    next();
};