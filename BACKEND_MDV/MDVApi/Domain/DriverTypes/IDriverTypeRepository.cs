using System.Threading.Tasks;
using MDVApi.Domain.Shared;

namespace MDVApi.Domain.DriverTypes
{
    public interface IDriverTypeRepository: IRepository<DriverType,DriverTypeId>
    {
        Task<DriverType> getDriverTypeByNameAsync(string driverTypeName);
    }
}