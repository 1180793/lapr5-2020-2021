export class User {
  userFirstName: string;
  userLastName: string;
  userEmail: string;
  userPassword: string;
  userRole: string;

  constructor(firstName: string,lastName: string,email: string,password: string,role: string){
    this.userFirstName=firstName;
    this.userLastName=lastName;
    this.userEmail=email;
    this.userPassword=password;
    this.userRole=role;
  }

  get firstName() : string{
    return this.firstName;
  }


  get lastName() : string{
    return this.lastName;
  }

  get email() : string{
    return this.email;
  }

  get password() : string{
    return this.password;
  }

  get role() : string{
    return this.role;
  }

}
