using MDVApi.Domain.Shared;

namespace MDVApi.Domain.DriverDuties
{
    public interface IDriverDutyRepository: IRepository<DriverDuty,DriverDutyId>
    {
    }
}