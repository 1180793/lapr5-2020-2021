using System;

namespace MDVApi.Domain.Vehicles
{
    public class VehicleDto
    {
        public Guid Id { get; set; }
        public string matricula { get; set; }
        public string vin { get; set; }
        public string vehicleTypeName { get; set; }
        public DateTime startDate { get; set; }
        public VehicleDto(Guid Id, string matricula, string vin, string vehicleTypeName, DateTime startDate){

            this.Id = Id;
            this.matricula = matricula;
            this.vin = vin;
            this.vehicleTypeName = vehicleTypeName;
            this.startDate = startDate;    
        } 
    }

}