import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DriverTypeService } from 'src/app/services/driver-type/driver-type.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-driver-type-create',
    templateUrl: './driver-type-create.component.html',
    styleUrls: ['./driver-type-create.component.css']
})
export class DriverTypeCreateComponent implements OnInit {

    //Service
    driverTypeService = new DriverTypeService(this.http);

    //Variables
    createDriverTypeForm!: FormGroup
    name = ''

    constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.driverTypeForm();
    }

    //reactive form
    driverTypeForm() {
        this.createDriverTypeForm = this.fb.group({
            name: ['', [Validators.maxLength(250)]]
        })
    }

    getName() {
        return this.createDriverTypeForm.get('name')?.value;
    }

    onCreateDriverType() {
        if (this.createDriverTypeForm.valid) {
            this.driverTypeService.addDriverType(this.getName());
            this.createDriverTypeForm.reset();
        }

    }

    public hasError = (controlName: string, errorName: string) => {
        return this.createDriverTypeForm.controls[controlName].hasError(errorName);
    }

}
