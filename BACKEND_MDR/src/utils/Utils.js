const EnergySourceList = require('./EnergySourceUtils');
const OrientationList = require('./OrientationUtils');

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function validateEnergySourceByName(name) {
    var retBoolean = false;
    EnergySourceList.forEach(element => {
        var areEqual = element.name.toUpperCase() === name.toUpperCase();
        if (areEqual == true) {
            retBoolean = true;
        }
    });
    return retBoolean;
}

function getEnergySourceByParameter(parameter) {
    var retEnergy = null;
    EnergySourceList.forEach(element => {
        var areEqual = element.parameter == parameter;
        if (areEqual) {
            retEnergy = element.name;
        }
    });
    return retEnergy;
}

function validateOrientation(orientation) {
    var retBoolean = false;
    OrientationList.forEach(element => {
        var areEqual = element.toUpperCase() === orientation.toUpperCase();
        if (areEqual == true) {
            retBoolean = true;
        }
    });
    return retBoolean;
}

exports.round = round;
exports.validateEnergySourceByName = validateEnergySourceByName;
exports.getEnergySourceByParameter = getEnergySourceByParameter;
exports.validateOrientation = validateOrientation;