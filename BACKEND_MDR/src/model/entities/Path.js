const mongoose = require('mongoose');
const PathSchema = require('../../schemas/PathSchema').pathSchema;

const LineRepository = require('../../repository/LineRepository');
const lineRepo = new LineRepository();

const PathNode = require('./PathNode');
const createPathNodeFromJSON = require('./PathNode').createPathNodeFromJSON;

async function validateLine(lineKey) {
    return new Promise((resolve, reject) => {
        lineRepo.exists(lineKey, function (err, line) {
            if (err) {
                reject(err);
            }
            else {
                resolve(line);
            }
        });
    });
}

/**
 * Creates Path instance from JSON Object
 * @param {JSON} pathJSON 
 */
async function createPathFromJSON(pathJSON) {
    return new Promise(async (resolve, reject) => {
        var path = null;
        var Path = mongoose.model('Path', PathSchema);
        try {
            var pathNodeList = (pathJSON.pathNodeList != null) ? pathJSON.pathNodeList : [];
            var finalPathNodeList = [];

            // Query Line
            let foundLine = await validateLine(pathJSON.line);
            if (foundLine == null) {
                reject('Invalid Line!');
            }
            var foundLineID = foundLine._id;
            
            for (const element of pathNodeList) {
                var pathNode = await createPathNodeFromJSON(element);
                finalPathNodeList.push(pathNode);
            }

            path = new Path({
                key: pathJSON.key,
                line: foundLineID,
                isEmpty: pathJSON.isEmpty,
                pathNodeList: finalPathNodeList
            });
            //console.log("createPathFromJSON -> path", path)
            resolve(path);
        } catch (err) {
            reject(err);
        }
    });
}

exports.Path = mongoose.model('Path', PathSchema);
exports.createPathFromJSON = createPathFromJSON;