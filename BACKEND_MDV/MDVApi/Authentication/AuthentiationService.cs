
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace MDVApi.Domain.Authentication
{
    public class AuthenticationService
    {
        
        private static readonly string headerKey = "x-auth-token";
        private static readonly string authRoute = "api/auth";
        
        public AuthenticationService()
        {
           
        }

       public async Task<HttpClient> login(IConfiguration _configuration){

            try{
                
                HttpClient client = new HttpClient();

                Login login = new Login("fmadureira@gmail.com", "macaco");

                string conString = _configuration.GetConnectionString("MDR") + authRoute;

                HttpContent httpContent = new StringContent(JsonSerializer.Serialize(login), Encoding.UTF8, "application/json");


                var token = await client.PostAsync(conString, httpContent);
    
                var tokenCont = await token.Content.ReadAsStringAsync();

                Authentication auth = JsonSerializer.Deserialize<Authentication>(tokenCont);

                client.DefaultRequestHeaders.Add(headerKey, auth.token);

                return client;

            }catch(Exception e){
                Console.WriteLine("Unable to Authenticate " + e);
                return null;
            }
            
        }

    }
}