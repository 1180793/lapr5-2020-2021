export const environment = {
    production: true,
    uri_mdv: "https://mdv-lapr5.azurewebsites.net/api",
    uri: "http://51.140.27.199/mdr/api",
    mapbox: {
        accessToken: 'pk.eyJ1IjoiMTE4MDgzOCIsImEiOiJja2l4aDZteWUzdGVqMnlxanRhYjJ2djUyIn0.dvS3JyjwWWA1DQ6XYg-M1Q',
    },
    mapboxgl: {
        accessToken: 'pk.eyJ1IjoiMTE4MDgzOCIsImEiOiJja2l4aDZteWUzdGVqMnlxanRhYjJ2djUyIn0.dvS3JyjwWWA1DQ6XYg-M1Q',
    },
};
