import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Line } from 'src/app/model/model/Line';
import { LineTrip } from 'src/app/model/model/LineTrip';
import { Path } from 'src/app/model/model/Path';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LineService } from 'src/app/services/line/line.service';
import { PathService } from 'src/app/services/path/path.service';
import { TripService } from 'src/app/services/trip/trip.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-trips-create',
  templateUrl: './trips-create.component.html',
  styleUrls: ['./trips-create.component.css'],
})
export class LineTripsCreateComponent implements OnInit {
  //Services----------------------------------------------------------------------------------------------------------

  lineService = new LineService(this.http);
  pathService = new PathService(this.http);
  tripService: TripService = new TripService(this.http);

  //Variables----------------------------------------------------------------------------------------------------------

  selectedLine: Line;
  isEmpty: boolean;
  startTime: string;
  frequency: number;
  numberOfTrips: number;
  goPath: Path;
  returnPath: Path;

  //Lists---------------------------------------------------------------------------------------------------------------

  lineList = new Array<any>();
  pathList = new Array<any>();
  pathsToShow = new Array<any>();

  //Form---------------------------------------------------------------------------------------------------------------

  createTripsForm: FormGroup;
  form: FormGroup;

  //--------------------------------------------------------------------------------------------------------------------

  constructor(
    public fb: FormBuilder,
    public http: HttpClient,
    private jwtAuth: AuthService,
    private router: Router
  ) {
    const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
    if (!hasPermission) {
      Swal.fire('Error!', 'Access Denied!', 'error')
      this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
    }
    this.startTime = '00:00';
  }

  ngOnInit(): void {
    this.reactiveForm();
    this.populateLines();
    this.populatePaths();
    this.listenForLines();
    this.isEmpty = false;
  }

  //Reactive Form-------------------------------------------------------------------------------------------------------

  reactiveForm() {
    this.createTripsForm = this.fb.group({
      selectedLine: [''],
      startTime: [''],
      frequency: [''],
      numberOfTrips: [''],
      goPath: [''],
      returnPath: [''],
    });
  }

  //Populate Local Lists-------------------------------------------------------------------------------------------------

  async populateLines() {
    (await this.lineService.getLines()).subscribe(
      (value) => {
        this.lineList = value;
      },
      (error) => {
        Swal.fire('Error!', error.error + '!', 'error')
        console.log(error);
      }
    );
  }

  async populatePaths() {
    (await this.pathService.getPaths()).subscribe(
      (value) => {
        this.pathList = value;
      },
      (error) => {
        Swal.fire('Error!', error.error + '!', 'error')
        console.log(error);
      }
    );
  }

  //Aux Methods---------------------------------------------------------------------------------------------------------------

  listenForLines() {
    this.createTripsForm
      .get('selectedLine')
      .valueChanges.subscribe((liner: Line) => {
        var line: any = this.createTripsForm.get('selectedLine')?.value;
        this.selectedLine = line;
        console.log('listeline', line);
        var result = [];
        if (line != null) {
          this.pathList.forEach((path: any) => {
            if (path._line.name === line._name) {
              result.push(path);
            }
          });
        }

        this.pathsToShow = result;
      });
  }

  getPathNodeList(path: any) {
    var finalS = new String();
    path._pathNodeList.forEach((element) => {
      finalS = finalS.concat(
        element.node.shortName.toString() +
        '(' +
        element.duration +
        ',' +
        element.distance +
        ') ',
        '--> '
      );
    });

    return finalS.trim().slice(0, -3);
  }

  convertTimeInSeconds(time: string): number {
    var splitted = time.split(':');
    var hours: number = Number(splitted[0]);
    var minutes = Number(splitted[1]);
    var seconds: number = 0;

    seconds += hours * 3600;
    seconds += minutes * 60;
    return seconds;
  }

  //Get Values----------------------------------------------------------------------------------------------------------------

  getLine(): any {
    if (this.selectedLine == undefined || this.selectedLine == null) {
      return '';
    }
    return this.selectedLine;
  }

  getStartTime(): number {
    if (this.createTripsForm.get('startTime')?.value == undefined || this.createTripsForm.get('startTime')?.value == null) {
      return 0;
    }
    return this.convertTimeInSeconds(
      this.createTripsForm.get('startTime')?.value
    );
  }

  getFrequency(): number {

    if (this.createTripsForm.get('frequency')?.value == undefined || this.createTripsForm.get('frequency')?.value == null) {
      return 0;
    }
    return this.createTripsForm.get('frequency')?.value;
  }

  getNumberOfTrips() {
    if (this.createTripsForm.get('numberOfTrips')?.value == undefined || this.createTripsForm.get('numberOfTrips')?.value == null) {
      return '';
    }

    return this.createTripsForm.get('numberOfTrips')?.value;
  }

  getGoPath(): any {
    if (this.goPath == undefined || this.goPath == null) {
      return '';
    }
    return this.goPath;
  }

  getReturnPath(): any {
    if (this.returnPath == undefined || this.returnPath == null) {
      return '';
    }
    return this.returnPath;
  }

  //Create Trips--------------------------------------------------------------------------------------------------------------

  onCreateTrips() {
    if (this.createTripsForm.valid) {
      // console.log('line', this.getLine()._name);
      // console.log('startTime', this.getStartTime());
      // console.log('frequency', this.getFrequency());
      // console.log('NoTs', this.getNumberOfTrips());
      // console.log('goPath', this.getGoPath()._key);
      // console.log('returnPath', this.getReturnPath()._key);

      this.tripService.addLineTrip(
        this.isEmpty,
        this.getLine()._name,
        this.getGoPath()._key,
        this.getReturnPath()._key,
        this.getFrequency(),
        this.getNumberOfTrips(),
        this.getStartTime()
      );
      this.createTripsForm.reset();
    } else {
      Swal.fire('Error!', 'Invalid inputs for Trips creation!', 'error')
      this.createTripsForm.reset();
    }
  }
}
