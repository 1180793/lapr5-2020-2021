using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.Trips;
using MDVApi.Domain.WorkBlocks;
using MDVApi.Domain.PassingTimes;

namespace MDVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkBlocksController : ControllerBase
    {
        private readonly WorkBlockService _service;
        private readonly TripService _serviceT;
        private readonly PassingTimeService _serivcePt;
        private readonly VehicleDutyService _sericeVd;


        public WorkBlocksController(WorkBlockService service, TripService serviceT, PassingTimeService serivcePt, VehicleDutyService sericeVd/*, DriverDutyService sericeDd*/)
        {
            _service = service;
            _serviceT = serviceT;
            _serivcePt = serivcePt;
            _sericeVd = sericeVd;
        }

        // GET: api/WorkBlocks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkBlockDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/WorkBlocks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkBlockDto>> GetGetById(Guid id)
        {

            var workBlock = await _service.GetByIdAsync(new WorkBlockId(id));

            if (workBlock == null)
            {
                return NotFound();
            }

            return workBlock;
        }

        [HttpGet("vehicleDuty/{id}")]
        public async Task<ActionResult<IList<WorkBlockDto>>> GetGetByVehicleDutyId(Guid id)
        {

            var workBlock = await _service.getWorkBlocksByVehicleDuty(new VehicleDutyId(id));

            if (workBlock == null)
            {
                return NotFound();
            }

            return Ok(workBlock);
        }

        // POST: api/WorkBlocks
        [HttpPost]
        public async Task<ActionResult<WorkBlockDto>> Create(CreatingWorkBlockDto dto)
        {

            try
            {
                 IList<WorkBlockDto> workBlockA = new List<WorkBlockDto>();

                //Get VehicleDuty
                VehicleDuty vehicleDuty = await _sericeVd.GetVehicleDutyByIdAsync(new VehicleDutyId(dto.VehicleDutyId));

                if (vehicleDuty == null)
                    return BadRequest("Invalid Vehicle Duty.");

                //Get all Trips for vehicleDuty
                var tripIds = _serviceT.getTripIdsByVehicleDuty(vehicleDuty.Id);

                foreach (Guid id in tripIds)
                {
                   
                    //Get all PassingTimes for the given Trip
                    IList<PassingTimeDto> passingTimesList = await _serivcePt.getAllPassingTimesForTripAsync(id);
                    
                    //Get workblocks for same vDuty
                    var workBlocks = await _service.getWorkBlocksByVehicleDuty(vehicleDuty.Id);

                    if (_service.isNull(workBlocks) == false)
                    {
                        long endT = workBlocks.Last().endTime;

                        passingTimesList = _serivcePt.RemoveUsedPassingTimes(passingTimesList, endT);

                        if (passingTimesList == null)
                        {
                            return BadRequest("WorkBlocks Completed.");
                        }
                    }

                    var createdWorkBlocks = _service.generateWorkBlocks(dto, passingTimesList, vehicleDuty);                   

                    foreach (WorkBlockAddDto wB in createdWorkBlocks)
                    {
                        var workBlock = await _service.AddAsync(wB, vehicleDuty);
                        workBlockA.Add(workBlock);
                    }

                }

                return CreatedAtAction(nameof(GetGetById), new { id = workBlockA.Last().Id }, workBlockA.Last());
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }

        }

    }
}
