using System;
using System.Collections.Generic;
using MDVApi.Domain.ValueObjects.PathNodes;

namespace MDVApi.Domain.ValueObjects.Paths
{
    public class Path
    {
        public Boolean isEmpty { get; set; }
        public IList<PathNode> pathNodeList { get; set; }
        
        public Path(Boolean isEmpty){

            this.isEmpty = isEmpty;
            //this.pathNodeList = pathNodeList;
            
        } 
    }

}