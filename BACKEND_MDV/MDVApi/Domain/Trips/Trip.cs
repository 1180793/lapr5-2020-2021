using System;
using MDVApi.Domain.PassingTimes;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.WorkBlocks;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Domain.Trips
{
    public class Trip : Entity<TripId>, IAggregateRoot
    {
        public Boolean isEmpty { get; set; }
        public string orientation { get; set; } 
        public string lineName { get; set; } 
        public string pathName { get; set; } 
        public Boolean isGenerated { get; set; }
        public IList<PassingTime> passingTimeList { get; set; }
        public IList<VehicleDuty> VehicleDuties  {get; set; }
    
        public Trip(Boolean isEmpty, string orientation, string lineName, string pathName){

            this.Id = new TripId(Guid.NewGuid());
            this.isEmpty = isEmpty;
            this.orientation = orientation;
            this.lineName = lineName;
            this.pathName = pathName;
            this.isGenerated = false;
        } 

    }
}