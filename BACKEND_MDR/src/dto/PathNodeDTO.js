
class PathNodeDTO {
    
    constructor(pathNode) {
        this._node = pathNode.node;
        this._duration = pathNode.duration;
        this._distance = pathNode.distance;
    }

    get node() {
        return this._node;
    }

    get duration() {
        return this._duration;
    }

    get distance() {
        return this._distance;
    }
}

module.exports = PathNodeDTO;