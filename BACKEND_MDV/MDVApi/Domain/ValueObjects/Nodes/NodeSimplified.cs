
namespace MDVApi.Domain.ValueObjects.Nodes
{
    public class NodeSimplified
    {
        public string name { get; set; }
        public string shortName { get; set; }
        public bool isDepot { get; set; }
        public bool isReliefPoint { get; set; }
    
    }
}