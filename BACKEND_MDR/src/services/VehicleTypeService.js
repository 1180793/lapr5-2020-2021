const VehicleTypeRepository = require('../repository/VehicleTypeRepository');
const VehicleTypeDTO = require('../dto/VehicleTypeDTO');
const validateVehicleType = require('../schemas/VehicleTypeSchema').validate;
const createVehicleTypeFromJSON = require('../model/entities/VehicleType').createVehicleTypeFromJSON;

class VehicleTypeService {

    constructor() { }

    async createVehicleType(vehicleTypeJSON) {
        return new Promise(async (resolve, reject) => {
            const vehicleTypeRepo = new VehicleTypeRepository();
            try {
                const { error } = validateVehicleType(vehicleTypeJSON);
                if (error) {
                    throw new Error(error.message);
                } else {
                    var vehicleTypeValidationDTO = new VehicleTypeDTO(vehicleTypeJSON);
                }
            } catch (err) {
                reject(`Invalid Vehicle Type. ${err.message}`);
            }
            if (vehicleTypeValidationDTO == null) {
                reject("Invalid Vehicle Type.");
            } else {
                // console.log(vehicleTypeValidationDTO);
                try {
                    const pExistingVehicleType = new Promise((resolve, reject) => {
                        vehicleTypeRepo.exists(vehicleTypeJSON.name, function (err, existingVehicleType) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(existingVehicleType);
                            }
                        })
                    });

                    pExistingVehicleType
                        .then(function (existingVehicleType) {
                            if (existingVehicleType == null) {
                                const vehicleTypeToSave = createVehicleTypeFromJSON(vehicleTypeJSON);
                                vehicleTypeRepo.save(vehicleTypeToSave, error => {
                                    if (error) {
                                        reject(error)
                                    } else {
                                        resolve(vehicleTypeValidationDTO);
                                    }
                                });
                            } else {
                                reject('Repeated Vehicle Type!');
                            }
                        })
                        .catch((reason) => {
                            reject(reason)
                        });
                } catch (err) {
                    reject(err);
                }
            }
        });
    }

    async findAll() {
        return new Promise(async (resolve, reject) => {
            const vehicleTypeRepo = new VehicleTypeRepository();
            vehicleTypeRepo.findAll().then(vehicleTypes => {
                var lstVehicleTypesDTO = [];
                vehicleTypes.forEach((element) => {
                    let vehicleTypeDTO = new VehicleTypeDTO(element);
                    lstVehicleTypesDTO.push(vehicleTypeDTO);
                });
                resolve(lstVehicleTypesDTO);
            });
        });
    }
    
    async findByName(name) {
        return new Promise(async (resolve, reject) => {
            const vehicleTypeRepo = new VehicleTypeRepository();
            vehicleTypeRepo.findByName(name, (err, vehicleType) => {
                if (vehicleType != null) {
                    const vehicleTypeDTO = new VehicleTypeDTO(vehicleType);
                    resolve(vehicleTypeDTO);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async validateVehicleType(vehicleTypeName) {
        return new Promise((resolve, reject) => {
            const vehicleTypeRepo = new VehicleTypeRepository();
            vehicleTypeRepo.exists(vehicleTypeName, function (err, vehicleType) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(vehicleType);
                }
            });
        });
    }

}

module.exports = VehicleTypeService;