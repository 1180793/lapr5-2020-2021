using System.Collections.Generic;
using MDVApi.Domain.ValueObjects.LinePaths;

namespace MDVApi.Domain.ValueObjects.Lines
{
    public class CreatingLineDto
    {
        public string _name { get; set; }
        public string _color { get; set; }
        public Dictionary<string, string> _startNode { get; set; }
        public Dictionary<string, string> _endNode { get; set; }
        public IList<string> _allowedVehicleTypes { get; set; }
        public IList<string> _allowedDriverTypes { get; set; }
        public IList<CreatingLinePathDto> _linePaths { get; set; }

    }

}