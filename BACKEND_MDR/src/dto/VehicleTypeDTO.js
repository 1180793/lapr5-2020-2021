class VehicleTypeDTO {

    constructor(vehicleType) {
        this._name = vehicleType.name;
        this._autonomy = Number(vehicleType.autonomy);
        this._cost = Number(vehicleType.cost);
        this._averageSpeed = Number(vehicleType.averageSpeed);
        this._energySource = vehicleType.energySource;
        this._consumption = Number(vehicleType.consumption);
        this._emissions = Number(vehicleType.emissions);
    }

    get name() {
        return this._name;
    }

    get autonomy() {
        return this._autonomy;
    }
    
    get cost() {
        return this._cost;
    }
    
    get averageSpeed() {
        return this._averageSpeed;
    }
    
    get energySource() {
        return this._energySource;
    }
    
    get consumption() {
        return this._consumption;
    }
    
    get emissions() {
        return this._emissions;
    }

}

module.exports = VehicleTypeDTO;