using MDVApi.Domain.ValueObjects.Paths;

namespace MDVApi.Domain.ValueObjects.LinePaths
{
    public class CreatingLinePathDto
    {
        public PathSimplified path { get; set; }
        public string orientation { get; set; }
    
    }

}