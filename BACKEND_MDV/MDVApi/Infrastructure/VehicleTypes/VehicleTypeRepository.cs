using System;
using System.Threading.Tasks;
using MDVApi.Domain.VehicleTypes;
using MDVApi.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace MDVApi.Infrastructure.VehicleTypes
{
    public class VehicleTypeRepository : BaseRepository<VehicleType, VehicleTypeId>,IVehicleTypeRepository
    {

        private readonly DbSet<VehicleType> _objs;

        public VehicleTypeRepository(MDVApiDbContext context):base(context.VehicleTypes)
        {
           _objs = context.VehicleTypes ?? throw new ArgumentNullException(nameof(context.VehicleTypes));
        }

        public async Task<VehicleType> getVehicleTypeByNameAsync(string name)
        {
           return await _objs.Where(x => name.Equals(x.name)).FirstOrDefaultAsync();
        }
       
    }
}