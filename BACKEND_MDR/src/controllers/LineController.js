const sendJsonResponse = require('../middleware/sendJsonResponse');
const LineService = require('../services/LineService');

class LineController {

    listAll(req, res) {
        let lineService = new LineService();
        lineService.findAllSorted(req.query).then((lines) => {
            if (Array.isArray(lines) && lines.length) {
                sendJsonResponse(res, 200, lines);
            } else {
                sendJsonResponse(res, 404, "Lines not found!");
            }
        });
    }

    details(req, res) {
        let lineService = new LineService();
        lineService.findByName(req.params.id).then((line) => {
            if (line == null || Object.keys(line).length === 0) {
                sendJsonResponse(res, 404, "Line not found!");
            }
            else {
                sendJsonResponse(res, 200, line);
            }
        });
    }

    create(req, res, next) {
        const lineService = new LineService();
        lineService.createLine(req.body).then((line) => {
            sendJsonResponse(res, 201, line);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                sendJsonResponse(res, 400, reason);
            }
        });
    }
}

module.exports = LineController;