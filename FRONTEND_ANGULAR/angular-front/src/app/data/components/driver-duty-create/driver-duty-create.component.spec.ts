import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverDutyCreateComponent } from './driver-duty-create.component';

describe('DriverDutyCreateComponent', () => {
  let component: DriverDutyCreateComponent;
  let fixture: ComponentFixture<DriverDutyCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverDutyCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverDutyCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
