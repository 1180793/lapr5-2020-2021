const mongoose = require('mongoose');
const Joi = require('joi');

const validateOrientation = require('../utils/Utils').validateOrientation;

// Persistence Schema
const linePathSchema = new mongoose.Schema({
    path: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Path',
        required: true
    },
    orientation: {
        type: String,
        validate: {
            validator: validateOrientation,
            message: props => `${props.value} is not a valid Orientation!`
        },
        required: true
    }
}, { retainKeyOrder: true, _id: false });

const joiValidateOrientation = (value, helpers) => {
    var isValid = validateOrientation(value);
    if (!isValid) {
        return helpers.error("any.invalid");
    } else {
        return value;
    }
};

// Joi Schema
const joiLinePathSchema = Joi.object({
    key: Joi.string()
        .required(),
    path: Joi.string()
        .required(),
    orientation : Joi.string()
        .required()
        .custom(joiValidateOrientation, "Orientation Validation")
});

// Validate
function validateLinePath(linePath) {
    return joiLinePathSchema.validate(linePath);
}

exports.joiLinePathSchema = joiLinePathSchema;
exports.linePathSchema = linePathSchema;
exports.validate = validateLinePath;
