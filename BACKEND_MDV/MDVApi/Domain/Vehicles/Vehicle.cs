using System;
using MDVApi.Domain.VehicleTypes;
using MDVApi.Domain.Shared;
using System.ComponentModel.DataAnnotations.Schema;

namespace MDVApi.Domain.Vehicles
{
    public class Vehicle : Entity<VehicleId>, IAggregateRoot
    {
        public string matricula { get; set; }
        public string vin { get; set; }
        public VehicleTypeId vehicleTypeId { get; set; }
        [ForeignKey("vehicleTypeId")]
        public VehicleType vehicleType { get; set; }
        public DateTime startDate { get; set; }
        public Vehicle(string matricula, string vin){

            this.Id = new VehicleId(Guid.NewGuid());
            this.matricula = matricula;
            this.vin = vin;
            this.startDate = DateTime.Now;    
        } 
    }

}