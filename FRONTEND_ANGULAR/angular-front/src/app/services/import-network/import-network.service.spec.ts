import { TestBed } from '@angular/core/testing';

import { ImportNetworkService } from './import-network.service';

describe('ImportNetworkService', () => {
    let service: ImportNetworkService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ImportNetworkService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});