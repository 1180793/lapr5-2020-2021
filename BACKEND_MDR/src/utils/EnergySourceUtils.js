// Energy Source Values
module.exports = Object.freeze([
        {
            name: "Diesel",
            parameter: 23
        }, 
        {
            name: "Gasoline",
            parameter: 24
        }, 
        {
            name: "LGP",
            parameter: 25
        },
        {
            name: "Hydrogen",
            parameter: 26
        }
    ]
);