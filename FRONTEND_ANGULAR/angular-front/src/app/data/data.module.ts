import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineCreateComponent } from './components/line-create/line-create.component';
import { NodeCreateComponent } from './components/node-create/node-create.component';
import { VehicleTypeCreateComponent } from './components/vehicle-type-create/vehicle-type-create.component';
import { DriverTypeCreateComponent } from './components/driver-type-create/driver-type-create.component';
import { PathCreateComponent } from './components/path-create/path-create.component';
import { NodeShowComponent } from './components/node-show/node-show.component';
import { PathShowComponent } from './components/path-show/path-show.component';
import { LineShowComponent } from './components/line-show/line-show.component';
import { VehicleCreateComponent } from './components/vehicle-create/vehicle-create.component';
import { DriverCreateComponent } from './components/driver-create/driver-create.component';
import { WorkblockCreateComponent } from './components/workblock-create/workblock-create.component';
import { VehicleDutyShowComponent } from './components/vehicle-duty-show/vehicle-duty-show.component';
import { LineTripsCreateComponent } from './components/trips-create/trips-create.component';
import { TripCreateComponent } from './components/trip-create/trip-create.component';
import { TripShowComponent } from './components/trip-show/trip-show.component';
import { ImportScheduleComponent } from './components/import-schedule/import-schedule.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DriverDutyCreateComponent } from './components/driver-duty-create/driver-duty-create.component';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class DataModule { }
export const dataComponents = [LineCreateComponent,
  NodeCreateComponent,
  VehicleTypeCreateComponent,
  DriverTypeCreateComponent,
  PathCreateComponent,
  NodeShowComponent,
  PathShowComponent,
  LineShowComponent,
  VehicleCreateComponent,
  TripCreateComponent,
  DriverCreateComponent,
  VehicleTypeCreateComponent,
  WorkblockCreateComponent,
  VehicleDutyShowComponent,
  LineTripsCreateComponent,
  TripShowComponent,
  ImportScheduleComponent,
  DriverDutyCreateComponent
]
