const mongoose = require('mongoose');
const Joi = require('joi');

const LinePathSchema = require('./LinePathSchema').linePathSchema;

// Persistence Schema
const lineSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    color: {
        type: String,
        required: true,
        pattern: /RGB\((\d{1,3}),[ ]*(\d{1,3}),[ ]*(\d{1,3})\)/,
    },
    startNode: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Node'
    },
    endNode: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Node'
    },
    allowedVehicleTypes: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'VehicleType'
        }]
    },
    allowedDriverTypes: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'DriverType'
        }]
    },
    linePaths: {
        type: [LinePathSchema],
        required: false
    }
});

const joiLineSchema = Joi.object({
    name: Joi.string()
        .required(),
    color: Joi.string()
        .required()
        .regex(/RGB\((\d{1,3}),[ ]*(\d{1,3}),[ ]*(\d{1,3})\)/),
    startNode: Joi.string()
        .required(),
    endNode: Joi.string()
        .required(),
    allowedVehicleTypes: Joi.array()
        .items(Joi.string()),
    allowedDriverTypes: Joi.array()
        .items(Joi.string())
}).unknown(true);

// Validate
function validateLine(line) {
    return joiLineSchema.validate(line);
}

exports.joiLineSchema = joiLineSchema;
exports.lineSchema = lineSchema;
exports.validate = validateLine;