const mongoose = require('mongoose');
const config = require('config');
const logger = require('./logging');

module.exports = function (env) {

    var db = "";
    var dbConnection = "";

    if(env === 'deployment'){
         db = config.get('db.group') + config.get('password') + config.get('db.uri');
         dbConnection = config.get('db.group') + "${password}" + config.get('db.uri');
    }else{
         db = config.get('db');
         dbConnection = config.get('db');
    }

    mongoose
        .connect(db, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => logger.info(`Connected to ${dbConnection}...`));
    require('../model/entities/Line').Line;
    mongoose.Promise = global.Promise;
}
