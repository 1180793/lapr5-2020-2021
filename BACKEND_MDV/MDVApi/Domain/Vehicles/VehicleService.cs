using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleTypes;

namespace MDVApi.Domain.Vehicles
{
    public class VehicleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleRepository _repo;
        private readonly IVehicleTypeRepository _repoVType;

        public VehicleService(IUnitOfWork unitOfWork, IVehicleRepository repo, IVehicleTypeRepository repoVehicleTypes)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._repoVType = repoVehicleTypes;
        }

        public async Task<List<VehicleDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();
            
            List<VehicleDto> listDto = list.ConvertAll<VehicleDto>(vehicle => 
                new VehicleDto(vehicle.Id.AsGuid(), vehicle.matricula, vehicle.vin, vehicle.vehicleType.name, vehicle.startDate));

            return listDto;
        }

        public async Task<VehicleDto> GetByIdAsync(VehicleId id)
        {
            var vehicle = await this._repo.GetByIdAsync(id);
            
            if(vehicle == null)
                return null;

            return new VehicleDto(vehicle.Id.AsGuid(), vehicle.matricula, vehicle.vin, vehicle.vehicleType.name, vehicle.startDate);
        } 

        public async Task<VehicleDto> AddAsync(CreatingVehicleDto dto, VehicleType vehicleType)
        {

            var vehicle = new Vehicle(dto.matricula, dto.vin);

            vehicle.vehicleType = vehicleType;

            vehicle = await this._repo.AddAsync(vehicle);

            await this._unitOfWork.CommitAsync();

            return new VehicleDto(vehicle.Id.AsGuid(), vehicle.matricula, vehicle.vin, vehicle.vehicleType.name, vehicle.startDate);
        }

        private async Task checkVehicleTypeIdAsync(VehicleTypeId vehicleTypeId)
        {
           var vehicleType = await _repoVType.GetByIdAsync(vehicleTypeId);
          
           if (vehicleType == null)
                throw new BusinessRuleValidationException("Invalid Vehicle Type Id.");
        } 

    }
}