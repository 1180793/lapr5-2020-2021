const sendJsonResponse = require('../middleware/sendJsonResponse');
const DriverTypeService = require('../services/DriverTypeService');

class DriverTypeController {

    listAll(req, res) {
        let driverTypeService = new DriverTypeService();
        driverTypeService.findAll().then((driverTypes) => {
            if (Array.isArray(driverTypes) && driverTypes.length) {
                sendJsonResponse(res, 200, driverTypes);
            } else { 
                sendJsonResponse(res, 404, "Driver Types not found!");
            }
        });
    }

    details(req, res) {
        let driverTypeService = new DriverTypeService();
        driverTypeService.findByName(req.params.id).then((driverType) => {
            if (driverType == null || Object.keys(driverType).length === 0) {
                sendJsonResponse(res, 404, "Driver Type not found!");
            }
            else {
                sendJsonResponse(res, 200, driverType);
            }
        });
    }

    create(req, res, next) {
        const driverTypeService = new DriverTypeService();
        driverTypeService.createDriverType(req.body).then((driverType) => {
            sendJsonResponse(res, 201, driverType);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                console.log(reason)
                sendJsonResponse(res, 400, reason);
            }
        });
    }
}

module.exports = DriverTypeController;