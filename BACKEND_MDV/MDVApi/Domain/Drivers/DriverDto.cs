using System;
using System.Collections.Generic;
using MDVApi.Domain.DriverTypes;

namespace MDVApi.Domain.Drivers
{
    public class DriverDto
    {
        public Guid Id { get; set; }
        public string name { get; set; }
        public DateTime birthDate { get; set; }
        public string driversLicenceNumber { get; set; }
        public DateTime driversLicenceDueDate { get; set; }
        public IList<DriverTypeId> driverTypes { get; set; } 

        public DriverDto(Guid Id, string name, DateTime birthDate, string driversLicenceNumber, DateTime driversLicenceDueDate)
        {

            this.Id = Id;
            this.name = name;
            this.birthDate = birthDate;
            this.driversLicenceNumber = driversLicenceNumber;
            this.driversLicenceDueDate = driversLicenceDueDate;
        }
    }

}