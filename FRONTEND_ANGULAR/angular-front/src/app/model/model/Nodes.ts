import { Coordinate } from './Coordinate';

export class Nodes {
  nodeName: string;
  nodeShortName: string;
  coordinates: Coordinate;
  isDepot: boolean;
  reliefPoint: boolean;

  constructor(nodeName: string, nodeShortName: string, latitude: number, longitude: number, isBusDepot: boolean, isReliefPoint: boolean) {
    this.nodeName = nodeName;
    this.nodeShortName = nodeShortName;
    this.coordinates = new Coordinate(latitude, longitude);
    this.isDepot = isBusDepot;
    this.reliefPoint = isReliefPoint;
  }

  get name(): string {
    return this.name;
  }


  get shortName(): string {
    return this.shortName;
  }

  get latitude(): number {
    return this.latitude;
  }

  get longitude(): number {
    return this.longitude;
  }

  get isBusDepot(): boolean {
    return this.isDepot;
  }

  get isReliefPoint(): boolean {
    return this.isReliefPoint;
  }
}
