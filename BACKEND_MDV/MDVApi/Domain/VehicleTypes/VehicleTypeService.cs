using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;

namespace MDVApi.Domain.VehicleTypes
{
    public class VehicleTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleTypeRepository _repo;

        public VehicleTypeService(IUnitOfWork unitOfWork, IVehicleTypeRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<VehicleTypeDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();
            
            List<VehicleTypeDto> listDto = list.ConvertAll<VehicleTypeDto>(vType => 
                new VehicleTypeDto{Id = vType.Id.AsGuid()});

            return listDto;
        }

        
        public async Task<VehicleTypeDto> GetByIdAsync(VehicleTypeId id)
        {
            var vehicleType = await this._repo.GetByIdAsync(id);
            
            if(vehicleType == null)
                return null;

            return new VehicleTypeDto {Id = vehicleType.Id.AsGuid(), name = vehicleType.name};
        }

        public async Task<VehicleTypeDto> AddAsync(CreatingVehicleTypeDto dto)
        {
            var vehicleType = new VehicleType(dto.name);

            await this._repo.AddAsync(vehicleType);

            await this._unitOfWork.CommitAsync();

            return new VehicleTypeDto {Id = vehicleType.Id.AsGuid(), name = vehicleType.name};
        }

        public async Task<VehicleType> getVehicleTypeByNameAsync(string vehicleName){

            var vehicleType = await _repo.getVehicleTypeByNameAsync(vehicleName);

            if (vehicleType == null)
                throw new BusinessRuleValidationException("Vehicle Type doesn't exist.");
            else
                return vehicleType;

        }

    }
}