

export class Driver {
    name: string;
    birthDate: string;
    driversLicenseNumber: string;
    driversLicenseDueDate: string;
    driverTypeNames: any[] = [];


    constructor(name: string, birthDate: string, driversLicenseNumber: string, driversLicenseDueDate: string, driverTypes: any[]) {
        this.name = name;
        this.birthDate = birthDate;
        this.driversLicenseDueDate = driversLicenseDueDate;
        this.driversLicenseNumber = driversLicenseNumber;
        if (driverTypes != null) {
            driverTypes.forEach(element => {
                this.driverTypeNames.push({
                    "name": element
                })
            })
        }
    }

    public getdriverName() {
        return this.name;
    }
    public getbirthDate() {
        return this.birthDate;
    }
    public getDriversLicenseDueDate() {
        return this.driversLicenseDueDate;
    }
    public getDriversLicenseNumber() {
        return this.driversLicenseNumber;
    }
    public getDriverTypes() {
        return this.driverTypeNames;
    }



}