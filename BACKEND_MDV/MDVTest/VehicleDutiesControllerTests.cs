using System;
using Xunit;
using Moq;
using MDVApi.Domain.Shared;
using MDVApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Newtonsoft.Json;
using System.Collections.Generic;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.Trips;

namespace MDVTest
{
    public class VehicleDutiesControllerTests
    {
        private readonly ITestOutputHelper _output;

        public VehicleDutiesControllerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void PostVehicleDuty()
        {
            string name = "V 1";
            string color =  "RGB(96,02,41)";
            DateTime date = DateTime.Today;
            string orientation = "Go";
            string lineName = "LineTest";
            string pathName = "Path:3";
            IList<Guid> tripIds = new List<Guid>();

            //Trips
            IList<Trip> tripList = new List<Trip>();
            Trip trip = new Trip(false, orientation, lineName, pathName);
            tripList.Add(trip);
            tripIds.Add(trip.Id.AsGuid());
            
            //VehicleDuties
            CreatingVehicleDutyDto cvehicleDutyDto = new CreatingVehicleDutyDto{name = name, color = color, date = date, tripIds = tripIds};
            VehicleDuty vehicleDuty = new VehicleDuty(cvehicleDutyDto.name, cvehicleDutyDto.color, cvehicleDutyDto.date);
            vehicleDuty.Trips = tripList;
            VehicleDutyDto vehicleDutyDto = new VehicleDutyDto(vehicleDuty.Id.AsGuid(), vehicleDuty.name, vehicleDuty.color, vehicleDuty.depots, vehicleDuty.date);

            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockRepositoryVehicleDuty = new Mock<IVehicleDutyRepository>();
            var mockRepositoryTrip = new Mock<ITripRepository>();

            mockUnitOfWork.Setup(uW => uW.CommitAsync()).ReturnsAsync(1);
            mockRepositoryVehicleDuty.Setup(d => d.AddAsync(It.IsAny<VehicleDuty>())).ReturnsAsync(vehicleDuty);
            mockRepositoryTrip.Setup(t => t.GetByIdAsync(It.IsAny<TripId>())).ReturnsAsync(trip);

            var VehicleDutyService = new VehicleDutyService(mockUnitOfWork.Object, mockRepositoryVehicleDuty.Object);
            var TripService = new TripService(mockUnitOfWork.Object, mockRepositoryTrip.Object);
            

            var controller = new VehicleDutiesController(VehicleDutyService, TripService);

            // Act
            ActionResult<VehicleDutyDto> actionResult = await controller.Create(cvehicleDutyDto);

            var okResult = actionResult.Result as ObjectResult;

            var actualDriver = okResult.Value as VehicleDutyDto;

            
            _output.WriteLine(JsonConvert.SerializeObject(actualDriver));
            _output.WriteLine(JsonConvert.SerializeObject(vehicleDutyDto));

            // Assert
            Assert.NotNull(actionResult);
            Assert.Equal(vehicleDutyDto.name, actualDriver.name);
        }
    }
}
