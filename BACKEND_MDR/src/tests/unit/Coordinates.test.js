const { expect } = require('@jest/globals');
const Coordinates = require('../../model/valueObjects/Coordinates');

test('Check if coordinate validation is passing', () => {

    var coords = new Coordinates(0, 0, 0, 0);
    expect(coords).toBeDefined;

})

test('Check if coordinate validation is failing', () => {

    expect(() => {
        var coords = new Coordinates(91, 0, 0);
    }).toThrow();

    expect(() => {
        var coords = new Coordinates(-91, 0, 0);
    }).toThrow();

    expect(() => {
        var coords = new Coordinates(0, 181, 0);
    }).toThrow();

    expect(() => {
        var coords = new Coordinates(0, -181, 0);
    }).toThrow();

    expect(() => {
        var coords = new Coordinates(0, 181, -1);
    }).toThrow();

    expect(() => {
        var coords = new Coordinates(0, 181, 9001);
    }).toThrow();
})

test('Check if creating DriverType from DTO works', () => {


    var coords = new Coordinates(0, 12, 91);





    expect(coords.latitude).toBe(0);
    expect(coords.longitude).toBe(12);
    expect(coords.elevation).toBe(91);




})