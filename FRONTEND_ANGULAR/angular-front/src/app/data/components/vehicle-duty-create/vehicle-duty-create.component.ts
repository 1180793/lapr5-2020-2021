import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ColorPickerControl, ColorsTable } from '@iplab/ngx-color-picker';
import { PathMap } from 'src/app/mappers/PathMap';
import { Path } from 'src/app/model/model/Path';
import { Trip } from 'src/app/model/model/Trip';
import { AuthService } from 'src/app/services/auth/auth.service';
import { PathService } from 'src/app/services/path/path.service';
import { TripService } from 'src/app/services/trip/trip.service';
import { VehicleDutyService } from 'src/app/services/vehicle-duty/vehicle-duty.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-vehicle-duty-create',
  templateUrl: './vehicle-duty-create.component.html',
  styleUrls: ['./vehicle-duty-create.component.css'],
})
export class VehicleDutyCreateComponent implements OnInit {

  //Services------------------------------------------------------------------------------------------------------------------

  tripService: TripService = new TripService(this.http);
  vehicleDutyService: VehicleDutyService = new VehicleDutyService(this.http);
  pathService = new PathService(this.http);

  //Variables-----------------------------------------------------------------------------------------------------------------

  name: string;
  color: string;
  date: string;
  trips: string[] = [];

  //Lists----------------------------------------------------------------------------------------------------------------------

  tripList: string[] = [];
  pathList = new Array<Path>();
  pathMap = new PathMap();

  //Form------------------------------------------------------------------------------------------------------------------------

  createVehicleDutyForm: FormGroup;

  //Table-----------------------------------------------------------------------------------------------------------------------

  selection = new SelectionModel<any>(true, []);

  dataSource = new MatTableDataSource<any>(this.tripList);

  displayedColumns: string[] = [
    'select',
    'line',
    'pathNodeList',
    'Orientation',
    'isEmpty',
  ];

  //---------------------------------------------------------------------------------------------------------------------------
  constructor(
    public http: HttpClient,
    public fb: FormBuilder,
    private jwtAuth: AuthService,
    private router: Router
  ) {
    const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
    if (!hasPermission) {
      Swal.fire('Error!', 'Access Denied!', 'error')
      this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
    }
  }

  ngOnInit(): void {
    this.reactiveForm();

    this.populateTrips();

    this.populatePaths();

    setTimeout(() => {
      this.initializeDataSource();
    }, 1000);
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  //Create Form------------------------------------------------------------------------------------------------------------------------

  reactiveForm() {
    this.createVehicleDutyForm = this.fb.group({
      name: ['', [Validators.maxLength(200)]],
      color: [''],
      date: [''],
      selectedTrips: [[]],
    });
  }

  //Populates local lists-------------------------------------------------------------------------------------------------------

  async populateTrips() {
    (await this.tripService.getTrips()).subscribe(
      (value) => {

        this.tripList = value;
      },
      (error) => {
        Swal.fire('Error!', error.error + '!', 'error')

      }
    );
  }

  async populatePaths() {
    (await this.pathService.getPaths()).subscribe(
      (value) => {

        this.pathList = value;
      },
      (error) => {
        Swal.fire('Error!', error.error + '!', 'error')
        console.log(error);
      }
    );
  }

  //Color Picker--------------------------------------------------------------------------------------------------------

  public chromeControl = new ColorPickerControl()
    .setValueFrom(ColorsTable.aquamarine)
    .hideAlphaChannel();

  //Table---------------------------------------------------------------------------------------------------------------

  initializeDataSource() {
    this.dataSource = new MatTableDataSource(this.tripList);
    this.dataSource.paginator = this.paginator;
  }

  get selectedTrips() {
    return this.selection.selected;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  //Auxiliar Methods-------------------------------------------------------------------------------------------------------

  getPathNodeList(path: any) {
    var finalS = new String();
    path._pathNodeList.forEach((element) => {
      finalS = finalS.concat(
        element.node.shortName.toString() +
        '(' +
        element.duration +
        ',' +
        element.distance +
        ') ',
        '--> '
      );
    });

    return finalS.trim().slice(0, -3);
  }

  getPathById(id: string): any {
    var ret;
    this.pathList.forEach((element: any) => {
      if (element._key == id) {
        ret = element;
      }
    });
    return ret;
  }

  getPathNodeListByID(id: string) {
    var path = this.getPathById(id);
    return this.getPathNodeList(path);
  }

  getTripOrientation(trip: any) {
    var path = this.getPathById(trip.pathName);
    console.log(this.tripList);
    console.log(path.orientation);
    return path.orientation;
  }

  //Get Values----------------------------------------------------------------------------------------------------------------

  getName() {
    return this.createVehicleDutyForm.get('name')?.value;
  }

  getColor() {
    var color = this.chromeControl.value.getRgba();
    var nC = 'RGB(' + color.getRed() + ',' + color.getGreen() + ',' + color.getBlue() + ')';

    return nC;
  }

  getDate() {
    var temp = this.createVehicleDutyForm.get('date')?.value;
    var date = new DatePipe('en').transform(temp, 'yyyy-MM-dd');
    console.log('date', date);
    return date;
  }

  getTrips() {
    var result: string[] = [];
    var trips = this.selectedTrips;
    trips.forEach((trip) => {
      result.push(trip.id);
    });
    return result;
  }

  checkIfEmpty(isEmpty: boolean) {
    if (isEmpty) {
      return 'Empty'
    }
    else {
      return 'Not Empty'
    }
  }

  //Create Vehicle Duty------------------------------------------------------------------------------------------------------

  onCreateVehicleDuty() {
    if (this.createVehicleDutyForm.valid && this.checkValues()) {
      console.log('name', this.getName());
      console.log('color', this.getColor());
      console.log('date', this.getDate());
      console.log('trips', this.getTrips());




      this.vehicleDutyService.addVehicleDuty(
        this.getName(),
        this.getColor(),
        this.getDate(),
        this.getTrips()
      );
      this.createVehicleDutyForm.reset();
      this.selection.deselect;
    } else {

      this.createVehicleDutyForm.reset();
      this.selection.deselect;
    }
  }

  checkValues() {
    if (this.getName() === undefined || this.getName() == '' || this.getName() === null) {
      Swal.fire('Error!', 'Name can\'t be empty!', 'error')
      return false;
    } if (this.getColor() === undefined || this.getColor() === null) {
      Swal.fire('Error!', 'Please select a color !', 'error')
      return false
    }
    if (this.getDate() == undefined || this.getDate() == null) {
      Swal.fire('Error!', 'Please select a date !', 'error')
      return false
    }
    if (this.getTrips() == undefined || this.getTrips() === null) {
      Swal.fire('Error!', 'Please select at least one trip!', 'error')
      return false
    }
    else {
      return true;
    }
  }
}
