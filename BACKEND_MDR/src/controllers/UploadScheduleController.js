const xml2js = require('xml2js');
var fs = require('fs');
var DOMParser = require('xmldom').DOMParser;
var path = require('path');
const jwt = require("jsonwebtoken");
const config = require("config");
var querystring = require('querystring');
var http = require('http');

const Roles = require('../model/entities/Roles')
const sendJsonResponse = require('../middleware/sendJsonResponse');

const NodeService = require('../services/NodeService');
const nodeService = new NodeService();

const VehicleTypeService = require('../services/VehicleTypeService');
const vehicleTypeService = new VehicleTypeService();

const DriverTypeService = require('../services/DriverTypeService');
const driverTypeService = new DriverTypeService();

const LineRepository = require('../repository/LineRepository');
const lineRepo = new LineRepository();
const LineService = require('../services/LineService');
const lineService = new LineService();

const PathService = require('../services/PathService');
const pathService = new PathService();

const getEnergySourceByParameter = require('../utils/Utils').getEnergySourceByParameter;

class UploadScheduleController {

    async uploadSchedule(req, res, next) {
        const token = req.header("x-auth-token");
        if (config.get("requiresAuth")) {
            if (!token) {
                return res.status(401).send("Access denied. No token provided.");
            }

            try {
                const decoded = jwt.verify(token, config.get("jwtPrivateKey"));
                req.user = decoded;
            } catch (ex) {
                res.status(400).send("Invalid token.");
            }

            if (!req.user.roles.includes(Roles.DATA_ADMINISTRATOR.id)) {
                return res.status(403).send("Access denied.");
            }
        }
        try {
            const glxFile = './receivedScheduleFile/' + req.file.originalname;
            let fileExtension = path.extname(req.file.originalname);
            console.log("Received Schedule File", glxFile);
            if (fileExtension == '.xml' || fileExtension == '.glx') {
                fs.readFile(glxFile, 'utf8', async function (err, data) {
                    const entities = new DOMParser().parseFromString(data);
                    let parser = new xml2js.Parser(
                        {
                            attrNameProcessors: [xml2js.processors.firstCharLowerCase],
                            tagNameProcessors: [xml2js.processors.firstCharLowerCase],
                            attrValueProcessors: [xml2js.processors.parseBooleans],
                            explicitRoot: false,
                            mergeAttrs: true,
                            explicitArray: false,
                        });

                    let nodeList = entities.getElementsByTagName('Node');
                    let pathList = entities.getElementsByTagName('Path');
                    let lineList = entities.getElementsByTagName('Line');

                    var tripOptions = {
                        hostname: config.get('mdvURL'),
                        port: 5000,
                        path: '/api/trips',
                        method: 'POST',
                        headers: {
                            'x-auth-token': token,
                            'Content-Type': 'application/json'
                            //     'Content-Length': Buffer.byteLength(data)
                        }
                    };

                    // WorkBlock List
                    let workBlockList = entities.getElementsByTagName('WorkBlock');

                    // Import Trips
                    let tripList = entities.getElementsByTagName('Trip');
                    let tripPromises = [];
                    let nTrips = 0;

                    for (let index = 0; index < (tripList.length); index++) {
                        var p = new Promise(function (resolve, reject) {
                            parser.parseString(tripList[index], async (err, result) => {
                                var fetchedLine = await fetchLine(parser, lineList, result.line);
                                var fetchedPath = await fetchPath(parser, pathList, result.path);
                                var firstPassingTime = result.passingTimes.passingTime[0].time;
                                var tripJSON = {
                                    isEmpty: result.isEmpty,
                                    lineName: fetchedLine,
                                    pathName: fetchedPath,
                                    startTime: firstPassingTime
                                };
                                // console.log(tripJSON);
                                var httpreq = http.request(tripOptions, function (response) {
                                    var id = '';
                                    response.setEncoding('utf8');
                                    response.on('data', function (chunk) {
                                        if (response.statusCode != 400) {
                                            id = JSON.parse(chunk).id;
                                        }
                                    });
                                    response.on('end', function () {
                                        // console.log('Status: ' + response.statusCode);
                                        // if (response.statusCode == 400) {
                                        //     console.log('JSON: ', JSON.stringify(tripJSON));
                                        // }
                                        // console.log('Message: ' + response.statusMessage);
                                        // console.log(result.key);

                                        resolve([result.key, id]);
                                    })
                                });
                                httpreq.write(JSON.stringify(tripJSON));
                                httpreq.on('error', (error) => {
                                    console.error(error)
                                })
                                httpreq.end();

                            });
                        });
                        tripPromises.push(p);
                    }

                    Promise.all(tripPromises).then((tripMap) => {
                        nTrips = tripMap.length;

                        // Vehicle Duties
                        let vehicleDutiesList = entities.getElementsByTagName('VehicleDuty');
                        let vehicleDutyPromises = [];
                        let nVehicleDuties = 0;


                        var vehicleDutyOptions = {
                            hostname: config.get('mdvURL'),
                            port: 5000,
                            path: '/api/vehicleDuties',
                            method: 'POST',
                            headers: {
                                'x-auth-token': token,
                                'Content-Type': 'application/json'
                                //     'Content-Length': Buffer.byteLength(data)
                            }
                        };

                        for (let index = 0; index < (vehicleDutiesList.length); index++) {
                            var p = new Promise(function (resolve, reject) {
                                parser.parseString(vehicleDutiesList[index], async (err, result) => {

                                    var name = result.name;
                                    var color = result.color;
                                    var date = (new Date()).toISOString().split('T')[0];
                                    var tripIds = await getTripsFromWorkBlocks(parser, result, workBlockList, tripMap);

                                    var vehicleDutyJSON = {
                                        'name': name,
                                        'color': color,
                                        'date': date,
                                        'tripIds': tripIds
                                    };
                                    // console.log(vehicleDutyJSON);
                                    var httpreq = http.request(vehicleDutyOptions, function (response) {
                                        var id = '';
                                        response.setEncoding('utf8');
                                        response.on('data', function (chunk) {
                                            if (response.statusCode != 400) {
                                                id = JSON.parse(chunk).id;
                                            }
                                        });
                                        response.on('end', function () {
                                            // console.log('Status: ' + response.statusCode);
                                            // if (response.statusCode == 400) {
                                            //     console.log('JSON: ', JSON.stringify(vehicleDutyJSON));
                                            // }
                                            // console.log('Message: ' + response.statusMessage);
                                            // console.log(result.key);

                                            resolve([result.key, id]);
                                        })
                                    });
                                    httpreq.write(JSON.stringify(vehicleDutyJSON));
                                    httpreq.on('error', (error) => {
                                        console.error(error)
                                    })
                                    httpreq.end();
                                });
                            });
                            vehicleDutyPromises.push(p);
                        }

                        Promise.all(vehicleDutyPromises).then((vehicleDutyMap) => {
                            nVehicleDuties = vehicleDutyMap.length;

                            // WorkBlocks
                            let workBlocksList = entities.getElementsByTagName('WorkBlock');
                            let workBlockPromises = [];
                            let nWorkBlocks = 0;


                            var workBlockOptions = {
                                hostname: config.get('mdvURL'),
                                port: 5000,
                                path: '/api/workblocks',
                                method: 'POST',
                                headers: {
                                    'x-auth-token': token,
                                    'Content-Type': 'application/json'
                                    //     'Content-Length': Buffer.byteLength(data)
                                }
                            };

                            for (let index = 0; index < (workBlocksList.length); index++) {
                                var p = new Promise(function (resolve, reject) {
                                    parser.parseString(workBlocksList[index], async (err, result) => {

                                        var isCrewTravelTime = result.isCrewTravelTime;
                                        var vehicleDutyId = await findVehicleDutyForWorkBlock(parser, result, vehicleDutyMap, vehicleDutiesList);
                                        var duration = result.endTime - result.startTime;
                                        var numberOfBlocks = 1;

                                        var workBlockJSON = {
                                            'isCrewTravelTime': isCrewTravelTime,
                                            'vehicleDutyId': vehicleDutyId,
                                            'duration': duration,
                                            'numberOfBlocks': numberOfBlocks
                                        };
                                        // console.log(vehicleDutyJSON);
                                        var httpreq = http.request(workBlockOptions, function (response) {
                                            var id = '';
                                            response.setEncoding('utf8');
                                            response.on('data', function (chunk) {
                                                if (response.statusCode != 400) {
                                                    id = JSON.parse(chunk).id;
                                                } else {
                                                    // console.log('CHUNK', JSON.parse(chunk));
                                                }
                                            });
                                            response.on('end', function () {
                                                // console.log('Status: ' + response.statusCode);
                                                // if (response.statusCode == 400) {
                                                //     console.log('JSON: ', JSON.stringify(workBlockJSON));
                                                // }
                                                // console.log('Message: ' + response.statusMessage);
                                                // console.log(result.key);

                                                resolve([result.key, id]);
                                            })
                                        });
                                        httpreq.write(JSON.stringify(workBlockJSON));
                                        httpreq.on('error', (error) => {
                                            console.error(error)
                                        })
                                        httpreq.end();
                                    });
                                });
                                workBlockPromises.push(p);
                            }

                            Promise.all(workBlockPromises).then((workBlocksMap) => {
                                nWorkBlocks = workBlocksMap.length;

                                // Driver Duty
                                let driverDutyList = entities.getElementsByTagName('DriverDuty');
                                let driverDutyPromises = [];
                                let nDriverDuties = 0;

                                var driverDutyOptions = {
                                    hostname: config.get('mdvURL'),
                                    port: 5000,
                                    path: '/api/driverduties',
                                    method: 'POST',
                                    headers: {
                                        'x-auth-token': token,
                                        'Content-Type': 'application/json'
                                        //     'Content-Length': Buffer.byteLength(data)
                                    }
                                };

                                for (let index = 0; index < (driverDutyList.length); index++) {
                                    var p = new Promise(function (resolve, reject) {
                                        parser.parseString(driverDutyList[index], async (err, result) => {

                                            var name = result.name;
                                            var color = result.color;
                                            var workBlockIds = await findWorkBlocksForDriverDuty(parser, result, workBlocksMap);

                                            var driverDutyJSON = {
                                                'name': name,
                                                'color': color,
                                                'workBlockIds': workBlockIds
                                            };
                                            // console.log(driverDutyJSON);
                                            var httpreq = http.request(driverDutyOptions, function (response) {
                                                var id = '';
                                                response.setEncoding('utf8');
                                                response.on('data', function (chunk) {
                                                    if (response.statusCode != 400) {
                                                        id = JSON.parse(chunk).id;
                                                    } else {
                                                        // console.log('CHUNK', JSON.parse(chunk));
                                                    }
                                                });
                                                response.on('end', function () {
                                                    // console.log('Status: ' + response.statusCode);
                                                    // if (response.statusCode == 400) {
                                                    //     console.log('JSON: ', JSON.stringify(driverDutyJSON));
                                                    // }
                                                    // console.log('Message: ' + response.statusMessage);
                                                    // console.log(result.key);

                                                    resolve([result.key, id]);
                                                })
                                            });
                                            httpreq.write(JSON.stringify(driverDutyJSON));
                                            httpreq.on('error', (error) => {
                                                console.error(error)
                                            })
                                            httpreq.end();
                                        });
                                    });
                                    driverDutyPromises.push(p);
                                }

                                Promise.all(driverDutyPromises).then((driverDutiesMap) => {
                                    nDriverDuties = driverDutiesMap.length;

                                    sendJsonResponse(res, 200, {
                                        message: 'Schedule File imported successfully.',
                                        trips: nTrips,
                                        workBlocks: nWorkBlocks,
                                        vehicleDuties: nVehicleDuties,
                                        driverDuties: nDriverDuties
                                    });
                                });
                            });
                        });
                    });
                });
            } else {
                sendJsonResponse(res, 400, { message: `Invalid File Type. '${fileExtension}'\nAllowed File Types: '.glx', '.xml'` });
            }
        } catch (err) {
            console.log("UploadScheduleController -> upload -> err", err)
            sendJsonResponse(res, 400, { message: 'Invalid File.' });
        }

    }

}

async function fetchNode(parser, nodeList, nodeKey) {
    var nodeToReturn = null;
    for (let index = 0; index < nodeList.length; index++) {
        parser.parseString(nodeList[index], async (err, result) => {
            if (nodeKey == result.key) {
                nodeToReturn = result.shortName;
            }
        });
    }
    return nodeToReturn;
}

async function fetchPath(parser, pathList, pathKey) {
    var pathToReturn = null;
    for (let index = 0; index < pathList.length; index++) {
        parser.parseString(pathList[index], async (err, result) => {
            if (pathKey == result.key) {
                pathToReturn = result.key;
            }
        });
    }
    return pathToReturn;
}

async function fetchLine(parser, lineList, lineKey) {
    var lineToReturn = null;
    for (let index = 0; index < lineList.length; index++) {
        parser.parseString(lineList[index], async (err, result) => {
            if (lineKey == result.key) {
                lineToReturn = result.name;
            }
        });
    }
    return lineToReturn;
}

async function getTripsFromWorkBlocks(parser, result, workBlockList, tripMap) {
    var workBlocks = await getWorkBlocks(result);
    var tripIds = await getTripsFromWorkBlock(parser, workBlocks, workBlockList, tripMap);
    return tripIds;
}

async function getWorkBlocks(result) {
    var workBlocks = [];
    for (let x = 0; x < Object.keys(result.workBlocks.ref).length; x++) {
        var workBlockKey = result.workBlocks.ref[x].key;
        workBlocks.push(workBlockKey);
    }
    return workBlocks;
}

async function getTripsFromWorkBlock(parser, workBlocks, workBlockList, tripMap) {
    var tripList = [];
    for (let x = 0; x < workBlockList.length; x++) {
        parser.parseString(workBlockList[x], async (err, result) => {
            if (workBlocks.includes(result.key)) {
                if (typeof result.trips.ref !== 'undefined' && result.trips.ref) {
                    for (let y = 0; y < Object.keys(result.trips.ref).length; y++) {
                        // console.log('TRIPS: ', result.trips.ref);
                        try {
                            var tripKey = result.trips.ref[y].key;
                            var tripId = await fetchTripID(tripKey, tripMap);
                            if (tripId != 'n') {
                                tripList.push(tripId);
                            }
                        } catch (err) {
                            var tripKey = result.trips.ref.key;
                            var tripId = await fetchTripID(tripKey, tripMap);
                            if (tripId != 'n') {
                                tripList.push(tripId);
                            }
                        }
                    }
                }
            }

        });
    }
    return tripList;
}

async function fetchTripID(tripKey, tripMap) {
    var retValue = 'n';
    for (let x = 0; x < tripMap.length; x++) {
        if (tripMap[x][0] == tripKey) {
            retValue = tripMap[x][1];
            if (tripMap[x][1] == '') {
                retValue = 'n';
            }
        }
    }
    return retValue;
}

async function findVehicleDutyForWorkBlock(parser, result, vehicleDutyMap, vehicleDutiesList) {
    var key = result.key;
    var returnValue = '';
    for (let x = 0; x < vehicleDutiesList.length; x++) {
        parser.parseString(vehicleDutiesList[x], async (err, vehicleDuty) => {
            if (typeof vehicleDuty.workBlocks.ref !== 'undefined' && vehicleDuty.workBlocks.ref) {
                for (let y = 0; y < Object.keys(vehicleDuty.workBlocks.ref).length; y++) {
                    var blockKey = '';
                    try {
                        blockKey = vehicleDuty.workBlocks.ref[y].key;
                    } catch (err) {
                        blockKey = vehicleDuty.workBlocks.ref.key;
                    }
                    if (blockKey == key) {
                        returnValue = vehicleDuty.key;
                    }
                }
            }
        });
    }
    var finalRet = await fetchVehicleDutyID(returnValue, vehicleDutyMap);
    return finalRet;
}

async function fetchVehicleDutyID(vehicleDutyKey, vehicleDutyMap) {
    var retValue = 'n';
    for (let x = 0; x < vehicleDutyMap.length; x++) {
        if (vehicleDutyMap[x][0] == vehicleDutyKey) {
            retValue = vehicleDutyMap[x][1];
            if (vehicleDutyMap[x][1] == '') {
                retValue = 'n';
            }
        }
    }
    return retValue;
}

async function findWorkBlocksForDriverDuty(parser, result, workBlocksMap) {
    var workBlocks = [];
    for (let x = 0; x < Object.keys(result.workBlocks.ref).length; x++) {
        var workBlockKey = result.workBlocks.ref[x].key;
        workBlocks.push(workBlockKey);
    }
    var finalWorkBlocks = [];
    for (let x = 0; x < workBlocksMap.length; x++) {
        if (workBlocks.includes(workBlocksMap[x][0])) {
            finalWorkBlocks.push(workBlocksMap[x][1]);
        }
    }
    return finalWorkBlocks;
}

module.exports = UploadScheduleController;