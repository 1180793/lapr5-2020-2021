// Constantes defined in meters

module.exports = Object.freeze({
    MAX_ELEVATION: 9000,
    MIN_ELEVATION: 0,
    MAX_LONGITUDE: 180,
    MIN_LONGITUDE: -180,
    MAX_LATITUDE: 90,
    MIN_LATITUDE: -90
});