using System;
using System.ComponentModel.DataAnnotations;

namespace MDVApi.Domain.Trips
{
    public class CreatingTripDto
    {
        public Boolean isEmpty { get; set; }
        //[Required(ErrorMessage = "Orientation is required")]
        public string orientation { get; set; }
        [Required(ErrorMessage = "Line is required")]
        public string lineName { get; set; }
        [Required(ErrorMessage = "Path is required")]
        public string pathName { get; set; }
        [Required(ErrorMessage = "Start Time is required")]
        public long startTime { get; set; }
        
        public CreatingTripDto(Boolean isEmpty, string lineName, string pathName, int startTime){

            this.isEmpty = isEmpty;
            this.lineName = lineName;
            this.pathName = pathName;
            this.startTime = startTime; 
        } 
    }
}