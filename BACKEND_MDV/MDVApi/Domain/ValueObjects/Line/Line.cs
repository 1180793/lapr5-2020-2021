using System;
using System.Collections.Generic;
using MDVApi.Domain.ValueObjects.LinePaths;

namespace MDVApi.Domain.ValueObjects.Lines
{
    public class Line
    {
        public string Name { get; set; }
        public string color { get; set; }
        //public List<LinePath> linePathList { get; set; }
        
        public Line(string Name, string color){

            this.Name = Name;
            this.color = color;
            
        } 
    }

}