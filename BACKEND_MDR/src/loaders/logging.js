const winston = require('winston');
// require('winston-mongodb');
require('express-async-errors');

var options = {
    file: {
        level: 'info',
        filename: 'logfile.log',
        handleExceptions: true,
        handleRejections: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    mongodb: {
        db: 'mongodb://localhost:27017/test',
        collection: 'log',
        level: 'info',
        options: {
            useUnifiedTopology: true
        },
        storeHost: true
    }
};

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.prettyPrint(),
        winston.format.simple()
    ),
    transports: [
        new winston.transports.File(options.file),
        // new winston.transports.MongoDB(options.mongodb)
    ],
    exitOnError: false // do not exit on handled exceptions
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        handleExceptions: true
    }));
}

process.on('unhandledRejection', (ex) => {
    console.log("UNHANDLED REJECTION");
    throw (ex);
});

module.exports = logger;