import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PathNodeMap } from 'src/app/mappers/PathNodeMap';
import { PathNode } from 'src/app/model/model/PathNode';
import { AppSettings } from 'src/utils/AppSettings';


@Injectable({
  providedIn: 'root'
})
export class PathNodeService {

  uri = AppSettings.API_ENDPOINT; // servers address
  mapper = new PathNodeMap();

  constructor(private http: HttpClient) {
  }

  getPathNodes() {
    return this.http.get('${this.uri}/pathNodes');
  }

  getPathNodesById(id: number) {
    return this.http.get('${this.uri}/pathNodes/${id}');
  }

  addPathNode(key: number, node: Node, duration: number, distance: number) {
    const pathNode = {
      key: key,
      node: node,
      duration: duration,
      distance: distance
    }
    return this.http.post('${this.uri}/pathNodes/add', pathNode);
  }

  //addPathNode using Mapper
  addPathNodeDto(pathNode: PathNode) {
    return this.http.post('${this.uri}/pathNodes/add', this.mapper.toDTO(pathNode));
  }

  deletePathNodes(id: number) {
    return this.http.get('${this.uri}/pathNodes/delete/${id}');
  }

}
