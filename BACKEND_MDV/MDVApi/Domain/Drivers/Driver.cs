using System;
using System.Collections.Generic;
using MDVApi.Domain.DriverTypes;
using MDVApi.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDVApi.Domain.Drivers
{
    public class Driver : Entity<DriverId>, IAggregateRoot
    {
        
        public string name { get; set; }
        public DateTime birthDate { get; set; }
        public string driversLicenceNumber { get; set; }
        public DateTime driversLicenceDueDate { get; set; }
        public IList<DriverType> DriverTypes { get; set; } = new List<DriverType>();

        public Driver(string name, DateTime birthDate, string driversLicenceNumber, DateTime driversLicenceDueDate)
        {

            this.Id = new DriverId(Guid.NewGuid());
            this.name = name;
            this.birthDate = birthDate;
            this.driversLicenceNumber = driversLicenceNumber;
            this.driversLicenceDueDate = driversLicenceDueDate;
        }
    }
}