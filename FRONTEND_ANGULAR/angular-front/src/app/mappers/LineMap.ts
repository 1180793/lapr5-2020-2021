import { Line } from "../model/model/Line";

export class LineMap {

    public toDTO(line: Line) {
        return {
            name: line.LineName,
            color: line.LineColor,
            startNode: line.LineStartNode,
            endNode: line.LineEndNode,
            allowedVehicleTypes: line.allowedVehicleTypes,
            allowedDriverTypes: line.LineAllowedDriverTypes,

        }
    }

    public static async toDomain(raw: any) {
        return new Line(raw.name, raw.color, raw.startNode, raw.endNode, raw.allowedVehicleTypes, raw.allowedDriverTypes);
    }

}
