namespace MDVApi.Domain.VehicleTypes
{
    public class CreatingVehicleTypeDto
    {
        public string name { get; set; }
        
        public CreatingVehicleTypeDto(string name)
        {
            this.name = name;
           
        }
    }
}