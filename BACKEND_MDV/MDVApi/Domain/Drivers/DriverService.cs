using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.DriverTypes;

namespace MDVApi.Domain.Drivers
{
    public class DriverService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDriverRepository _repo;
        private readonly IDriverTypeRepository _repoVType;

        public DriverService(IUnitOfWork unitOfWork, IDriverRepository repo, IDriverTypeRepository repoVehicleTypes)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._repoVType = repoVehicleTypes;
        }

        public async Task<List<DriverDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<DriverDto> listDto = list.ConvertAll<DriverDto>(driver =>
                new DriverDto(driver.Id.AsGuid(), driver.name, driver.birthDate, driver.driversLicenceNumber, driver.driversLicenceDueDate /*, driver.driverTypes*/));

            return listDto;
        }

        public async Task<DriverDto> GetByIdAsync(DriverId id)
        {
            var driver = await this._repo.GetByIdAsync(id);

            if (driver == null)
                return null;

            return  new DriverDto(driver.Id.AsGuid(), driver.name, driver.birthDate, driver.driversLicenceNumber, driver.driversLicenceDueDate /*, driver.driverTypes*/);

        }

        public async Task<DriverDto> AddAsync(CreatingDriverDto dto, List<DriverType> driverTypeList)
        {

            var driver = new Driver(dto.name, dto.birthDate, dto.driversLicenceNumber, dto.driversLicenceDueDate);

            foreach(DriverType dType in driverTypeList){
                driver.DriverTypes.Add(dType);    
            }
            
            await this._repo.AddAsync(driver);

            await this._unitOfWork.CommitAsync();

            return  new DriverDto(driver.Id.AsGuid(), driver.name, driver.birthDate, driver.driversLicenceNumber, driver.driversLicenceDueDate /*, driver.driverTypes*/);
        }

    }
}