using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;


namespace MDVApi.Domain.DriverDuties
{
    public class CreatingDriverDutyDto
    {
         [Required(ErrorMessage = "Driver Duty name is required")]
        public string name{ get; set; }
         [Required(ErrorMessage = "Driver Duty color is required")]
        public string color { get; set; }

        [Required(ErrorMessage = "You must select at least 1 work block")]
        public IList<Guid> workBlockIds { get; set; }

    }
}