const mongoose = require('mongoose');
const NodeSchema = require('../schemas/NodeSchema').nodeSchema;
const Node = mongoose.model('Node', NodeSchema);

class NodeRepository {

    constructor() { }

    /**
     * Retorna uma promessa com todos os Nodes
     */
    findAll() {
        return Node
            .find()
            .exec();
    }

    /**
     * Retorna uma promessa com todos os Nodes ordenados
     * @param {(err,item)} cb 
     */
    findAllSorted(sortParameter, filterName, cb) {
        if (sortParameter != null && (sortParameter == 'name')) {
            if (filterName != null) {
                return Node
                    .find({
                        name: {
                            '$regex': filterName,
                            '$options': 'i'
                        }
                    })
                    .sort({ name: 1 })
                    .exec(cb);
            } else {
                return Node
                    .find()
                    .sort({ name: 1 })
                    .exec(cb);
            }
        } else if (sortParameter != null && (sortParameter == 'id' || sortParameter == 'code' || sortParameter == 'shortName')) {
            if (filterName != null) {
                return Node
                    .find({
                        name: {
                            '$regex': filterName,
                            '$options': 'i'
                        }
                    })
                    .sort({ shortName: 1 })
                    .exec(cb);
            } else {
                return Node
                    .find()
                    .sort({ shortName: 1 })
                    .exec(cb);

            }
        } else {
            if (filterName != null) {
                return Node
                    .find({
                        name: {
                            '$regex': filterName,
                            '$options': 'i'
                        }
                    })
                    .exec(cb);
            } else {
                return Node
                    .find()
                    .exec(cb);
            }
        }
    }

    /**
     * Encontra um Node por Name
     * @param {String} nodeName 
     * @param {(err,item)} cb 
     */
    findByName(nodeName, cb) {
        Node
            .findOne({
                shortName: { $regex: new RegExp("^" + nodeName + "$", "i") }
            })
            .exec(cb);
    }

    /**
     * Checks if Node exists
     * @param {String} nodeShortName 
     * @param {(err,item)} cb 
     */
    exists(nodeShortName, cb) {
        Node
            .findOne({
                shortName: { $regex: new RegExp("^" + nodeShortName + "$", "i") }
            })
            .exec(cb);
    }

    /**
     * Guarda um Node
     * @param {Node} node 
     * @param {(err,savedNode)} cb 
     */
    save(node, cb) {
        // console.log('Saving node: ' + node);
        node.save(function (err, oi) {
            if (err) cb(new Error("Invalid Node Save"));
            else
                cb(err, oi);
        });
    }
}

module.exports = NodeRepository;