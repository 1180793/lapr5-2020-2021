using System.ComponentModel;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleDuties;
using MDVApi.Domain.DriverDuties;
using MDVApi.Domain.WorkBlocks;

namespace MDVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverDutiesController : ControllerBase
    {
        private readonly DriverDutyService _service;
        private readonly WorkBlockService _serviceWb;
        private static readonly string driverNameException = "IX_DriverDuties_name";

        public DriverDutiesController(DriverDutyService service, WorkBlockService serviceWb)
        {
            _service = service;
            _serviceWb = serviceWb;
        }

        // GET: api/Vehicle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DriverDutyDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }


        // GET: api/Vehicle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DriverDutyDto>> GetGetById(Guid id)
        {

            var driverDuty = await _service.GetByIdAsync(new DriverDutyId(id));

            if (driverDuty == null)
            {
                return NotFound();
            }

            return driverDuty;
        }

        // POST: api/Vehicle
        [HttpPost]
        public async Task<ActionResult<DriverDutyDto>> Create(CreatingDriverDutyDto dto)
        {

            try
            {
                if(dto.workBlockIds.Count == 0){
                    return BadRequest("Select At Least one workBlock");
                }
                
                var workBlocks = await _serviceWb.GetWorkBlocksForDriver(dto.workBlockIds);

                if(workBlocks == null){
                    return BadRequest("Invalid WorkBlocks");
                }

                var driverDuty = await _service.AddAsync(dto, workBlocks);

                return CreatedAtAction(nameof(GetGetById), new { id = driverDuty.Id }, driverDuty);
            }
           catch (Exception ex)
            {
                if( ex.InnerException.Message.Contains(driverNameException)){
                    return BadRequest("A driver duty with the given name is already registered.");
                }

                return BadRequest(new {Message = ex.InnerException.Message});
            }

        }

    }
}
