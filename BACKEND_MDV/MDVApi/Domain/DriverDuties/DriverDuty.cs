using System;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.WorkBlocks;

namespace MDVApi.Domain.DriverDuties
{
    public class DriverDuty : Entity<DriverDutyId>, IAggregateRoot
    {
        public string name{ get; set; }
        public string color { get; set; }
        public IList<WorkBlock> WorkBlocks{ get; set; } = new List<WorkBlock>();

        public DriverDuty(string name, string color){
            
            this.Id = new DriverDutyId(Guid.NewGuid());
            this.name = name;
            this.color = color;
        }

    }
}