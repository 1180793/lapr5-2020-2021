const UserRepository = require('../repository/UserRepository');

class UserService {

    constructor() { }

    async findUserByEmail(email) {
        return new Promise(async (resolve, reject) => {
            const userRepo = new UserRepository();
            try {
                userRepo.findByEmail(email, (err, user) => {
                    resolve(user);
                });
            } catch (err) {
                resolve(null);
            }
        });
    }

    async deleteUser(id) {
        return new Promise(async (resolve, reject) => {
            const userRepo = new UserRepository();
            try {
                userRepo.deleteUser(id, (err, user) => {
                    resolve(true);
                });
            } catch (err) {
                reject(false);
            }
        });
    }
}

module.exports = UserService;