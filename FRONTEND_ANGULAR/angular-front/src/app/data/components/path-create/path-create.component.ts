import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { LineMap } from 'src/app/mappers/LineMap';
import { Line } from 'src/app/model/model/Line';
import { Nodes } from 'src/app/model/model/Nodes';
import { PathNode } from 'src/app/model/model/PathNode';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LineService } from 'src/app/services/line/line.service';
import { NodeService } from 'src/app/services/node/node.service';
import { PathService } from 'src/app/services/path/path.service';
import { AppSettings } from 'src/utils/AppSettings';
import { MathUtils } from 'src/utils/MathUtils';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-path-create',
    templateUrl: './path-create.component.html',
    styleUrls: ['./path-create.component.styl']
})
export class PathCreateComponent implements OnInit {

    //Services 
    nodeService = new NodeService(this.http);
    lineService = new LineService(this.http);
    pathService = new PathService(this.http);

    //Mappers
    lineMap = new LineMap();

    //Form
    createPathForm: FormGroup;
    pathNodeForm: FormGroup;

    //Variables
    line: ''
    orientation: '';
    isEmpty: boolean;
    selectedNode: '';
    isFirstNode = true;

    //Lists for population
    nodeList: any;
    pathNodeList = new Array<PathNode>();

    lineList: any;
    orientationList: any;
    displayedColumns: string[];
    @ViewChild(MatTable, { static: true }) table: MatTable<any>;

    constructor(public fb: FormBuilder, public http: HttpClient, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
        this.displayedColumns = ['node', 'distance', 'time'];
        this.orientationList = ['Go', 'Return']
    }

    ngOnInit(): void {
        this.populateLines();
        this.pathCreateForm();
        this.populateNodes();
        this.createPathNodeForm()

    }

    pathCreateForm() {
        this.createPathForm = this.fb.group({
            line: ['', [Validators.maxLength(200)]],
            orientation: ['',],
            isEmpty: [false,],
        })
    }

    createPathNodeForm() {
        this.pathNodeForm = this.fb.group({
            selectedNode: ['',],
            distance: [0, [Validators.min(0)]],
            time: [0, [Validators.min(0),]],
            isFirstNode: [true,]
        })
    }

    async populateLines() {
        (await this.lineService.getLines()).subscribe(
            value => {
                this.lineList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );

    }
    async populateNodes() {
        (await this.nodeService.getNodes()).subscribe(
            value => {
                this.nodeList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')
                console.log(error);
            }
        );

    }

    public hasError = (controlName: string, errorName: string) => {
        return this.createPathForm.controls[controlName].hasError(errorName);
    }
    public hasErrorPathNode = (controlName: string, errorName: string) => {
        return this.pathNodeForm.controls[controlName].hasError(errorName);
    }

    getIsFirsNode() {
        if (this.pathNodeList.length == 0) {
            this.isFirstNode = true;
        }
        this.isFirstNode = false;
    }

    getLine() {
        return this.createPathForm.get('line')?.value;
    }



    getOrientation() {
        return this.createPathForm.get('orientation')?.value;
    }

    getTime() {
        return this.pathNodeForm.get('time')?.value;
    }

    getDistance() {
        return this.pathNodeForm.get('distance')?.value;
    }

    getIsEmpty() {
        if (this.pathNodeList.length == 0) {
            return true;
        }
        return false;
    }

    makeid() {
        var length = 30;
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }


    //retorna a lista  
    async onAddNode() {
        this.getIsFirsNode();
        var selectedNode = this.pathNodeForm.get('selectedNode')?.value;
        var pathNode, distance, time;
        var isRepetead = this.pathNodeList.find(e => e.node === selectedNode);
        if (isRepetead == undefined) {
            distance = this.getDistance();
            time = this.getTime();
            if ((distance || time) == undefined) {
                time = 0;
                distance = 0;
            }

            pathNode = new PathNode(selectedNode, time, distance);
            console.log('NAO REPETIDO')
            console.log(distance);
            console.log(time);

            this.pathNodeList.push(pathNode);
            this.table.renderRows();
        } else {
            Swal.fire('Error!', 'Repeated Node!', 'error')
        }

    }
    async onRemoveNode() {
        this.getIsFirsNode();
        this.pathNodeList.pop();
        this.table.renderRows();
        if (this.pathNodeList.length == 0) {
            this.pathNodeForm.setValue({
                selectedNode: this.selectedNode,
                distance: 0,
                time: 0,
                isFirstNode: true
            })

        }
    }

    onCreatePath() {

        if (this.createPathForm.valid && this.pathNodeForm.valid) {

            this.pathService.addPath(this.makeid(), this.getLine(), this.getOrientation(), this.getIsEmpty(), this.pathNodeList)
            this.createPathForm.reset();
            this.pathNodeForm.reset();
        }
        else {
            Swal.fire('Error!', 'Invalid Line!', 'error')
        }
    }

}
