const mongoose = require('mongoose');
const Joi = require('joi');
const Roles = require('../model/entities/Roles');

// Persistence Schema
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        minlength: 5,
        maxlength: 50
    },
    email: {
        type: String,
        minlength: 5,
        maxlength: 255,
        unique: true,
        sparse: true
    },
    password: {
        type: String,
        minlength: 5,
        maxlength: 1024
    },
    roles: {
        type: Array,
        default: [ Roles.CLIENT.id ]
    },
    isActive: {
        type: Boolean,
        default: true
    }
});

// Joi Schema
const joiUserSchema = Joi.object({
    name: Joi.string()
        .min(5)
        .max(50)
        .required(),
    email: Joi.string()
        .min(5)
        .max(255)
        .required()
        .email(),
    password: Joi.string()
        .min(5)
        .max(255)
        .required()
});

// Validate
function validateUser(user) {
    return joiUserSchema.validate(user);
}

// Joi Schema
const joiLoginSchema = Joi.object({
    email: Joi.string()
        .min(5)
        .max(255)
        .required()
        .email(),
    password: Joi.string()
        .min(5)
        .max(255)
        .required()
});

// Validate
function validateLogin(login) {
    return joiLoginSchema.validate(login);
}

exports.joiUserSchema = joiUserSchema;
exports.userSchema = userSchema;
exports.validate = validateUser;
exports.validateLogin = validateLogin;