import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LineService } from 'src/app/services/line/line.service';
import { PathService } from 'src/app/services/path/path.service';
import { AppSettings } from 'src/utils/AppSettings';
import { TripService } from 'src/app/services/trip/trip.service';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatTableDataSource } from '@angular/material/table';

import { Line } from 'src/app/model/model/Line';
import { SelectionModel } from '@angular/cdk/collections';
import { Path } from 'src/app/model/model/Path';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-trip-create',
  templateUrl: './trip-create.component.html',
  styleUrls: ['./trip-create.component.css']
})
export class TripCreateComponent implements OnInit {

  //services
  pathService = new PathService(this.http);
  lineService = new LineService(this.http);
  tripService = new TripService(this.http);

  //Variables
  isEmpty: boolean = false;
  startTime: string;
  path: string;

  //lists
  pathList = new Array<Path>();
  pathListTable = new Array<Path>();
  lineList = new Array<Line>();

  //for the table
  displayedColumns: string[] = ['select', 'line', 'pathNodeList'];
  dataSource: any;
  form: FormGroup;
  createTripForm: FormGroup;

  lineNamesList: any;

  selection = new SelectionModel<any>(false, []);

  dataSource1 = new MatTableDataSource<Path>(this.pathListTable);

  reactiveForm() {
    this.form = this.fb.group({
      lineNames: [[]],
    });
  }
  tripCreateForm() {
    this.createTripForm = this.fb.group({
      isEmpty: [false,],
      path: ['',],
      startTime: ['',],

    })
  }

  constructor(public http: HttpClient, public fb: FormBuilder, private jwtAuth: AuthService, private router: Router) {
    const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
    if (!hasPermission) {
      Swal.fire('Error!', 'Access Denied!', 'error')
      this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
    }

    this.isEmpty = false;
    this.startTime = '00:00';
  }

  ngOnInit(): void {
    this.populateLines();
    this.populatePaths();

    this.reactiveForm();
    this.tripCreateForm();

    setTimeout(() => {
      this.initializeDataSource();
    }, 1000);
  }

  //returns the selected line name
  get selectedLineName() {
    return this.form.get('lineNames').value;
  }

  get IsEmpty(): boolean {
    if (!this.createTripForm.get('isEmpty')?.value == undefined) {
      this.isEmpty = false;
      return this.isEmpty;
    }
    return this.createTripForm.get('isEmpty')?.value;
  }

  get StartTime() {
    return this.createTripForm.get('startTime')?.value;
  }

  initializeDataSource() {
    this.dataSource = new MatTableDataSource(this.pathList);
    this.dataSource.filterPredicate = function (
      data: any,
      filter: string
    ): boolean {
      var ret = false;
      if (filter == '[]') {
        ret = true;
      } else {
        ret = filter.includes(data._line.name);
      }
      return ret;
    };
  }

  async populateLines() {
    (await this.lineService.getLines()).subscribe(
      (value) => {
        this.lineList = value;
      },
      (error) => {
        Swal.fire('Error!', error.error + '!', 'error')

      }
    );
  }

  async populatePaths() {
    (await this.pathService.getPaths()).subscribe(
      (value) => {
        this.pathList = value;
      },
      (error) => {
        Swal.fire('Error!', error.error + '!', 'error')

      }
    );
  }

  getLinesToSearch() {
    return this.form.get('lineNames').value;
  }

  onSearchLines() {
    var linesToSearch = this.getLinesToSearch();
    this.dataSource.filter = JSON.stringify(linesToSearch);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPathNodeList(path: any) {
    var finalS = new String();
    path._pathNodeList.forEach((element: any) => {
      finalS = finalS.concat(
        element.node.shortName.toString() +
        '(' +
        element.duration +
        ',' +
        element.distance +
        ') ',
        '--> '
      );
    });

    return finalS.trim().slice(0, -3);
  }

  getPathFromTable() {
    if (this.selection.selected[0] == undefined) {
      return 0;
    } else {
      return this.selection.selected[0]._key;
    }

  }

  public hasError = (controlName: string, errorName: string) => {
    return this.createTripForm.controls[controlName].hasError(errorName);
  }

  getIsEmptyChecked(checkBox: MatCheckbox) {
    if (this.isEmpty == false && checkBox.checked) {
      this.isEmpty = true;
    }
    this.isEmpty = false;

  }

  convertTimeInSeconds(time: string) {

    if (time == null || time == undefined) {
      return 0;
    } else {
      var splitted = time.split(":")
      var hours: number = Number(splitted[0]);
      var minutes = Number(splitted[1]);
      var seconds: number = 0;

      seconds += hours * 3600;
      seconds += minutes * 60;
      return seconds;
    }


  }

  onCreateTrip() {

    if (this.createTripForm.valid && this.checkIfNull()) {
      console.log(this.IsEmpty);
      console.log(this.StartTime);
      console.log(this.selectedLineName);
      console.log(this.getPathFromTable());

      console.log('VALID FORM')
      this.tripService.addTrip(this.IsEmpty, this.selectedLineName, this.getPathFromTable(), this.convertTimeInSeconds(this.StartTime));

      this.createTripForm.reset();

    }

  }
  checkIfNull() {
    if (this.StartTime == null || this.StartTime == undefined) {
      Swal.fire('Error!', 'Start Time cannot be null!', 'error')
      return false;
    } if (this.selectedLineName == null || this.selectedLineName == undefined) {
      Swal.fire('Error!', 'You must choose a line!', 'error')
      return false;
    } if (this.getPathFromTable() == null || this.getPathFromTable() == undefined) {
      Swal.fire('Error!', 'You must choose at least one path!', 'error')
      return false;
    } else {
      return true;
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource1.data.forEach(row => this.selection.select(row));
  }



}
