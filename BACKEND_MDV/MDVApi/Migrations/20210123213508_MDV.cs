﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDVApi.Migrations
{
    public partial class MDV : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DriverDuties",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    name = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    color = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverDuties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Drivers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    birthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    driversLicenceNumber = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    driversLicenceDueDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DriverTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    isEmpty = table.Column<bool>(type: "bit", nullable: false),
                    orientation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lineName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    pathName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isGenerated = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleDuties",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    name = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    color = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    depots = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleDuties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DriverDriverType",
                columns: table => new
                {
                    DriverTypesId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DriversId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverDriverType", x => new { x.DriverTypesId, x.DriversId });
                    table.ForeignKey(
                        name: "FK_DriverDriverType_Drivers_DriversId",
                        column: x => x.DriversId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverDriverType_DriverTypes_DriverTypesId",
                        column: x => x.DriverTypesId,
                        principalTable: "DriverTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PassingTimes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    time = table.Column<long>(type: "bigint", nullable: false),
                    nodeName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isUsed = table.Column<bool>(type: "bit", nullable: false),
                    isReliefPoint = table.Column<bool>(type: "bit", nullable: false),
                    tripId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PassingTimes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PassingTimes_Trips_tripId",
                        column: x => x.tripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TripVehicleDuty",
                columns: table => new
                {
                    TripsId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    VehicleDutiesId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripVehicleDuty", x => new { x.TripsId, x.VehicleDutiesId });
                    table.ForeignKey(
                        name: "FK_TripVehicleDuty_Trips_TripsId",
                        column: x => x.TripsId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TripVehicleDuty_VehicleDuties_VehicleDutiesId",
                        column: x => x.VehicleDutiesId,
                        principalTable: "VehicleDuties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkBlocks",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    startTime = table.Column<long>(type: "bigint", nullable: false),
                    endTime = table.Column<long>(type: "bigint", nullable: false),
                    startNode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    endNode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isCrewTravelTime = table.Column<bool>(type: "bit", nullable: false),
                    isActive = table.Column<bool>(type: "bit", nullable: false),
                    VehicleDutyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    DriverDutyId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TripId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkBlocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkBlocks_DriverDuties_DriverDutyId",
                        column: x => x.DriverDutyId,
                        principalTable: "DriverDuties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkBlocks_Trips_TripId",
                        column: x => x.TripId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkBlocks_VehicleDuties_VehicleDutyId",
                        column: x => x.VehicleDutyId,
                        principalTable: "VehicleDuties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    matricula = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    vin = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    vehicleTypeId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    startDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicles_VehicleTypes_vehicleTypeId",
                        column: x => x.vehicleTypeId,
                        principalTable: "VehicleTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DriverDriverType_DriversId",
                table: "DriverDriverType",
                column: "DriversId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverDuties_name",
                table: "DriverDuties",
                column: "name",
                unique: true,
                filter: "[name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_driversLicenceNumber",
                table: "Drivers",
                column: "driversLicenceNumber",
                unique: true,
                filter: "[driversLicenceNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PassingTimes_tripId",
                table: "PassingTimes",
                column: "tripId");

            migrationBuilder.CreateIndex(
                name: "IX_TripVehicleDuty_VehicleDutiesId",
                table: "TripVehicleDuty",
                column: "VehicleDutiesId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleDuties_name",
                table: "VehicleDuties",
                column: "name",
                unique: true,
                filter: "[name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_matricula",
                table: "Vehicles",
                column: "matricula",
                unique: true,
                filter: "[matricula] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_vehicleTypeId",
                table: "Vehicles",
                column: "vehicleTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_vin",
                table: "Vehicles",
                column: "vin",
                unique: true,
                filter: "[vin] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_DriverDutyId",
                table: "WorkBlocks",
                column: "DriverDutyId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_TripId",
                table: "WorkBlocks",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_VehicleDutyId",
                table: "WorkBlocks",
                column: "VehicleDutyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverDriverType");

            migrationBuilder.DropTable(
                name: "PassingTimes");

            migrationBuilder.DropTable(
                name: "TripVehicleDuty");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "WorkBlocks");

            migrationBuilder.DropTable(
                name: "Drivers");

            migrationBuilder.DropTable(
                name: "DriverTypes");

            migrationBuilder.DropTable(
                name: "VehicleTypes");

            migrationBuilder.DropTable(
                name: "DriverDuties");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "VehicleDuties");
        }
    }
}
