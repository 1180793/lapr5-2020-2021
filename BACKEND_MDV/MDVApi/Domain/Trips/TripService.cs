using System.Threading.Tasks;
using System.Collections.Generic;
using MDVApi.Domain.Shared;
using MDVApi.Domain.ValueObjects.Lines;
using MDVApi.Domain.ValueObjects.LinePaths;
using MDVApi.Domain.ValueObjects.Paths;
using MDVApi.Domain.PassingTimes;
using MDVApi.Domain.VehicleDuties;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using System.Text.Json;
using System;

namespace MDVApi.Domain.Trips
{
    public class TripService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITripRepository _repo;
        private static readonly string lineRoute = "api/lines/";
        private static readonly string lineException = "Line not found!";
        private static readonly string pathRoute = "api/paths/";
        private static readonly string pathException = "Path not found!";

        public TripService(IUnitOfWork unitOfWork, ITripRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<TripDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<TripDto> listDto = list.ConvertAll<TripDto>(trip =>
                new TripDto(trip.Id.AsGuid(), trip.isEmpty, trip.orientation, trip.lineName, trip.pathName, trip.isGenerated));

            return listDto;
        }

        public async Task<List<TripDto>> GetByLineAsync(string line)
        {

            var list = await this._repo.GetByLineAsync(line);

            List<TripDto> listDto = list.ConvertAll<TripDto>(trip =>
                new TripDto(trip.Id.AsGuid(), trip.isEmpty, trip.orientation, trip.lineName, trip.pathName, trip.isGenerated));

            return listDto;
        }

        public async Task<TripDto> GetByIdAsync(TripId id)
        {
            var trip = await this._repo.GetByIdAsync(id);

            if (trip == null)
                return null;

            return new TripDto(trip.Id.AsGuid(), trip.isEmpty, trip.orientation, trip.lineName, trip.pathName, trip.isGenerated);
        }

        public async Task<Trip> GetTripByIdAsync(TripId id)
        {
            var trip = await this._repo.GetByIdAsync(id);

            if (trip == null)
                return null;

            return trip;
        }

        public async Task<TripDto> AddAsync(CreatingTripDto dto, CreatingLineDto line, CreatingPathDto path, IList<PassingTime> passingTimesList)
        {
            //Looks for the Path Orientation
            foreach (CreatingLinePathDto linePath in line._linePaths)
            {
                if (linePath.path.key == path._key)
                {
                    dto.orientation = linePath.orientation;
                    break;
                }
            }

            var trip = new Trip(dto.isEmpty, dto.orientation, dto.lineName, dto.pathName);

            trip.passingTimeList = passingTimesList;

            await this._repo.AddAsync(trip);

            await this._unitOfWork.CommitAsync();

            return new TripDto(trip.Id.AsGuid(), trip.isEmpty, trip.orientation, trip.lineName, trip.pathName, trip.isGenerated);
        }

        public async Task<TripDto> AddLineAsync(CreatingLineTripDto dto, string path, IList<PassingTime> passingTimesList, string orientation)
        {

            var trip = new Trip(dto.isEmpty, orientation, dto.lineName, path);

            trip.passingTimeList = passingTimesList;

            await this._repo.AddAsync(trip);

            await this._unitOfWork.CommitAsync();

            return new TripDto(trip.Id.AsGuid(), trip.isEmpty, trip.orientation, trip.lineName, trip.pathName, trip.isGenerated);
        }

        public IList<Guid> getTripIdsByVehicleDuty(VehicleDutyId vehicleDutyId)
        {

            var trips = _repo.getTripsByVehicleDuty(vehicleDutyId);

            if (trips == null)
                throw new BusinessRuleValidationException("Trip doesn't exist.");

            IList<Guid> tripIds = new List<Guid>();
            
            foreach (Trip trip in trips)
            {
                tripIds.Add(trip.Id.AsGuid());
            }

            return tripIds;

        }

        public async Task<CreatingLineDto> getLineForTrip(HttpClient client, string lineName, IConfiguration _configuration)
        {

            string conString = _configuration.GetConnectionString("MDR") + lineRoute + lineName;

            var lineRes = await client.GetAsync(conString);

            var linecont = await lineRes.Content.ReadAsStringAsync();

            if (linecont.Contains(lineException))
            {
                return null;
            }

            CreatingLineDto line = JsonSerializer.Deserialize<CreatingLineDto>(linecont);

            return line;
        }

        public async Task<CreatingPathDto> getPathForTrip(HttpClient client, string pathName, IConfiguration _configuration)
        {

            string conString = _configuration.GetConnectionString("MDR") + pathRoute + pathName;

            var pathRes = await client.GetAsync(conString);

            var pathcont = await pathRes.Content.ReadAsStringAsync();

            if (pathcont.Contains(pathException))
            {
                return null;
            }

            CreatingPathDto path = JsonSerializer.Deserialize<CreatingPathDto>(pathcont); ;

            return path;
        }

    }
}