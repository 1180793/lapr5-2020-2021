export class DriverType {

    name: String;

    constructor(name : String){
        this.name = name;
    }

    public get driverTypeName() {
        return this.name;
    }

}