const sendJsonResponse = require('../middleware/sendJsonResponse');
const AuthService = require('../services/AuthService');
const _ = require('lodash');

class RegisterController {

    registerUser(req, res, next) {
        const authService = new AuthService();
        authService.registerUser(req.body).then((user) => {
            res.header('x-auth-token', user.token).send(user);
        }).catch((reason) => {
            if (reason == "404") {
                sendJsonResponse(res, 404, "null");
            } else {
                sendJsonResponse(res, 400, reason);
            }
        });
    }
}

module.exports = RegisterController;