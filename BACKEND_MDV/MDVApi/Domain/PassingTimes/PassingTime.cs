using System;
using System.ComponentModel.DataAnnotations.Schema;
using MDVApi.Domain.Shared;
using MDVApi.Domain.Trips;
using MDVApi.Domain.ValueObjects.Nodes;

namespace MDVApi.Domain.PassingTimes
{
    public class PassingTime : Entity<PassingTimeId>
    {
        public long time { get; set; }
        public string nodeName { get; set; }
        public Boolean isUsed { get; set; }
        public Boolean isReliefPoint { get; set; }
        public TripId tripId { get; set; }
        
        [ForeignKey("tripId")]
        public Trip trip { get; set; }

        public PassingTime(long time, string nodeName, Boolean isReliefPoint){

            this.Id = new PassingTimeId(Guid.NewGuid());
            this.time = time;
            this.nodeName = nodeName;
            this.isUsed = true;
            this.isReliefPoint = isReliefPoint;  
        }

        public CreatingPassingTimeDto PassingTimeToDto(){
            return new CreatingPassingTimeDto(this.time, this.nodeName, this.isUsed, this.isReliefPoint);
        }

    }
}