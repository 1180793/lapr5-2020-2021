export class VehicleType {


    name: String;
    autonomy: Number;
    cost: Number;
    averageSpeed: Number;
    energySource: Number;
    consumption: Number;
    emissions: Number;

    constructor(name: String, autonomy: Number, cost: Number, averageSpeed: Number, energySource: Number, consumption: Number, emissions: Number) {

        this.name = name;
        this.autonomy = autonomy;
        this.cost = cost;
        this.averageSpeed = averageSpeed;
        this.energySource = energySource;
        this.consumption = consumption;
        this.emissions = emissions;
    }


    public get vehicleTypeDescription() {
        return this.name;
    }
    public get vehicleTypeAutonomy() {
        return this.autonomy;
    }
    public get vehicleTypeCost() {
        return this.cost;
    }
    public get vehicleTypeAverageSpeed() {
        return this.averageSpeed;
    }
    public get vehicleTypeEnergySource() {
        return this.energySource;
    }
    public get vehicleTypeConsumption() {
        return this.consumption;
    }
    public get vehicleTypeEmissions() {
        return this.emissions;
    }

}