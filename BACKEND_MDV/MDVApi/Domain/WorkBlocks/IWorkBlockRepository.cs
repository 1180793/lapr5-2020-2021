using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MDVApi.Domain.Shared;
using MDVApi.Domain.VehicleDuties;

namespace MDVApi.Domain.WorkBlocks
{
    public interface IWorkBlockRepository: IRepository<WorkBlock,WorkBlockId>
    {
        Task<IList<WorkBlock>> getWorkBlocksByVehicleDutyAsync(VehicleDutyId vehicleDutyId);

    }
}