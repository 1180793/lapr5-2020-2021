import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ImportScheduleService } from 'src/app/services/import-schedule/import-schedule.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'

@Component({
    selector: 'app-import-schedule',
    templateUrl: './import-schedule.component.html',
    styleUrls: ['./import-schedule.component.css']
})
export class ImportScheduleComponent implements OnInit {
    createImportScheduleForm!: FormGroup;
    selectedFiles: FileList;
    currentFile: File;
    message = '';

    constructor(
        public fb: FormBuilder,
        private importScheduleService: ImportScheduleService, private jwtAuth: AuthService, private router: Router) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
    }

    ngOnInit(): void {
        this.reactiveForm();
    }
    reactiveForm() {
        this.createImportScheduleForm = this.fb.group({
            glxFile: ['', [Validators.required]],
        });
    }
    selectFile(event: any): void {
        this.selectedFiles = event.target.files;
    }

    upload(): void {
        if (this.selectedFiles == null || this.selectedFiles == undefined) {
            Swal.fire('Error!', 'You must choose a file to upload!', 'error')
            return;
        }

        this.currentFile = this.selectedFiles[0];

        this.importScheduleService.upload(this.currentFile).subscribe(
            (event) => {
                if (event instanceof HttpResponse) {
                    this.message = event.body.message + ` Imported ${event.body.trips} Trips, ${event.body.workBlocks} Work Blocks, ${event.body.vehicleDuties} Vehicle Duties and ${event.body.driverDuties} Driver Duties `;
                    Swal.fire('Success!', this.message + '!', 'success')
                }
            },
            (err) => {
                console.log(
                    '🚀 ~ file: import-schedule.component.ts ~ line 45 ~ ImportScheduleComponent ~ upload ~ err',
                    err
                );
                this.message = 'Could not upload the file!';
                this.currentFile = undefined;
                Swal.fire('Error!', 'Invalid Network File! ' + err.error.message, 'error')
            }
        );
        this.selectedFiles = undefined;
    }

}
