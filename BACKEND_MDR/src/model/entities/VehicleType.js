const mongoose = require('mongoose');
const VehicleTypeSchema = require('../../schemas/VehicleTypeSchema').vehicleTypeSchema;

/**
 * Creates VehicleType instance from JSON Object
 * @param {JSON} VehicleTypeJSON 
 */
function createVehicleTypeFromJSON(vehicleTypeJSON) {
    var vehicleType = null;
    var VehicleType = mongoose.model('VehicleType', VehicleTypeSchema);
    try {
        vehicleType = new VehicleType({
            name: vehicleTypeJSON.name,
            autonomy: vehicleTypeJSON.autonomy,
            cost: vehicleTypeJSON.cost,
            averageSpeed: vehicleTypeJSON.averageSpeed,
            energySource: vehicleTypeJSON.energySource,
            consumption: vehicleTypeJSON.consumption,
            emissions: vehicleTypeJSON.emissions
        });
    } catch (e) {
        return null;
    }
    return vehicleType;
}

/**
 * Creates VehicleType instance from DTO
 * @param {VehicleTypeDTO} VehicleTypeDTO 
 */
function createVehicleTypeFromDTO(vehicleTypeDTO) {
    var vehicleType = null;
    var VehicleType = mongoose.model('VehicleType', VehicleTypeSchema);
    try {
        vehicleType = new VehicleType({
            name: vehicleTypeDTO.name,
            autonomy: vehicleTypeDTO.autonomy,
            cost: vehicleTypeDTO.cost,
            averageSpeed: vehicleTypeDTO.averageSpeed,
            energySource: vehicleTypeDTO.energySource,
            consumption: vehicleTypeDTO.consumption,
            emissions: vehicleTypeDTO.emissions
        });

    } catch (e) {
        return null;
    }
    return vehicleType;
}

exports.VehicleType = mongoose.model('VehicleType', VehicleTypeSchema);
exports.createVehicleTypeFromJSON = createVehicleTypeFromJSON;
exports.createVehicleTypeFromDTO = createVehicleTypeFromDTO;