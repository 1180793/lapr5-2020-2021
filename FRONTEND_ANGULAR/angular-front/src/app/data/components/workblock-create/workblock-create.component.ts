import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { AuthService } from 'src/app/services/auth/auth.service';
import { AuthEvent } from 'src/app/services/auth/AuthEvent';
import { VehicleDutyService } from 'src/app/services/vehicle-duty/vehicle-duty.service';
import { WorkBlockService } from 'src/app/services/work-block/work-block.service';
import { AppSettings } from 'src/utils/AppSettings';
import Swal from 'sweetalert2'
@Component({
    selector: 'app-workblock-create',
    templateUrl: './workblock-create.component.html',
    styleUrls: ['./workblock-create.component.css']
})
export class WorkblockCreateComponent implements OnInit {

    workBlockForm: FormGroup;
    control: FormControl;

    workBlockService = new WorkBlockService(this.http);
    vehicleDutyService = new VehicleDutyService(this.http);

    return: string;

    private _unsubscribeAll: Subject<any>;

    //List for Population 
    vehicleDutyList: any;

    constructor(
        private builder: FormBuilder,
        public http: HttpClient,
        private authEvent: AuthEvent,
        private jwtAuth: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        const hasPermission = this.jwtAuth.hasRole(AppSettings.DATA_ADMINISTRATOR);
        if (!hasPermission) {
            Swal.fire('Error!', 'Access Denied!', 'error')
            this.router.navigateByUrl(AppSettings.NO_PERMISSION_REDIRECT);
        }
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.control = this.builder.control('', Validators.required);

        this.workBlockForm = new FormGroup({
            isCrewTravelTime: new FormControl(''),
            vehicleDuty: new FormControl('', Validators.required),
            duration: new FormControl('', Validators.compose(
                [Validators.min(1), Validators.required])),
            numberOfBlocks: new FormControl('', Validators.compose(
                [Validators.min(1), Validators.required]))
        });

        this.route.queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => this.return = params['return'] || '/');

        this.populateVehicleDuties();
    }

    ngOnDestroy() {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    async populateVehicleDuties() {
        (await this.vehicleDutyService.getVehicleDuties()).subscribe(
            value => {
                this.vehicleDutyList = value;
            },
            error => {
                Swal.fire('Error!', error.error + '!', 'error')

            }
        );
    }

    onCreateWorkBlock() {
        if (this.workBlockForm.valid) {
            const workBlockData = this.workBlockForm.value;
            //console.log("🚀 ~ file: workblock-create.component.ts ~ line 95 ~ WorkblockCreateComponent ~ onCreateWorkBlock ~ workBlockData", workBlockData)
            this.workBlockService.addWorkBlock(workBlockData.isCrewTravelTime, workBlockData.vehicleDuty, workBlockData.duration, workBlockData.numberOfBlocks);
        }
        else {
            this.validatorMessage();
        }
    }

    validatorMessage() {
        if (this.workBlockForm.get('vehicleDuty').invalid) {
            if (this.workBlockForm.get('vehicleDuty').hasError('required')) {
                Swal.fire('Error!', 'Vechile Duty is required!', 'error')
            }
        } else if (this.workBlockForm.get('duration').invalid) {
            if (this.workBlockForm.get('duration').hasError('required')) {
                Swal.fire('Error!', 'Please choose a duration!', 'error')
            } else if (this.workBlockForm.get('duration').hasError('min')) {
                Swal.fire('Error!', 'Duration must be positive!', 'error')
            }
        } else if (this.workBlockForm.get('numberOfBlocks').invalid) {
            if (this.workBlockForm.get('numberOfBlocks').hasError('required')) {
                Swal.fire('Error!', 'Please choose a number of work blocks!', 'error')
            } else if (this.workBlockForm.get('numberOfBlocks').hasError('min')) {
                Swal.fire('Error!', 'Number of work blocks must be positive!', 'error')
            }
        }
    }

}
