const mongoose = require('mongoose');
const Joi = require('joi');

const pathNodeSchema = require('./PathNodeSchema').pathNodeSchema;
const joiPathNodeSchema = require('./PathNodeSchema').joiPathNodeSchema;

const validateOrientation = require('../utils/Utils').validateOrientation;

// Persistence Schema
const pathSchema = new mongoose.Schema({
    key: {
        type: String,
        unique: true,
        required: true,
    },
    line: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Line'
    },
    isEmpty: {
        type: Boolean,
        default: false
    },
    pathNodeList: [pathNodeSchema]
});

const joiValidateOrientation = (value, helpers) => {
    var isValid = validateOrientation(value);
    if (!isValid) {
        return helpers.error("any.invalid");
    } else {
        return value;
    }
};

const joiPathSchema = Joi.object({
    key: Joi.string()
        .required(),
    line: Joi.string()
        .required(),
    orientation: Joi.string()
        .required()
        .custom(joiValidateOrientation, "Orientation Validation"),
    isEmpty: Joi.boolean()
        .default(false),
    pathNodeList: Joi.array()
        .items(joiPathNodeSchema)
        .min(2)
        .required()
}).unknown(true);

// Validate
function validatePath(path) {
    return joiPathSchema.validate(path);
}

exports.joiPathSchema = joiPathSchema;
exports.pathSchema = pathSchema;
exports.validate = validatePath;