import { WorkBlock } from './../model/model/WorkBlock';

export class WorkBlockMap {

    public toDTO(workBlock: WorkBlock) {
        var workBlockDTO = {
            isCrewTravelTime: workBlock.isCrewTravelTime,
            vehicleDutyID: workBlock.vehicleDutyID,
            duration: workBlock.duration,
            numberOfBlocks: workBlock.numberOfBlocks
        }
        return workBlockDTO;
    }

    public async toDomain(raw: any) {
        return new WorkBlock(raw.isCrewTravelTime, raw.vehicleDutyID, raw.duration, raw.numberOfBlocks);
    }
}
