import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStoreService } from 'src/app/services/auth/local-store.service';

export class HeaderInterceptor implements HttpInterceptor {
    private ls: LocalStoreService = new LocalStoreService();
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Clone the request to add the new header
        const token = this.ls.getItem('JWT_TOKEN');
        if (token) {
            const clonedRequest = req.clone({ headers: req.headers.append('x-auth-token', token) });
            return next.handle(clonedRequest);
        } else {
            return next.handle(req);
        }

        // Pass the cloned request instead of the original request to the next handle
    }
}
