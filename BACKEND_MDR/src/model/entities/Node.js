const mongoose = require('mongoose');
const NodeSchema = require('../../schemas/NodeSchema').nodeSchema;

/**
 * Creates Node instance from JSON Object
 * @param {JSON} nodeJSON 
 */
function createNodeFromJSON(nodeJSON) {
    var node = null;
    var Node = mongoose.model('Node', NodeSchema);
    try {
        node = new Node({
            // key: nodeJSON.key,
            name: nodeJSON.name,
            coordinates: {
                latitude: nodeJSON.coordinates.latitude,
                longitude: nodeJSON.coordinates.longitude,
                elevation: nodeJSON.coordinates.elevation,
            },
            shortName: nodeJSON.shortName,
            isDepot: nodeJSON.isDepot,
            isReliefPoint: nodeJSON.isReliefPoint
        });
    } catch (e) {
        return null;
    }
    return node;
}

/**
 * Creates Node instance from DTO
 * @param {NodeDTO} nodeDTO 
 */
function createNodeFromDTO(nodeDTO) {
    var node = null;
    var Node = mongoose.model('Node', NodeSchema);
    try {
        node = new Node({
            // key: nodeDTO.key,
            name: nodeDTO.name,
            coordinates: {
                latitude: nodeDTO.coordinates.latitude,
                longitude: nodeDTO.coordinates.longitude,
                elevation: nodeDTO.coordinates.elevation
            },
            shortName: nodeDTO.shortName,
            isDepot: nodeDTO.isDepot,
            isReliefPoint: nodeDTO.isReliefPoint
        });

    } catch (e) {
        return null;
    }
    return node;
}

exports.Node = mongoose.model('Node', NodeSchema);
exports.createNodeFromJSON = createNodeFromJSON;
exports.createNodeFromDTO = createNodeFromDTO;