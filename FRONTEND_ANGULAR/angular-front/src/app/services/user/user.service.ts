import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserMap } from 'src/app/mappers/UserMap';
import { User } from 'src/app/model/model/User';


@Injectable({
    providedIn: 'root'
})
export class UserService {

    uri = 'http://localhost:4000';
    mapper = new UserMap();

    constructor(private http: HttpClient) {
    }

    getUsers() {
        return this.http.get('${this.uri}/users');
    }

    getUsersById(id: number) {
        return this.http.get('${this.uri}/users/${id}');
    }

    addUser(userFirstName: string, userLastName: string, userEmail: string, userPassword: string, userRole: string) {
        const user = {
            userFirstName: userFirstName,
            userLastName: userLastName,
            userEmail: userEmail,
            userPassword: userPassword,
            userRole: userRole
        }
        return this.http.post('${this.uri}/users/add', user);
    }

    //addUser using Mapper
    addUserDto(user: User) {
        return this.http.post('${this.uri}/users/add', this.mapper.toDTO(user));
    }

    deleteUser(id: number) {
        return this.http.get('${this.uri}/users/delete/${id}');
    }

}
