import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/utils/AppSettings';

@Injectable({
    providedIn: 'root'
})
export class ImportNetworkService {

    baseUrl = AppSettings.API_ENDPOINT;

    constructor(private http: HttpClient) {
    }

    upload(file: File): Observable<HttpEvent<any>> {
        const formData: FormData = new FormData();

        formData.append('networkFile', file);

        const req = new HttpRequest('POST', `${this.baseUrl}/network`, formData, {
            reportProgress: true,
            responseType: 'json'
        });

        return this.http.request(req);
    }

}
