const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const dataAdmin = require('../middleware/dataAdministrator');

const LineController = require('../controllers/LineController');

var lineController = new LineController();

router.get('/', [auth], lineController.listAll);
router.get('/:id', [auth, dataAdmin], lineController.details);
router.post('/', [auth, dataAdmin], lineController.create);

module.exports = router;
