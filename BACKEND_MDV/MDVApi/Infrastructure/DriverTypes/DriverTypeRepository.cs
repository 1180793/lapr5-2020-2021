using System;
using MDVApi.Domain.DriverTypes;
using MDVApi.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace MDVApi.Infrastructure.DriverTypes
{
    public class DriverTypeRepository : BaseRepository<DriverType, DriverTypeId>,IDriverTypeRepository
    {
        private readonly DbSet<DriverType> _objs;

        public DriverTypeRepository(MDVApiDbContext context):base(context.DriverTypes)
        {
            _objs = context.DriverTypes ?? throw new ArgumentNullException(nameof(context.DriverTypes));
        }

        public async Task<DriverType> getDriverTypeByNameAsync(string name)
        {
            return await _objs.Where(x => name.Equals(x.name)).FirstOrDefaultAsync();
        }
        
    }
} 