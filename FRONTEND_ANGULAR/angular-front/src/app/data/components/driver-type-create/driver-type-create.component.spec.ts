import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverTypeCreateComponent } from './driver-type-create.component';

describe('DriverTypeCreateComponent', () => {
  let component: DriverTypeCreateComponent;
  let fixture: ComponentFixture<DriverTypeCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverTypeCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverTypeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
