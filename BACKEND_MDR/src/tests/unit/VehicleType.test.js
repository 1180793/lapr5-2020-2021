/**
 * @jest-environment node
 */
//mudar smp o environment quando o teste usa algo com mongoose

const VehicleType = require('../../../src/model/entities/VehicleType');
const VehicleTypeDTO = require('../../../src/dto/VehicleTypeDTO');
const { expect } = require('@jest/globals');

test('Check if vehicle type JSON create is working', () => {

    var json = {
        "name": "TEST,TEST;TEST",
        "autonomy": 50,
        "cost": 20.20,
        "averageSpeed": 90,
        "energySource": "23",
        "consumption": 8.92,
        "emissions": 29.0
    };

    var vehicleType = VehicleType.createVehicleTypeFromJSON(json);

    expect(vehicleType.name).toBe("TEST,TEST;TEST");
    expect(vehicleType.autonomy).toBe(50);
    expect(vehicleType.cost).toBe(20.20);
    expect(vehicleType.averageSpeed).toBe(90);
    expect(vehicleType.energySource).toBe("23");
    expect(vehicleType.consumption).toBe(8.92);
    expect(vehicleType.emissions).toBe(29.0);

})

test('Check if vehicle type JSON create is working', () => {

    var dto = new VehicleTypeDTO({
        "name": "TEST,TEST;TEST",
        "autonomy": "50",
        "cost": "20.20",
        "averageSpeed": "90",
        "energySource": "23",
        "consumption": "8.92",
        "emissions": "29.0"
    });

    var vehicleType = VehicleType.createVehicleTypeFromDTO(dto);

    expect(vehicleType.name).toBe("TEST,TEST;TEST");
    expect(vehicleType.autonomy).toBe(50);
    expect(vehicleType.cost).toBe(20.20);
    expect(vehicleType.averageSpeed).toBe(90);
    expect(vehicleType.energySource).toBe("23");
    expect(vehicleType.consumption).toBe(8.92);
    expect(vehicleType.emissions).toBe(29.0);
});
