using System.Threading.Tasks;
using MDVApi.Domain.Shared;

namespace MDVApi.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MDVApiDbContext _context;

        public UnitOfWork(MDVApiDbContext context)
        {
            this._context = context;
        }

        public async Task<int> CommitAsync()
        {
            return await this._context.SaveChangesAsync();
        }
    }
}