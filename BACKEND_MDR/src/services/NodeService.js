const NodeRepository = require('../repository/NodeRepository');
const NodeDTO = require('../dto/NodeDTO');
const validateNode = require('../schemas/NodeSchema').validate;
const createNodeFromJSON = require('../model/entities/Node').createNodeFromJSON;

class NodeService {

    constructor() { }

    async createNode(nodeJSON) {
        return new Promise(async (resolve, reject) => {
            const nodeRepo = new NodeRepository();
            try {
                const { error } = validateNode(nodeJSON);
                if (error) {
                    throw new Error(error.message);
                } else {
                    var nodeValidationDTO = new NodeDTO(nodeJSON);
                }
            } catch (err) {
                console.log(nodeJSON);
                reject(`Invalid Node. ${err.message}`);
            }
            if (nodeValidationDTO == null) {
                reject("Invalid Node.");
            } else {
                // console.log(nodeValidationDTO);
                try {
                    const pExistingNode = new Promise((resolve, reject) => {
                        nodeRepo.exists(nodeJSON.shortName, function (err, existingNode) {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(existingNode);
                            }
                        })
                    });

                    pExistingNode
                        .then(function (existingNode) {
                            if (existingNode == null) {
                                const nodeToSave = createNodeFromJSON(nodeJSON);
                                nodeRepo.save(nodeToSave, error => {
                                    if (error) {
                                        reject(error)
                                    } else {
                                        resolve(nodeValidationDTO);
                                    }
                                });
                            } else {
                                reject('Repeated Node ShortName!');
                            }
                        })
                        .catch((reason) => {
                            reject(reason)
                        });
                } catch (err) {
                    reject(err);
                }
            }
        });
    }

    async findAllSorted(customFilters) {
        return new Promise(async (resolve, reject) => {
            const nodeRepo = new NodeRepository();
            nodeRepo.findAllSorted(customFilters.sortBy, customFilters.filterBy, (err, nodes) => {
                var lstNodeDTO = [];
                nodes.forEach((element) => {
                    let nodeDTO = new NodeDTO(element);
                    lstNodeDTO.push(nodeDTO);
                });
                resolve(lstNodeDTO);
            });
        });
    }

    async findByName(name) {
        return new Promise(async (resolve, reject) => {
            const nodeRepo = new NodeRepository();
            nodeRepo.findByName(name, (err, node) => {
                if (node != null) {
                    const nodeDTO = new NodeDTO(node);
                    resolve(nodeDTO);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async validateNode(nodeShortName) {
        return new Promise((resolve, reject) => {
            const nodeRepo = new NodeRepository();
            nodeRepo.exists(nodeShortName, function (err, node) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(node);
                }
            });
        });
    }

}

module.exports = NodeService;