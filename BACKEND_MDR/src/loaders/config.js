const config = require('config');

module.exports = function () {
    let err = new Error('FATAL ERROR: JSON Web Token Private Key is not defined.'); 
    try {
        if (!config.get('jwtPrivateKey')) {
            throw err;
        }
    } catch (error) {
        throw err;
    }
}