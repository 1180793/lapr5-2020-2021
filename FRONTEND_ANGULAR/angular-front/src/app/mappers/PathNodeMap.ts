import { PathNode } from "src/app/model/model/PathNode";

export class PathNodeMap {

    public toDTO(pathNode: PathNode) {
        return {
            node: pathNode.PathNode,
            duration: pathNode.PathNodeDuration,
            distance: pathNode.LineStartDistance
        }
    }

    public static async toDomain(raw: any) {
        return new PathNode(raw.node, raw.duration, raw.distance);
    }

}
