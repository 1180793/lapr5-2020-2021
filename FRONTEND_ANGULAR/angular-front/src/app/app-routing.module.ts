import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/components/login/login.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { ProfileComponent } from './auth/components/profile/profile.component';
import { SplashscreenComponent } from './auth/components/splashscreen/splashscreen.component';
import { NavBarComponent } from './components/components/nav-bar/nav-bar.component';
import { LineCreateComponent } from './data/components/line-create/line-create.component';
import { NodeCreateComponent } from './data/components/node-create/node-create.component';
import { VehicleTypeCreateComponent } from './data/components/vehicle-type-create/vehicle-type-create.component';
import { DriverTypeCreateComponent } from './data/components/driver-type-create/driver-type-create.component';
import { ImportNetworkComponent } from './data/components/import-network/import-network.component';
import { MapComponent } from './view/components/map/map.component';
import { PathCreateComponent } from './data/components/path-create/path-create.component';
import { NodeShowComponent } from './data/components/node-show/node-show.component';
import { PathShowComponent } from './data/components/path-show/path-show.component';
import { LineShowComponent } from './data/components/line-show/line-show.component';
import { AppSettings } from 'src/utils/AppSettings';
import { VehicleCreateComponent } from './data/components/vehicle-create/vehicle-create.component';
import { NoPermissionComponent } from './auth/components/no-permission/no-permission.component';
import { TripCreateComponent } from './data/components/trip-create/trip-create.component';
import { DriverCreateComponent } from './data/components/driver-create/driver-create.component';
import { VehicleDutyCreateComponent } from './data/components/vehicle-duty-create/vehicle-duty-create.component';
import { WorkblockCreateComponent } from './data/components/workblock-create/workblock-create.component';
import { VehicleDutyShowComponent } from './data/components/vehicle-duty-show/vehicle-duty-show.component';
import { LineTripsCreateComponent } from './data/components/trips-create/trips-create.component';
import { TripShowComponent } from './data/components/trip-show/trip-show.component';
import { ImportScheduleComponent } from './data/components/import-schedule/import-schedule.component';
import { DriverDutyCreateComponent } from './data/components/driver-duty-create/driver-duty-create.component';


export const routes = [
    {
        path: 'auth/splash',
        component: SplashscreenComponent,
        label: 'Splashscreen'
    },
    {
        path: 'auth/login',
        component: LoginComponent,
        label: 'Login',
        requiresAuth: false
    },
    {
        path: 'auth/register',
        component: RegisterComponent,
        label: 'Register',
        requiresAuth: false
    },
    {
        path: 'profile',
        component: ProfileComponent,
        label: 'My Profile',
        requiresAuth: true
    },
    {
        path: 'data/line/create',
        component: LineCreateComponent,
        label: 'Create Line',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/line/show',
        component: LineShowComponent,
        label: 'Show Lines',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },

    {
        path: 'data/node/create',
        component: NodeCreateComponent,
        label: 'Create Node',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/node/show',
        component: NodeShowComponent,
        label: 'Show Nodes',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/vehicleType/create',
        component: VehicleTypeCreateComponent,
        label: 'Create Vehicle Type',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/driverType/create',
        component: DriverTypeCreateComponent,
        label: 'Create Driver Type',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/path/create',
        component: PathCreateComponent,
        label: 'Create Path',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/path/show',
        component: PathShowComponent,
        label: 'Show Paths',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/network/upload',
        component: ImportNetworkComponent,
        label: 'Import Network File',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/schedule/upload',
        component: ImportScheduleComponent,
        label: 'Import Schedule File',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/vehicle/create',
        component: VehicleCreateComponent,
        label: 'Create Vehicles',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/trip/create',
        component: TripCreateComponent,
        label: 'Create Trips',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/trip/create-trips',
        component: LineTripsCreateComponent,
        label: 'Create Multiple Trips',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/trip/show',
        component: TripShowComponent,
        label: 'Show Trips',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR, AppSettings.CLIENT]
    },
    {
        path: 'data/driver/create',
        component: DriverCreateComponent,
        label: 'Create Drivers',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/vehicle-duty/create',
        component: VehicleDutyCreateComponent,
        label: 'Create Vehicle Duty',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/vehicle-duty/show',
        component: VehicleDutyShowComponent,
        label: 'Show Vehicle Duty',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/driver-duty/create',
        component: DriverDutyCreateComponent,
        label: 'Create Driver Duty',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'data/workblock/create',
        component: WorkblockCreateComponent,
        label: 'Create Work Block',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR]
    },
    {
        path: 'view/map',
        component: MapComponent,
        label: 'Network Map',
        requiresAuth: true,
        roles: [AppSettings.DATA_ADMINISTRATOR, AppSettings.GESTOR, AppSettings.CLIENT]
    },
    {
        path: 'access-denied',
        component: NoPermissionComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [SplashscreenComponent,
    NoPermissionComponent,
    LoginComponent,
    RegisterComponent,
    LineCreateComponent,
    NodeCreateComponent,
    VehicleTypeCreateComponent,
    ImportNetworkComponent,
    MapComponent,
    NavBarComponent,
    NodeShowComponent,
    PathShowComponent,
    TripShowComponent,
    VehicleDutyCreateComponent,
    VehicleDutyShowComponent]
